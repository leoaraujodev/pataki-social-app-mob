# Pataki

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Screens

You can find the design for the screen in our figma link:

https://www.figma.com/file/pJ8Z0rXrWt3q6GqED4Y299/APP?node-id=30%3A0

And check the project wireframe in this Google drive file:

https://drive.google.com/file/d/1SvSHoict841iZvD9Wl2o2uoXaJN8uLgm/view?usp=sharing
