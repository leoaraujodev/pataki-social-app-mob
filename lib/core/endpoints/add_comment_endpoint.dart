import 'package:pataki/core/models/add_comment_request.dart';
import 'package:pataki/core/models/add_comment_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class AddCommentEndpoint
    extends PatakiEndpoint<AddCommentRequest, AddCommentResponse, Map<String, dynamic>> {
  String path() => "/comment";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<AddCommentRequest, AddCommentResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  AddCommentRequest request;

  AddCommentEndpoint(this.request);

  @override
  AddCommentResponse responseModelFromJson(Map<String, dynamic> json) {
    return AddCommentResponse.fromJson(json);
  }
}
