import 'package:pataki/core/models/add_tag_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class AddTagEndpoint extends PatakiEndpoint<AddTagRequest, Pet, Map<String, dynamic>> {
  String path() => "/pet/edit";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<AddTagRequest, Pet> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  AddTagRequest request;

  AddTagEndpoint(this.request);

  Pet responseModelFromJson(Map<String, dynamic> json) {
    return Pet.fromJson(json);
  }
}
