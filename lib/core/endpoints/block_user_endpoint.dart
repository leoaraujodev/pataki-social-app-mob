import 'package:pataki/core/models/block_user_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class BlockUserEndpoint
    extends PatakiEndpoint<BlockUserRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/user/block";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<BlockUserRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  BlockUserRequest request;

  BlockUserEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
