import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';
import 'package:pataki/core/models/chat_message.dart';
import 'package:pataki/core/models/create_chat_message_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class CreateChatMessageEndpoint
    extends PatakiEndpoint<CreateChatMessageRequest, ChatMessage, Map<String, dynamic>> {
  String path() => "/chat/room";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<CreateChatMessageRequest, ChatMessage> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  CreateChatMessageRequest request;

  CreateChatMessageEndpoint(this.request);

  ChatMessage responseModelFromJson(Map<String, dynamic> json) {
    return ChatMessage.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 1080);
      return [
        (MultipartFile.fromBytes('image', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
