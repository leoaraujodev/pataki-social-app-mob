import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';
import 'package:pataki/core/models/create_group_request.dart';
import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class CreateGroupEndpoint extends PatakiEndpoint<CreateGroupRequest, Group, Map<String, dynamic>> {
  String path() => "/group";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<CreateGroupRequest, Group> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  CreateGroupRequest request;

  CreateGroupEndpoint(this.request);

  Group responseModelFromJson(Map<String, dynamic> json) {
    return Group.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 320);

      return [
        (MultipartFile.fromBytes('image', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
