import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';

import 'package:pataki/core/models/create_pet_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class CreatePetEndpoint extends PatakiEndpoint<CreatePetRequest, Pet, Map<String, dynamic>> {
  String path() => "/pet";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<CreatePetRequest, Pet> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  CreatePetRequest request;

  CreatePetEndpoint(this.request);

  Pet responseModelFromJson(Map<String, dynamic> json) {
    return Pet.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 320);
      return [
        (MultipartFile.fromBytes('picture', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
