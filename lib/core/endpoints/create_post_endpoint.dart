import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';
import 'package:pataki/core/models/create_post_request.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class CreatePostEndpoint
    extends PatakiEndpoint<CreatePostRequest, TimelinePost, Map<String, dynamic>> {
  String path() => "/timeline/store";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<CreatePostRequest, TimelinePost> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  CreatePostRequest request;

  CreatePostEndpoint(this.request);

  TimelinePost responseModelFromJson(Map<String, dynamic> json) {
    return TimelinePost.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      if (request.fileType == FileType.video) {
        return [
          (MultipartFile.fromBytes('image', request.image.readAsBytesSync(),
              contentType: MediaType('video', 'mp4'), filename: 'video.mp4'))
        ];
      } else {
        var image = decodeImage(request.image.readAsBytesSync());
        image = copyResize(image, width: 1080);
        return [
          (MultipartFile.fromBytes('image', encodeJpg(image, quality: 100),
              contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
        ];
      }
    }
    return null;
  }
}
