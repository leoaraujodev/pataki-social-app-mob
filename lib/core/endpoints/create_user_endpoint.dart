import 'package:pataki/core/models/create_user_request.dart';
import 'package:pataki/core/models/login_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class CreateUserEndpoint extends PatakiEndpoint<CreateUserRequest, LoginResponse, Map<String, dynamic>> {
  String path() => "/user";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<CreateUserRequest, LoginResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  CreateUserRequest request;

  CreateUserEndpoint(this.request);

  LoginResponse responseModelFromJson(Map<String, dynamic> json) {
    return LoginResponse.fromJson(json);
  }

}
