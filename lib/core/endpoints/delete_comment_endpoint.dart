import 'package:pataki/core/models/delete_comment_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class DeleteCommentEndpoint
    extends PatakiEndpoint<DeleteCommentRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/comment/delete";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<DeleteCommentRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  DeleteCommentRequest request;

  DeleteCommentEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
