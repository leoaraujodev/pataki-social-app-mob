import 'package:pataki/core/models/delete_pet_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class DeletePetEndpoint extends PatakiEndpoint<DeletePetRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/pet/delete";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<DeletePetRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  DeletePetRequest request;

  DeletePetEndpoint(this.request);

  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }

}
