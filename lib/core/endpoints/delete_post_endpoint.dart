import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class DeletePostEndpoint
    extends PatakiEndpoint<DeletePostRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/timeline/delete";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<DeletePostRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  DeletePostRequest request;

  DeletePostEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
