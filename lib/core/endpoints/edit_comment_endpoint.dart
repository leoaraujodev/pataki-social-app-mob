import 'package:pataki/core/models/edit_comment_request.dart';
import 'package:pataki/core/models/edit_comment_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class EditCommentEndpoint
    extends PatakiEndpoint<EditCommentRequest, EditCommentResponse, Map<String, dynamic>> {
  String path() => "/comment/edit";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<EditCommentRequest, EditCommentResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EditCommentRequest request;

  EditCommentEndpoint(this.request);

  @override
  EditCommentResponse responseModelFromJson(Map<String, dynamic> json) {
    return EditCommentResponse.fromJson(json);
  }
}
