import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';
import 'package:pataki/core/models/edit_group_request.dart';
import 'package:pataki/core/models/group_details.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class EditGroupEndpoint
    extends PatakiEndpoint<EditGroupRequest, GroupDetails, Map<String, dynamic>> {
  String path() => "/group/${request.groupId.toString()}";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<EditGroupRequest, GroupDetails> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EditGroupRequest request;

  EditGroupEndpoint(this.request);

  GroupDetails responseModelFromJson(Map<String, dynamic> json) {
    return GroupDetails.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 320);

      return [
        (MultipartFile.fromBytes('image', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
