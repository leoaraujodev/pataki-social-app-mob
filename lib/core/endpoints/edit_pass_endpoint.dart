import 'package:pataki/core/models/edit_password_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class EditPassEndpoint
    extends PatakiEndpoint<EditPassRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/user/new-password";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<EditPassRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EditPassRequest request;

  EditPassEndpoint(this.request);

  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
