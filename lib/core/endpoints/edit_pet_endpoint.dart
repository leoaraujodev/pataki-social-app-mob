import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';

import 'package:pataki/core/models/edit_pet_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class EditPetEndpoint extends PatakiEndpoint<EditPetRequest, Pet, Map<String, dynamic>> {
  String path() => "/pet/edit";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<EditPetRequest, Pet> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EditPetRequest request;

  EditPetEndpoint(this.request);

  Pet responseModelFromJson(Map<String, dynamic> json) {
    return Pet.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 320);
      return [
        (MultipartFile.fromBytes('picture', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
