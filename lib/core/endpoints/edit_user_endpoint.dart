import 'package:http/http.dart';
import 'dart:async';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart';

import 'package:pataki/core/models/edit_user_request.dart';
import 'package:pataki/core/models/user_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class EditUserEndpoint extends PatakiEndpoint<EditUserRequest, UserResponse, Map<String, dynamic>> {
  String path() => "/user";
  HttpMethod method() => HttpMethod.MULTIPART;
  PatakiServer<EditUserRequest, UserResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();
  EditUserRequest request;

  EditUserEndpoint(this.request);

  @override
  UserResponse responseModelFromJson(Map<String, dynamic> json) {
    return UserResponse.fromJson(json);
  }

  @override
  Future<Iterable<MultipartFile>> multipartFiles() async {
    if (request.image != null) {
      var image = decodeImage(request.image.readAsBytesSync());
      image = copyResize(image, width: 320);
      return [
        (MultipartFile.fromBytes('image', encodeJpg(image, quality: 100),
            contentType: MediaType('image', 'jpg'), filename: 'image.jpg'))
      ];
    }
    return null;
  }
}
