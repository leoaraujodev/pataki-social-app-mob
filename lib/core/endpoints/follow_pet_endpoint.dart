
import 'package:pataki/core/models/follow_user_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class FollowPetEndpoint
    extends PatakiEndpoint<FollowPetRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/user/follow";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<FollowPetRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  FollowPetRequest request;

  FollowPetEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
