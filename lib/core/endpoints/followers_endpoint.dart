import 'package:pataki/core/models/pet_info_request.dart';
import 'package:pataki/core/models/user_list_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class FollowersEndpoint
    extends PatakiEndpoint<PetInfoRequest, UserListResponse, List<dynamic>> {
  String path() => "/pets/${request.petId.toString()}/followers";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<PetInfoRequest, UserListResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  PetInfoRequest request;

  FollowersEndpoint(this.request);

  @override
  UserListResponse responseModelFromJson(List<dynamic> json) {
    return UserListResponse.fromJson(json);
  }
}
