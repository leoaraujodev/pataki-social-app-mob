import 'package:pataki/core/models/forgot_password_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class ForgotPasswordEndpoint extends PatakiEndpoint<ForgotPasswordRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/forgot/pwd";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<ForgotPasswordRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();
  ForgotPasswordRequest request;

  ForgotPasswordEndpoint(this.request);

  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }

}
