import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_blocked_tutor_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetBlockedTutorsEndpoint
    extends PatakiEndpoint<EmptyRequest, GetBlockedTutorResponse, List<dynamic>> {
  String path() => "/user/blockeds";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, GetBlockedTutorResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  GetBlockedTutorsEndpoint(this.request);

  @override
  GetBlockedTutorResponse responseModelFromJson(List<dynamic> json) {
    return GetBlockedTutorResponse.fromJson(json);
  }
}
