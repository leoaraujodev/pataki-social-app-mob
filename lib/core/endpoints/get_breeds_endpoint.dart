import 'package:pataki/core/models/breeds_list_response.dart';
import 'package:pataki/core/models/pet_type_info_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetBreedsEndpoint
    extends PatakiEndpoint<PetTypeInfoRequest, BreedsListResponse, List<dynamic>> {
  String path() => "/pet/type/${request.petTypeId.toString()}/breeds";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<PetTypeInfoRequest, BreedsListResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  PetTypeInfoRequest request;

  GetBreedsEndpoint(this.request);

  @override
  BreedsListResponse responseModelFromJson(List<dynamic> json) {
    return BreedsListResponse.fromJson(json);
  }
}
