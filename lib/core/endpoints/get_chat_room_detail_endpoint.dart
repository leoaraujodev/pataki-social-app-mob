import 'package:pataki/core/models/get_chat_room_detail_request.dart';
import 'package:pataki/core/models/get_chat_room_detail_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetChatRoomDetailEndpoint extends PatakiEndpoint<GetChatRoomDetailRequest,
    GetChatRoomDetailResponse, Map<String, dynamic>> {
  String path() => "/chat/room";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<GetChatRoomDetailRequest, GetChatRoomDetailResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GetChatRoomDetailRequest request;

  GetChatRoomDetailEndpoint(this.request);

  @override
  GetChatRoomDetailResponse responseModelFromJson(Map<String, dynamic> json) {
    return GetChatRoomDetailResponse.fromJson(json);
  }
}
