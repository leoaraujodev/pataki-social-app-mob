import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_chat_rooms_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetChatRoomsEndpoint extends PatakiEndpoint<EmptyRequest, GetChatRoomsResponse, List<dynamic>> {
  String path() => "/chat/rooms";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, GetChatRoomsResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  GetChatRoomsEndpoint(this.request);

  @override
  GetChatRoomsResponse responseModelFromJson(List<dynamic> json) {
    return GetChatRoomsResponse.fromJson(json);
  }
}
