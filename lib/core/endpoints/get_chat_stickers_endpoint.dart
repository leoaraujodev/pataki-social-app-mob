import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_stickers_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetChatStickersEndpoint extends PatakiEndpoint<EmptyRequest, GetStickersResponse, List<dynamic>> {
  String path() => "/chat/stickers";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, GetStickersResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  GetChatStickersEndpoint(this.request);

  @override
  GetStickersResponse responseModelFromJson(List<dynamic> json) {
    return GetStickersResponse.fromJson(json);
  }
}
