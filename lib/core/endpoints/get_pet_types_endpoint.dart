import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_pet_types_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetPetTypes extends PatakiEndpoint<EmptyRequest, GetPetTypeResponse, List<dynamic>> {
  String path() => "/pet/types";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, GetPetTypeResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  GetPetTypes(this.request);

  @override
  GetPetTypeResponse responseModelFromJson(List<dynamic> json) {
    return GetPetTypeResponse.fromJson(json);
  }
}
