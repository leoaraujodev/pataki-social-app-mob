import 'package:pataki/core/models/tricks_by_group_request.dart';
import 'package:pataki/core/models/tricks_by_group_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetTricksByGroupEndpoint
    extends PatakiEndpoint<TricksByGroupRequest, TricksByGroupResponse, List<dynamic>> {
  String path() => "/tricks/group/${request.groupId.toString()}";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<TricksByGroupRequest, TricksByGroupResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  TricksByGroupRequest request;

  GetTricksByGroupEndpoint(this.request);

  @override
  TricksByGroupResponse responseModelFromJson(List<dynamic> json) {
    return TricksByGroupResponse.fromJson(json);
  }
}
