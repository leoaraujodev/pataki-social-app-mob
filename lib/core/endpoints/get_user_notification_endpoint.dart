import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_user_notifications_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetUserNotificationEndpoint
    extends PatakiEndpoint<EmptyRequest, GetUserNotificationsResponse, List<dynamic>> {
  String path() => "/notifications";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, GetUserNotificationsResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  GetUserNotificationEndpoint(this.request);

  @override
  GetUserNotificationsResponse responseModelFromJson(List<dynamic> json) {
    return GetUserNotificationsResponse.fromJson(json);
  }
}

