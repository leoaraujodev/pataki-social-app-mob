import 'package:pataki/core/models/pet_list_response.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GetUserPetsEndpoint extends PatakiEndpoint<UserInfoRequest, PetListResponse, List<dynamic>> {
  String path() => "/user/${request.userId.toString()}/pets";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<UserInfoRequest, PetListResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  UserInfoRequest request;

  GetUserPetsEndpoint(this.request);

  @override
  PetListResponse responseModelFromJson(List<dynamic> json) {
    return PetListResponse.fromJson(json);
  }

}