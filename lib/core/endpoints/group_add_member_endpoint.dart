import 'package:pataki/core/models/group_add_member_request.dart';
import 'package:pataki/core/models/member.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GroupAddMemberEndpoint
    extends PatakiEndpoint<GroupAddMemberRequest, Member, Map<String, dynamic>> {
  String path() => "/group/${request.groupId.toString()}/member";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<GroupAddMemberRequest, Member> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GroupAddMemberRequest request;

  GroupAddMemberEndpoint(this.request);

  Member responseModelFromJson(Map<String, dynamic> json) {
    return Member.fromJson(json);
  }
}
