import 'package:pataki/core/models/group_details.dart';
import 'package:pataki/core/models/group_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GroupDetailsEndpoint
    extends PatakiEndpoint<GroupRequest, GroupDetails, Map<String, dynamic>> {
  String path() => "/group/${request.groupId.toString()}/details";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<GroupRequest, GroupDetails> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GroupRequest request;

  GroupDetailsEndpoint(this.request);

  GroupDetails responseModelFromJson(Map<String, dynamic> json) {
    return GroupDetails.fromJson(json);
  }
}
