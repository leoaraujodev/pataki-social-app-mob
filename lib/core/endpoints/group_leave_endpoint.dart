import 'package:pataki/core/models/group_leave_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GroupLeaveEndpoint
    extends PatakiEndpoint<GroupLeaveRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/group/${request.groupId.toString()}/member";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<GroupLeaveRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GroupLeaveRequest request;

  GroupLeaveEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
