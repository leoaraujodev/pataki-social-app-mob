import 'package:pataki/core/models/group_list_request.dart';
import 'package:pataki/core/models/group_list_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GroupListEndpoint extends PatakiEndpoint<GroupListRequest, GroupListResponse, List<dynamic>> {
  String path() => "/user/${request.userId.toString()}/groups";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<GroupListRequest, GroupListResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GroupListRequest request;

  GroupListEndpoint(this.request);

  @override
  GroupListResponse responseModelFromJson(List<dynamic> json) {
    return GroupListResponse.fromJson(json);
  }
}
