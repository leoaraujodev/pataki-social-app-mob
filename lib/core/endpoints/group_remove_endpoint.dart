import 'package:pataki/core/models/group_remove_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class GroupRemoveEndpoint
    extends PatakiEndpoint<GroupRemoveRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/group/${request.groupId.toString()}";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<GroupRemoveRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  GroupRemoveRequest request;

  GroupRemoveEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
