import 'package:pataki/core/models/like_comment_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class LikeCommentEndpoint
    extends PatakiEndpoint<LikeCommentRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/comment/like";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<LikeCommentRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  LikeCommentRequest request;

  LikeCommentEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
