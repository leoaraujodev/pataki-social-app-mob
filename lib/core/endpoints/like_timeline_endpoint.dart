import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class LikeTimelineEndpoint
    extends PatakiEndpoint<LikeTimelineRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/timeline/like";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<LikeTimelineRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  LikeTimelineRequest request;

  LikeTimelineEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
