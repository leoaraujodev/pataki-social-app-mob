import 'package:pataki/core/models/login_request.dart';
import 'package:pataki/core/models/login_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class LoginEndpoint extends PatakiEndpoint<LoginRequest, LoginResponse, Map<String, dynamic>> {

  LoginEndpoint(this.request);
  LoginRequest request;

  String path() => "/auth/login";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<LoginRequest, LoginResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  @override
  LoginResponse responseModelFromJson(Map<String, dynamic> json) {
    return LoginResponse.fromJson(json);
  }

}
