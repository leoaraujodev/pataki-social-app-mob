import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class LogoutEndpoint
    extends PatakiEndpoint<EmptyRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/logout";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<EmptyRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();
  EmptyRequest request;

  LogoutEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
