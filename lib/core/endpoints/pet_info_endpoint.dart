import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_info_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class PetInfoEndpoint extends PatakiEndpoint<PetInfoRequest, Pet, Map<String, dynamic>> {
  String path() => "/pet/" + request.petId.toString();
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<PetInfoRequest, Pet> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  PetInfoRequest request;

  PetInfoEndpoint(this.request);

  Pet responseModelFromJson(Map<String, dynamic> json) {
    return Pet.fromJson(json);
  }
}
