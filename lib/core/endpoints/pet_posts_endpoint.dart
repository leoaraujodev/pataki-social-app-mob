import 'package:pataki/core/models/pet_posts_request.dart';
import 'package:pataki/core/models/pet_posts_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class PetPostsEndpoint
    extends PatakiEndpoint<PetPostsRequest, PetPostsResponse, Map<String, dynamic>> {
  String path() => "/pet/${request.petId}/posts";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<PetPostsRequest, PetPostsResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  PetPostsRequest request;

  PetPostsEndpoint(this.request);

  @override
  PetPostsResponse responseModelFromJson(Map<String, dynamic> json) {
    return PetPostsResponse.fromJson(json);
  }
}
