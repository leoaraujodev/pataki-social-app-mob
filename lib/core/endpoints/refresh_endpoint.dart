import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/refresh_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class RefreshEndpoint extends PatakiEndpoint<EmptyRequest, RefreshResponse, Map<String, dynamic>> {
  String path() => "/auth/refresh";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<EmptyRequest, RefreshResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  RefreshEndpoint(this.request);

  @override
  RefreshResponse responseModelFromJson(Map<String, dynamic> json) {
    return RefreshResponse.fromJson(json);
  }

}
