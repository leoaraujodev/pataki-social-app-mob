import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/reminder_create_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class ReminderTrickCreateEndpoint
    extends PatakiEndpoint<ReminderCreateRequest, Reminder, Map<String, dynamic>> {
  String path() => "/tricks/${request.trickId}/notification/add";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<ReminderCreateRequest, Reminder> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  ReminderCreateRequest request;

  ReminderTrickCreateEndpoint(this.request);

  Reminder responseModelFromJson(Map<String, dynamic> json) {
    return Reminder.fromJson(json);
  }
}
