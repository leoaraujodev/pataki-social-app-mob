import 'package:pataki/core/models/reminder_delete_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class ReminderTrickDeleteEndpoint
    extends PatakiEndpoint<ReminderDeleteRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/tricks/${request.trickId.toString()}/notification/delete";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<ReminderDeleteRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  ReminderDeleteRequest request;

  ReminderTrickDeleteEndpoint(this.request);

  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
