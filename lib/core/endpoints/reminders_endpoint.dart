import 'package:pataki/core/models/reminders_request.dart';
import 'package:pataki/core/models/reminders_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class RemindersEndpoint extends PatakiEndpoint<RemindersRequest, RemindersResponse, List<dynamic>> {
  String path() => "/pet/${request.petId}/notifications";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<RemindersRequest, RemindersResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  RemindersRequest request;

  RemindersEndpoint(this.request);

  @override
  RemindersResponse responseModelFromJson(List<dynamic> json) {
    return RemindersResponse.fromJson(json);
  }
}
