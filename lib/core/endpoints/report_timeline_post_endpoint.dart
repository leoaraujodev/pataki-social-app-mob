import 'package:pataki/core/models/report_timeline_post_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class ReportTimelinePostEndpoint extends PatakiEndpoint<ReportTimelinePostRequest,
    SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/timeline/report";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<ReportTimelinePostRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  ReportTimelinePostRequest request;

  ReportTimelinePostEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
