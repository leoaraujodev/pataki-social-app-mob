import 'package:pataki/core/models/report_user_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class ReportUserEndpoint
    extends PatakiEndpoint<ReportUserRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/user/report";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<ReportUserRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  ReportUserRequest request;

  ReportUserEndpoint(this.request);

  @override
  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
