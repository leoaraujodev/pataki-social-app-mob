import 'package:pataki/core/models/search_request.dart';
import 'package:pataki/core/models/search_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class SearchEndpoint extends PatakiEndpoint<SearchRequest, SearchResponse, Map<String, dynamic>> {
  String path() => "/search";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<SearchRequest, SearchResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  SearchRequest request;

  SearchEndpoint(this.request);

  @override
  SearchResponse responseModelFromJson(Map<String, dynamic> json) {
    return SearchResponse.fromJson(json);
  }
}
