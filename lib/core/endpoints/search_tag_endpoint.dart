import 'package:pataki/core/models/search_tag_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class SearchTagEndpoint extends PatakiEndpoint<SearchTagRequest, Pet, Map<String, dynamic>> {
  String path() => "/pet/tag";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<SearchTagRequest, Pet> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  SearchTagRequest request;

  SearchTagEndpoint(this.request);

  Pet responseModelFromJson(Map<String, dynamic> json) {
    return json == null ? null : Pet.fromJson(json);
  }
}
