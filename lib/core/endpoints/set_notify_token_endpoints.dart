import 'package:pataki/core/models/set_notify_token_request.dart';
import 'package:pataki/core/models/user_info_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class SetNotifyTokenEndpoint
    extends PatakiEndpoint<SetNotifyTokenRequest, UserInfoResponse, Map<String, dynamic>> {
  String path() => "/user";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<SetNotifyTokenRequest, UserInfoResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  SetNotifyTokenRequest request;

  SetNotifyTokenEndpoint(this.request);

  @override
  UserInfoResponse responseModelFromJson(Map<String, dynamic> json) {
    return UserInfoResponse.fromJson(json);
  }
}
