import 'package:pataki/core/models/single_timeline_request.dart';
import 'package:pataki/core/models/timeline_post.dart';

import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class SingleTimelineEndpoint
    extends PatakiEndpoint<SingleTimelineRequest, TimelinePost, Map<String, dynamic>> {
  String path() => "/timeline/${request.id}";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<SingleTimelineRequest, TimelinePost> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  SingleTimelineRequest request;

  SingleTimelineEndpoint(this.request);

  @override
  TimelinePost responseModelFromJson(Map<String, dynamic> json) {
    return TimelinePost.fromJson(json);
  }
}
