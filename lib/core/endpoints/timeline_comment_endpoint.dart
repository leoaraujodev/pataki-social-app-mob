import 'package:pataki/core/models/timeline_comment_request.dart';
import 'package:pataki/core/models/timeline_comment_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class TimelineCommentEndpoint
    extends PatakiEndpoint<TimelineCommentRequest, TimelineCommentResponse, Map<String, dynamic>> {
  String path() => "/timeline/${request.timelineId}/comments";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<TimelineCommentRequest, TimelineCommentResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  TimelineCommentRequest request;

  TimelineCommentEndpoint(this.request);

  @override
  TimelineCommentResponse responseModelFromJson(Map<String, dynamic> json) {
    return TimelineCommentResponse.fromJson(json);
  }
}
