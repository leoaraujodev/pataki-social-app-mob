import 'package:pataki/core/models/timeline_request.dart';
import 'package:pataki/core/models/timeline_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class TimelineEndpoint extends PatakiEndpoint<TimelineRequest, TimelineResponse, Map<String, dynamic>> {
  String path() => "/timeline";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<TimelineRequest, TimelineResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  TimelineRequest request;

  TimelineEndpoint(this.request);

  @override
  TimelineResponse responseModelFromJson(Map<String, dynamic> json) {
    return TimelineResponse.fromJson(json);
  }

}
