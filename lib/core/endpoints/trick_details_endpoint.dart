import 'package:pataki/core/models/trick.dart';
import 'package:pataki/core/models/trick_details_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class TrickDetailsEndpoint
    extends PatakiEndpoint<TrickDetailsRequest, Trick, Map<String, dynamic>> {
  String path() => "/tricks/trick/${request.trickId.toString()}";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<TrickDetailsRequest, Trick> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  TrickDetailsRequest request;

  TrickDetailsEndpoint(this.request);

  Trick responseModelFromJson(Map<String, dynamic> json) {
    return Trick.fromJson(json);
  }
}
