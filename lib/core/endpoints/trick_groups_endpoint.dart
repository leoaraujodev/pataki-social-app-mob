import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/trick_groups_list_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class TrickGroupsEndpoint
    extends PatakiEndpoint<EmptyRequest, TrickGroupsListResponse, List<dynamic>> {
  String path() => "/tricks/groups";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<EmptyRequest, TrickGroupsListResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  EmptyRequest request;

  TrickGroupsEndpoint(this.request);

  @override
  TrickGroupsListResponse responseModelFromJson(List<dynamic> json) {
    return TrickGroupsListResponse.fromJson(json);
  }
}
