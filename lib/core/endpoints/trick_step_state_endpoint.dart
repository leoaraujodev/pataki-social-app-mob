import 'package:pataki/core/models/trick_step_state_request.dart';
import 'package:pataki/core/models/trick_step_state_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class TrickStepStateEndpoint
    extends PatakiEndpoint<TrickStepStateRequest, TrickStepStateResponse, Map<String, dynamic>> {
  String path() => "/tricks/set-state";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<TrickStepStateRequest, TrickStepStateResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  TrickStepStateRequest request;

  TrickStepStateEndpoint(this.request);

  @override
  TrickStepStateResponse responseModelFromJson(Map<String, dynamic> json) {
    return TrickStepStateResponse.fromJson(json);
  }
}
