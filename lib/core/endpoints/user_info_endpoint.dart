import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/models/user_info_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class UserInfoEndpoint
    extends PatakiEndpoint<UserInfoRequest, UserInfoResponse, Map<String, dynamic>> {
  String path() => "/user/" + request.userId.toString();
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<UserInfoRequest, UserInfoResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  UserInfoRequest request;

  UserInfoEndpoint(this.request);

  UserInfoResponse responseModelFromJson(Map<String, dynamic> json) {
    return UserInfoResponse.fromJson(json);
  }
}
