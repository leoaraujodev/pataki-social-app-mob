import 'package:pataki/core/models/timeline_response.dart';
import 'package:pataki/core/models/user_posts_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class UserPostsEndpoint
    extends PatakiEndpoint<UserPostsRequest, TimelineResponse, Map<String, dynamic>> {
  String path() => "/user/${request.userId}/posts";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<UserPostsRequest, TimelineResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  UserPostsRequest request;

  UserPostsEndpoint(this.request);

  @override
  TimelineResponse responseModelFromJson(Map<String, dynamic> json) {
    return TimelineResponse.fromJson(json);
  }
}
