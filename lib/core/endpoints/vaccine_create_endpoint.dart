import 'package:pataki/core/models/vaccine.dart';
import 'package:pataki/core/models/vaccine_create_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class VaccineCreateEndpoint
    extends PatakiEndpoint<VaccineCreateRequest, Vaccine, Map<String, dynamic>> {
  String path() => "/pet/${request.petId}/vaccine/add";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<VaccineCreateRequest, Vaccine> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  VaccineCreateRequest request;

  VaccineCreateEndpoint(this.request);

  Vaccine responseModelFromJson(Map<String, dynamic> json) {
    return Vaccine.fromJson(json);
  }
}
