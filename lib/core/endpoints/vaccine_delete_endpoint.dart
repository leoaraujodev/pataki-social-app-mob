import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/models/vaccine_delete_request.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class VaccineDeleteEndpoint
    extends PatakiEndpoint<VaccineDeleteRequest, SuccessMessageResponse, Map<String, dynamic>> {
  String path() => "/pet/${request.petId}/vaccine/delete";
  HttpMethod method() => HttpMethod.POST;
  PatakiServer<VaccineDeleteRequest, SuccessMessageResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  VaccineDeleteRequest request;

  VaccineDeleteEndpoint(this.request);

  SuccessMessageResponse responseModelFromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse.fromJson(json);
  }
}
