import 'package:pataki/core/models/vaccines_request.dart';
import 'package:pataki/core/models/vaccines_response.dart';
import 'package:pataki/core/server/pataki_endpoint.dart';
import 'package:pataki/core/server/pataki_server.dart';

class VaccinesEndpoint extends PatakiEndpoint<VaccinesRequest, VaccinesResponse, List<dynamic>> {
  String path() => "/pet/${request.petId}/vaccines";
  HttpMethod method() => HttpMethod.GET;
  PatakiServer<VaccinesRequest, VaccinesResponse> server() => PatakiServer(this);
  Map<String, dynamic> requestBody() => request.toJson();

  VaccinesRequest request;

  VaccinesEndpoint(this.request);

  @override
  VaccinesResponse responseModelFromJson(List<dynamic> json) {
    return VaccinesResponse.fromJson(json);
  }
}
