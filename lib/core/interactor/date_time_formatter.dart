class DateTimeFormatter {
  String execute(DateTime date) {
    final localDateTime = date.toLocal();

    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    final year = DateTime(now.year);

    String hour = localDateTime.hour < 10 ? "0" : "";
    hour += localDateTime.hour.toString();
    hour += ":";
    hour += localDateTime.minute < 10 ? "0" : "";
    hour += localDateTime.minute.toString();

    if (localDateTime.isAfter(today)) {
      return "Hoje às " + hour;
    } else if (localDateTime.isAfter(yesterday)) {
      return "Ontem às " + hour;
    } else if (localDateTime.isAfter(year)) {
      return "${localDateTime.day} de ${_formatMonth(localDateTime.month)}";
    } else {
      return "${localDateTime.day} de ${_formatMonth(localDateTime.month)} de ${localDateTime.year}";
    }
  }

  String _formatMonth(int month) {
    switch (month) {
      case DateTime.january:
        return "janeiro";
      case DateTime.february:
        return "fevereiro";
      case DateTime.march:
        return "março";
      case DateTime.april:
        return "abril";
      case DateTime.may:
        return "maio";
      case DateTime.june:
        return "junho";
      case DateTime.july:
        return "julho";
      case DateTime.august:
        return "agosto";
      case DateTime.september:
        return "setembro";
      case DateTime.october:
        return "outubro";
      case DateTime.november:
        return "novembro";
      case DateTime.december:
        return "dezembro";
      default:
        throw "onzembro";
    }
  }
}
