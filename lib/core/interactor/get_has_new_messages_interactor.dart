import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetHasNewMessagesInteractor {
  Future<bool> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("has_new_messages");
  }
}
