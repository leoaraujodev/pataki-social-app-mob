import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetLastChatIdInteractor {
  Future<int> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("last_chat_id");
  }
}
