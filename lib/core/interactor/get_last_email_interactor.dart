import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetLastEmailInteractor {
  Future<String> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("last_email");
  }
}
