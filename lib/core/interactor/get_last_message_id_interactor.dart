import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetLastMessageIdInteractor {
  Future<int> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("last_message_id");
  }
}
