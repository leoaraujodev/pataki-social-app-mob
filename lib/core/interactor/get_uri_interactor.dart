import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetUriInteractor {
  Future<String> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("deep_linking_uri");
  }
}
