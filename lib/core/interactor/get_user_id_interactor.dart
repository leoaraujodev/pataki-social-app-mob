import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class GetUserIdInteractor {
  Future<int> execute() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get("user_id");
  }
}
