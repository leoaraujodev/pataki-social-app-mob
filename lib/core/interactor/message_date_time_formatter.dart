class MessageDateTimeFormatter {
  String execute(DateTime date) {
    final localDateTime = date.toLocal();

    final now = DateTime.now();
    final year = DateTime(now.year);

    String hour = localDateTime.hour < 10 ? "0" : "";
    hour += localDateTime.hour.toString();
    hour += ":";
    hour += localDateTime.minute < 10 ? "0" : "";
    hour += localDateTime.minute.toString();

    String day = localDateTime.day < 10 ? "0" : "";
    day += localDateTime.day.toString();
    day += "/";
    day += localDateTime.month < 10 ? "0" : "";
    day += localDateTime.month.toString();

    if (localDateTime.isAfter(year)) {
      return "$day $hour";
    } else {
      return "$day/${localDateTime.year} $hour";
    }
  }
}
