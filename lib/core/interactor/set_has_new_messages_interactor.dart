import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SetHasNewMessagesInteractor {
  Future<bool> execute(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("has_new_messages", value);
  }
}
