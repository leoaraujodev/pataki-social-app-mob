import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SetLastChatIdInteractor {
  Future<bool> execute(int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt("last_chat_id", value);
  }
}
