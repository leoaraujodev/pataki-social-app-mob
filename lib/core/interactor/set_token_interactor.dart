import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SetTokenInteractor {

  Future<bool> execute(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("token", value);
  }

}