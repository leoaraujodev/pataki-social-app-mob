import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SetUriInteractor {
  Future<bool> execute(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("deep_linking_uri", value);
  }
}
