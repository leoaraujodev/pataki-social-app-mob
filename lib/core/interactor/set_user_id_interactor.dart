import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SetUserIdInteractor {
  Future<bool> execute(int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt("user_id", value);
  }
}
