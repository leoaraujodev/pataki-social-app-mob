class TimeDifferenceFormatter {
  String execute(DateTime date) {
    final localDateTime = date.toLocal();

    final now = DateTime.now();
    final year = DateTime(now.year);

    final difference = now.difference(localDateTime);

    if (difference.inMinutes < 1) {
      return "Agora mesmo";
    } else if (difference.inMinutes < 60) {
      return "${difference.inMinutes}m";
    } else if (difference.inHours < 24) {
      return "${difference.inHours}h";
    } else if (difference.inDays < 31) {
      return "${difference.inDays}d";
    } else if (localDateTime.isAfter(year)) {
      return "${localDateTime.day}/${localDateTime.month}";
    } else {
      return "${localDateTime.day}/${localDateTime.month}/${localDateTime.year}";
    }
  }
}
