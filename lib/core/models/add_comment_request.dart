import 'package:pataki/core/server/pataki_request.dart';

class AddCommentRequest extends PatakiRequest {
  final int timelineId;
  final String comment;

  AddCommentRequest({
    this.timelineId,
    this.comment,
  });

  Map<String, dynamic> toJson() {
    return {
      'timeline_id': timelineId.toString(),
      'comment': comment,
    };
  }
}
