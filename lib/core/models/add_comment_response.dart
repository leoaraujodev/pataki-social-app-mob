import 'package:pataki/core/server/pataki_response.dart';

class AddCommentResponse extends PatakiResponse {
  AddCommentResponse({
    this.comment,
    this.createdAt,
    this.id,
    this.liked,
  });

  final String comment;
  final DateTime createdAt;
  final int id;
  final bool liked;

  factory AddCommentResponse.fromJson(Map<String, dynamic> json) => AddCommentResponse(
        comment: json["comment"] == null ? null : json["comment"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        id: json["id"] == null ? null : json["id"],
        liked: json["liked"] == null ? null : json["liked"],
      );
}
