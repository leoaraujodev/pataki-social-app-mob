import 'package:pataki/core/server/pataki_request.dart';

class AddTagRequest extends PatakiRequest {
  final int id;
  final String tag;

  AddTagRequest({
    this.id,
    this.tag,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id.toString(),
      'tag': tag,
    };
  }
}
