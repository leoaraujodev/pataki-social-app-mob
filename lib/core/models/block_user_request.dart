import 'package:pataki/core/server/pataki_request.dart';

class BlockUserRequest extends PatakiRequest {
  final int id;
  final bool block;

  BlockUserRequest({
    this.id,
    this.block,
  });

  Map<String, dynamic> toJson() {
    return {
      'user_id': id.toString(),
      'block': block ? "1" : "0",
    };
  }
}
