import 'package:pataki/core/models/pet_type_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Breed extends PatakiResponse {
  Breed({
    this.id,
    this.name,
    this.type,
  });

  final int id;
  final String name;
  final PetType type;

  factory Breed.fromJson(Map<String, dynamic> json) => Breed(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        type: json["type"] == null ? null : PetType.fromJson(json["type"]),
      );
}
