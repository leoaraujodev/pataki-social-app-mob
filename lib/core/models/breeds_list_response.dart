import 'package:pataki/core/server/pataki_response.dart';
import 'breed.dart';

class BreedsListResponse extends PatakiResponse {
  final List<Breed> pets;

  BreedsListResponse({
    this.pets,
  });

  factory BreedsListResponse.fromJson(List<dynamic> json) {
    return BreedsListResponse(pets: List<Breed>.from(json.map((x) => Breed.fromJson(x))));
  }
}
