import 'dart:io';

import 'package:pataki/core/models/sticker.dart';
import 'package:pataki/core/server/pataki_response.dart';

enum ChatMessageStatus { sending, sent, error }

extension ChatMessageStatusText on ChatMessageStatus {
  String toText() {
    switch (this) {
      case ChatMessageStatus.sending:
        return "Enviando";
      case ChatMessageStatus.sent:
        return "Enviado";
      case ChatMessageStatus.error:
        return "Erro";
      default:
        throw "Unknown ChatMessageStatus";
    }
  }
}

class ChatMessage extends PatakiResponse {
  ChatMessage({
    this.id,
    this.senderId,
    this.message,
    this.imagePath,
    this.createdAt,
    this.sticker,
    this.image,
    this.status,
  });

  final num id;
  final int senderId;
  final String message;
  final String imagePath;
  final DateTime createdAt;
  final Sticker sticker;
  final File image;
  final ChatMessageStatus status;

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
        id: json["id"] == null ? null : json["id"],
        senderId: json["sender_id"] == null ? null : json["sender_id"],
        message: json["message"] == null ? null : json["message"],
        imagePath: json["image_path"] == null ? null : json["image_path"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        sticker: json["sticker"] == null ? null : Sticker.fromJson(json["sticker"]),
        status: ChatMessageStatus.sent,
      );

  ChatMessage copyWith({
    num id,
    int senderId,
    String message,
    String imagePath,
    DateTime createdAt,
    Sticker sticker,
    File image,
    ChatMessageStatus status,
  }) =>
      ChatMessage(
        id: id ?? this.id,
        senderId: senderId ?? this.senderId,
        message: message ?? this.message,
        imagePath: imagePath ?? this.imagePath,
        createdAt: createdAt ?? this.createdAt,
        sticker: sticker ?? this.sticker,
        image: image ?? this.image,
        status: status ?? this.status,
      );
}
