import 'package:pataki/core/models/chat_message.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class ChatRoom extends PatakiResponse {
  ChatRoom({
    this.id,
    this.contact,
    this.lastMessage,
    this.createdAt,
  });

  final int id;
  final UserModel contact;
  final ChatMessage lastMessage;
  final DateTime createdAt;

  factory ChatRoom.fromJson(Map<String, dynamic> json) => ChatRoom(
        id: json["id"] == null ? null : json["id"],
        contact: json["contact"] == null ? null : UserModel.fromJson(json["contact"]),
        lastMessage:
            json["last_message"] == null ? null : ChatMessage.fromJson(json["last_message"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
      );
}
