import 'package:pataki/core/models/user_model.dart';

class CommentModel {
  CommentModel({
    this.id,
    this.comment,
    this.totLikes,
    this.createdAt,
    this.liked,
    this.user,
  });

  final int id;
  final String comment;
  final int totLikes;
  final DateTime createdAt;
  final bool liked;
  final UserModel user;

  CommentModel copyWith({
    int id,
    String comment,
    int totLikes,
    DateTime createdAt,
    bool liked,
    UserModel user,
  }) =>
      CommentModel(
        id: id ?? this.id,
        comment: comment ?? this.comment,
        totLikes: totLikes ?? this.totLikes,
        createdAt: createdAt ?? this.createdAt,
        liked: liked ?? this.liked,
        user: user ?? this.user,
      );

  factory CommentModel.fromJson(Map<String, dynamic> json) => CommentModel(
        id: json["id"] == null ? null : json["id"],
        comment: json["comment"] == null ? null : json["comment"],
        totLikes: json["tot_likes"] == null ? null : json["tot_likes"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        liked: json["liked"] == null ? null : json["liked"],
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
      );
}
