import 'dart:io';
import 'package:pataki/core/server/pataki_request.dart';

class CreateChatMessageRequest extends PatakiRequest {
  CreateChatMessageRequest({
    this.roomId,
    this.message,
    this.stickerId,
    this.image,
  });

  final int roomId;
  final int stickerId;
  final String message;
  final File image;


  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'room_id': roomId,
    };

    if (stickerId != null) {
      json.addAll({'sticker_id': stickerId});
    }

    if (message != null) {
      json.addAll({'message': message});
    }

    return json;
  }

}

