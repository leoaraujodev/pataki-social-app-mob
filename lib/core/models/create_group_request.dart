import 'dart:io';

import 'package:pataki/core/server/pataki_request.dart';

class CreateGroupRequest extends PatakiRequest {
  CreateGroupRequest({this.name, this.desc, this.privacy, this.image});

  final String name;
  final String desc;
  final String privacy;
  final File image;

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'desc': desc,
      'privacy': privacy,
    };
  }
}
