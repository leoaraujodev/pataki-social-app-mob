import 'dart:io';

import 'package:pataki/core/server/pataki_request.dart';

class CreatePetRequest extends PatakiRequest {
  CreatePetRequest({this.name, this.size, this.weight, this.breedId, this.birthDate, this.image});

  final String name;
  final String size;
  final double weight;
  final int breedId;
  final DateTime birthDate;
  final File image;

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'size': size,
      'weight': '$weight',
      'breed_id': '$breedId',
      'birth_date': birthDate,
    };
  }
}
