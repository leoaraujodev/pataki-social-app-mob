import 'dart:io';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/server/pataki_request.dart';

class CreatePostRequest extends PatakiRequest {
  CreatePostRequest({
    this.petId,
    this.description,
    this.image,
    this.groupId,
    this.fileType,
  });

  final int petId;
  final String description;
  final File image;
  final int groupId;
  final FileType fileType;

  Map<String, dynamic> toJson() {
    return groupId != null
        ? {
            'pet_id': petId.toString(),
            'desc': description,
            'group_id': groupId.toString(),
          }
        : {
            'pet_id': petId.toString(),
            'desc': description,
          };
  }
}
