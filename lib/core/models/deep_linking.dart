import 'package:flutter/services.dart' show PlatformException;
import 'package:get/get.dart';
import 'package:pataki/core/interactor/get_token_interactor.dart';
import 'package:pataki/core/interactor/get_uri_interactor.dart';
import 'package:pataki/core/interactor/set_uri_interactor.dart';
import 'package:uni_links/uni_links.dart';

mixin DeepLinking {
  List<String> possibleRoutes = [
    'pet_screen',
    'profile_screen',
    'single_post_screen',
    'group_screen',
  ];

  Future initUniLinks() async {
    try {
      final String initialLink = await getInitialLink();

      await setNewPath(initialLink);
    } on PlatformException {}
  }

  Future<Null> onChangeUniLinks(sub) async {
    sub = getLinksStream().listen((String initialLink) async {
      await setNewPath(initialLink);

      final token = await GetTokenInteractor().execute();
      if (token != null) {
        redirectPageTo();
      }
    }, onError: (err) {});
  }

  setNewPath(String initialLink) async {
    if (initialLink == null) {
      return;
    }

    final List linkWithPath = initialLink.split("#");

    if (linkWithPath.length == 1) {
      return;
    }

    final String link = linkWithPath[1];
    final String stringName = link.split("/")[1];

    if (possibleRoutes.contains(stringName) && link.split("/").length == 3) {
      await SetUriInteractor().execute(link);
    }
  }

  redirectPageTo() async {
    final String link = await GetUriInteractor().execute();

    if (link == null || link.isEmpty) {
      return;
    }

    await SetUriInteractor().execute("");

    Get.toNamed(
      link,
      arguments: {"timestamp": DateTime.now().toIso8601String()},
    );
  }
}
