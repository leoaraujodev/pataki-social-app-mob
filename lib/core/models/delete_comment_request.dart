import 'package:pataki/core/server/pataki_request.dart';

class DeleteCommentRequest extends PatakiRequest {
  final int id;

  DeleteCommentRequest({this.id});

  Map<String, dynamic> toJson() {
    return {'id': id.toString()};
  }
}
