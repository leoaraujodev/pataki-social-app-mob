import 'package:pataki/core/server/pataki_request.dart';

class DeletePetRequest extends PatakiRequest {
  final int petId;

  DeletePetRequest({this.petId});

  Map<String, dynamic> toJson() {
    return {'id': petId.toString()};
  }
}
