import 'package:pataki/core/server/pataki_request.dart';

class DeletePostRequest extends PatakiRequest {
  final int id;

  DeletePostRequest({this.id});

  Map<String, dynamic> toJson() {
    return {'id': id.toString()};
  }
}
