import 'package:pataki/core/server/pataki_request.dart';

class EditCommentRequest extends PatakiRequest {
  final int id;
  final String comment;

  EditCommentRequest({
    this.id,
    this.comment,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id.toString(),
      'comment': comment,
    };
  }
}
