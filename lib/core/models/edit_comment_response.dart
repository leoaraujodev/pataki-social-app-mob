import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class EditCommentResponse extends PatakiResponse {
  EditCommentResponse({this.comment});

  final CommentModel comment;

  factory EditCommentResponse.fromJson(Map<String, dynamic> json) => EditCommentResponse(
        comment: json["comment"] == null ? null : CommentModel.fromJson(json["comment"]),
      );
}
