import 'dart:io';

import 'package:pataki/core/server/pataki_request.dart';

class EditGroupRequest extends PatakiRequest {
  EditGroupRequest({
    this.groupId,
    this.name,
    this.desc,
    this.privacy,
    this.image,
  });

  final int groupId;
  final String name;
  final String desc;
  final String privacy;
  final File image;

  Map<String, dynamic> toJson() {
    // return image == null
    //     ? {
    //         '_method': 'put',
    //         'name': name,
    //         'desc': desc,
    //         'privacy': privacy,
    //         'remove_image': '1',
    //       }
    //     :
    return {
      '_method': 'put',
      'name': name,
      'desc': desc,
      'privacy': privacy,
      'image': image,
    };
  }
}
