import 'package:pataki/core/server/pataki_request.dart';

class EditPassRequest extends PatakiRequest {
  final String oldPassword;
  final String newPassword;

  EditPassRequest({
    this.oldPassword,
    this.newPassword,
  });

  Map<String, dynamic> toJson() {
    return {
      '_method': 'put',
      'cur_password': oldPassword,
      'new_password': newPassword,
    };
  }
}
