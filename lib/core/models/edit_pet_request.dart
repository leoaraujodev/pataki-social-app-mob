import 'package:pataki/core/server/pataki_request.dart';
import 'dart:io';

class EditPetRequest extends PatakiRequest {
  final int id;
  final String name;
  final String size;
  final double weight;
  final int breedId;
  final DateTime birthDate;
  final File image;

  EditPetRequest({
    this.id,
    this.name,
    this.size,
    this.weight,
    this.breedId,
    this.birthDate,
    this.image,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id.toString(),
      'name': name,
      'size': size,
      'weight': '$weight',
      'breed_id': '$breedId',
      'birth_date': birthDate,
    };
  }
}
