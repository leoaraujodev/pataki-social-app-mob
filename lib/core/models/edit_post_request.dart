import 'dart:io';

import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/server/pataki_request.dart';

class EditPostRequest extends PatakiRequest {
  EditPostRequest({
    this.id,
    this.description,
    this.image,
    this.removeImage,
    this.fileType,
  });

  final int id;
  final String description;
  final File image;
  final bool removeImage;
  final FileType fileType;

  Map<String, dynamic> toJson() {
    return {
      'id': id.toString(),
      'desc': description,
      'remove_image': removeImage ? "1" : "0",
    };
  }
}
