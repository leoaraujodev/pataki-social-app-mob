import 'package:pataki/core/server/pataki_request.dart';
import 'dart:io';

class EditUserRequest extends PatakiRequest {
  final int id;
  final String name;
  final String email;
  final String phone;
  final String description;
  final File image;

  EditUserRequest({this.id, this.name, this.email, this.phone, this.description, this.image});

  Map<String, dynamic> toJson() {
    return {
      '_method': 'put',
      'id': id.toString(),
      'name': name,
      'email': email,
      'phone': phone,
      'desc': description,
    };
  }
}
