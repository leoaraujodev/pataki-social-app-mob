import 'package:pataki/core/server/pataki_request.dart';

class EmptyRequest extends PatakiRequest {
  
  EmptyRequest();

  Map<String, dynamic> toJson() {
    return null;
  }

}