enum FileType { image, video }

getFileType(String imagePath) {
  String fileExtension = imagePath.split(".").last.toLowerCase();

  if (fileExtension == "jpeg" ||
      fileExtension == "jpg" ||
      fileExtension == "png" ||
      fileExtension == "gif" ||
      fileExtension == "bmp" ||
      fileExtension == "webp" ||
      fileExtension == "wbmp") {
    return FileType.image;
  }

  if (fileExtension == "mp4" ||
      fileExtension == "mov" ||
      fileExtension == "avi" ||
      fileExtension == "webm" ||
      fileExtension == "wmv") {
    return FileType.video;
  }

  return null;
}
