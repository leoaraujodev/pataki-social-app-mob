import 'package:pataki/core/server/pataki_request.dart';

class FollowPetRequest extends PatakiRequest {
  final int petId;
  final bool follow;

  FollowPetRequest({this.petId, this.follow});

  Map<String, dynamic> toJson() {
    return {
      'pet_id': petId.toString(),
      'follow': follow ? "1" : "0",
    };
  }
}
