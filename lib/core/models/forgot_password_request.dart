import 'package:pataki/core/server/pataki_request.dart';

class ForgotPasswordRequest extends PatakiRequest {
  final String email;

  ForgotPasswordRequest({this.email});

  Map<String, dynamic> toJson() {
    return {
      'email' : email
    };
  }

}