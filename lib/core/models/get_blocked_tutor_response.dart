import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetBlockedTutorResponse extends PatakiResponse {
  final List<UserModel> blockedTutors;

  GetBlockedTutorResponse({
    this.blockedTutors,
  });

  factory GetBlockedTutorResponse.fromJson(List<dynamic> json) {
    return GetBlockedTutorResponse(
        blockedTutors: List<UserModel>.from(json.map((x) => UserModel.fromJson(x))));
  }
}
