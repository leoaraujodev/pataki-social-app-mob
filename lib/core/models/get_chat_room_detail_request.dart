
import 'package:pataki/core/server/pataki_request.dart';

class GetChatRoomDetailRequest extends PatakiRequest {
  final int userId;
  final int lastId;
  final int firstId;

  GetChatRoomDetailRequest({
    this.userId,
    this.lastId,
    this.firstId,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'user_id': userId,
    };

    if (lastId != null) {
      json.addAll({'last_id': lastId});
    }

    if (firstId != null) {
      json.addAll({'first_id': firstId});
    }

    return json;
  }
}