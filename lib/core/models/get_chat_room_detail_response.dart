import 'package:pataki/core/models/chat_message.dart';
import 'package:pataki/core/models/chat_room.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetChatRoomDetailResponse extends PatakiResponse {
  final ChatRoom room;
  final List<ChatMessage> messages;

  GetChatRoomDetailResponse({this.room, this.messages});

  factory GetChatRoomDetailResponse.fromJson(Map<String, dynamic> json) {
    return GetChatRoomDetailResponse(
        room: ChatRoom.fromJson(json['room']),
      messages:
      json["messages"] == null ? null : List<ChatMessage>.from(json["messages"].map((x) => ChatMessage.fromJson(x))),
    );
  }
}

