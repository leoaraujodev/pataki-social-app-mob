import 'package:pataki/core/models/chat_room.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetChatRoomsResponse extends PatakiResponse {
  final List<ChatRoom> rooms;

  GetChatRoomsResponse({
    this.rooms,
  });

  factory GetChatRoomsResponse.fromJson(List<dynamic> json) {
    return GetChatRoomsResponse(
        rooms:  List<ChatRoom>.from(json.map((x) => ChatRoom.fromJson(x)))
    );
  }
}