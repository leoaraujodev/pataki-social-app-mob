import 'package:pataki/core/models/pet_type_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetPetTypeResponse extends PatakiResponse {
  final List<PetType> petTypes;

  GetPetTypeResponse({
    this.petTypes,
  });

  factory GetPetTypeResponse.fromJson(List<dynamic> json) {
    return GetPetTypeResponse(
        petTypes:  List<PetType>.from(json.map((x) => PetType.fromJson(x)))
    );
  }
}