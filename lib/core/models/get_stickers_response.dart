import 'package:pataki/core/models/sticker.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetStickersResponse extends PatakiResponse {
  final List<Sticker> stickers;

  GetStickersResponse({
    this.stickers,
  });

  factory GetStickersResponse.fromJson(List<dynamic> json) {
    return GetStickersResponse(
        stickers:  List<Sticker>.from(json.map((x) => Sticker.fromJson(x)))
    );
  }
}