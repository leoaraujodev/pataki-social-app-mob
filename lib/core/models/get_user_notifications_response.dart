import 'package:pataki/core/models/user_notification.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GetUserNotificationsResponse extends PatakiResponse {
  final List<UserNotification> notifications;

  GetUserNotificationsResponse({
    this.notifications,
  });

  factory GetUserNotificationsResponse.fromJson(List<dynamic> json) {
    return GetUserNotificationsResponse(
        notifications: List<UserNotification>.from(json.map((x) => UserNotification.fromJson(x))));
  }
}
