import 'package:pataki/core/server/pataki_response.dart';
import 'package:pataki/core/models/member.dart';

class Group extends PatakiResponse {
  Group({
    this.id,
    this.name,
    this.desc,
    this.createdAt,
    this.updatedAt,
    this.privacy,
    this.imagePath,
    this.totalMembers,
    this.isMember,
    this.lastMembers,
  });

  final int id;
  final String name;
  final String desc;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String privacy;
  final String imagePath;
  final int totalMembers;
  final bool isMember;
  final List<Member> lastMembers;

  factory Group.fromJson(Map<String, dynamic> json) => Group(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        desc: json["desc"] == null ? null : json["desc"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        privacy: json["privacy"] == null ? null : json["privacy"],
        imagePath: json["image_path"] == null ? null : json["image_path"],
        totalMembers: json["tot_members"] == null ? null : json["tot_members"],
        isMember: json["is_member"] == null ? null : json["is_member"],
        lastMembers: json["last_members"] == null
            ? null
            : List<Member>.from(json["last_members"].map((x) => Member.fromJson(x))),
      );
}
