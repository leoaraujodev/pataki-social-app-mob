import 'package:pataki/core/server/pataki_request.dart';

class GroupAddMemberRequest extends PatakiRequest {
  final int groupId;
  final int userId;

  GroupAddMemberRequest({this.groupId, this.userId});

  Map<String, dynamic> toJson() {
    return {
      'user_id': userId.toString(),
    };
  }
}
