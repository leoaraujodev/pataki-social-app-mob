import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/server/pataki_response.dart';
import 'package:pataki/core/models/member.dart';

class GroupDetails extends PatakiResponse {
  GroupDetails({
    this.group,
    this.members,
    this.posts,
  });

  Group group;
  final List<Member> members;
  final List<TimelinePost> posts;

  factory GroupDetails.fromJson(Map<String, dynamic> json) => GroupDetails(
        group: json["group"] == null ? null : Group.fromJson(json["group"]),
        members: json["members"] == null
            ? null
            : List<Member>.from(json["members"].map((x) => Member.fromJson(x))),
        posts: json["posts"] == null
            ? null
            : List<TimelinePost>.from(json["posts"].map((x) => TimelinePost.fromJson(x))),
      );
}
