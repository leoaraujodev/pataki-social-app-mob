import 'package:pataki/core/server/pataki_request.dart';

class GroupLeaveRequest extends PatakiRequest {
  final int groupId;
  final int userId;

  GroupLeaveRequest({this.groupId, this.userId});

  Map<String, dynamic> toJson() {
    return {
      'user_id': userId.toString(),
      '_method': 'delete',
    };
  }
}
