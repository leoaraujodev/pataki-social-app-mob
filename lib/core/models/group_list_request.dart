import 'package:pataki/core/server/pataki_request.dart';

class GroupListRequest extends PatakiRequest {
  final int userId;

  GroupListRequest({this.userId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
