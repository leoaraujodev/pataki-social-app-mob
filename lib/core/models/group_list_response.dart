import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/server/pataki_response.dart';

class GroupListResponse extends PatakiResponse {
  final List<Group> groups;

  GroupListResponse({
    this.groups,
  });

  factory GroupListResponse.fromJson(List<dynamic> json) {
    return GroupListResponse(groups: List<Group>.from(json.map((x) => Group.fromJson(x))));
  }
}
