import 'package:pataki/core/server/pataki_request.dart';

class GroupRemoveRequest extends PatakiRequest {
  final int groupId;

  GroupRemoveRequest({this.groupId});

  Map<String, dynamic> toJson() {
    return {
      '_method': 'delete',
    };
  }
}
