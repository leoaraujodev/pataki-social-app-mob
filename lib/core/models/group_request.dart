import 'package:pataki/core/server/pataki_request.dart';

class GroupRequest extends PatakiRequest {
  final int groupId;
  final int lastId;

  GroupRequest({this.groupId, this.lastId});

  Map<String, dynamic> toJson() {
    return lastId != null ? {'last_post_id': lastId.toString()} : {};
  }
}
