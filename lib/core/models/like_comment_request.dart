import 'package:pataki/core/server/pataki_request.dart';

class LikeCommentRequest extends PatakiRequest {
  final int commentId;
  final bool like;

  LikeCommentRequest({
    this.commentId,
    this.like,
  });

  Map<String, dynamic> toJson() {
    return {
      'comment_id': commentId.toString(),
      'like': like ? "1" : "0",
    };
  }
}
