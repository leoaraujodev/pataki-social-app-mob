class LikeModel {
  LikeModel({
    this.id,
    this.name,
    this.email,
    this.createdAt,
    this.updatedAt,
    this.desc,
    this.isAdmin,
    this.uuid,
    this.onesiginalToken,
    this.phone,
    this.timelineId,
    this.userId,
    this.imagePath,
    this.isBlocked,
  });

  final int id;
  final String name;
  final String email;
  final DateTime createdAt;
  final DateTime updatedAt;
  final dynamic desc;
  final bool isAdmin;
  final String uuid;
  final String onesiginalToken;
  final String phone;
  final int timelineId;
  final int userId;
  final String imagePath;
  final bool isBlocked;

  factory LikeModel.fromJson(Map<String, dynamic> json) => LikeModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        desc: json["desc"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        onesiginalToken: json["onesiginal_token"] == null ? null : json["onesiginal_token"],
        phone: json["phone"] == null ? null : json["phone"],
        timelineId: json["timeline_id"] == null ? null : json["timeline_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        imagePath: json["image_path"] == null ? null : json["image_path"],
        isBlocked: json["is_blocked"] == null ? null : json["is_blocked"],
      );
}
