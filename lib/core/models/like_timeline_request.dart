import 'package:pataki/core/server/pataki_request.dart';

class LikeTimelineRequest extends PatakiRequest {
  final int timelineId;
  final bool like;

  LikeTimelineRequest({
    this.timelineId,
    this.like,
  });

  Map<String, dynamic> toJson() {
    return {
      'timeline_id': timelineId.toString(),
      'like': like ? "1" : "0",
    };
  }
}
