import 'package:pataki/core/server/pataki_request.dart';

class LoginRequest extends PatakiRequest {
  final String username;
  final String password;

  LoginRequest({this.username, this.password});

  Map<String, dynamic> toJson() {
    return {
      'email' : username,
      'password' : password
    };
  }

}