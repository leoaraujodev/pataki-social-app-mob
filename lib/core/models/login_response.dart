import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/models/vaccine.dart';
import 'package:pataki/core/server/pataki_response.dart';

class LoginResponse extends PatakiResponse {
  final String token;
  final UserModel user;
  final List<Vaccine> petVaccines;
  final List<Reminder> petReminders;
  final List<Reminder> trickReminders;

  LoginResponse({
    this.token,
    this.user,
    this.petVaccines,
    this.petReminders,
    this.trickReminders,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      token: json['access_token'],
      user: UserModel.fromJson(json['user']),
      petVaccines: json['pets_vaccines'] == null
          ? null
          : List<Vaccine>.from(json['pets_vaccines'].map((x) => Vaccine.fromJson(x))),
      petReminders: json['pets_notifications'] == null
          ? null
          : List<Reminder>.from(json['pets_notifications'].map((x) => Reminder.fromJson(x))),
      trickReminders: json['tricks_notifications'] == null
          ? null
          : List<Reminder>.from(json['tricks_notifications'].map((x) => Reminder.fromJson(x))),
    );
  }
}
