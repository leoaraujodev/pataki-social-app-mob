import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Media extends PatakiResponse {
  Media({this.imagePath}) : this.fileType = getFileType(imagePath);

  final String imagePath;
  final FileType fileType;

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        imagePath: json["image_path"] == null ? null : json["image_path"],
      );
}
