import 'package:pataki/core/server/pataki_response.dart';
import 'package:pataki/core/models/user_model.dart';

class Member extends PatakiResponse {
  Member({
    this.profile,
    this.user,
  });

  final int profile;
  final UserModel user;

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        profile: json["profile"] == null ? null : json["profile"],
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
      );
}
