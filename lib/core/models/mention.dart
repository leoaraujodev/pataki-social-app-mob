import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/pet/pet_screen.dart';

class Mention {
  String petName;
  int petId;
  int initPositionInAt;
  int finalPositionInAt;

  Mention({
    this.petName,
    this.petId,
    this.initPositionInAt,
    this.finalPositionInAt,
  });

  setPosition(int quantity) {
    this.initPositionInAt = this.initPositionInAt + quantity;
    this.finalPositionInAt = this.finalPositionInAt + quantity;
  }

  static List<TextSpan> listTextSpanWithRoutes(String text) {
    if (text == null) {
      return null;
    }

    Iterable<Match> matches = getTagMatches(text);

    List<TextSpan> textList = [];
    int cursor = 0;

    matches.forEach((match) {
      List petNameAndPetId = getPetNameAndPetId(match);
      String petName = petNameAndPetId[1];
      int petId = int.tryParse(petNameAndPetId[0]);

      textList.add(
        TextSpan(text: text.substring(cursor, match.start)),
      );
      textList.add(
        TextSpan(
          text: "@$petName",
          style: TextStyle(
            color: Colors.blue,
          ),
          recognizer: TapGestureRecognizer()
            ..onTap = () {
              Get.toNamed(
                PetScreen.routeName(petId),
                arguments: {"timestamp": DateTime.now().toIso8601String()},
              );
            },
        ),
      );

      cursor = match.end;
    });

    textList.add(
      TextSpan(text: text.substring(cursor, text.length)),
    );

    return textList;
  }

  static String textWithAt(String text) {
    if (text == null) {
      return "";
    }

    Iterable<Match> matches = getTagMatches(text);

    int cursor = 0;
    String textWithAt = "";

    matches.forEach((match) {
      List petNameAndPetId = getPetNameAndPetId(match);
      String petName = petNameAndPetId[1];

      textWithAt = textWithAt + text.substring(cursor, match.start) + "@$petName";

      cursor = match.end;
    });

    textWithAt = textWithAt + text.substring(cursor, text.length);

    return textWithAt;
  }

  static List<Mention> listMentions(String text) {
    if (text == null) {
      return null;
    }

    Iterable<Match> matches = getTagMatches(text);

    List<Mention> mentions = [];
    int movePositionChangeTagToAt = 0;

    matches.forEach((match) {
      List petNameAndPetId = getPetNameAndPetId(match);
      String petName = petNameAndPetId[1];
      int petId = int.tryParse(petNameAndPetId[0]);

      Mention mention = Mention(
        petName: petName,
        petId: petId,
        initPositionInAt: match.start - movePositionChangeTagToAt,
        finalPositionInAt: match.start - movePositionChangeTagToAt + "@$petName".length,
      );

      mentions.add(mention);
      movePositionChangeTagToAt = match.group(0).length - "@$petName".length;
    });

    return mentions;
  }

  static Iterable<Match> getTagMatches(String text) {
    RegExp regex = RegExp("<PTK-P>[^<]+</PTK-P>");

    return regex.allMatches(text);
  }

  static List getPetNameAndPetId(Match match) {
    String mentionWithTag = match.group(0);
    return mentionWithTag.replaceAll("<PTK-P>", "").replaceAll("</PTK-P>", "").split("|");
  }
}
