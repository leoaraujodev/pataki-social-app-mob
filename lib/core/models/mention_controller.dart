import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/following_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/mention.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_list_response.dart';
import 'package:pataki/core/models/user_info_request.dart';

class MentionController {
  final textController = TextEditingController();
  final textFocusNode = FocusNode();

  final petsFollowed = RxList<Pet>();
  final petsToMention = RxList<Pet>();
  final petsMentioned = <Mention>[];

  String petToMention;
  int mentionPosition = 0;
  String textControllerWithMentions = "";

  int oldBaseCursorPosition = 0;
  int oldExtentCursorPosition = 0;
  int newBaseCursorPosition = 0;
  int newExtentCursorPosition = 0;

  mentionControllerReset() {
    oldBaseCursorPosition = 0;
    oldExtentCursorPosition = 0;
    newBaseCursorPosition = 0;
    newExtentCursorPosition = 0;
    mentionPosition = 0;
    textControllerWithMentions = "";
    petsMentioned.clear();
  }

  editMentionOnInit(String text) async {
    mentionControllerReset();
    await getPetsFolloweds();

    final toAdd = Mention.listMentions(text);

    if (toAdd != null) {
      petsMentioned.addAll(toAdd);
    }

    textController.value = TextEditingValue(
      text: Mention.textWithAt(text),
      selection: TextSelection.fromPosition(
        TextPosition(offset: Mention.textWithAt(text).length),
      ),
    );
    textControllerWithMentions = textController.text;

    oldBaseCursorPosition = textController.selection.baseOffset;
    oldExtentCursorPosition = textController.selection.extentOffset;
    newBaseCursorPosition = textController.selection.baseOffset;
    newExtentCursorPosition = textController.selection.extentOffset;
  }

  Future<void> getPetsFolloweds() async {
    try {
      int userId = await GetUserIdInteractor().execute();
      PetListResponse response = await FollowingEndpoint(UserInfoRequest(userId: userId)).call();

      petsFollowed.clear();
      petsFollowed.addAll(response.pets);
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  showPetsList() {
    if (!textFocusNode.hasFocus) {
      return;
    }

    petToMention = null;
    petsToMention.clear();

    final int cursorPosition = textController.selection.baseOffset;
    int lastCursorOrMentionPosition = 0;

    if (petsMentioned.isNotEmpty) {
      petsMentioned.sort(
          (mentionA, mentionB) => mentionA.finalPositionInAt.compareTo(mentionB.finalPositionInAt));

      petsMentioned.forEach((Mention mention) {
        if (cursorPosition > mention.initPositionInAt) {
          lastCursorOrMentionPosition = mention.finalPositionInAt;
        }
      });
    }

    final String commentTextBetweenMentionAndCursor = cursorPosition >= lastCursorOrMentionPosition
        ? textController.text.substring(lastCursorOrMentionPosition, cursorPosition)
        : "";

    if (!commentTextBetweenMentionAndCursor.contains("@")) {
      return;
    }

    final Match match = '@'.allMatches(commentTextBetweenMentionAndCursor).last;

    if (match.end > cursorPosition) {
      return;
    }

    petToMention = commentTextBetweenMentionAndCursor.substring((match.end));
    mentionPosition = match.start + lastCursorOrMentionPosition;

    final tempIterable = petsFollowed.where(
      (Pet pet) => (pet.name.toLowerCase().contains(petToMention.toLowerCase()) &&
          !petsMentioned.any((Mention mention) => mention.petId == pet.id)),
    );
    petsToMention.addAll(tempIterable);
  }

  dealWithModifications() {
    newBaseCursorPosition = textController.selection.baseOffset;
    newExtentCursorPosition = textController.selection.extentOffset;

    updateTextWithMentions();

    oldBaseCursorPosition = newBaseCursorPosition;
    oldExtentCursorPosition = newExtentCursorPosition;
  }

  updateTextWithMentions() {
    if (textControllerWithMentions == textController.text) {
      return;
    }

    final int numberOfCharactersChanged = (newBaseCursorPosition < oldBaseCursorPosition &&
            oldBaseCursorPosition == oldExtentCursorPosition)
        ? removingOneCharacter()
        : anotherOperations();

    petsMentioned.forEach((mention) {
      if (oldExtentCursorPosition <= mention.initPositionInAt) {
        mention.setPosition(numberOfCharactersChanged);
      }
    });
  }

  int removingOneCharacter() {
    textControllerWithMentions = textControllerWithMentions.replaceRange(
      oldBaseCursorPosition - 1,
      oldBaseCursorPosition,
      "",
    );

    petsMentioned.removeWhere(
      (Mention mention) {
        bool cursorBaseIsToTheRightOfACharacterMention =
            mention.finalPositionInAt >= oldBaseCursorPosition &&
                oldBaseCursorPosition > mention.initPositionInAt;

        return cursorBaseIsToTheRightOfACharacterMention;
      },
    );

    return -1;
  }

  int anotherOperations() {
    String charactersAdded =
        textController.text.substring(oldBaseCursorPosition, newBaseCursorPosition);

    textControllerWithMentions = textControllerWithMentions.replaceRange(
      oldBaseCursorPosition,
      oldExtentCursorPosition,
      charactersAdded,
    );

    petsMentioned.removeWhere(
      (Mention mention) {
        bool cursorBaseIsInsideMention = mention.finalPositionInAt > oldBaseCursorPosition &&
            oldBaseCursorPosition > mention.initPositionInAt;
        bool cursorExtentIsInsideMention = mention.finalPositionInAt > oldExtentCursorPosition &&
            oldExtentCursorPosition > mention.initPositionInAt;
        bool mentionIsBetweenCursorBaseAndExtend =
            mention.finalPositionInAt <= oldExtentCursorPosition &&
                oldBaseCursorPosition <= mention.initPositionInAt;

        return cursorBaseIsInsideMention ||
            cursorExtentIsInsideMention ||
            mentionIsBetweenCursorBaseAndExtend;
      },
    );

    return (oldBaseCursorPosition - oldExtentCursorPosition + charactersAdded.length);
  }

  replaceMentionAndSave(Pet pet) {
    final String newText = textController.text.replaceRange(
      mentionPosition + 1,
      mentionPosition + 1 + petToMention.length,
      pet.name,
    );

    oldBaseCursorPosition = mentionPosition + 1;
    oldExtentCursorPosition = mentionPosition + 1 + petToMention.length;

    textController.value = TextEditingValue(
      text: newText,
      selection: TextSelection.fromPosition(
        TextPosition(offset: mentionPosition + 1 + pet.name.length),
      ),
    );

    Mention mention = Mention(
      petName: pet.name,
      petId: pet.id,
      initPositionInAt: mentionPosition,
      finalPositionInAt: mentionPosition + 1 + pet.name.length,
    );

    petsMentioned.add(mention);

    dealWithModifications();
    showPetsList();
  }

  String textControllerWithMentionsToTag() {
    String textControllerWithMentionsInTag = textControllerWithMentions;

    int newLength = 0;

    petsMentioned.forEach((Mention mention) {
      String mentionWithTag = "<PTK-P>${mention.petId}|${mention.petName}</PTK-P>";

      textControllerWithMentionsInTag = textControllerWithMentionsInTag.replaceRange(
        mention.initPositionInAt + newLength,
        mention.finalPositionInAt + newLength,
        mentionWithTag,
      );

      newLength = newLength -
          (mention.finalPositionInAt - mention.initPositionInAt) +
          mentionWithTag.length;
    });

    return textControllerWithMentionsInTag;
  }
}
