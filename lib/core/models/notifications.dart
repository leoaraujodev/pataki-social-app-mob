import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

mixin Notifications {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static Future<void> initializeNotifications() async {
    final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    const initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    final initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification: (int id, String title, String body, String payload) async {},
    );
    final initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {});
  }

  Future<void> scheduleVaccineNotification({
    int id,
    String petName,
    String vaccineName,
    DateTime notificationDate,
  }) async {
    final int hour = 9;

    final dateTimeNotification = DateTime(
      notificationDate.year,
      notificationDate.month,
      notificationDate.day,
      hour,
    );

    if (!dateTimeNotification.isAfter(DateTime.now())) {
      return;
    }

    final scheduledNotificationDateTime = tz.TZDateTime.from(dateTimeNotification, tz.local);

    // The 1 before the $id identifies that this is a vaccine notification
    final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Channel for vaccine id 1$id',
      'Channel for $vaccineName',
      'Channel for vaccine notification',
      importance: Importance.max,
      priority: Priority.high,
    );

    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
    );

    // The 1 before the $id identifies that this is a vaccine notification
    await flutterLocalNotificationsPlugin.zonedSchedule(
      int.tryParse("1$id"),
      "Dia de Vacina",
      "$petName receberá a vacina $vaccineName hoje",
      scheduledNotificationDateTime,
      platformChannelSpecifics,
      payload: 'item $id',
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  Future scheduleReminderDailyNotification({
    int id,
    String title,
    String reminderName,
    String time,
  }) async {
    // The 2 before the $id identifies hat this is a reminder notification
    // The 0 is to specify that this is a daily notification
    final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Channel for reminder id 20$id',
      'Channel for $reminderName',
      'Channel for reminder notification',
      importance: Importance.max,
      priority: Priority.high,
    );

    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
    );

    // The 2 before the $id identifies hat this is a reminder notification
    // The 0 is to specify that this is a daily notification
    await flutterLocalNotificationsPlugin.zonedSchedule(
      int.tryParse("20$id"),
      "$title",
      "$reminderName",
      _nextInstanceOfHourAndMinute(time: time),
      platformChannelSpecifics,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
      matchDateTimeComponents: DateTimeComponents.time,
    );
  }

  Future scheduleReminderNotification({
    int id,
    String title,
    String reminderName,
    int weekday,
    String time,
  }) async {
    // The 2 before the $id identifies hat this is a reminder notification
    // The weekday is to specify from which weekday is the notification
    final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Channel for reminder id 2$weekday$id',
      'Channel for $reminderName',
      'Channel for reminder notification',
      importance: Importance.max,
      priority: Priority.high,
    );

    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
    );

    // The 2 before the $id identifies that this is a reminder notification
    // The weekday is to specify from which weekday is the notification
    await flutterLocalNotificationsPlugin.zonedSchedule(
      int.tryParse("2$weekday$id"),
      "$title",
      "$reminderName",
      _nextInstanceOfWeekday(weekday: weekday, time: time),
      platformChannelSpecifics,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
      matchDateTimeComponents: DateTimeComponents.dayOfWeekAndTime,
    );
  }

  tz.TZDateTime _nextInstanceOfHourAndMinute({String time}) {
    final hour = int.tryParse(time.substring(0, 2));
    final minute = int.tryParse(time.substring(3, 5));

    final now = DateTime.now();
    final tz.TZDateTime tzNow = tz.TZDateTime.now(tz.local);
    final hourGMT = now.hour - tzNow.hour;
    tz.TZDateTime scheduledDate = tz.TZDateTime(
      tz.local,
      tzNow.year,
      tzNow.month,
      tzNow.day,
      hour - hourGMT,
      minute,
    );

    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  tz.TZDateTime _nextInstanceOfWeekday({int weekday, String time}) {
    tz.TZDateTime scheduledDate = _nextInstanceOfHourAndMinute(time: time);
    while (scheduledDate.weekday != weekday) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }

  Future checkPendingNotifications() async {
    final List<PendingNotificationRequest> pendingNotificationRequests =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return pendingNotificationRequests;
  }

  Future<void> cancelNotification(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> cancelDailyOrWeeklyNotifications({Reminder reminder, bool allDaysTrue}) async {
    // The first digit before the $id identifies that this is a reminder notification
    // The second digit is to specify from which weekday is the notification
    (reminder.onSun == true)
        ? await cancelNotification(int.tryParse("27${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onMon == true)
        ? await cancelNotification(int.tryParse("21${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onTue == true)
        ? await cancelNotification(int.tryParse("22${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onWed == true)
        ? await cancelNotification(int.tryParse("23${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onThu == true)
        ? await cancelNotification(int.tryParse("24${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onFri == true)
        ? await cancelNotification(int.tryParse("25${reminder.id}"))
        : allDaysTrue = false;
    (reminder.onSat == true)
        ? await cancelNotification(int.tryParse("26${reminder.id}"))
        : allDaysTrue = false;

    if (allDaysTrue) {
      // The first digit is to identify that is a reminder notification
      // The digit that identifies a daily notification is 0
      await cancelNotification(int.tryParse("20${reminder.id}"));
    }
  }

  Future<void> cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  notificationDays(Reminder reminder) {
    final List daysList = [];
    bool allDaysTrue = true;

    (reminder.onSun == true) ? daysList.add("Dom") : allDaysTrue = false;
    (reminder.onMon == true) ? daysList.add("Seg") : allDaysTrue = false;
    (reminder.onTue == true) ? daysList.add("Ter") : allDaysTrue = false;
    (reminder.onWed == true) ? daysList.add("Qua") : allDaysTrue = false;
    (reminder.onThu == true) ? daysList.add("Qui") : allDaysTrue = false;
    (reminder.onFri == true) ? daysList.add("Sex") : allDaysTrue = false;
    (reminder.onSat == true) ? daysList.add("Sáb") : allDaysTrue = false;

    if (allDaysTrue) {
      return "Todos dias";
    }
    return daysList.join(' / ');
  }
}
