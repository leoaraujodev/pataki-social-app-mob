class PatakiException implements Exception {

  final String message;

  PatakiException(this.message);

}