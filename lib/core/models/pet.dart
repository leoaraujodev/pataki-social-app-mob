import 'package:pataki/core/models/breed.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Pet extends PatakiResponse {
  Pet({
    this.id,
    this.name,
    this.size,
    this.weight,
    this.breedId,
    this.birthDate,
    this.createdAt,
    this.updatedAt,
    this.imagePath,
    this.followed,
    this.tutor,
    this.breed,
    this.totFollowers,
  });

  final int id;
  final String name;
  final String size;
  final String weight;
  final int breedId;
  final DateTime birthDate;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String imagePath;
  final bool followed;
  final UserModel tutor;
  final Breed breed;
  final int totFollowers;

  factory Pet.fromJson(Map<String, dynamic> json) => Pet(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        size: json["size"] == null ? null : json["size"],
        weight: json["weight"] == null ? null : json["weight"],
        breedId: json["breed_id"] == null ? null : IntParser(json["breed_id"]).toInt(),
        birthDate: json["birth_date"] == null ? null : DateTime.parse(json["birth_date"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        imagePath: json["image_path"] == null ? null : json["image_path"],
        followed: json["followed"] == null ? null : json["followed"],
        tutor: json["tutor"] == null ? null : UserModel.fromJson(json["tutor"]),
        breed: json["breed"] == null ? null : Breed.fromJson(json["breed"]),
        totFollowers:
            json["tot_followers"] == null ? null : IntParser(json["tot_followers"]).toInt(),
      );
}

class IntParser {
  final dynamic value;
  IntParser(this.value);

  int toInt() {
    if (value.runtimeType == int) {
      return value;
    }
    if (value.runtimeType == String) {
      return int.tryParse(value);
    }
    return null;
  }
}
