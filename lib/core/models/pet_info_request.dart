import 'package:pataki/core/server/pataki_request.dart';

class PetInfoRequest extends PatakiRequest {
  final int petId;

  PetInfoRequest({this.petId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
