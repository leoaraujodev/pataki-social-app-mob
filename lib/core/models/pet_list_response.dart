import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_response.dart';

class PetListResponse extends PatakiResponse {
  final List<Pet> pets;

  PetListResponse({
    this.pets,
  });

  factory PetListResponse.fromJson(List<dynamic> json) {
    return PetListResponse(
        pets:  List<Pet>.from(json.map((x) => Pet.fromJson(x)))
    );
  }
}