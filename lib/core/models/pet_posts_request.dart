import 'package:pataki/core/server/pataki_request.dart';

class PetPostsRequest extends PatakiRequest {
  final int petId;
  final int lastId;

  PetPostsRequest({this.petId, this.lastId});

  Map<String, dynamic> toJson() {
    return lastId != null ? {'last_id': lastId.toString()} : {};
  }
}
