import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/server/pataki_response.dart';

class PetPostsResponse extends PatakiResponse {
  final List<TimelinePost> posts;

  PetPostsResponse({this.posts});

  factory PetPostsResponse.fromJson(Map<String, dynamic> json) {
    return PetPostsResponse(
      posts: List<TimelinePost>.from(json["data"].map((x) => TimelinePost.fromJson(x))),
    );
  }
}
