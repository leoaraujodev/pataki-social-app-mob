import 'package:pataki/core/server/pataki_request.dart';

class PetTypeInfoRequest extends PatakiRequest {
  final int petTypeId;

  PetTypeInfoRequest({this.petTypeId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
