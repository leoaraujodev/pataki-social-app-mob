import 'package:pataki/core/server/pataki_response.dart';

class PetType extends PatakiResponse {
  final int id;
  final String name;

  PetType({this.id, this.name});

  factory PetType.fromJson(Map<String, dynamic> json) {
    return PetType(
        id: json['id'],
        name: json['name']
    );
  }
}