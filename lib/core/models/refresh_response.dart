import 'package:pataki/core/server/pataki_response.dart';

class RefreshResponse extends PatakiResponse {
  final String token;

  RefreshResponse({this.token});

  factory RefreshResponse.fromJson(Map<String, dynamic> json) {
    return RefreshResponse(
      token: json['access_token'],
    );
  }
}
