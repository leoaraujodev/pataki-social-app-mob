import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Reminder extends PatakiResponse {
  Reminder({
    this.id,
    this.name,
    this.time,
    this.onSun,
    this.onMon,
    this.onTue,
    this.onWed,
    this.onThu,
    this.onFri,
    this.onSat,
    this.pet,
  });

  final int id;
  final String name;
  final String time;
  final bool onSun;
  final bool onMon;
  final bool onTue;
  final bool onWed;
  final bool onThu;
  final bool onFri;
  final bool onSat;
  final Pet pet;

  factory Reminder.fromJson(Map<String, dynamic> json) {
    return Reminder(
      id: json["id"] == null ? null : json["id"],
      name: json["name"] == null ? null : json["name"],
      time: json["time"] == null ? null : json["time"],
      onSun: json["on_sun"] == null ? null : BoolParser(json["on_sun"]).toBool(),
      onMon: json["on_mon"] == null ? null : BoolParser(json["on_mon"]).toBool(),
      onTue: json["on_tue"] == null ? null : BoolParser(json["on_tue"]).toBool(),
      onWed: json["on_wed"] == null ? null : BoolParser(json["on_wed"]).toBool(),
      onThu: json["on_thu"] == null ? null : BoolParser(json["on_thu"]).toBool(),
      onFri: json["on_fri"] == null ? null : BoolParser(json["on_fri"]).toBool(),
      onSat: json["on_sat"] == null ? null : BoolParser(json["on_sat"]).toBool(),
      pet: json["pet"] == null ? null : Pet.fromJson(json["pet"]),
    );
  }
}

class BoolParser {
  final dynamic value;
  BoolParser(this.value);

  bool toBool() {
    if (value.runtimeType == bool) {
      return value;
    }
    if (value.runtimeType == String) {
      return bool.fromEnvironment(value);
    }
    return null;
  }
}
