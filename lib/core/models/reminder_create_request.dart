import 'package:pataki/core/server/pataki_request.dart';

class ReminderCreateRequest extends PatakiRequest {
  final String name;
  final String time;
  final bool onSun;
  final bool onMon;
  final bool onTue;
  final bool onWed;
  final bool onThu;
  final bool onFri;
  final bool onSat;
  final int petId;
  final int trickId;

  ReminderCreateRequest({
    this.name,
    this.time,
    this.onSun,
    this.onMon,
    this.onTue,
    this.onWed,
    this.onThu,
    this.onFri,
    this.onSat,
    this.petId,
    this.trickId,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name.toString(),
      'time': time.toString(),
      'on_sun': onSun.toString(),
      'on_mon': onMon.toString(),
      'on_tue': onTue.toString(),
      'on_wed': onWed.toString(),
      'on_thu': onThu.toString(),
      'on_fri': onFri.toString(),
      'on_sat': onSat.toString(),
    };
  }
}
