import 'package:pataki/core/server/pataki_request.dart';

class ReminderDeleteRequest extends PatakiRequest {
  final int reminderId;
  final int petId;
  final int trickId;

  ReminderDeleteRequest({
    this.reminderId,
    this.petId,
    this.trickId,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': reminderId.toString(),
    };
  }
}
