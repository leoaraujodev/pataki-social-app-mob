import 'package:pataki/core/server/pataki_request.dart';

class RemindersRequest extends PatakiRequest {
  final int petId;

  RemindersRequest({this.petId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
