import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/server/pataki_response.dart';

class RemindersResponse extends PatakiResponse {
  final List<Reminder> reminders;

  RemindersResponse({this.reminders});

  factory RemindersResponse.fromJson(List<dynamic> json) {
    return RemindersResponse(
      reminders: List<Reminder>.from(json.map((x) => Reminder.fromJson(x))),
    );
  }
}
