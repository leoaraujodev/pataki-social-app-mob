enum ReportReasons {
  inappropriate,
  plagiarism,
  nudity,
  spam,
  others,
}

extension ReportReasonsMethods on ReportReasons {
  String toRequestString() {
    switch (this) {
      case ReportReasons.inappropriate:
        return "inappropriate";
      case ReportReasons.plagiarism:
        return "plagiarism";
      case ReportReasons.nudity:
        return "nudity";
      case ReportReasons.spam:
        return "spam";
      case ReportReasons.others:
        return "others";
      default:
        throw "Unknown ReportReason";
    }
  }

  String toDisplayString() {
    switch (this) {
      case ReportReasons.inappropriate:
        return "Impróprio";
      case ReportReasons.plagiarism:
        return "Plágio";
      case ReportReasons.nudity:
        return "Nudez";
      case ReportReasons.spam:
        return "Spam";
      case ReportReasons.others:
        return "Outros";
      default:
        throw "Unknown ReportReason";
    }
  }
}
