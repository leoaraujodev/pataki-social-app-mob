import 'package:pataki/core/models/report_reasons.dart';
import 'package:pataki/core/server/pataki_request.dart';

class ReportTimelinePostRequest extends PatakiRequest {
  final int id;
  final String comment;
  final ReportReasons reason;

  ReportTimelinePostRequest({
    this.id,
    this.comment,
    this.reason,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id.toString(),
      'comment': comment,
      'reason': reason.toRequestString(),
    };
  }
}
