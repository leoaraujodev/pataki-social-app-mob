import 'package:pataki/core/server/pataki_request.dart';

enum SearchFilters {
  all,
  tutors,
  pets,
  groups,
}

class SearchRequest extends PatakiRequest {
  final int page;
  final String term;
  final SearchFilters filter;

  SearchRequest({
    this.page,
    this.term,
    this.filter,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'page': page,
      'term': term,
    };

    if (filter == SearchFilters.pets) {
      json.addAll({'filter': 'pets'});
    } else if (filter == SearchFilters.tutors) {
      json.addAll({'filter': 'tutors'});
    } else if (filter == SearchFilters.groups) {
      json.addAll({'group': 'groups'});
    }

    return json;
  }
}
