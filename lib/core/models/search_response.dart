import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class SearchResponse extends PatakiResponse {
  SearchResponse({
    this.currentPage,
    this.perPage,
    this.hasNextPage,
    this.tutors,
    this.pets,
    this.groups,
  });

  final int currentPage;
  final int perPage;
  final bool hasNextPage;
  final List<UserModel> tutors;
  final List<Pet> pets;
  final List<Group> groups;

  factory SearchResponse.fromJson(Map<String, dynamic> json) => SearchResponse(
        currentPage: json["current_page"] == null ? null : json["current_page"],
        perPage: json["per_page"] == null ? null : json["per_page"],
        hasNextPage: json["has_next_page"] == null ? null : json["has_next_page"],
        tutors: json["tutors"] == null
            ? null
            : List<UserModel>.from(json["tutors"].map((x) => UserModel.fromJson(x))),
        pets:
            json["pets"] == null ? null : List<Pet>.from(json["pets"].map((x) => Pet.fromJson(x))),
        groups: json["groups"] == null
            ? null
            : List<Group>.from(json["groups"].map((x) => Group.fromJson(x))),
      );
}
