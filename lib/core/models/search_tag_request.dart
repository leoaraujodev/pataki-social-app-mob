import 'package:pataki/core/server/pataki_request.dart';

class SearchTagRequest extends PatakiRequest {
  final String tag;

  SearchTagRequest({
    this.tag,
  });

  Map<String, dynamic> toJson() {
    return {'tag': tag};
  }
}
