import 'package:pataki/core/server/pataki_request.dart';

class SetNotifyTokenRequest extends PatakiRequest {
  final String onesignalToken;

  SetNotifyTokenRequest({this.onesignalToken});

  Map<String, dynamic> toJson() {
    return {
      '_method': 'put',
      'onesiginal_token': onesignalToken,
    };
  }
}
