import 'package:pataki/core/server/pataki_request.dart';

class SingleTimelineRequest extends PatakiRequest {
  final int id;

  SingleTimelineRequest({this.id});

  Map<String, dynamic> toJson() => {};
}
