import 'package:pataki/core/models/media.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Step extends PatakiResponse {
  Step({
    this.id,
    this.desc,
    this.file,
    this.media,
  });

  final int id;
  final String desc;
  final String file;
  final Media media;

  factory Step.fromJson(Map<String, dynamic> json) {
    return Step(
      id: json["id"] == null ? null : json["id"],
      desc: json["desc"] == null ? null : json["desc"],
      file: json["file"] == null ? null : json["file"],
      media: Media.fromJson(json),
    );
  }
}
