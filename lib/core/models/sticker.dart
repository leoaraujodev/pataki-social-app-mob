import 'package:pataki/core/server/pataki_response.dart';

class Sticker extends PatakiResponse {
  Sticker({
    this.id,
    this.path,
  });

  final int id;
  final String path;

  factory Sticker.fromJson(Map<String, dynamic> json) => Sticker(
    id: json["id"] == null ? null : json["id"],
    path: json["path"] == null ? null : json["path"],
  );
}
