import 'package:pataki/core/server/pataki_response.dart';

class SuccessMessageResponse extends PatakiResponse {
  final String message;

  SuccessMessageResponse({this.message});

  factory SuccessMessageResponse.fromJson(Map<String, dynamic> json) {
    return SuccessMessageResponse(
      message: json['message'],
    );
  }
}