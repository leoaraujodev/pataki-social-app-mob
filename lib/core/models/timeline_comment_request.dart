import 'package:pataki/core/server/pataki_request.dart';

class TimelineCommentRequest extends PatakiRequest {
  final int timelineId;
  final int page;

  TimelineCommentRequest({
    this.timelineId,
    this.page,
  });

  Map<String, dynamic> toJson() {
    return {'page': page};
  }
}
