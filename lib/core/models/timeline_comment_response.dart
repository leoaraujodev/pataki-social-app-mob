import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class TimelineCommentResponse extends PatakiResponse {
  TimelineCommentResponse({
    this.currentPage,
    this.data,
    this.from,
    this.lastPage,
    this.perPage,
    this.to,
    this.total,
  });

  final int currentPage;
  final List<CommentModel> data;
  final int from;
  final int lastPage;
  final int perPage;
  final int to;
  final int total;

  factory TimelineCommentResponse.fromJson(Map<String, dynamic> json) => TimelineCommentResponse(
        currentPage: json["current_page"] == null ? null : json["current_page"],
        data: json["data"] == null
            ? null
            : List<CommentModel>.from(json["data"].map((x) => CommentModel.fromJson(x))),
        from: json["from"] == null ? null : json["from"],
        lastPage: json["last_page"] == null ? null : json["last_page"],
        perPage: json["per_page"] == null ? null : json["per_page"],
        to: json["to"] == null ? null : json["to"],
        total: json["total"] == null ? null : json["total"],
      );
}
