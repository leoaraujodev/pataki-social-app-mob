import 'package:flutter/material.dart';
import 'package:pataki/core/models/like_model.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class TimelinePost extends PatakiResponse {
  TimelinePost({
    this.id,
    this.desc,
    this.totLikes,
    this.totComment,
    this.createdAt,
    this.updatedAt,
    this.imagePath,
    this.liked,
    this.lastComment,
    this.user,
    this.pet,
    this.likes,
  });

  final int id;
  final String desc;
  final int totLikes;
  final int totComment;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String imagePath;
  final bool liked;
  final dynamic lastComment;
  final UserModel user;
  final Pet pet;
  final List<LikeModel> likes;

  TimelinePost copyWith({
    int id,
    String desc,
    int totLikes,
    int totComment,
    DateTime createdAt,
    DateTime updatedAt,
    @required String imagePath,
    bool liked,
    dynamic lastComment,
    UserModel user,
    Pet pet,
    List<LikeModel> likes,
  }) =>
      TimelinePost(
        id: id ?? this.id,
        desc: desc ?? this.desc,
        totLikes: totLikes ?? this.totLikes,
        totComment: totComment ?? this.totComment,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        imagePath: imagePath,
        liked: liked ?? this.liked,
        lastComment: lastComment ?? this.lastComment,
        user: user ?? this.user,
        pet: pet ?? this.pet,
        likes: likes ?? this.likes,
      );

  factory TimelinePost.fromJson(Map<String, dynamic> json) => TimelinePost(
        id: json["id"],
        desc: json["desc"],
        totLikes: json["tot_likes"],
        totComment: json["tot_comment"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        imagePath: json["image_path"],
        liked: json["liked"],
        lastComment: json["last_comment"],
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        pet: json["pet"] == null ? null : Pet.fromJson(json["pet"]),
        likes: json["likes"] == null
            ? null
            : List<LikeModel>.from(json["likes"].map((x) => LikeModel.fromJson(x))),
      );
}
