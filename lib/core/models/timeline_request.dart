import 'package:pataki/core/server/pataki_request.dart';

class TimelineRequest extends PatakiRequest {
  final int lastId;

  TimelineRequest({this.lastId});

  Map<String, dynamic> toJson() {
    return lastId != null ? {'last_id': lastId.toString()} : {};
  }
}
