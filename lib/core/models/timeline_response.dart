import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/server/pataki_response.dart';

class TimelineResponse extends PatakiResponse {
  final List<TimelinePost> posts;

  TimelineResponse({this.posts});

  factory TimelineResponse.fromJson(Map<String, dynamic> json) {
    return TimelineResponse(
      posts: List<TimelinePost>.from(json["data"].map((x) => TimelinePost.fromJson(x))),
    );
  }
}
