import 'package:pataki/core/server/pataki_response.dart';

class Tip extends PatakiResponse {
  Tip({
    this.id,
    this.tip,
  });

  final int id;
  final String tip;

  factory Tip.fromJson(Map<String, dynamic> json) => Tip(
        id: json["id"] == null ? null : json["id"],
        tip: json["tip"] == null ? null : json["tip"],
      );
}
