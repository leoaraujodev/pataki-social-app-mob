import 'package:pataki/core/models/media.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/step.dart';
import 'package:pataki/core/models/tip.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Trick extends PatakiResponse {
  Trick({
    this.id,
    this.title,
    this.desc,
    this.difficulty,
    this.file,
    this.media,
    this.state,
    this.tips,
    this.steps,
    this.notifications,
  });

  final int id;
  final String title;
  final String desc;
  final int difficulty;
  final String file;
  String state;
  final List<Tip> tips;
  final List<Step> steps;
  final List<Reminder> notifications;
  final Media media;

  factory Trick.fromJson(Map<String, dynamic> json) => Trick(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        desc: json["desc"] == null ? null : json["desc"],
        difficulty: json["difficulty"] == null ? null : json["difficulty"],
        file: json["file"] == null ? null : json["file"],
        media: Media.fromJson(json),
        state: json["state"] == null ? null : json["state"],
        tips:
            json["tips"] == null ? null : List<Tip>.from(json["tips"].map((x) => Tip.fromJson(x))),
        steps: json["steps"] == null
            ? null
            : List<Step>.from(json["steps"].map((x) => Step.fromJson(x))),
        notifications: json["notifications"] == null
            ? null
            : List<Reminder>.from(json["notifications"].map((x) => Reminder.fromJson(x))),
      );
}
