import 'package:pataki/core/server/pataki_request.dart';

class TrickDetailsRequest extends PatakiRequest {
  final int trickId;

  TrickDetailsRequest({this.trickId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
