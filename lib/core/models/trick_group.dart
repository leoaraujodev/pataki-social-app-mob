import 'package:pataki/core/server/pataki_response.dart';

class TrickGroup extends PatakiResponse {
  TrickGroup({
    this.id,
    this.name,
    this.desc,
  });

  final int id;
  final String name;
  final String desc;

  factory TrickGroup.fromJson(Map<String, dynamic> json) => TrickGroup(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        desc: json["desc"] == null ? null : json["desc"],
      );
}
