import 'package:pataki/core/models/trick_group.dart';
import 'package:pataki/core/server/pataki_response.dart';

class TrickGroupsListResponse extends PatakiResponse {
  final List<TrickGroup> trickGroups;

  TrickGroupsListResponse({
    this.trickGroups,
  });

  factory TrickGroupsListResponse.fromJson(List<dynamic> json) {
    return TrickGroupsListResponse(
        trickGroups: List<TrickGroup>.from(json.map((x) => TrickGroup.fromJson(x))));
  }
}
