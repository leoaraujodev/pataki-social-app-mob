import 'package:pataki/core/server/pataki_request.dart';

class TrickStepStateRequest extends PatakiRequest {
  final int trickId;
  final String state;

  TrickStepStateRequest({this.trickId, this.state});

  Map<String, dynamic> toJson() {
    return {
      'trick_id': trickId.toString(),
      'state': state,
    };
  }
}
