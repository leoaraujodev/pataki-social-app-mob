import 'package:pataki/core/server/pataki_response.dart';

class TrickStepStateResponse extends PatakiResponse {
  final int userId;
  final int trickId;
  final String state;
  final int id;

  TrickStepStateResponse({
    this.userId,
    this.trickId,
    this.state,
    this.id,
  });

  factory TrickStepStateResponse.fromJson(Map<String, dynamic> json) {
    return TrickStepStateResponse(
      userId: json["user_id"] == null ? null : json["user_id"],
      trickId: json["trick_id"] == null ? null : json["trick_id"],
      state: json["state"] == null ? null : json["state"],
      id: json["id"] == null ? null : json["id"],
    );
  }
}
