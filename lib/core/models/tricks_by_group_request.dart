import 'package:pataki/core/server/pataki_request.dart';

class TricksByGroupRequest extends PatakiRequest {
  final int groupId;

  TricksByGroupRequest({this.groupId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
