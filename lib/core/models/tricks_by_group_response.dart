import 'package:pataki/core/models/trick.dart';
import 'package:pataki/core/server/pataki_response.dart';

class TricksByGroupResponse extends PatakiResponse {
  final List<Trick> tricks;

  TricksByGroupResponse({
    this.tricks,
  });

  factory TricksByGroupResponse.fromJson(List<dynamic> json) {
    return TricksByGroupResponse(tricks: List<Trick>.from(json.map((x) => Trick.fromJson(x))));
  }
}
