import 'package:pataki/core/server/pataki_request.dart';

class UserInfoRequest extends PatakiRequest {
  final int userId;

  UserInfoRequest({this.userId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
