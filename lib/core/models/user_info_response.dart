import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class UserInfoResponse extends PatakiResponse {
  UserInfoResponse({
    this.user,
  });

  final UserModel user;

  factory UserInfoResponse.fromJson(Map<String, dynamic> json) => UserInfoResponse(
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
      );
}
