import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class UserListResponse extends PatakiResponse {
  final List<UserModel> users;

  UserListResponse({
    this.users,
  });

  factory UserListResponse.fromJson(List<dynamic> json) {
    return UserListResponse(users: List<UserModel>.from(json.map((x) => UserModel.fromJson(x))));
  }
}
