import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_response.dart';

class UserModel extends PatakiResponse {
  UserModel({
    this.id,
    this.name,
    this.email,
    this.createdAt,
    this.updatedAt,
    this.desc,
    this.isAdmin,
    this.uuid,
    this.onesiginalToken,
    this.phone,
    this.followed,
    this.imagePath,
    this.pets,
    this.following,
    this.groups,
    this.courses,
  });

  final int id;
  final String name;
  final String email;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String desc;
  final bool isAdmin;
  final String uuid;
  final String onesiginalToken;
  final String phone;
  final bool followed;
  final String imagePath;
  final List<Pet> pets;
  final int following;
  final int groups;
  final int courses;

  UserModel copyWith({
    int id,
    String name,
    String email,
    DateTime createdAt,
    DateTime updatedAt,
    String desc,
    bool isAdmin,
    String uuid,
    String onesiginalToken,
    String phone,
    bool followed,
    String imagePath,
    List<Pet> pets,
    int following,
    int groups,
    int courses,
  }) =>
      UserModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        desc: desc ?? this.desc,
        isAdmin: isAdmin ?? this.isAdmin,
        uuid: uuid ?? this.uuid,
        onesiginalToken: onesiginalToken ?? this.onesiginalToken,
        phone: phone ?? this.phone,
        followed: followed ?? this.followed,
        imagePath: imagePath ?? this.imagePath,
        pets: pets ?? this.pets,
        following: following ?? this.following,
        groups: groups ?? this.groups,
        courses: courses ?? this.courses,
      );

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        desc: json["desc"] == null ? null : json["desc"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        onesiginalToken: json["onesiginal_token"] == null ? null : json["onesiginal_token"],
        phone: json["phone"] == null ? null : json["phone"],
        followed: json["followed"] == null ? null : json["followed"],
        imagePath: json["image_path"] == null ? null : json["image_path"],
        pets:
            json["pets"] == null ? null : List<Pet>.from(json["pets"].map((x) => Pet.fromJson(x))),
        following: json["following"] == null ? null : json["following"],
        groups: json["groups"] == null ? null : json["groups"],
        courses: json["courses"] == null ? null : json["courses"],
      );
}
