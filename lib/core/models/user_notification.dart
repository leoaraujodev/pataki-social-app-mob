import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class UserNotification extends PatakiResponse {
  UserNotification({
    this.id,
    this.action,
    this.message,
    this.createdAt,
    this.user,
    this.pet,
    this.timeline,
  });

  final int id;
  final String message;
  final String action;
  final DateTime createdAt;
  final UserModel user;
  final Pet pet;
  final TimelinePost timeline;

  factory UserNotification.fromJson(Map<String, dynamic> json) => UserNotification(
        id: json["id"] == null ? null : json["id"],
        message: json["message"] == null ? null : json["message"],
        action: json["action"] == null ? null : json["action"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        pet: json["pet"] == null ? null : Pet.fromJson(json["pet"]),
        timeline: json["timeline"] == null ? null : TimelinePost.fromJson(json["timeline"]),
      );
}
