import 'package:pataki/core/server/pataki_request.dart';

class UserPostsRequest extends PatakiRequest {
  final int userId;
  final int lastId;

  UserPostsRequest({
    this.userId,
    this.lastId,
  });

  Map<String, dynamic> toJson() {
    return lastId != null ? {'last_id': lastId.toString()} : {};
  }
}
