import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/server/pataki_response.dart';

class UserResponse extends PatakiResponse {
  final UserModel user;

  UserResponse({this.user});

  factory UserResponse.fromJson(Map<String, dynamic> json) {
    return UserResponse(
      user: UserModel.fromJson(json['user']),
    );
  }
}
