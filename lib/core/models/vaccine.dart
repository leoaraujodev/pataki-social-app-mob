import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/server/pataki_response.dart';

class Vaccine extends PatakiResponse {
  Vaccine({
    this.id,
    this.name,
    this.date,
    this.pet,
  });

  final int id;
  final String name;
  final DateTime date;
  final Pet pet;

  factory Vaccine.fromJson(Map<String, dynamic> json) => Vaccine(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        pet: json["pet"] == null ? null : Pet.fromJson(json["pet"]),
      );
}
