import 'package:pataki/core/server/pataki_request.dart';

class VaccineCreateRequest extends PatakiRequest {
  final String name;
  final DateTime date;
  final int petId;

  VaccineCreateRequest({
    this.name,
    this.date,
    this.petId,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name.toString(),
      'date': date.toString(),
    };
  }
}
