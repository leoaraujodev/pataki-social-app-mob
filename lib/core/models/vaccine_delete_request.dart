import 'package:pataki/core/server/pataki_request.dart';

class VaccineDeleteRequest extends PatakiRequest {
  final int vaccineId;
  final int petId;

  VaccineDeleteRequest({
    this.vaccineId,
    this.petId,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': vaccineId.toString(),
    };
  }
}
