import 'package:pataki/core/server/pataki_request.dart';

class VaccinesRequest extends PatakiRequest {
  final int petId;

  VaccinesRequest({this.petId});

  Map<String, dynamic> toJson() {
    return {};
  }
}
