import 'package:pataki/core/models/vaccine.dart';
import 'package:pataki/core/server/pataki_response.dart';

class VaccinesResponse extends PatakiResponse {
  final List<Vaccine> vaccines;

  VaccinesResponse({this.vaccines});

  factory VaccinesResponse.fromJson(List<dynamic> json) {
    return VaccinesResponse(
      vaccines: List<Vaccine>.from(json.map((x) => Vaccine.fromJson(x))),
    );
  }
}
