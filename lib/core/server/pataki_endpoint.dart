import 'dart:async';
// ignore: implementation_imports
import 'package:http/src/multipart_file.dart';

import 'pataki_response.dart';
import 'pataki_request.dart';
import 'pataki_server.dart';

enum HttpMethod { POST, GET, PUT, DELETE, MULTIPART }

abstract class PatakiEndpoint<REQUEST extends PatakiRequest, RESPONSE extends PatakiResponse,
    RESPONSEFORMAT> {
  String path();
  HttpMethod method();
  PatakiServer<REQUEST, RESPONSE> server();
  Map<String, dynamic> requestBody();

  Future<RESPONSE> call() {
    return server().fetch();
  }

  RESPONSE responseModelFromJson(RESPONSEFORMAT json);

  Future<Iterable<MultipartFile>> multipartFiles() async {
    return null;
    // File image;
    // request.files.add(
    //     http.MultipartFile.fromBytes(
    //         'file',
    //         await image.readAsBytes(),
    //     contentType: MediaType('image', 'jpeg')));
  }
}
