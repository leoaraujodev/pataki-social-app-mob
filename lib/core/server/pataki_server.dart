import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pataki/core/interactor/get_token_interactor.dart';
import 'package:pataki/core/models/pataki_exception.dart';

import 'pataki_endpoint.dart';
import 'pataki_request.dart';
import 'pataki_response.dart';

class PatakiServer<REQUEST extends PatakiRequest, RESPONSE extends PatakiResponse> {
  String patakiUrl = "app.pataki.com.br";
  String apiUrl() => "https://$patakiUrl/api";
  PatakiEndpoint endpoint;

  PatakiServer(this.endpoint);

  Future<RESPONSE> fetch() async {
    return GetTokenInteractor().execute().then((token) {
      if (token != null) {
        return fetchWithHeader(header: {'Authorization': 'Bearer $token'});
      } else {
        return fetchWithHeader();
      }
    });
  }

  Future<RESPONSE> fetchWithHeader({Map<String, String> header}) async {
    switch (endpoint.method()) {
      case HttpMethod.POST:
        return post(header: header);
        break;
      case HttpMethod.GET:
        return get(header: header);
        break;
      case HttpMethod.PUT:
        return put(header: header);
        break;
      case HttpMethod.DELETE:
        return delete(header: header);
        break;
      case HttpMethod.MULTIPART:
        return multipart(header: header);
        break;
    }
    return null;
  }

  Future<RESPONSE> get({Map<String, String> header}) async {
    Map<String, String> parameters;
    if (endpoint.requestBody() != null) {
      parameters = Map.fromIterables(
          endpoint.requestBody().keys, endpoint.requestBody().values.map((e) => e.toString()));
    }
    var uri = Uri.https(patakiUrl, "/api${endpoint.path()}", parameters);
    var response = await http.get(uri, headers: header);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      final json = response.body == null || response.body == "" ? null : jsonDecode(response.body);
      return endpoint.responseModelFromJson(json);
    } else {
      var errorResult = jsonDecode(response.body);
      if (errorResult["message"] != null) {
        throw PatakiException(errorResult["message"]);
      }
      throw Exception('Failed to get');
    }
  }

  Future<RESPONSE> post({Map<String, String> header}) async {
    var response = await http.post("${apiUrl()}${endpoint.path()}",
        headers: header, body: endpoint.requestBody());

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return endpoint.responseModelFromJson(jsonDecode(response.body));
    } else {
      var errorResult = jsonDecode(response.body);
      if (errorResult["message"] != null) {
        throw PatakiException(errorResult["message"]);
      }
      throw Exception('Failed to post, response code = ${response.statusCode}');
    }
  }

  Future<RESPONSE> put({Map<String, String> header}) async {
    var response = await http.put("${apiUrl()}${endpoint.path()}", headers: header);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return endpoint.responseModelFromJson(jsonDecode(response.body));
    } else {
      var errorResult = jsonDecode(response.body);
      if (errorResult["message"] != null) {
        throw PatakiException(errorResult["message"]);
      }
      throw Exception('Failed to update');
    }
  }

  Future<RESPONSE> delete({Map<String, String> header}) async {
    var response = await http.delete("${apiUrl()}${endpoint.path()}", headers: header);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return endpoint.responseModelFromJson(jsonDecode(response.body));
    } else {
      var errorResult = jsonDecode(response.body);
      if (errorResult["message"] != null) {
        throw PatakiException(errorResult["message"]);
      }
      throw Exception('Failed to delete');
    }
  }

  Future<RESPONSE> multipart({Map<String, String> header}) async {
    var url = Uri.parse("${apiUrl()}${endpoint.path()}");
    var request = new http.MultipartRequest("POST", url);

    request.headers.addAll(header);

    request.fields.addAll(endpoint.requestBody().map((x, y) => MapEntry(x, y.toString())));

    var multipartFiles = await endpoint.multipartFiles();
    if (multipartFiles != null) {
      request.files.addAll(multipartFiles);
    }

    var response =
        await http.Response.fromStream(await request.send()).timeout(Duration(seconds: 30));

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return endpoint.responseModelFromJson(jsonDecode(response.body));
    } else {
      var errorResult = jsonDecode(response.body);
      if (errorResult["message"] != null) {
        throw PatakiException(errorResult["message"]);
      }
      throw Exception('Failed to post, response with status code: ${response.statusCode}');
    }
  }
}
