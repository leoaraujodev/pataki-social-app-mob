import 'package:get/get.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/notifications/notifications_controller.dart';
import 'package:pataki/screens/splash/splash_controller.dart';

class NotificationService extends GetxService {
  final _oneSignal = OneSignal.shared;

  @override
  void onInit() {
    _oneSignal.init(
      "f204f28d-5554-438f-9949-645cbfc1b9c8",
      iOSSettings: {OSiOSSettings.autoPrompt: true},
    );

    activateInFocusNotifications();

    _oneSignal.setNotificationOpenedHandler(_handleNotificationOpened);

    _oneSignal.setNotificationReceivedHandler(_handleNotificationReceived);

    super.onInit();
  }

  void _handleNotificationOpened(OSNotificationOpenedResult openedResult) {
    navigateToNotificationScreen();
  }

  void _handleNotificationReceived(OSNotification notification) {
    Get.find<NotificationsController>().getInitialNotifications();
  }

  Future<void> navigateToNotificationScreen() async {
    while (getSplashController() != null) {
      await Future.delayed(100.milliseconds);
    }
    try {
      Get.find<HomeController>().changePage(2);
      Get.find<NotificationsController>().getInitialNotifications();
    } catch (e) {}
  }

  SplashController getSplashController() {
    try {
      return Get.find<SplashController>();
    } catch (e) {
      return null;
    }
  }

  Future<String> getUserId() async {
    final status = await _oneSignal.getPermissionSubscriptionState();

    return status.subscriptionStatus.userId;
  }

  Future<void> activateInFocusNotifications() =>
      _oneSignal.setInFocusDisplayType(OSNotificationDisplayType.notification);

  Future<void> deactivateInFocusNotifications() =>
      _oneSignal.setInFocusDisplayType(OSNotificationDisplayType.none);
}
