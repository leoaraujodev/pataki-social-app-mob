import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pataki/core/models/deep_linking.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/services/notification_service.dart';
import 'package:pataki/screens/about/about_binding.dart';
import 'package:pataki/screens/about/about_screen.dart';
import 'package:pataki/screens/blocked_list/blocked_list_binding.dart';
import 'package:pataki/screens/blocked_list/blocked_list_screen.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_binding.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_screen.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_screen.dart';
import 'package:pataki/screens/comment/comment_binding.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/comment/edit_comment_binding.dart';
import 'package:pataki/screens/comment/edit_comment_screen.dart';
import 'package:pataki/screens/create_group/create_group_binding.dart';
import 'package:pataki/screens/create_group/create_group_screen.dart';
import 'package:pataki/screens/create_group_post/create_group_post_binding.dart';
import 'package:pataki/screens/create_group_post/create_group_post_screen.dart';
import 'package:pataki/screens/create_reminder/pet/create_pet_reminder_binding.dart';
import 'package:pataki/screens/create_reminder/pet/create_pet_reminder_screen.dart';
import 'package:pataki/screens/create_reminder/user/create_user_reminder_binding.dart';
import 'package:pataki/screens/create_reminder/user/create_user_reminder_screen.dart';
import 'package:pataki/screens/create_vaccine/create_vaccine_binding.dart';
import 'package:pataki/screens/create_vaccine/create_vaccine_screen.dart';
import 'package:pataki/screens/edit_group/edit_group_binding.dart';
import 'package:pataki/screens/edit_group/edit_group_screen.dart';
import 'package:pataki/screens/edit_pass/edit_pass_binding.dart';
import 'package:pataki/screens/edit_pass/edit_pass_screen.dart';
import 'package:pataki/screens/edit_pet/edit_pet_binding.dart';
import 'package:pataki/screens/edit_pet/edit_pet_screen.dart';
import 'package:pataki/screens/edit_user/edit_user_binding.dart';
import 'package:pataki/screens/edit_user/edit_user_screen.dart';
import 'package:pataki/screens/following/following_binding.dart';
import 'package:pataki/screens/following/following_screen.dart';
import 'package:pataki/screens/create_post/create_post_binding.dart';
import 'package:pataki/screens/create_post/create_post_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_binding.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/group/group_binding.dart';
import 'package:pataki/screens/group/group_screen.dart';
import 'package:pataki/screens/group_list/group_list_binding.dart';
import 'package:pataki/screens/group_list/group_list_screen.dart';
import 'package:pataki/screens/home/home_binding.dart';
import 'package:pataki/screens/home/home_screen.dart';
import 'package:pataki/screens/forgot_pass/forgot_pass_screen.dart';
import 'package:pataki/screens/member_list/member_list_binding.dart';
import 'package:pataki/screens/member_list/member_list_screen.dart';
import 'package:pataki/screens/likes/likes_binding.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/pet/pet_binding.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/pet_list/pet_list_binding.dart';
import 'package:pataki/screens/pet_list/pet_list_screen.dart';
import 'package:pataki/screens/policy/policy_screen.dart';
import 'package:pataki/screens/profile/profile_binding.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/screens/register/register_binding.dart';
import 'package:pataki/screens/register/register_screen.dart';
import 'package:pataki/screens/register_pet/register_pet_binding.dart';
import 'package:pataki/screens/register_pet/register_pet_screen.dart';
import 'package:pataki/screens/register_pet_option_screen/register_pet_option_screen.dart';
import 'package:pataki/screens/reminder/reminder_binding.dart';
import 'package:pataki/screens/reminder/reminder_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_binding.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/screens/report_user/report_user_binding.dart';
import 'package:pataki/screens/report_user/report_user_screen.dart';
import 'package:pataki/screens/single_post/single_post_binding.dart';
import 'package:pataki/screens/single_post/single_post_screen.dart';
import 'package:pataki/screens/splash/splash_binding.dart';
import 'package:pataki/screens/terms/terms_screen.dart';
import 'package:pataki/screens/training_category/training_category_binding.dart';
import 'package:pataki/screens/training_category/training_category_screen.dart';
import 'package:pataki/screens/training_detail/training_detail_binding.dart';
import 'package:pataki/screens/training_detail/training_detail_screen.dart';
import 'package:pataki/screens/vaccine/vaccine_binding.dart';
import 'package:pataki/screens/vaccine/vaccine_screen.dart';
import 'package:pataki/screens/welcome_screen.dart';
import 'screens/login/login_screen.dart';
import 'screens/login/login_binding.dart';
import 'screens/splash/splash_screen.dart';
import 'package:timezone/data/latest.dart' as tz;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  tz.initializeTimeZones();
  await Notifications.initializeNotifications();

  Get.put(NotificationService());

  runApp(PatakiApp());
}

class PatakiApp extends StatefulWidget {
  @override
  _PatakiAppState createState() => _PatakiAppState();
}

class _PatakiAppState extends State<PatakiApp> with DeepLinking {
  StreamSubscription sub;

  @override
  initState() {
    super.initState();
    initUniLinks();
    onChangeUniLinks(sub);
  }

  @override
  dispose() {
    print("sub_disposed");
    if (sub != null) {
      sub.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pataki',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: SplashScreen.id,
      getPages: [
        GetPage(
          name: SplashScreen.id,
          page: () => SplashScreen(),
          binding: SplashBinding(),
          transitionDuration: Duration(milliseconds: 0),
        ),
        GetPage(
          name: WelcomeScreen.id,
          page: () => WelcomeScreen(),
        ),
        GetPage(
          name: LoginScreen.id,
          page: () => LoginScreen(),
          binding: LoginBinding(),
        ),
        GetPage(
          name: ForgotPassScreen.id,
          page: () => ForgotPassScreen(),
        ),
        GetPage(
          name: RegisterScreen.id,
          page: () => RegisterScreen(),
          binding: RegisterBinding(),
        ),
        GetPage(
          name: RegisterPetScreen.id,
          page: () => RegisterPetScreen(),
          binding: RegisterPetBinding(),
        ),
        GetPage(
          name: EditPetScreen.id,
          page: () => EditPetScreen(),
          binding: EditPetBinding(),
        ),
        GetPage(
          name: EditUserScreen.id,
          page: () => EditUserScreen(),
          binding: EditUserBinding(),
        ),
        GetPage(
          name: WelcomeScreen.id,
          page: () => WelcomeScreen(),
        ),
        GetPage(
          name: RegisterPetOptionScreen.id,
          page: () => RegisterPetOptionScreen(),
        ),
        GetPage(
          name: HomeScreen.id,
          page: () => HomeScreen(),
          binding: HomeBinding(),
        ),
        GetPage(
          name: CommentScreen.routeName(":timelineId"),
          page: () => CommentScreen(),
          binding: CommentBinding(),
        ),
        GetPage(
          name: EditCommentScreen.routeName,
          page: () => EditCommentScreen(),
          binding: EditCommentBinding(),
        ),
        GetPage(
          name: ProfileScreen.routeName(":userId"),
          page: () => ProfileScreen(),
          binding: ProfileBinding(),
        ),
        GetPage(
          name: ReportTimelinePostScreen.routeName,
          page: () => ReportTimelinePostScreen(),
          binding: ReportTimelinePostBinding(),
        ),
        GetPage(
          name: ReportUserScreen.routeName,
          page: () => ReportUserScreen(),
          binding: ReportUserBinding(),
        ),
        GetPage(
          name: FollowingScreen.routeName(":userId"),
          page: () => FollowingScreen(),
          binding: FollowingBinding(),
        ),
        GetPage(
          name: CreatePostScreen.routeName,
          page: () => CreatePostScreen(),
          binding: CreatePostBinding(),
        ),
        GetPage(
          name: EditPostScreen.routeName(":timelineId"),
          page: () => EditPostScreen(),
          binding: EditPostBinding(),
        ),
        GetPage(
          name: PetScreen.routeName(":petId"),
          page: () => PetScreen(),
          binding: PetBinding(),
        ),
        GetPage(
          name: TermsScreen.routeName,
          page: () => TermsScreen(),
        ),
        GetPage(
          name: PolicyScreen.routeName,
          page: () => PolicyScreen(),
        ),
        GetPage(
          name: PetListScreen.routeName(":userId"),
          page: () => PetListScreen(),
          binding: PetListBinding(),
        ),
        GetPage(
          name: AboutScreen.routeName,
          page: () => AboutScreen(),
          binding: AboutBinding(),
        ),
        GetPage(
          name: BlockedListScreen.routeName,
          page: () => BlockedListScreen(),
          binding: BlockedListBinding(),
        ),
        GetPage(
          name: VaccineScreen.routeName(":petId"),
          page: () => VaccineScreen(),
          binding: VaccineBinding(),
        ),
        GetPage(
          name: CreateVaccineScreen.routeName(":petId"),
          page: () => CreateVaccineScreen(),
          binding: CreateVaccineBinding(),
        ),
        GetPage(
          name: ReminderScreen.routeName(":petId"),
          page: () => ReminderScreen(),
          binding: ReminderBinding(),
        ),
        GetPage(
          name: CreatePetReminderScreen.routeName(":petId"),
          page: () => CreatePetReminderScreen(),
          binding: CreatePetReminderBinding(),
        ),
        GetPage(
          name: CreateUserReminderScreen.routeName(":trickId"),
          page: () => CreateUserReminderScreen(),
          binding: CreateUserReminderBinding(),
        ),
        GetPage(
          name: SinglePostScreen.routeName(":timelineId"),
          page: () => SinglePostScreen(),
          binding: SinglePostBinding(),
        ),
        GetPage(
          name: TrainingCategoryScreen.routeName,
          page: () => TrainingCategoryScreen(),
          binding: TrainingCategoryBinding(),
        ),
        GetPage(
          name: TrainingDetailScreen.routeName,
          page: () => TrainingDetailScreen(),
          binding: TrainingDetailBinding(),
        ),
        GetPage(
          name: ChatRoomsScreen.routeName,
          page: () => ChatRoomsScreen(),
        ),
        GetPage(
          name: ChatRoomDetailsScreen.routeName(":contactId"),
          page: () => ChatRoomDetailsScreen(),
          binding: ChatRoomDetailsBinding(),
        ),
        GetPage(
          name: GroupListScreen.routeName(":userId"),
          page: () => GroupListScreen(),
          binding: GroupListBinding(),
        ),
        GetPage(
          name: CreateGroupScreen.id,
          page: () => CreateGroupScreen(),
          binding: CreateGroupBinding(),
        ),
        GetPage(
          name: GroupScreen.routeName(":groupId"),
          page: () => GroupScreen(),
          binding: GroupBinding(),
        ),
        GetPage(
          name: EditGroupScreen.id,
          page: () => EditGroupScreen(),
          binding: EditGroupBinding(),
        ),
        GetPage(
          name: MemberListScreen.routeName,
          page: () => MemberListScreen(),
          binding: MemberListBinding(),
        ),
        GetPage(
          name: CreateGroupPostScreen.routeName,
          page: () => CreateGroupPostScreen(),
          binding: CreateGroupPostBinding(),
        ),
        GetPage(
          name: LikesScreen.routeName(":timelineId"),
          page: () => LikesScreen(),
          binding: LikesBinding(),
        ),
        GetPage(
          name: EditPassScreen.id,
          page: () => EditPassScreen(),
          binding: EditPassBinding(),
        ),
      ],
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [Locale('pt')],
    );
  }
}
