import 'package:get/get.dart';
import 'package:pataki/screens/about/about_controller.dart';

class AboutBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AboutController());
  }
}
