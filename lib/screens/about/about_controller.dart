import 'package:get/get.dart';
import 'package:package_info/package_info.dart';

class AboutController extends GetxController {
  RxString versionName = "".obs;

  @override
  void onInit() {
    start();
    super.onInit();
  }

  Future<void> start() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    versionName.value = packageInfo.version;
  }

}
