import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/about/about_controller.dart';
import 'package:pataki/screens/policy/policy_screen.dart';
import 'package:pataki/screens/terms/terms_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';

double screenWidth;

class AboutScreen extends GetView<AboutController> {
  static String routeName = "/about_screen";

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: "Sobre",
        centerTitle: true,
      ),
      body: Container(
        padding: screenWidth >= 375
            ? EdgeInsets.symmetric(vertical: 16, horizontal: 32)
            : EdgeInsets.symmetric(vertical: 0, horizontal: 32),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                if (screenWidth >= 375) SizedBox(height: 10),
                Text(
                  "Pataki",
                  style: TextStyle(
                    fontSize: 24,
                    color: PatakiColors.orange,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: screenWidth >= 375 ? 30 : 10),
                Text(
                  "\t\tOferecemos tudo que o seu pet precisa em um único lugar. Nosso ecossistema de soluções foi desenhado para receber você e seu pet com toda qualidade, conforto e praticidade que vocês merecem!",
                  style: TextStyle(fontSize: screenWidth >= 375 ? 16 : 13),
                ),
                SizedBox(height: screenWidth >= 375 ? 20 : 10),
                Text(
                  "\t\tO Pataki veio para te conectar ainda mais a esse incrível universo pet.",
                  style: TextStyle(fontSize: screenWidth >= 375 ? 16 : 13),
                ),
                SizedBox(height: screenWidth >= 375 ? 40 : 20),
                Obx(
                  () => Text(
                    "Versão: ${controller.versionName.value}",
                    style: TextStyle(fontSize: 14, color: PatakiColors.gray),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                LargeTextButton(
                  text: "Termos de uso",
                  backgroundColor: PatakiColors.orange,
                  onPressed: () => Get.toNamed(TermsScreen.routeName),
                ),
                SizedBox(height: 10),
                LargeTextButton(
                  text: "Política de privacidade",
                  backgroundColor: PatakiColors.orange,
                  onPressed: () => Get.toNamed(PolicyScreen.routeName),
                ),
                SizedBox(height: 10),
                LargeTextButton(
                  text: "Bibliotecas de código aberto",
                  backgroundColor: PatakiColors.orange,
                  onPressed: () => showLicensePage(context: context),
                ),
                SizedBox(height: 40),
              ],
            )
          ],
        ),
      ),
    );
  }
}
