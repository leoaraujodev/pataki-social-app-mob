import 'package:get/get.dart';
import 'package:pataki/screens/blocked_list/blocked_list_controller.dart';

class BlockedListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BlockedListController());
  }
}
