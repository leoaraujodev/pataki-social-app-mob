import 'package:get/get.dart';
import 'package:pataki/core/endpoints/block_user_endpoint.dart';
import 'package:pataki/core/endpoints/get_blocked_tutors_endpoint.dart';
import 'package:pataki/core/models/block_user_request.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_blocked_tutor_response.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/models/user_model.dart';

class BlockedListController extends GetxController {
  final Rx<UserModel> user = UserModel().obs;

  final RxList<UserModel> blockedTutors = RxList([]);

  final loadingUserInfo = true.obs;
  final errorOnUserInfo = false.obs;

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getBlockedUsers();
    super.onInit();
  }

  Future<void> getBlockedUsers() async {
    loadingUserInfo.value = true;
    errorOnUserInfo.value = false;

    try {
      GetBlockedTutorResponse response = await GetBlockedTutorsEndpoint(EmptyRequest()).call();

      blockedTutors.clear();

      if (response.blockedTutors != null) {
        blockedTutors.addAll(response.blockedTutors);
      }

      user.refresh();
    } catch (e) {
      errorOnUserInfo.value = true;
      _showErrorSnackbar();
    }

    loadingUserInfo.value = false;
  }

  Future<void> unblockUser(int userId) async {
    try {
      SuccessMessageResponse response = await BlockUserEndpoint(BlockUserRequest(
        id: userId,
        block: false,
      )).call();

      getBlockedUsers();
      Get.back();
      Get.snackbar("Pronto.", response.message);
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
