import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/screens/blocked_list/blocked_list_controller.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_button.dart';
import 'package:pataki/uikit/widgets/build_tutor.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class BlockedListScreen extends GetView<BlockedListController> {
  static String routeName = "/blocked_list_screen";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Usuários Bloqueados",
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingUserInfo.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnUserInfo.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getBlockedUsers,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                if (controller.blockedTutors.isEmpty) {
                  return Center(
                    child: Text(
                      "Nenhum tutor bloqueado",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.grey),
                    ),
                  );
                }
                return ListView(
                  shrinkWrap: true,
                  children: [
                    BuildCard(
                      children: controller.blockedTutors
                          .map(
                            (tutor) => Stack(
                              alignment: Alignment.center,
                              children: [
                                BuildTutor(
                                  tutor: tutor,
                                  maxCharacters: 20,
                                  onPressed: null,
                                ),
                                Positioned(
                                  right: 8,
                                  child: BuildButton(
                                    text: "Desbloquear",
                                    textColor: Colors.black87,
                                    borderColor: PatakiColors.lightGray,
                                    onPressed: () => Get.dialog(
                                      ConfirmDialog(
                                        onConfirmPressed: () => controller.unblockUser(
                                          tutor.id,
                                        ),
                                        action: "desbloquear",
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
