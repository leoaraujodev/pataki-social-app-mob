import 'package:get/get.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_controller.dart';

class ChatRoomDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ChatRoomDetailsController(), tag: Get.arguments["timestamp"]);
  }
}
