import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/endpoints/create_chat_message_endpoint.dart';
import 'package:pataki/core/endpoints/get_chat_room_detail_endpoint.dart';
import 'package:pataki/core/endpoints/get_chat_stickers_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/chat_message.dart';
import 'package:pataki/core/models/chat_room.dart';
import 'package:pataki/core/models/create_chat_message_request.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_chat_room_detail_request.dart';
import 'package:pataki/core/models/sticker.dart';

class ChatRoomDetailsController extends GetxController {
  int contactId = int.tryParse(Get.parameters["contactId"]);
  int userId;
  int firstId;
  int lastId;
  Timer timer;

  bool _fetchingNewMutex = false;
  bool _fetchingOldMutex = false;

  final scrollController = ScrollController();
  final textController = TextEditingController();

  final loadingInitialMessages = true.obs;
  final errorOnInitialMessages = false.obs;

  final hasMoreMessages = false.obs;
  final errorOnMoreMessages = false.obs;

  final image = Rx<File>();
  final picker = ImagePicker();
  final room = Rx<ChatRoom>();
  final messages = RxList<ChatMessage>([]);
  final stickers = RxList<Sticker>([]);

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      image.value = File(pickedFile.path);
    }
  }

  @override
  void onInit() {
    getInitialMessages();
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMoreMessages.value && !errorOnMoreMessages.value) getMoreMessages();
      }
    });
    super.onInit();
  }

  Future<void> getInitialMessages() async {
    loadingInitialMessages.value = true;
    errorOnInitialMessages.value = false;

    try {
      userId = await GetUserIdInteractor().execute();

      final stickersResponse = await GetChatStickersEndpoint(EmptyRequest()).call();
      stickers.assignAll(stickersResponse.stickers);

      final roomDetailsResponse = await GetChatRoomDetailEndpoint(
        GetChatRoomDetailRequest(userId: contactId),
      ).call();
      room.value = roomDetailsResponse.room;
      messages.assignAll(roomDetailsResponse.messages);

      if (roomDetailsResponse.messages.isNotEmpty) {
        //Response pagination is reversed
        firstId = roomDetailsResponse.messages.last.id;
        lastId = roomDetailsResponse.messages.first.id;

        if (roomDetailsResponse.messages.length < 25) {
          hasMoreMessages.value = false;
        } else {
          hasMoreMessages.value = true;
        }
      }

      timer = Timer.periodic(2.seconds, (_) => refreshMessages());
    } catch (e) {
      errorOnInitialMessages.value = true;
      _showErrorSnackbar();
    }

    loadingInitialMessages.value = false;
  }

  Future<void> refreshMessages() async {
    if (_fetchingNewMutex) return;
    _fetchingNewMutex = true;

    try {
      final roomDetailsResponse = await GetChatRoomDetailEndpoint(
        GetChatRoomDetailRequest(userId: contactId, lastId: lastId),
      ).call();

      if (roomDetailsResponse.messages.isNotEmpty) {
        lastId = messages.first.id;

        roomDetailsResponse.messages.forEach((newMessage) {
          int index = messages.indexWhere((message) => message.id == newMessage.id);

          if (index == -1) {
            messages.add(newMessage);
          }
        });

        messages.sort((b, a) => a.id.compareTo(b.id));

        if (scrollController.hasClients && scrollController.offset < Get.height) {
          scrollController.jumpTo(0);
        }
      }
    } catch (e) {}

    _fetchingNewMutex = false;
  }

  Future<void> getMoreMessages() async {
    if (_fetchingOldMutex) return;
    _fetchingOldMutex = true;

    try {
      final roomDetailsResponse = await GetChatRoomDetailEndpoint(
        GetChatRoomDetailRequest(
          userId: int.tryParse(Get.parameters["contactId"]),
          firstId: firstId,
        ),
      ).call();

      if (roomDetailsResponse.messages.isNotEmpty) {
        firstId = roomDetailsResponse.messages.last.id;
        if (roomDetailsResponse.messages.length < 25) {
          hasMoreMessages.value = false;
        } else {
          hasMoreMessages.value = true;
        }

        roomDetailsResponse.messages.forEach((newMessage) {
          int index = messages.indexWhere((message) => message.id == newMessage.id);

          if (index == -1) {
            messages.add(newMessage);
          }
        });
        messages.sort((b, a) => a.id.compareTo(b.id));
      }
    } catch (e) {
      _showErrorSnackbar();
    }

    _fetchingOldMutex = false;
  }

  void sendImageTextMessage() async {
    final text = textController.text;
    final selectedImage = image.value;
    if (text.isEmpty && selectedImage == null) return;
    textController.clear();
    image.value = null;

    _postMessage(text: text, selectedImage: selectedImage);
  }

  void sendStickerMessage({Sticker sticker}) async {
    _postMessage(sticker: sticker);
  }

  void retryToPostMessage(ChatMessage message) {
    final text = message.message;
    final selectedImage = message.image;
    final sticker = message.sticker;
    messages.remove(message);
    _postMessage(text: text, sticker: sticker, selectedImage: selectedImage);
  }

  Future<void> _postMessage({String text, Sticker sticker, File selectedImage}) async {
    if (scrollController.hasClients) scrollController.jumpTo(0);
    final placeholderMessage = ChatMessage(
      id: double.infinity,
      createdAt: DateTime.now(),
      imagePath: null,
      senderId: userId,
      message: text != "" ? text : null,
      sticker: sticker,
      image: selectedImage,
      status: ChatMessageStatus.sending,
    );
    messages.insert(0, placeholderMessage);
    messages.sort((b, a) => a.id.compareTo(b.id));

    ChatMessage createdChatMessage;
    try {
      createdChatMessage = await CreateChatMessageEndpoint(
        CreateChatMessageRequest(
          roomId: room.value.id,
          message: text != "" ? text : null,
          image: selectedImage,
          stickerId: sticker?.id,
        ),
      ).call();
    } catch (e) {
      _showErrorSnackbar();
      createdChatMessage = placeholderMessage.copyWith(status: ChatMessageStatus.error);
    }

    int placeholderIndex = messages.indexOf(placeholderMessage);
    if (placeholderIndex != -1) {
      int createdChatIndex = messages.indexWhere((chat) => chat.id == createdChatMessage.id);
      if (createdChatMessage.id != double.infinity && createdChatIndex != -1) {
        messages.removeAt(placeholderIndex);
      } else {
        messages[placeholderIndex] = createdChatMessage;
      }
    }
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onClose() {
    if (timer != null) timer.cancel();
    textController.dispose();
    scrollController.dispose();
    super.onClose();
  }

  bool isContactMessage(int messageSenderId) {
    return messageSenderId == room.value.contact.id;
  }
}
