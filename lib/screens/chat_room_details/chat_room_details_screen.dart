import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/chat_message_bubble.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/stickers_dialog.dart';
import 'package:pataki/uikit/widgets/user_image.dart';

class ChatRoomDetailsScreen extends GetView<ChatRoomDetailsController> {
  static String routeName(id) => "/chat_room/$id";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Obx(
            () => controller.loadingInitialMessages.value
                ? Container()
                : Row(
                    children: [
                      UserImage(imagePath: controller.room.value.contact.imagePath),
                      Flexible(
                        child: Text(
                          controller.room.value.contact.name,
                          style: TextStyle(
                            color: Color(0xFF3F414E),
                            fontWeight: FontWeight.normal,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
          centerTitle: false,
          brightness: Brightness.light,
          iconTheme: IconThemeData(color: Color(0xFF3F414E)),
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: Obx(
          () {
            if (controller.loadingInitialMessages.value) {
              return Center(child: RefreshProgressIndicator());
            }

            if (controller.errorOnInitialMessages.value) {
              return Center(
                child: GestureDetector(
                  onTap: controller.getInitialMessages,
                  child: RefreshProgressIndicator(
                    value: 1.0,
                  ),
                ),
              );
            }

            return Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Container(
                    color: Colors.grey[50],
                    child: ListView.builder(
                      controller: controller.scrollController,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      shrinkWrap: true,
                      reverse: true,
                      physics: AlwaysScrollableScrollPhysics(),
                      itemCount: controller.messages.length,
                      itemBuilder: (context, index) {
                        Widget bubble = ChatMessageBubble(
                          message: controller.messages[index],
                          isContactMessage: controller.isContactMessage(
                            controller.messages[index].senderId,
                          ),
                          retryToPost: controller.retryToPostMessage,
                        );

                        if (index == controller.messages.length - 1 &&
                            controller.hasMoreMessages.value) {
                          return Column(
                            children: [
                              Center(child: RefreshProgressIndicator()),
                              bubble,
                            ],
                          );
                        } else {
                          return bubble;
                        }
                      },
                    ),
                  ),
                ),
                SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (controller.image.value != null)
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 10),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Image.file(
                                controller.image.value,
                                height: 50,
                              ),
                              IconButton(
                                icon: Icon(Icons.cancel_outlined, color: Color(0xFF90AECC)),
                                onPressed: () => controller.image.value = null,
                              ),
                            ],
                          ),
                        ),
                      Container(
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              width: 40,
                              child: IconButton(
                                padding: EdgeInsets.only(left: 8),
                                icon: Icon(Icons.image_outlined, color: Color(0xFF90AECC)),
                                onPressed: () => Get.dialog(
                                  ImagePickerDialog(
                                    onCameraPressed: () => controller.getImage(ImageSource.camera),
                                    onGalleryPressed: () =>
                                        controller.getImage(ImageSource.gallery),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: 40,
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                visualDensity: VisualDensity.compact,
                                icon: Icon(Icons.emoji_emotions_outlined, color: Color(0xFF90AECC)),
                                onPressed: () => Get.dialog(
                                  StickersDialog(
                                    stickers: controller.stickers,
                                    onStickerPressed: (sticker) =>
                                        controller.sendStickerMessage(sticker: sticker),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: CommentTextField(
                                hintText: "Mensagem",
                                textInputAction: TextInputAction.newline,
                                keyboardType: TextInputType.multiline,
                                controller: controller.textController,
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.send, color: PatakiColors.orange),
                              onPressed: controller.sendImageTextMessage,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
