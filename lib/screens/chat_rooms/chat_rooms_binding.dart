import 'package:get/get.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_controller.dart';

class ChatRoomsBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ChatRoomsController());
  }
}
