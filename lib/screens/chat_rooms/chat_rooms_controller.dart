import 'dart:async';

import 'package:get/get.dart';
import 'package:pataki/core/endpoints/get_chat_rooms_endpoint.dart';
import 'package:pataki/core/interactor/get_has_new_messages_interactor.dart';
import 'package:pataki/core/interactor/get_last_chat_id_interactor.dart';
import 'package:pataki/core/interactor/get_last_message_id_interactor.dart';
import 'package:pataki/core/interactor/set_has_new_messages_interactor.dart';
import 'package:pataki/core/interactor/set_last_chat_id_interactor.dart';
import 'package:pataki/core/interactor/set_last_message_id_interactor.dart';
import 'package:pataki/core/models/chat_room.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_chat_rooms_response.dart';

class ChatRoomsController extends GetxController {
  final loadingInitialRooms = true.obs;
  final errorOnInitialRooms = false.obs;
  bool _fetchingMutex = false;
  final rooms = RxList<ChatRoom>([]);
  Timer timer;

  final hasNewMessages = false.obs;
  int _lastChatId;
  int _lastMessageId;

  @override
  void onInit() {
    getInitialRooms();
    super.onInit();
  }

  Future<void> getInitialRooms() async {
    loadingInitialRooms.value = true;
    errorOnInitialRooms.value = false;

    try {
      hasNewMessages.value = (await GetHasNewMessagesInteractor().execute()) ?? false;
      _lastChatId = await GetLastChatIdInteractor().execute();
      _lastMessageId = await GetLastMessageIdInteractor().execute();

      final roomResponse = await _getRoomResponse();
      _setValues(roomResponse);
      timer = Timer.periodic(30.seconds, (_) => refreshRooms());
    } catch (e) {
      errorOnInitialRooms.value = true;
      _showErrorSnackbar();
    }

    loadingInitialRooms.value = false;
  }

  Future<void> refreshRooms() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    try {
      final roomResponse = await _getRoomResponse();
      _setValues(roomResponse);
    } catch (e) {
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  void _setValues(GetChatRoomsResponse response) {
    rooms.assignAll(response.rooms);
    rooms.sort((b, a) {
      if (a.lastMessage == null || b.lastMessage == null) {
        return 0;
      }
      return a.lastMessage.createdAt.compareTo(b.lastMessage?.createdAt);
    });

    if (rooms.isEmpty) return;

    int newLastChatId = rooms.first.id;
    int newLastMessageId = rooms.first.lastMessage?.id;

    if (newLastChatId != _lastChatId || newLastMessageId != _lastMessageId) {
      hasNewMessages.value = true;
      SetHasNewMessagesInteractor().execute(true);

      _lastChatId = newLastChatId;
      SetLastChatIdInteractor().execute(newLastChatId);

      _lastMessageId = newLastMessageId;
      SetLastMessageIdInteractor().execute(newLastMessageId);
    }
  }

  void markMessagesAsRead() {
    hasNewMessages.value = false;
    SetHasNewMessagesInteractor().execute(false);
  }

  void _showErrorSnackbar() {
    Get.snackbar(
        "Erro de conexão", "Verifique sua conexão com a Internet, você pode ter novas mensagens");
  }

  Future<GetChatRoomsResponse> _getRoomResponse() => GetChatRoomsEndpoint(EmptyRequest()).call();

  @override
  void onClose() {
    if (timer != null) timer.cancel();
    super.onClose();
  }
}
