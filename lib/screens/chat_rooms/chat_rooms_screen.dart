import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_screen.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_controller.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/room_tile.dart';

class ChatRoomsScreen extends GetView<ChatRoomsController> {
  static String routeName = "/chat_rooms";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(title: "Chat"),
      body: Obx(
        () {
          if (controller.loadingInitialRooms.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnInitialRooms.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getInitialRooms,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return RefreshIndicator(
            onRefresh: controller.refreshRooms,
            child: SafeArea(
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                itemCount: controller.rooms.length,
                itemBuilder: (context, index) => RoomTile(
                  room: controller.rooms[index],
                  onPressed: () async {
                    await Get.toNamed(
                      ChatRoomDetailsScreen.routeName(controller.rooms[index].contact.id),
                      arguments: {"timestamp": DateTime.now().toIso8601String()},
                    );
                    controller.refreshRooms();
                  },
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
