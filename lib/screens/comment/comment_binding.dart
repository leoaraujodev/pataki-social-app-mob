import 'package:get/get.dart';
import 'package:pataki/screens/comment/comment_controller.dart';

class CommentBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CommentController(), tag: Get.arguments["timestamp"]);
  }
}
