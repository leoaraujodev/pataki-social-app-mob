import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/add_comment_endpoint.dart';
import 'package:pataki/core/endpoints/delete_comment_endpoint.dart';
import 'package:pataki/core/endpoints/like_comment_endpoint.dart';
import 'package:pataki/core/endpoints/timeline_comment_endpoint.dart';
import 'package:pataki/core/endpoints/user_info_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/add_comment_request.dart';
import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/core/models/delete_comment_request.dart';
import 'package:pataki/core/models/like_comment_request.dart';
import 'package:pataki/core/models/mention.dart';
import 'package:pataki/core/models/mention_controller.dart';
import 'package:pataki/core/models/timeline_comment_request.dart';
import 'package:pataki/core/models/timeline_comment_response.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/models/user_info_request.dart';

class CommentController extends GetxController with MentionController {
  int _timelineId = int.parse(Get.parameters["timelineId"]);
  final void Function(int, int) updateCommentCount = Get.arguments["updateCommentCount"];
  final String timestamp = Get.arguments["timestamp"];

  final scrollController = ScrollController();
  final comments = RxList<CommentModel>([]);
  final user = Rx<UserModel>();

  final loadingInitialComments = true.obs;
  final errorOnInitialComments = false.obs;

  final hasMoreComments = false.obs;
  final errorOnMoreComments = false.obs;

  int _totalComments = 0;

  int _currentPage = 1;
  bool _fetchingMutex = false;
  bool _likingMutex = false;

  @override
  void onInit() {
    mentionControllerReset();
    getInitialComments();

    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMoreComments.value && !errorOnMoreComments.value) getMoreComments();
      }
    });

    textController.addListener(() {
      dealWithModifications();
      showPetsList();
    });
    super.onInit();
  }

  Future<void> getInitialComments() async {
    loadingInitialComments.value = true;
    errorOnInitialComments.value = false;

    try {
      int userId = await GetUserIdInteractor().execute();
      final userResponse = await UserInfoEndpoint(UserInfoRequest(userId: userId)).call();
      user.value = userResponse.user;

      await getPetsFolloweds();

      final commentResponse = await _getTimelineCommentResponse();
      _setValues(commentResponse);
    } catch (e) {
      errorOnInitialComments.value = true;
      _showErrorSnackbar();
    }

    loadingInitialComments.value = false;
  }

  Future<void> refreshComments() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousCurrentPage = _currentPage;

    try {
      _currentPage = 1;
      final response = await _getTimelineCommentResponse();
      comments.clear();
      _setValues(response);
    } catch (e) {
      _currentPage = previousCurrentPage;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMoreComments() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMoreComments.value = false;

    try {
      final response = await _getTimelineCommentResponse();
      _setValues(response);
    } catch (e) {
      errorOnMoreComments.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<TimelineCommentResponse> _getTimelineCommentResponse() =>
      TimelineCommentEndpoint(TimelineCommentRequest(timelineId: _timelineId, page: _currentPage))
          .call();

  void _setValues(TimelineCommentResponse response) {
    comments.addAll(response.data);

    comments.forEach((comment) {
      Mention.listTextSpanWithRoutes(comment.comment);
    });

    _totalComments = response.total;
    updateCommentCount(_timelineId, _totalComments);

    if (_currentPage == response.lastPage) {
      hasMoreComments.value = false;
    } else {
      hasMoreComments.value = true;
      _currentPage++;
    }
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> addComment() async {
    try {
      String comment = textControllerWithMentionsToTag();

      final response = await AddCommentEndpoint(AddCommentRequest(
        timelineId: _timelineId,
        comment: comment,
      )).call();

      comments.insert(
        0,
        CommentModel(
          id: response.id,
          comment: response.comment,
          createdAt: response.createdAt,
          liked: response.liked,
          totLikes: 0,
          user: user.value,
        ),
      );

      _totalComments++;
      updateCommentCount(_timelineId, _totalComments);

      mentionControllerReset();
      textController.clear();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> likeComment(int commentId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = comments.indexWhere((comment) => comment.id == commentId);

      comments[index] = comments[index].copyWith(
        totLikes: like ? comments[index].totLikes + 1 : comments[index].totLikes - 1,
        liked: like,
      );

      await LikeCommentEndpoint(LikeCommentRequest(commentId: commentId, like: like)).call();
    } catch (e) {
      int index = comments.indexWhere((comment) => comment.id == commentId);

      comments[index] = comments[index].copyWith(
        totLikes: !like ? comments[index].totLikes + 1 : comments[index].totLikes - 1,
        liked: !like,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deleteComment(int id) async {
    try {
      await DeleteCommentEndpoint(DeleteCommentRequest(id: id)).call();

      int index = comments.indexWhere((comment) => comment.id == id);

      comments.removeAt(index);

      _totalComments--;
      updateCommentCount(_timelineId, _totalComments);

      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    scrollController.dispose();
    textController.dispose();
    super.onClose();
  }
}
