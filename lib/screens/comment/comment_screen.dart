import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/screens/comment/comment_controller.dart';
import 'package:pataki/screens/comment/edit_comment_screen.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/comment_tile.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/user_image.dart';

class CommentScreen extends GetView<CommentController> {
  static String routeName(timelineId) => "/comment_screen/$timelineId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(title: "Comentários"),
        body: Column(
          children: [
            Expanded(
              child: Obx(
                () {
                  if (controller.loadingInitialComments.value) {
                    return Center(child: RefreshProgressIndicator());
                  }

                  if (controller.errorOnInitialComments.value) {
                    return Center(
                      child: GestureDetector(
                        onTap: controller.getInitialComments,
                        child: RefreshProgressIndicator(
                          value: 1.0,
                        ),
                      ),
                    );
                  }

                  if (controller.petsToMention.isNotEmpty) return _petToMentions();

                  return RefreshIndicator(
                    child: SafeArea(
                      child: ListView.builder(
                        physics: AlwaysScrollableScrollPhysics(),
                        controller: controller.scrollController,
                        itemCount: controller.comments.length,
                        itemBuilder: (context, index) {
                          Widget tile = CommentTile(
                            comment: controller.comments[index],
                            onLikePressed: () => controller.likeComment(
                              controller.comments[index].id,
                              !controller.comments[index].liked,
                            ),
                            onCommentPressed: () => Get.toNamed(
                              ProfileScreen.routeName(controller.comments[index].user.id),
                              arguments: {"timestamp": DateTime.now().toIso8601String()},
                            ),
                            onCommentLongPressed: () => showCommentOptionsDialog(
                              controller.comments[index],
                            ),
                          );

                          if (index == controller.comments.length - 1 &&
                              controller.hasMoreComments.value) {
                            return Column(
                              children: [
                                tile,
                                Padding(
                                  padding: EdgeInsets.only(bottom: 20),
                                  child: Obx(
                                    () => controller.errorOnMoreComments.value
                                        ? Center(
                                            child: GestureDetector(
                                              onTap: controller.getMoreComments,
                                              child: RefreshProgressIndicator(
                                                value: 1.0,
                                              ),
                                            ),
                                          )
                                        : RefreshProgressIndicator(),
                                  ),
                                ),
                              ],
                            );
                          }

                          return tile;
                        },
                      ),
                    ),
                    onRefresh: controller.refreshComments,
                  );
                },
              ),
            ),
            SafeArea(
              child: Container(
                margin: EdgeInsets.all(20),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Obx(() => UserImage(imagePath: controller.user.value?.imagePath)),
                    Expanded(
                      child: CommentTextField(
                        textInputAction: TextInputAction.send,
                        focusNode: controller.textFocusNode,
                        controller: controller.textController,
                        onSubmitted: (comment) {
                          controller.addComment();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showCommentOptionsDialog(CommentModel comment) {
    if (comment.user.id == controller.user.value.id)
      Get.dialog(
        OptionsDialog(
          title: "Opções do Comentário",
          onEditPressed: () async {
            await Get.offNamed(
              EditCommentScreen.routeName,
              arguments: {
                "comment": comment,
                "user": controller.user.value,
                "timestamp": DateTime.now().toIso8601String(),
              },
            );
            controller.refreshComments();
          },
          onDeletePressed: () {
            Get.back();
            Get.dialog(ConfirmDialog(onConfirmPressed: () => controller.deleteComment(comment.id)));
          },
        ),
      );
  }

  Widget _petToMentions() {
    return ListView(
      shrinkWrap: true,
      children: [
        BuildCard(
          children: controller.petsToMention
              .map(
                (pet) => BuildPet(
                  pet: pet,
                  onPressed: () {
                    controller.replaceMentionAndSave(pet);
                  },
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
