import 'package:get/get.dart';
import 'package:pataki/screens/comment/edit_comment_controller.dart';

class EditCommentBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditCommentController(), tag: Get.arguments["timestamp"]);
  }
}
