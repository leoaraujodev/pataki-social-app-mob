import 'package:get/get.dart';
import 'package:pataki/core/endpoints/edit_comment_endpoint.dart';
import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/core/models/edit_comment_request.dart';
import 'package:pataki/core/models/mention_controller.dart';
import 'package:pataki/core/models/user_model.dart';

class EditCommentController extends GetxController with MentionController {
  CommentModel comment = Get.arguments["comment"];
  UserModel user = Get.arguments["user"];

  @override
  void onInit() async {
    await editMentionOnInit(comment.comment);

    textController.addListener(() {
      dealWithModifications();
      showPetsList();
    });
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> editComment(int id) async {
    try {
      String comment = textControllerWithMentionsToTag();

      await EditCommentEndpoint(
        EditCommentRequest(id: id, comment: comment),
      ).call();

      textController.clear();
      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
