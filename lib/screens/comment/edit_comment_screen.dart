import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/comment/edit_comment_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/user_image.dart';

class EditCommentScreen extends GetView<EditCommentController> {
  static String routeName = "/edit_comment_screen";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "Editar Comentário"),
      backgroundColor: Colors.white,
      body: Obx(
        () => ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(20),
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                UserImage(imagePath: controller.user?.imagePath),
                Expanded(
                  child: CommentTextField(
                    controller: controller.textController,
                    focusNode: controller.textFocusNode,
                    autofocus: true,
                    maxLines: 100,
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            if (controller.petsToMention.isNotEmpty) _petToMentions(),
            if (controller.petsToMention.isEmpty)
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(child: Container()),
                  FlatButton(
                    color: PatakiColors.yellow,
                    shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                    onPressed: () => controller.editComment(controller.comment.id),
                    child: Text("Salvar"),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  Widget _petToMentions() {
    return BuildCard(
      children: controller.petsToMention
          .map(
            (pet) => BuildPet(
              pet: pet,
              onPressed: () {
                controller.replaceMentionAndSave(pet);
              },
            ),
          )
          .toList(),
    );
  }
}
