import 'package:get/get.dart';
import 'package:pataki/screens/create_group/create_group_controller.dart';

class CreateGroupBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CreateGroupController());
  }
}
