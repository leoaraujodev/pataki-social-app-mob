import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/endpoints/create_group_endpoint.dart';
import 'package:pataki/core/models/create_group_request.dart';
import 'package:pataki/screens/validators/name_validator.dart';

class CreateGroupController extends GetxController with NameValidator {
  final formKey = GlobalKey<FormState>();
  final image = Rx<File>();
  final picker = ImagePicker();

  final descriptionFocusNode = FocusNode();
  final isDescriptionValid = true.obs;
  final descriptionAutovalidateMode = AutovalidateMode.disabled.obs;
  final descriptionController = TextEditingController();

  final privacyFocusNode = FocusNode();
  final isPrivacyValid = false.obs;
  final privacyAutovalidateMode = AutovalidateMode.disabled.obs;
  final RxString privacyValue = "".obs;

  final creating = false.obs;

  @override
  void onInit() {
    nameFocusNode.addListener(() {
      if (!nameFocusNode.hasFocus) {
        nameValidator(nameController.text);
        nameOnChanged(nameController.text);
      }
    });
    descriptionFocusNode.addListener(() {
      if (!descriptionFocusNode.hasFocus) {
        descriptionValidator(descriptionController.text);
        descriptionOnChanged(descriptionController.text);
      }
    });
    privacyFocusNode.addListener(() {
      if (!privacyFocusNode.hasFocus) {
        privacyValidator(privacyValue.value);
        privacyOnChanged(privacyValue.value);
      }
    });
    super.onInit();
  }

  @override
  void onClose() {
    nameFocusNode.dispose();
    descriptionFocusNode.dispose();
    privacyFocusNode.dispose();
    super.onClose();
  }

  String descriptionValidator(String value) {
    descriptionAutovalidateMode.value = AutovalidateMode.always;

    return null;
  }

  void descriptionOnChanged(String value) {
    if (descriptionAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = descriptionValidator(value);
    if (error == null) {
      isDescriptionValid.value = true;
    } else {
      isDescriptionValid.value = false;
    }
  }

  String privacyValidator(dynamic value) {
    privacyAutovalidateMode.value = AutovalidateMode.always;

    if (value != null) {
      return null;
    }
    return 'Selecione uma opção';
  }

  void privacyOnChanged(String value) {
    if (privacyAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = privacyValidator(value);
    if (error == null) {
      privacyValue.value = value;

      isPrivacyValid.value = true;
    } else {
      isPrivacyValid.value = false;
    }
  }

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      image.value = File(pickedFile.path);
    }
  }

  Future<void> submit() async {
    nameValidator(nameController.text);
    nameOnChanged(nameController.text);
    descriptionValidator(descriptionController.text);
    descriptionOnChanged(descriptionController.text);
    // Uncomment when you add the options of public/private
    // privacyValidator(privacyValue.value);
    // privacyOnChanged(privacyValue.value);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seus dados, um ou mais campos apresentam erros");
      return;
    }

    createGroup();
  }

  Future<void> createGroup() async {
    if (creating.value == true) return;
    creating.value = true;

    try {
      await CreateGroupEndpoint(
        CreateGroupRequest(
          name: nameController.text,
          desc: descriptionController.text,
          privacy: 'public',
          // Uncomment when you add the options of public/private and remove line above
          // privacy: privacyValue.value,
          image: image.value,
        ),
      ).call();

      Get.back(result: true);
      Get.snackbar("Sucesso", "Grupo criado com sucesso");
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
    creating.value = false;
  }
}
