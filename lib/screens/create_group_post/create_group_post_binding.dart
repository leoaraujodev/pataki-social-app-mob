import 'package:get/get.dart';
import 'package:pataki/screens/create_group_post/create_group_post_controller.dart';

class CreateGroupPostBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CreateGroupPostController());
  }
}
