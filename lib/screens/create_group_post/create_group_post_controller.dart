import 'dart:io';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/endpoints/create_post_endpoint.dart';
import 'package:pataki/core/endpoints/get_user_pets_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/create_post_request.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/mention_controller.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:video_compress/video_compress.dart';

class CreateGroupPostController extends GetxController with MentionController {
  final groupId = Get.arguments['groupId'];

  final selectedPet = Rx<Pet>();
  final image = Rx<File>();
  final thumbnailImage = Rx<File>();
  final fileType = Rx<FileType>();
  final description = RxString("");
  final picker = ImagePicker();
  final creating = false.obs;
  final pets = RxList<Pet>([]);

  final loadingPets = true.obs;
  final errorOnLoadingPets = false.obs;

  @override
  onInit() {
    mentionControllerReset();
    getPets();

    textController.addListener(() {
      dealWithModifications();
      showPetsList();
    });
    super.onInit();
  }

  Future<void> getPets() async {
    loadingPets.value = true;
    errorOnLoadingPets.value = false;

    try {
      int userId = await GetUserIdInteractor().execute();
      final response = await GetUserPetsEndpoint(UserInfoRequest(userId: userId)).call();
      pets.addAll(response.pets);
      if (pets.length == 1) selectedPet.value = pets.first;

      await getPetsFolloweds();
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
      errorOnLoadingPets.value = true;
    }

    loadingPets.value = false;
  }

  bool get canCreate => image.value != null;

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      await VideoCompress.cancelCompression();
      image.value = File(pickedFile.path);
      thumbnailImage.value = image.value;
      fileType.value = FileType.image;
    }
  }

  Future getVideo(ImageSource source) async {
    final pickedFile = await picker.getVideo(source: source);

    if (pickedFile != null) {
      await removeCurrentImage();
      image.value = File(pickedFile.path);
      thumbnailImage.value = await VideoCompress.getFileThumbnail(pickedFile.path);
      fileType.value = FileType.video;
    }
  }

  removeCurrentImage() async {
    await VideoCompress.cancelCompression();
    image.value = null;
    thumbnailImage.value = null;
    fileType.value = null;
  }

  Future<void> create() async {
    if (creating.value == true) return;
    creating.value = true;

    try {
      String desc = textControllerWithMentionsToTag();

      if (fileType.value == FileType.video) {
        MediaInfo mediaInfo = await VideoCompress.compressVideo(image.value.path);
        image.value = mediaInfo.file;
      }
      final post = await CreatePostEndpoint(
        CreatePostRequest(
          petId: selectedPet.value.id,
          description: desc,
          image: image.value,
          fileType: fileType.value,
          groupId: groupId,
        ),
      ).call();

      mentionControllerReset();
      textController.clear();

      Get.back(result: post);
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    creating.value = false;
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
