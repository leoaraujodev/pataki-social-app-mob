import 'package:get/get.dart';
import 'package:pataki/screens/create_post/create_post_controller.dart';

class CreatePostBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CreatePostController());
  }
}
