import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/screens/create_post/create_post_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class CreatePostScreen extends GetView<CreatePostController> {
  static String routeName = "/create_post_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(title: "Criar post"),
        body: SafeArea(
          child: Obx(
            () {
              if (controller.loadingPets.value) {
                return Center(child: RefreshProgressIndicator());
              }

              if (controller.errorOnLoadingPets.value) {
                return Center(
                  child: GestureDetector(
                    onTap: controller.getPets,
                    child: RefreshProgressIndicator(
                      value: 1.0,
                    ),
                  ),
                );
              }
              return CustomScrollView(
                slivers: [
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Text("Publicando como:"),
                          ),
                          DropdownButton<Pet>(
                            underline: SizedBox(),
                            isExpanded: true,
                            itemHeight: 55,
                            value: controller.selectedPet.value,
                            hint: Text("Selecione um pet"),
                            disabledHint: Text("Você não possui nenhum pet cadastrado"),
                            items: controller.pets
                                .map((e) => DropdownMenuItem(
                                      child: _buildPetPickerOption(e),
                                      value: e,
                                    ))
                                .toList(),
                            onChanged: (pet) {
                              controller.selectedPet.value = pet;
                            },
                          ),
                          if (controller.selectedPet.value != null) ...[
                            Divider(),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Text("Descrição:"),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: CommentTextField(
                                controller: controller.textController,
                                focusNode: controller.textFocusNode,
                                hintText: "Descreva o post (Opcional)",
                                minLines: 4,
                                onChanged: (value) {
                                  controller.description.value = value;
                                },
                              ),
                            ),
                            if (controller.petsToMention.isNotEmpty) _petToMentions(),
                            if (controller.petsToMention.isEmpty) _buildImagePickerSection(),
                          ]
                        ],
                      ),
                    ),
                  ),
                  if (controller.petsToMention.isEmpty)
                    SliverFillRemainingBoxAdapter(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
                        alignment: Alignment.bottomCenter,
                        child: SafeArea(
                          child: Obx(
                            () => LargeTextButton(
                              loading: controller.creating.value,
                              progress: (controller.fileType.value == FileType.video &&
                                      controller.progressIndicator.value < 0.99)
                                  ? controller.progressIndicator.value
                                  : null,
                              text: "SALVAR",
                              onPressed: controller.canCreate ? controller.create : null,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildPetPickerOption(Pet pet) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: EdgeInsets.only(right: 10),
          height: 35,
          width: 35,
          decoration: BoxDecoration(
            color: PatakiColors.white,
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(pet.imagePath.toString()),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Flexible(
          child: Text(
            pet.name,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Color(0xFF2B2B2B),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildImagePickerSection() {
    return controller.thumbnailImage.value == null
        ? Container(
            height: 150,
            width: Get.width,
            alignment: Alignment.center,
            child: Column(
              children: [
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: () => Get.dialog(
                    ImagePickerDialog(
                      onCameraPressed: () => controller.getImage(ImageSource.camera),
                      onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                    ),
                  ),
                  child: Text("Enviar Imagem"),
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: () => Get.dialog(
                    ImagePickerDialog(
                      onCameraPressed: () => controller.getVideo(ImageSource.camera),
                      onGalleryPressed: () => controller.getVideo(ImageSource.gallery),
                    ),
                  ),
                  child: Text("Enviar Vídeo"),
                ),
              ],
            ),
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 150,
                width: 150,
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  child: Image.file(
                    controller.thumbnailImage.value,
                  ),
                ),
              ),
              Column(
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: () => Get.dialog(
                      ImagePickerDialog(
                        onCameraPressed: () => controller.getImage(ImageSource.camera),
                        onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                      ),
                    ),
                    child: Text("Enviar Outra Imagem"),
                  ),
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: () => Get.dialog(
                      ImagePickerDialog(
                        onCameraPressed: () => controller.getVideo(ImageSource.camera),
                        onGalleryPressed: () => controller.getVideo(ImageSource.gallery),
                      ),
                    ),
                    child: Text("Enviar Outro Vídeo"),
                  ),
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: controller.removeCurrentImage,
                    child: Text("Remover"),
                  )
                ],
              )
            ],
          );
  }

  Widget _petToMentions() {
    return ListView(
      shrinkWrap: true,
      children: [
        BuildCard(
          children: controller.petsToMention
              .map(
                (pet) => BuildPet(
                  pet: pet,
                  onPressed: () {
                    controller.replaceMentionAndSave(pet);
                  },
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
