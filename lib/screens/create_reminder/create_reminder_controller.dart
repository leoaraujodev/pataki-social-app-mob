import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/notifications.dart';

abstract class CreateReminderController extends GetxController with Notifications {
  final formKey = GlobalKey<FormState>();

  final loadingInfo = true.obs;
  final errorOnInfo = false.obs;

  final reminderFocusNode = FocusNode();
  final reminderAutovalidateMode = AutovalidateMode.disabled.obs;
  final reminderController = TextEditingController();
  final isReminderValid = false.obs;

  final weekdayActived = Rx(
    {
      'Seg': false,
      'Ter': false,
      'Qua': false,
      'Qui': false,
      'Sex': false,
      'Sáb': false,
      'Dom': false
    },
  );
  final weekdaySentence = "Nenhum dia".obs;

  final timeSelected = "".obs;

  @override
  void onInit() {
    reminderFocusNode.addListener(() {
      if (!reminderFocusNode.hasFocus) {
        reminderValidator(reminderController.text);
        reminderOnChanged(reminderController.text);
      }
    });
    super.onInit();
  }

  void weekdayCreateSentence() {
    final activedItems = weekdayActived.value.keys.where((key) => weekdayActived.value[key]);
    final inactivedItems = weekdayActived.value.keys.where((key) => !weekdayActived.value[key]);

    if (activedItems.length == 0) {
      weekdaySentence.value = "Nenhum dia";
    } else if (activedItems.length == 2 &&
        activedItems.contains('Sáb') &&
        activedItems.contains('Dom')) {
      weekdaySentence.value = "Finais de semana";
    } else if (activedItems.length == 5 &&
        !activedItems.contains('Sáb') &&
        !activedItems.contains('Dom')) {
      weekdaySentence.value = "Dias da semana";
    } else if (activedItems.length <= 5) {
      weekdaySentence.value = (activedItems.first == 'Sáb' || activedItems.first == 'Dom')
          ? "Todo ${activedItems.join(", ")}"
          : "Toda ${activedItems.join(", ")}";
    } else if (activedItems.length == 6) {
      weekdaySentence.value = "Exceto ${inactivedItems.join("")}";
    } else {
      weekdaySentence.value = "Todos dias";
    }
  }

  void weekdayChangeStatus(String weekday) {
    weekdayActived.value[weekday] = !weekdayActived.value[weekday];
    weekdayActived.refresh();
    weekdayCreateSentence();
  }

  void showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  String reminderValidator(String value) {
    reminderAutovalidateMode.value = AutovalidateMode.always;

    if (value.isNotEmpty) {
      return null;
    }

    return 'Por favor preencha com o nome da rotina';
  }

  void reminderOnChanged(String value) {
    if (reminderAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = reminderValidator(value);
    if (error == null) {
      isReminderValid.value = true;
    } else {
      isReminderValid.value = false;
    }
  }

  @override
  void onClose() {
    reminderFocusNode.dispose();
    super.onClose();
  }
}
