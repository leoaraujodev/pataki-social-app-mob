import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:get/get.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_button.dart';
import 'package:pataki/uikit/widgets/rounded_text_button.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';

class CreateReminderScreen extends StatelessWidget {
  final context;
  final controller;
  final double screenWidth;

  CreateReminderScreen({
    @required this.context,
    @required this.controller,
    @required this.screenWidth,
  });

  @override
  Widget build(BuildContext localContext) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(
          title: "Adicionar rotina",
          centerTitle: true,
        ),
        body: Obx(
          () {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 48.0),
              child: ListView(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      TimePickerSpinner(
                        is24HourMode: true,
                        normalTextStyle: TextStyle(
                          fontSize: screenWidth >= 375 ? 50 : 40,
                          color: Colors.black38,
                        ),
                        highlightedTextStyle: TextStyle(
                          fontSize: screenWidth >= 375 ? 50 : 40,
                          color: Colors.black,
                        ),
                        itemHeight: screenWidth >= 375 ? 80 : 65,
                        itemWidth: screenWidth >= 375 ? 60 : 45,
                        spacing: 70,
                        isForce2Digits: true,
                        onTimeChange: (time) {
                          final String hour =
                              time.hour.toString().length == 2 ? "${time.hour}" : "0${time.hour}";
                          final String minute = time.minute.toString().length == 2
                              ? "${time.minute}"
                              : "0${time.minute}";

                          controller.timeSelected.value = hour + ":" + minute;
                        },
                      ),
                      Positioned(
                        top: screenWidth >= 375 ? 85 : 65,
                        child: Text(
                          ":",
                          style: TextStyle(fontSize: 50, color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        controller.weekdaySentence.value,
                        style: TextStyle(
                          fontSize: screenWidth >= 375 ? 18 : 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Icon(
                        Icons.calendar_today,
                        size: 30,
                        color: PatakiColors.orange,
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RoundedTextButton(
                        text: "S",
                        actived: controller.weekdayActived.value['Seg'],
                        onPressed: () => controller.weekdayChangeStatus('Seg'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "T",
                        actived: controller.weekdayActived.value['Ter'],
                        onPressed: () => controller.weekdayChangeStatus('Ter'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "Q",
                        actived: controller.weekdayActived.value['Qua'],
                        onPressed: () => controller.weekdayChangeStatus('Qua'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "Q",
                        actived: controller.weekdayActived.value['Qui'],
                        onPressed: () => controller.weekdayChangeStatus('Qui'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "S",
                        actived: controller.weekdayActived.value['Sex'],
                        onPressed: () => controller.weekdayChangeStatus('Sex'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "S",
                        actived: controller.weekdayActived.value['Sáb'],
                        onPressed: () => controller.weekdayChangeStatus('Sáb'),
                        screenWidth: screenWidth,
                      ),
                      RoundedTextButton(
                        text: "D",
                        actived: controller.weekdayActived.value['Dom'],
                        onPressed: () => controller.weekdayChangeStatus('Dom'),
                        screenWidth: screenWidth,
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        "Título",
                        style: TextStyle(
                          color: PatakiColors.orange,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Form(
                        key: controller.formKey,
                        child: FormTextField(
                          focusNode: controller.reminderFocusNode,
                          controller: controller.reminderController,
                          checkMark: _buildCheckMark(
                            autovalidateMode: controller.reminderAutovalidateMode.value,
                            isValid: controller.isReminderValid.value,
                          ),
                          hintText: "Nome da Rotina",
                          innerTextSize: screenWidth >= 375 ? 18 : 15,
                          innerTextWeight: FontWeight.w600,
                          autovalidateMode: controller.reminderAutovalidateMode.value,
                          validator: controller.reminderValidator,
                          onFieldSubmitted: (value) {
                            controller.reminderValidator(value);
                            controller.reminderOnChanged(value);
                          },
                          onChanged: controller.reminderOnChanged,
                          noBorder: true,
                          noEnabledBorder: true,
                          noInnerPadding: true,
                          noOutsidePadding: true,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  BuildButton(
                    text: "SALVAR",
                    textColor: Colors.white,
                    backgroundColor: PatakiColors.orange,
                    verticalPadding: 14,
                    horizontalPadding: 20,
                    onPressed: () => controller.createReminder(),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
