import 'package:get/get.dart';
import 'package:pataki/screens/create_reminder/pet/create_pet_reminder_controller.dart';

class CreatePetReminderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreatePetReminderController());
  }
}
