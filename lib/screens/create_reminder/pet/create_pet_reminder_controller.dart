import 'package:get/get.dart';
import 'package:pataki/core/endpoints/pet_info_endpoint.dart';
import 'package:pataki/core/endpoints/reminder_create_endpoint.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_info_request.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/reminder_create_request.dart';
import 'package:pataki/screens/create_reminder/create_reminder_controller.dart';

class CreatePetReminderController extends CreateReminderController {
  final int petId = int.parse(Get.parameters["petId"]);

  final pet = Pet().obs;

  @override
  void onInit() {
    super.onInit();
    getPetInfo();
  }

  Future<void> getPetInfo() async {
    loadingInfo.value = true;
    errorOnInfo.value = false;

    try {
      pet.value = await PetInfoEndpoint(
        PetInfoRequest(petId: petId),
      ).call();

      pet.refresh();
    } catch (e) {
      errorOnInfo.value = true;
      showErrorSnackbar();
    }

    loadingInfo.value = false;
  }

  Future<void> createReminder() async {
    if (formKey.currentState.validate()) {
      try {
        if (weekdaySentence.value == "Nenhum dia") {
          return;
        }

        Reminder createdReminder = await ReminderPetCreateEndpoint(ReminderCreateRequest(
          name: reminderController.text,
          time: timeSelected.value,
          onSun: weekdayActived.value['Dom'],
          onMon: weekdayActived.value['Seg'],
          onTue: weekdayActived.value['Ter'],
          onWed: weekdayActived.value['Qua'],
          onThu: weekdayActived.value['Qui'],
          onFri: weekdayActived.value['Sex'],
          onSat: weekdayActived.value['Sáb'],
          petId: petId,
        )).call();

        createNotification(createdReminder: createdReminder);

        Get.back(result: true);
      } catch (e) {
        Get.back();
        showErrorSnackbar();
      }
    }
  }

  createNotification({Reminder createdReminder}) {
    if (weekdaySentence.value == "Todos dias") {
      scheduleReminderDailyNotification(
        id: createdReminder.id,
        title: pet.value.name,
        reminderName: createdReminder.name,
        time: createdReminder.time,
      );
    } else {
      List days = weekdayActived.value.keys.toList();
      int weekday = 1;

      days.forEach((day) {
        if (weekdayActived.value[day]) {
          scheduleReminderNotification(
            id: createdReminder.id,
            title: pet.value.name,
            reminderName: createdReminder.name,
            weekday: weekday,
            time: createdReminder.time,
          );
        }

        weekday++;
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
