import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/create_reminder/pet/create_pet_reminder_controller.dart';
import 'package:pataki/screens/create_reminder/create_reminder_screen.dart';

double screenWidth;

class CreatePetReminderScreen extends GetView<CreatePetReminderController> {
  static String routeName(petId) => "/create_reminder_screen/$petId";

  final hourList = List.generate(24, (hour) => hour + 1);
  final minuteList = List.generate(60, (minute) => minute + 1);

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return CreateReminderScreen(
      context: context,
      controller: controller,
      screenWidth: screenWidth,
    );
  }
}
