import 'package:get/get.dart';
import 'package:pataki/screens/create_reminder/user/create_user_reminder_controller.dart';

class CreateUserReminderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateUserReminderController());
  }
}
