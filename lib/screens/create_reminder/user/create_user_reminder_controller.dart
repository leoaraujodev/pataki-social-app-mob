import 'package:get/get.dart';
import 'package:pataki/core/endpoints/reminder_trick_create_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/reminder_create_request.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/screens/create_reminder/create_reminder_controller.dart';

class CreateUserReminderController extends CreateReminderController {
  final int trickId = int.parse(Get.parameters["trickId"]);

  final userId = RxInt();

  final user = UserModel().obs;

  @override
  void onInit() {
    super.onInit();
    getUserInfo();
  }

  Future<void> getUserInfo() async {
    loadingInfo.value = true;

    try {
      userId.value = await GetUserIdInteractor().execute();

      errorOnInfo.value = false;

      user.refresh();
    } catch (e) {
      errorOnInfo.value = true;
      showErrorSnackbar();
    }

    loadingInfo.value = false;
  }

  Future<void> createReminder() async {
    if (formKey.currentState.validate()) {
      try {
        if (weekdaySentence.value == "Nenhum dia") {
          return;
        }

        Reminder createdReminder = await ReminderTrickCreateEndpoint(ReminderCreateRequest(
          name: reminderController.text,
          time: timeSelected.value,
          onSun: weekdayActived.value['Dom'],
          onMon: weekdayActived.value['Seg'],
          onTue: weekdayActived.value['Ter'],
          onWed: weekdayActived.value['Qua'],
          onThu: weekdayActived.value['Qui'],
          onFri: weekdayActived.value['Sex'],
          onSat: weekdayActived.value['Sáb'],
          trickId: trickId,
        )).call();

        createNotification(createdReminder: createdReminder);

        Get.back(result: true);
      } catch (e) {
        Get.back();
        showErrorSnackbar();
      }
    }
  }

  createNotification({Reminder createdReminder}) {
    if (weekdaySentence.value == "Todos dias") {
      scheduleReminderDailyNotification(
        id: createdReminder.id,
        title: "Hora do Treinamento",
        reminderName: createdReminder.name,
        time: createdReminder.time,
      );
    } else {
      List days = weekdayActived.value.keys.toList();
      int weekday = 1;

      days.forEach((day) {
        if (weekdayActived.value[day]) {
          scheduleReminderNotification(
            id: createdReminder.id,
            title: "Hora do Treinamento",
            reminderName: createdReminder.name,
            weekday: weekday,
            time: createdReminder.time,
          );
        }

        weekday++;
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
