import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/create_reminder/create_reminder_screen.dart';
import 'package:pataki/screens/create_reminder/user/create_user_reminder_controller.dart';

double screenWidth;

class CreateUserReminderScreen extends GetView<CreateUserReminderController> {
  static String routeName(trickId) => "/create_user_reminder_screen/$trickId";

  final hourList = List.generate(24, (hour) => hour + 1);
  final minuteList = List.generate(60, (minute) => minute + 1);

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return CreateReminderScreen(
      context: context,
      controller: controller,
      screenWidth: screenWidth,
    );
  }
}
