import 'package:get/get.dart';
import 'package:pataki/screens/create_vaccine/create_vaccine_controller.dart';

class CreateVaccineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateVaccineController());
  }
}
