import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/pet_info_endpoint.dart';
import 'package:pataki/core/endpoints/vaccine_create_endpoint.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_info_request.dart';
import 'package:pataki/core/models/vaccine.dart';
import 'package:pataki/core/models/vaccine_create_request.dart';

class CreateVaccineController extends GetxController with Notifications {
  final int petId = int.parse(Get.parameters["petId"]);
  final formKey = GlobalKey<FormState>();

  final pet = Pet().obs;

  final loadingPetInfo = true.obs;
  final errorOnPetInfo = false.obs;

  final vaccineFocusNode = FocusNode();
  final vaccineAutovalidateMode = AutovalidateMode.disabled.obs;
  final vaccineController = TextEditingController();
  final isVaccineValid = false.obs;

  final dateSelected = DateTime.now().obs;
  final dateToday = DateTime.now();

  @override
  void onInit() {
    vaccineFocusNode.addListener(() {
      if (!vaccineFocusNode.hasFocus) {
        vaccineValidator(vaccineController.text);
        vaccineOnChanged(vaccineController.text);
      }
    });
    getPetInfo();
    super.onInit();
  }

  Future<void> getPetInfo() async {
    loadingPetInfo.value = true;
    errorOnPetInfo.value = false;

    try {
      pet.value = await PetInfoEndpoint(
        PetInfoRequest(petId: petId),
      ).call();

      pet.refresh();
    } catch (e) {
      errorOnPetInfo.value = true;
      _showErrorSnackbar();
    }

    loadingPetInfo.value = false;
  }

  String vaccineValidator(String value) {
    vaccineAutovalidateMode.value = AutovalidateMode.always;

    if (value.isNotEmpty) {
      return null;
    }

    return 'Por favor preencha com o nome da vacina';
  }

  void vaccineOnChanged(String value) {
    if (vaccineAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = vaccineValidator(value);
    if (error == null) {
      isVaccineValid.value = true;
    } else {
      isVaccineValid.value = false;
    }
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<Null> selectDate() async {
    final DateTime picker = await showDatePicker(
      context: Get.context,
      initialDate: dateSelected.value,
      firstDate: DateTime(dateToday.year - 200),
      lastDate: DateTime(dateToday.year + 200),
      locale: Locale('pt'),
    );

    if (picker != null && picker != dateSelected.value) {
      dateSelected.value = picker;
    }
  }

  Future<void> createVaccine() async {
    if (formKey.currentState.validate()) {
      try {
        Vaccine createdVaccine = await VaccineCreateEndpoint(VaccineCreateRequest(
          name: vaccineController.text,
          date: dateSelected.value,
          petId: petId,
        )).call();

        await scheduleVaccineNotification(
          id: createdVaccine.id,
          petName: pet.value.name,
          vaccineName: vaccineController.text,
          notificationDate: dateSelected.value,
        );

        Get.back(result: true);
      } catch (e) {
        Get.back();
        _showErrorSnackbar();
      }
    }
  }

  @override
  void onClose() {
    vaccineFocusNode.dispose();
    super.onClose();
  }
}
