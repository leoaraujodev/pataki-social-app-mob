import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pataki/screens/create_vaccine/create_vaccine_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_button.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';

double screenWidth;

extension CapExtension on String {
  String get capitalizeFirstofEach => this.split(" ").map((str) => str.capitalize).join(" ");
}

class CreateVaccineScreen extends GetView<CreateVaccineController> {
  static String routeName(petId) => "/create_vaccine_screen/$petId";

  final dateFormat = DateFormat("EEEE, dd MMM", "pt");

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(
          title: "Adicionar vacina",
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Obx(
              () {
                if (controller.loadingPetInfo.value) {
                  return Center(child: RefreshProgressIndicator());
                }

                if (controller.errorOnPetInfo.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getPetInfo,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }

                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 48.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 30),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Vacina",
                            style: TextStyle(
                              color: PatakiColors.orange,
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Form(
                            key: controller.formKey,
                            child: FormTextField(
                              focusNode: controller.vaccineFocusNode,
                              controller: controller.vaccineController,
                              checkMark: _buildCheckMark(
                                autovalidateMode: controller.vaccineAutovalidateMode.value,
                                isValid: controller.isVaccineValid.value,
                              ),
                              hintText: "Nome da Vacina",
                              innerTextSize: screenWidth >= 375 ? 18 : 15,
                              innerTextWeight: FontWeight.w500,
                              autovalidateMode: controller.vaccineAutovalidateMode.value,
                              validator: controller.vaccineValidator,
                              onFieldSubmitted: (value) {
                                controller.vaccineValidator(value);
                                controller.vaccineOnChanged(value);
                              },
                              onChanged: controller.vaccineOnChanged,
                              noBorder: true,
                              noEnabledBorder: true,
                              noInnerPadding: true,
                              noOutsidePadding: true,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      FlatButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Icon(
                              Icons.calendar_today,
                              size: screenWidth >= 375 ? 30 : 25,
                              color: PatakiColors.orange,
                            ),
                            SizedBox(width: 10),
                            Text(
                              dateFormat
                                  .format(controller.dateSelected.value)
                                  .capitalizeFirstofEach,
                              style: TextStyle(
                                fontSize: screenWidth >= 375 ? 20 : 15,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 10),
                          ],
                        ),
                        onPressed: () => controller.selectDate(),
                      ),
                      SizedBox(height: 40),
                      BuildButton(
                        text: "SALVAR",
                        textColor: Colors.white,
                        backgroundColor: PatakiColors.orange,
                        verticalPadding: 14,
                        horizontalPadding: 20,
                        onPressed: () => controller.createVaccine(),
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
