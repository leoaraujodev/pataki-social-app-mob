import 'package:get/get.dart';
import 'package:pataki/screens/edit_group/edit_group_controller.dart';

class EditGroupBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditGroupController());
  }
}
