import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/screens/edit_group/edit_group_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_dropdown_field.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';

class EditGroupScreen extends GetView<EditGroupController> {
  EditGroupScreen({
    Key key,
  }) : super(key: key);
  static String id = "edit_group_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Editar Grupo",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildImagePicker(),
                      FormTextField(
                        focusNode: controller.nameFocusNode,
                        controller: controller.nameController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.nameAutovalidateMode.value,
                          isValid: controller.isNameValid.value,
                        ),
                        hintText: "Nome",
                        autovalidateMode: controller.nameAutovalidateMode.value,
                        validator: controller.nameValidator,
                        onFieldSubmitted: (value) {
                          controller.nameValidator(value);
                          controller.nameOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.descriptionFocusNode);
                        },
                        onChanged: controller.nameOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.descriptionFocusNode,
                        controller: controller.descriptionController,
                        hintText: "Descrição",
                        autovalidateMode: controller.descriptionAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.descriptionAutovalidateMode.value,
                          isValid: controller.isDescriptionValid.value,
                        ),
                        validator: controller.descriptionValidator,
                        onFieldSubmitted: (value) {
                          controller.descriptionValidator(value);
                          controller.descriptionOnChanged(value);
                          FocusScope.of(context).unfocus();
                          // Uncomment when you add the options of public/private
                          // FocusScope.of(context).requestFocus(controller.privacyFocusNode);
                        },
                        onChanged: controller.descriptionOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                      ),
                      // Removed option to create public/private groups
                      //
                      // FormDropdownField(
                      //   value: controller.privacyValue.value,
                      //   items: [
                      //     DropdownMenuItem(value: "public", child: Text("Público")),
                      //     DropdownMenuItem(value: "private", child: Text("Privado")),
                      //   ],
                      //   hintText: "Privacidade",
                      //   autovalidateMode: controller.privacyAutovalidateMode.value,
                      //   focusNode: controller.privacyFocusNode,
                      //   icon: _buildDropdownCheckMark(
                      //     autovalidateMode: controller.privacyAutovalidateMode.value,
                      //     isValid: controller.isPrivacyValid.value,
                      //   ),
                      //   validator: controller.privacyValidator,
                      //   onChanged: (value) {
                      //     controller.privacyValidator(value);
                      //     controller.privacyOnChanged(value);
                      //     FocusScope.of(context).unfocus();
                      //   },
                      // ),
                      LargeTextButton(
                        loading: controller.creating.value,
                        onPressed: controller.submit,
                        text: 'SALVAR',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildImagePicker() {
    return controller.image.value == null && controller.group.imagePath == null
        ? GestureDetector(
            onTap: () => Get.dialog(
              ImagePickerDialog(
                onCameraPressed: () => controller.getImage(ImageSource.camera),
                onGalleryPressed: () => controller.getImage(ImageSource.gallery),
              ),
            ),
            child: Container(
              height: 150,
              width: 150,
              margin: EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFC7D9EB)),
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: Icon(
                Icons.add_a_photo,
                size: 50,
                color: PatakiColors.gray,
              ),
            ),
          )
        : Container(
            height: 150,
            width: 150,
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xFFC7D9EB)),
              borderRadius: BorderRadius.all(Radius.circular(6)),
              image: DecorationImage(
                image: controller.image.value == null
                    ? NetworkImage(controller.group.imagePath)
                    : FileImage(controller.image.value),
                fit: BoxFit.cover,
              ),
            ),
            alignment: Alignment.bottomCenter,
            child: FlatButton(
              shape: StadiumBorder(),
              color: Colors.white.withOpacity(0.75),
              onPressed: () => Get.dialog(
                ImagePickerDialog(
                  onCameraPressed: () => controller.getImage(ImageSource.camera),
                  onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.cached),
                  SizedBox(width: 10),
                  Text("Alterar"),
                ],
              ),
            ),
          );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }

  _buildDropdownCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return Icon(Icons.keyboard_arrow_down);

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
