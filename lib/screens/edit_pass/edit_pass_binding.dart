import 'package:get/get.dart';
import 'package:pataki/screens/edit_pass/edit_pass_controller.dart';

class EditPassBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditPassController());
  }
}
