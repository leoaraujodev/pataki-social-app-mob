import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/edit_pass_endpoint.dart';
import 'package:pataki/core/models/edit_password_request.dart';
import 'package:pataki/core/models/pataki_exception.dart';

class EditPassController extends GetxController {
  final formKey = GlobalKey<FormState>();

  final oldPassFocusNode = FocusNode();
  final isOldPassValid = false.obs;
  final oldPassAutovalidateMode = AutovalidateMode.disabled.obs;
  final oldPassController = TextEditingController();

  final newPassFocusNode = FocusNode();
  final isNewPassValid = false.obs;
  final newPassAutovalidateMode = AutovalidateMode.disabled.obs;
  final newPassController = TextEditingController();

  final repeatPassFocusNode = FocusNode();
  final isRepeatPassValid = false.obs;
  final repeatPassAutovalidateMode = AutovalidateMode.disabled.obs;
  final repeatPassController = TextEditingController();

  final submitting = false.obs;

  @override
  onInit() {
    oldPassFocusNode.addListener(() {
      if (!oldPassFocusNode.hasFocus) {
        oldPassValidator(
          oldPassController.text,
        );
        oldPassOnChanged(
          oldPassController.text,
        );
      }
    });
    newPassFocusNode.addListener(() {
      if (!newPassFocusNode.hasFocus) {
        newPassValidator(
          newPassController.text,
        );
        newPassOnChanged(
          newPassController.text,
        );
      }
    });
    super.onInit();
  }

  String oldPassValidator(String value) {
    oldPassAutovalidateMode.value = AutovalidateMode.always;
    if (value.length >= 8) {
      return null;
    }

    return 'Senha precisa ter no mínimo 8 dígitos';
  }

  void oldPassOnChanged(String value) {
    if (oldPassAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = oldPassValidator(value);
    if (error == null) {
      isOldPassValid.value = true;
    } else {
      isOldPassValid.value = false;
    }
  }

  String newPassValidator(String value) {
    newPassAutovalidateMode.value = AutovalidateMode.always;
    if (value.length >= 8) {
      return null;
    }

    return 'Senha precisa ter no mínimo 8 dígitos';
  }

  void newPassOnChanged(String value) {
    if (newPassAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = newPassValidator(value);
    if (error == null) {
      isNewPassValid.value = true;
    } else {
      isNewPassValid.value = false;
    }
  }

  String repeatPassValidator(String value) {
    newPassAutovalidateMode.value = AutovalidateMode.always;
    repeatPassAutovalidateMode.value = AutovalidateMode.always;
    if (newPassController.text == repeatPassController.text) {
      return null;
    }

    return 'As senhas não correspondem';
  }

  void repeatPassOnChanged(String value) {
    if (newPassAutovalidateMode.value == AutovalidateMode.disabled ||
        repeatPassAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = repeatPassValidator(value);
    if (error == null) {
      isRepeatPassValid.value = true;
    } else {
      isRepeatPassValid.value = false;
    }
  }

  Future<void> submit() async {
    oldPassValidator(oldPassController.text);
    newPassValidator(newPassController.text);
    repeatPassValidator(repeatPassController.text);

    oldPassOnChanged(oldPassController.text);
    newPassOnChanged(newPassController.text);
    repeatPassOnChanged(repeatPassController.text);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seus dados, um ou mais campos apresentam erros");
      return;
    }

    if (submitting.value == true) return;
    submitting.value = true;

    final request = EditPassRequest(
      oldPassword: oldPassController.text,
      newPassword: newPassController.text,
    );

    try {
      await EditPassEndpoint(request).call();

      Get.back();
      Get.snackbar("Sucesso", "Senha atualizada com sucesso");
    } catch (exception) {
      if (exception is PatakiException) {
        Get.snackbar("Falha", exception.message);
      } else {
        Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
      }
    }
    submitting.value = false;
  }

  @override
  void onClose() {
    newPassFocusNode.dispose();
    oldPassFocusNode.dispose();
    repeatPassFocusNode.dispose();
    super.onClose();
  }
}
