import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/edit_pass/edit_pass_controller.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';

class EditPassScreen extends GetView<EditPassController> {
  EditPassScreen({Key key}) : super(key: key);
  static String id = "edit_pass_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Editar Senha",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FormTextField(
                        focusNode: controller.oldPassFocusNode,
                        controller: controller.oldPassController,
                        canObscure: true,
                        hintText: "Senha atual",
                        autovalidateMode: controller.oldPassAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.oldPassAutovalidateMode.value,
                          isValid: controller.isOldPassValid.value,
                        ),
                        validator: controller.oldPassValidator,
                        onFieldSubmitted: (value) {
                          controller.oldPassValidator(value);
                          controller.oldPassOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.newPassFocusNode);
                        },
                        onChanged: controller.oldPassOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.newPassFocusNode,
                        controller: controller.newPassController,
                        canObscure: true,
                        hintText: "Nova senha",
                        autovalidateMode: controller.newPassAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.newPassAutovalidateMode.value,
                          isValid: controller.isNewPassValid.value,
                        ),
                        validator: controller.newPassValidator,
                        onFieldSubmitted: (value) {
                          controller.newPassValidator(value);
                          controller.newPassOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.repeatPassFocusNode);
                        },
                        onChanged: controller.newPassOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.repeatPassFocusNode,
                        controller: controller.repeatPassController,
                        canObscure: true,
                        hintText: "Repita a nova senha",
                        autovalidateMode: controller.repeatPassAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.repeatPassAutovalidateMode.value,
                          isValid: controller.isRepeatPassValid.value,
                        ),
                        validator: controller.repeatPassValidator,
                        onFieldSubmitted: (value) {
                          controller.repeatPassValidator(value);
                          controller.repeatPassOnChanged(value);
                          FocusScope.of(context).unfocus();
                          controller.submit();
                        },
                        onChanged: controller.repeatPassOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                      ),
                      LargeTextButton(
                        loading: controller.submitting.value,
                        onPressed: controller.submit,
                        text: 'SALVAR',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
