import 'package:get/get.dart';
import 'package:pataki/screens/edit_pet/edit_pet_controller.dart';

class EditPetBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditPetController());
  }
}
