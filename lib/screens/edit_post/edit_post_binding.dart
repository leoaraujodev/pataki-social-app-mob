import 'package:get/get.dart';
import 'package:pataki/screens/edit_post/edit_post_controller.dart';

class EditPostBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditPostController());
  }
}
