import 'dart:io';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/endpoints/edit_post_endpoint.dart';
import 'package:pataki/core/endpoints/single_timeline_endpoint.dart';
import 'package:pataki/core/models/edit_post_request.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/mention_controller.dart';
import 'package:pataki/core/models/single_timeline_request.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:video_compress/video_compress.dart';

class EditPostController extends GetxController with MentionController {
  Rx<TimelinePost> originalPost = Rx<TimelinePost>();
  int timelinePostId = int.tryParse(Get.parameters["timelineId"]);

  final image = Rx<File>();
  final thumbnailImage = Rx<File>();
  final fileType = Rx<FileType>();
  final description = RxString("");
  final picker = ImagePicker();
  final removeImage = false.obs;
  final editing = false.obs;

  final RxBool loadingPost = true.obs;
  final RxBool errorOnLoadingPost = false.obs;

  @override
  onInit() async {
    await getOriginalPost();

    await editMentionOnInit(originalPost.value.desc);

    description.value = textController.text;

    textController.addListener(() {
      dealWithModifications();
      showPetsList();
    });
    super.onInit();
  }

  Future<void> getOriginalPost() async {
    loadingPost.value = true;
    errorOnLoadingPost.value = false;

    try {
      originalPost.value =
          await SingleTimelineEndpoint(SingleTimelineRequest(id: timelinePostId)).call();
    } catch (e) {
      errorOnLoadingPost.value = true;
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    loadingPost.value = false;
  }

  bool get canEdit => image.value != null || description.value.isNotEmpty;

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      await VideoCompress.cancelCompression();
      image.value = File(pickedFile.path);
      thumbnailImage.value = image.value;
      fileType.value = FileType.image;
      removeImage.value = false;
    }
  }

  Future getVideo(ImageSource source) async {
    final pickedFile = await picker.getVideo(source: source);

    if (pickedFile != null) {
      await removeCurrentImage();
      image.value = File(pickedFile.path);
      thumbnailImage.value = await VideoCompress.getFileThumbnail(pickedFile.path);
      fileType.value = FileType.video;
      removeImage.value = false;
    }
  }

  removeCurrentImage() async {
    await VideoCompress.cancelCompression();
    image.value = null;
    thumbnailImage.value = null;
    fileType.value = null;
    removeImage.value = true;
  }

  Future<void> edit() async {
    if (editing.value == true) return;
    editing.value = true;

    try {
      String desc = textControllerWithMentionsToTag();

      if (fileType.value == FileType.video) {
        MediaInfo mediaInfo = await VideoCompress.compressVideo(image.value.path);
        image.value = mediaInfo.file;
      }
      await EditPostEndpoint(
        EditPostRequest(
          id: originalPost.value.id,
          description: desc,
          image: image.value,
          fileType: fileType.value,
          removeImage: removeImage.value,
        ),
      ).call();

      textController.clear();
      Get.back();
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    editing.value = false;
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
