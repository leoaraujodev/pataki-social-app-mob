import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/screens/edit_post/edit_post_controller.dart';
import 'package:pataki/screens/timeline/timeline_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/static_media_player.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class EditPostScreen extends GetView<EditPostController> {
  static String routeName(timelineId) => "/edit_post_screen/$timelineId";

  final timelineController = Get.find<TimelineController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(title: "Editar post"),
        body: SafeArea(
          child: Obx(
            () {
              if (controller.loadingPost.value) {
                return Center(child: RefreshProgressIndicator());
              }

              if (controller.errorOnLoadingPost.value) {
                return Center(
                  child: GestureDetector(
                    onTap: controller.getOriginalPost,
                    child: RefreshProgressIndicator(
                      value: 1.0,
                    ),
                  ),
                );
              }
              return CustomScrollView(
                slivers: [
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Text("Publicando como:"),
                          ),
                          Container(
                            height: 55,
                            child: _buildPetPickerOption(
                              controller.originalPost.value.pet,
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: Text("Descrição:"),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: CommentTextField(
                              controller: controller.textController,
                              focusNode: controller.textFocusNode,
                              hintText: "Descreva o post (Opcional)",
                              minLines: 4,
                              onChanged: (value) {
                                controller.description.value = value;
                              },
                            ),
                          ),
                          if (controller.petsToMention.isNotEmpty) _petToMentions(),
                          // if (controller.petsToMention.isEmpty) _buildImagePickerSection(),
                          // For security reasons, the edit image/video for posts was removed.
                          // If in, future updates, the team want to add this feature again,
                          // all that you have to do is to uncomment the first line
                        ],
                      ),
                    ),
                  ),
                  if (controller.petsToMention.isEmpty)
                    SliverFillRemainingBoxAdapter(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
                        alignment: Alignment.bottomCenter,
                        child: SafeArea(
                          child: Obx(
                            () => LargeTextButton(
                              loading: controller.editing.value,
                              text: "SALVAR",
                              onPressed: controller.canEdit ? controller.edit : null,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildPetPickerOption(Pet pet) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: EdgeInsets.only(right: 10),
          height: 35,
          width: 35,
          decoration: BoxDecoration(
            color: PatakiColors.white,
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(pet.imagePath.toString()),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Flexible(
          child: Text(
            pet.name,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Color(0xFF2B2B2B),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildImagePickerSection() {
    bool hasImage = controller.thumbnailImage.value != null ||
        (controller.originalPost.value.imagePath != null && !controller.removeImage.value);
    return !hasImage
        ? Container(
            height: 150,
            width: Get.width,
            alignment: Alignment.center,
            child: Column(
              children: [
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: () => Get.dialog(
                    ImagePickerDialog(
                      onCameraPressed: () => controller.getImage(ImageSource.camera),
                      onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                    ),
                  ),
                  child: Text("Enviar Imagem"),
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: () => Get.dialog(
                    ImagePickerDialog(
                      onCameraPressed: () => controller.getVideo(ImageSource.camera),
                      onGalleryPressed: () => controller.getVideo(ImageSource.gallery),
                    ),
                  ),
                  child: Text("Enviar Vídeo"),
                ),
              ],
            ),
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 150,
                width: 150,
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  child: _buildThumbnailImage(),
                ),
              ),
              Column(
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: () => Get.dialog(
                      ImagePickerDialog(
                        onCameraPressed: () => controller.getImage(ImageSource.camera),
                        onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                      ),
                    ),
                    child: Text("Enviar Outra Imagem"),
                  ),
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: () => Get.dialog(
                      ImagePickerDialog(
                        onCameraPressed: () => controller.getVideo(ImageSource.camera),
                        onGalleryPressed: () => controller.getVideo(ImageSource.gallery),
                      ),
                    ),
                    child: Text("Enviar Outro Vídeo"),
                  ),
                  FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                    ),
                    onPressed: controller.removeCurrentImage,
                    child: Text("Remover Imagem"),
                  )
                ],
              )
            ],
          );
  }

  _buildThumbnailImage() {
    if (controller.thumbnailImage.value != null) return Image.file(controller.thumbnailImage.value);

    FileType fileType = getFileType(controller.originalPost.value.imagePath);

    if (fileType == FileType.image) {
      return Image.network(controller.originalPost.value.imagePath.toString());
    } else if (fileType == FileType.video) {
      return StaticMediaPlayer(videoPath: controller.originalPost.value.imagePath.toString());
    }

    return Container();
  }

  Widget _petToMentions() {
    return ListView(
      shrinkWrap: true,
      children: [
        BuildCard(
          children: controller.petsToMention
              .map(
                (pet) => BuildPet(
                  pet: pet,
                  onPressed: () {
                    controller.replaceMentionAndSave(pet);
                  },
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
