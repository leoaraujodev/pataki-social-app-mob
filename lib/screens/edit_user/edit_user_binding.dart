import 'package:get/get.dart';
import 'package:pataki/screens/edit_user/edit_user_controller.dart';

class EditUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditUserController());
  }
}
