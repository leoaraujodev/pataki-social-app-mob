import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/core/endpoints/edit_user_endpoint.dart';
import 'package:pataki/core/models/edit_user_request.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/screens/validators/email_validator.dart';
import 'package:pataki/screens/validators/name_validator.dart';

class EditUserController extends GetxController with EmailValidator, NameValidator {
  final UserModel originalUser = Get.arguments["user"];
  final formKey = GlobalKey<FormState>();
  final image = Rx<File>();
  final picker = ImagePicker();
  final descriptionFocusNode = FocusNode();
  final isDescriptionValid = false.obs;
  final descriptionAutovalidateMode = AutovalidateMode.disabled.obs;
  final descriptionController = TextEditingController();

  final submitting = false.obs;

  @override
  onInit() {
    nameController.text = originalUser.name;
    nameValidator(nameController.text);
    nameOnChanged(nameController.text);

    emailController.text = originalUser.email;
    emailValidator(emailController.text);
    emailOnChanged(emailController.text);

    descriptionController.text = originalUser.desc ?? '';
    descriptionValidator(descriptionController.text);
    descriptionOnChanged(descriptionController.text);

    nameFocusNode.addListener(() {
      if (!nameFocusNode.hasFocus) {
        nameValidator(nameController.text);
        nameOnChanged(nameController.text);
      }
    });
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailValidator(emailController.text);
        emailOnChanged(emailController.text);
      }
    });
    descriptionFocusNode.addListener(() {
      if (!descriptionFocusNode.hasFocus) {
        descriptionValidator(descriptionController.text);
        descriptionOnChanged(descriptionController.text);
      }
    });
    super.onInit();
  }

  String descriptionValidator(String value) {
    descriptionAutovalidateMode.value = AutovalidateMode.always;

    return null;
  }

  void descriptionOnChanged(String value) {
    if (descriptionAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = descriptionValidator(value);
    if (error == null) {
      isDescriptionValid.value = true;
    } else {
      isDescriptionValid.value = false;
    }
  }

  Future<void> submit() async {
    nameValidator(nameController.text);
    emailValidator(emailController.text);
    descriptionValidator(descriptionController.text);

    nameOnChanged(nameController.text);
    emailOnChanged(emailController.text);
    descriptionOnChanged(descriptionController.text);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seus dados, um ou mais campos apresentam erros");
      return;
    }

    if (submitting.value == true) return;
    submitting.value = true;

    var request = EditUserRequest(
      image: image.value,
      name: nameController.text,
      email: emailController.text,
      description: descriptionController.text,
    );

    try {
      var userResponse = await EditUserEndpoint(request).call();

      Get.back(result: userResponse.user);
      Get.snackbar("Sucesso", "Dados atualizados com sucesso");
    } catch (exception) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
    submitting.value = false;
  }

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      image.value = File(pickedFile.path);
    }
  }

  @override
  void onClose() {
    nameFocusNode.dispose();
    emailFocusNode.dispose();
    descriptionFocusNode.dispose();
    super.onClose();
  }
}
