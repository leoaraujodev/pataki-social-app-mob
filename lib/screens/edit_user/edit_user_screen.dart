import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/screens/edit_user/edit_user_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';

class EditUserScreen extends GetView<EditUserController> {
  EditUserScreen({Key key}) : super(key: key);
  static String id = "edit_user_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Editar Conta",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildImagePicker(),
                      FormTextField(
                        focusNode: controller.nameFocusNode,
                        controller: controller.nameController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.nameAutovalidateMode.value,
                          isValid: controller.isNameValid.value,
                        ),
                        hintText: "Nome",
                        autovalidateMode: controller.nameAutovalidateMode.value,
                        validator: controller.nameValidator,
                        onFieldSubmitted: (value) {
                          controller.nameValidator(value);
                          controller.nameOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.emailFocusNode);
                        },
                        onChanged: controller.nameOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.emailFocusNode,
                        controller: controller.emailController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.emailAutovalidateMode.value,
                          isValid: controller.isEmailValid.value,
                        ),
                        hintText: "Email",
                        autovalidateMode: controller.emailAutovalidateMode.value,
                        validator: controller.emailValidator,
                        onFieldSubmitted: (value) {
                          controller.emailValidator(value);
                          controller.emailOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.descriptionFocusNode);
                        },
                        onChanged: controller.emailOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.descriptionFocusNode,
                        controller: controller.descriptionController,
                        hintText: "Descrição",
                        autovalidateMode: controller.descriptionAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.descriptionAutovalidateMode.value,
                          isValid: controller.isDescriptionValid.value,
                        ),
                        validator: controller.descriptionValidator,
                        onFieldSubmitted: (value) {
                          controller.descriptionValidator(value);
                          controller.descriptionOnChanged(value);
                          FocusScope.of(context).unfocus();
                        },
                        onChanged: controller.descriptionOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                      ),
                      LargeTextButton(
                        loading: controller.submitting.value,
                        onPressed: controller.submit,
                        text: 'SALVAR',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildImagePicker() {
    return controller.image.value == null && controller.originalUser.imagePath == null
        ? GestureDetector(
            onTap: () => Get.dialog(
              ImagePickerDialog(
                onCameraPressed: () => controller.getImage(ImageSource.camera),
                onGalleryPressed: () => controller.getImage(ImageSource.gallery),
              ),
            ),
            child: Container(
              height: 150,
              width: 150,
              margin: EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFC7D9EB)),
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: Icon(
                Icons.add_a_photo,
                size: 50,
                color: PatakiColors.gray,
              ),
            ),
          )
        : Container(
            height: 150,
            width: 150,
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xFFC7D9EB)),
              borderRadius: BorderRadius.all(Radius.circular(6)),
              image: DecorationImage(
                image: controller.image.value == null
                    ? NetworkImage(controller.originalUser.imagePath)
                    : FileImage(controller.image.value),
                fit: BoxFit.cover,
              ),
            ),
            alignment: Alignment.bottomCenter,
            child: FlatButton(
              shape: StadiumBorder(),
              color: Colors.white.withOpacity(0.75),
              onPressed: () => Get.dialog(
                ImagePickerDialog(
                  onCameraPressed: () => controller.getImage(ImageSource.camera),
                  onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.cached),
                  SizedBox(width: 10),
                  Text("Alterar"),
                ],
              ),
            ),
          );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
