import 'package:get/get.dart';
import 'package:pataki/screens/following/following_controller.dart';

class FollowingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FollowingController());
  }
}
