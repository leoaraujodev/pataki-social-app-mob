import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/following_endpoint.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_list_response.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/models/user_model.dart';

class FollowingController extends GetxController {
  final int _userId = int.parse(Get.parameters["userId"]);

  final textController = TextEditingController();
  final RxList<UserModel> tutors = RxList([]);
  final RxList<Pet> pets = RxList([]);
  final RxList<Pet> petsSearched = RxList([]);
  final searching = true.obs;
  final searchingError = false.obs;
  final currentSearchTerm = RxString();

  @override
  void onInit() {
    getFollowing();
    debounce(currentSearchTerm, newSearch);
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getFollowing() async {
    searching.value = true;
    searchingError.value = false;

    try {
      PetListResponse response = await FollowingEndpoint(UserInfoRequest(userId: _userId)).call();

      pets.addAll(response.pets);
      petsSearched.addAll(response.pets);
    } catch (e) {
      searchingError.value = true;
      _showErrorSnackbar();
    }

    searching.value = false;
  }

  newSearch(String term) {
    petsSearched.clear();

    for (final pet in pets) {
      if (pet.name.toLowerCase().contains(term.toLowerCase())) {
        petsSearched.add(pet);
      }
    }

    petsSearched.refresh();
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
