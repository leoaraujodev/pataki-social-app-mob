import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/following/following_controller.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class FollowingScreen extends GetView<FollowingController> {
  static String routeName(userId) => "/following_screen/$userId";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Seguindo",
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: CommentTextField(
              controller: controller.textController,
              maxLines: 1,
              textInputAction: TextInputAction.search,
              hintText: "Buscar",
              onChanged: (String value) => controller.currentSearchTerm.value = value,
            ),
          ),
          Expanded(
            child: Obx(
              () {
                if (controller.searching.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                return ListView(
                  shrinkWrap: true,
                  children: [
                    BuildCard(
                      children: controller.petsSearched
                          .map(
                            (pet) => BuildPet(pet: pet),
                          )
                          .toList(),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
