import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/forgot_password_endpoint.dart';
import 'package:pataki/core/models/forgot_password_request.dart';
import 'package:pataki/core/models/pataki_exception.dart';
import 'package:pataki/screens/validators/email_validator.dart';

class ForgotPassController extends GetxController with EmailValidator {
  final formKey = GlobalKey<FormState>();
  final submitting = false.obs;

  final Map<String, String> responses = {
    "Verify your email account": "Email enviado com sucesso",
    "User not found": "Usuário não encontrado",
  };

  @override
  onInit() {
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasPrimaryFocus) {
        emailValidator(emailController.text);
        emailOnChanged(emailController.text);
      }
    });
    super.onInit();
  }

  Future<void> submit() async {
    emailValidator(emailController.text);
    emailOnChanged(emailController.text);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seu email, o campo apresenta erros");
      return;
    }
    submitting.value = true;
    try {
      await ForgotPasswordEndpoint(ForgotPasswordRequest(email: emailController.text)).call();

      Get.back();
      Get.snackbar("Sucesso", "Email enviado com sucesso");
    } catch (e) {
      if (e is PatakiException && e.message == "User not found") {
        Get.snackbar("Falha", "Usuário não encontrado");
        return;
      }
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    submitting.value = false;
  }

  @override
  void onClose() {
    emailFocusNode.dispose();
    super.onClose();
  }
}
