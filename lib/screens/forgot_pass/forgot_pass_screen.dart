import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/forgot_pass/forgot_pass_controller.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';

class ForgotPassScreen extends StatelessWidget {
  ForgotPassScreen({Key key}) : super(key: key);
  static String id = "forgot_pass_screen";

  final controller = Get.put(ForgotPassController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Recuperar Senha",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: FormTextField(
                          focusNode: controller.emailFocusNode,
                          controller: controller.emailController,
                          checkMark: _buildCheckMark(
                            autovalidateMode: controller.emailAutovalidateMode.value,
                            isValid: controller.isEmailValid.value,
                          ),
                          hintText: "Email",
                          autovalidateMode: controller.emailAutovalidateMode.value,
                          validator: controller.emailValidator,
                          onFieldSubmitted: (value) {
                            controller.emailValidator(value);
                            controller.emailOnChanged(value);
                          },
                          onChanged: controller.emailOnChanged,
                          textInputAction: TextInputAction.next,
                        ),
                      ),
                      LargeTextButton(
                        loading: controller.submitting.value,
                        onPressed: controller.submit,
                        text: 'ENVIAR',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
