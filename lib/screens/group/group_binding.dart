import 'package:get/get.dart';
import 'package:pataki/screens/group/group_controller.dart';

class GroupBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(GroupController(), tag: Get.arguments["timestamp"]);
  }
}
