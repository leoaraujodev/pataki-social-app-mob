import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/group_add_member_endpoint.dart';
import 'package:pataki/core/endpoints/group_details_endpoint.dart';
import 'package:pataki/core/endpoints/group_leave_endpoint.dart';
import 'package:pataki/core/endpoints/group_remove_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/group_add_member_request.dart';
import 'package:pataki/core/models/group_details.dart';
import 'package:pataki/core/models/group_leave_request.dart';
import 'package:pataki/core/models/group_remove_request.dart';
import 'package:pataki/core/models/group_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/member.dart';

class GroupController extends GetxController {
  final Rx<GroupDetails> groupDetails = GroupDetails(
    group: Group(
      id: int.tryParse(Get.parameters["groupId"]),
    ),
  ).obs;

  RxInt loggedUserId = 0.obs;

  Rx<Member> loggedMember = Rx<Member>();

  final scrollController = ScrollController();

  final loadingGroup = true.obs;
  final errorOnGroup = false.obs;

  final hasMorePosts = false.obs;
  final errorOnMorePosts = false.obs;

  int _lastId;
  bool _likingMutex = false;
  bool _fetchingMutex = false;

  @override
  void onInit() {
    getGroupDetails(true);

    scrollController.addListener(() {
      if (!scrollController.hasClients) {
        return;
      }

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMorePosts.value && !errorOnMorePosts.value) getMorePosts();
      }
    });
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getGroupDetails(bool updateAll) async {
    errorOnGroup.value = false;
    loadingGroup.value = true;

    try {
      loggedUserId.value = await GetUserIdInteractor().execute();

      final response = await GroupDetailsEndpoint(
        GroupRequest(groupId: groupDetails.value.group.id),
      ).call();

      updateAll ? setGroupDetails(response) : setGroup(response);
    } catch (e) {
      errorOnGroup.value = true;
      _showErrorSnackbar();
    }

    loadingGroup.value = false;
  }

  setGroupDetails(GroupDetails response) {
    if (response != null) {
      groupDetails.value = response;

      groupDetails.refresh();

      if (groupDetails.value.group.isMember) {
        loggedMember.value =
            groupDetails.value.members.firstWhere((member) => member.user.id == loggedUserId.value);

        loggedMember.refresh();
      }

      if (response.posts.isNotEmpty) _lastId = response.posts.last.id;

      if (response.posts.length < 10) {
        hasMorePosts.value = false;
      } else {
        hasMorePosts.value = true;
      }
    }
  }

  setGroup(GroupDetails response) {
    if (response != null) {
      groupDetails.value.group = response.group;

      groupDetails.refresh();

      if (groupDetails.value.group.isMember) {
        loggedMember.value =
            groupDetails.value.members.firstWhere((member) => member.user.id == loggedUserId.value);

        loggedMember.refresh();
      }
    }
  }

  Future<void> addGroupMember() async {
    try {
      final response = await GroupAddMemberEndpoint(
        GroupAddMemberRequest(
          groupId: groupDetails.value.group.id,
          userId: loggedUserId.value,
        ),
      ).call();

      Get.back();

      if (response != null && response.profile == 0) {
        getGroupDetails(true);
        Get.snackbar("Pronto", "Você entrou neste grupo.");
      } else {
        _showErrorSnackbar();
      }
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  Future<void> leaveGroup() async {
    try {
      final response = await GroupLeaveEndpoint(
        GroupLeaveRequest(
          groupId: groupDetails.value.group.id,
          userId: loggedUserId.value,
        ),
      ).call();

      Get.back();
      Get.back();
      Get.back();
      if (response != null) {
        Get.snackbar("Pronto", response.message);
      } else {
        _showErrorSnackbar();
      }
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  Future<void> deleteGroup() async {
    try {
      final response = await GroupRemoveEndpoint(
        GroupRemoveRequest(
          groupId: groupDetails.value.group.id,
        ),
      ).call();

      Get.back();
      Get.back();
      Get.back();
      if (response != null) {
        Get.snackbar("Pronto", response.message);
      } else {
        _showErrorSnackbar();
      }
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousLastId = _lastId;

    try {
      _lastId = null;
      final response = await _getGroupPostsResponse();
      groupDetails.value.posts.clear();
      _setValues(response);
    } catch (e) {
      _lastId = previousLastId;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMorePosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMorePosts.value = false;

    try {
      final response = await _getGroupPostsResponse();
      _setValues(response);
    } catch (e) {
      errorOnMorePosts.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<GroupDetails> _getGroupPostsResponse() =>
      GroupDetailsEndpoint(GroupRequest(groupId: groupDetails.value.group.id, lastId: _lastId))
          .call();

  void _setValues(GroupDetails response) {
    groupDetails.value.posts.addAll(response.posts);

    groupDetails.refresh();
    if (response.posts.isNotEmpty) {
      _lastId = response.posts.last.id;
    }

    if (response.posts.length < 10) {
      hasMorePosts.value = false;
    } else {
      hasMorePosts.value = true;
    }
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = groupDetails.value.posts.indexWhere((timeline) => timeline.id == timelineId);

      groupDetails.value.posts[index] = groupDetails.value.posts[index].copyWith(
        totLikes: like
            ? groupDetails.value.posts[index].totLikes + 1
            : groupDetails.value.posts[index].totLikes - 1,
        liked: like,
        imagePath: groupDetails.value.posts[index].imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();

      groupDetails.refresh();
    } catch (e) {
      int index = groupDetails.value.posts.indexWhere((timeline) => timeline.id == timelineId);

      groupDetails.value.posts[index] = groupDetails.value.posts[index].copyWith(
        totLikes: !like
            ? groupDetails.value.posts[index].totLikes + 1
            : groupDetails.value.posts[index].totLikes - 1,
        liked: !like,
        imagePath: groupDetails.value.posts[index].imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      int index = groupDetails.value.posts.indexWhere((post) => post.id == id);

      groupDetails.value.posts.removeAt(index);

      groupDetails.refresh();

      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  void updateCommentCount(int timelineId, int newCount) {
    int index = groupDetails.value.posts.indexWhere((post) => post.id == timelineId);

    if (index == -1) return;

    groupDetails.value.posts[index] = groupDetails.value.posts[index].copyWith(
      imagePath: groupDetails.value.posts[index].imagePath,
      totComment: newCount,
    );

    groupDetails.refresh();
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
