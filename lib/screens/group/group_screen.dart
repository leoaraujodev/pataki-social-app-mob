import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/create_group_post/create_group_post_screen.dart';
import 'package:pataki/screens/edit_group/edit_group_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/group/group_controller.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/member_list/member_list_screen.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';

class GroupScreen extends GetView<GroupController> {
  static String routeName(groupId) => "/group_screen/$groupId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(
          title: controller.groupDetails.value.group.name ?? "",
          centerTitle: true,
          actions: [
            _buildMoreOptions(),
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: Obx(
                () {
                  if (controller.loadingGroup.value) {
                    return Center(child: RefreshProgressIndicator());
                  }
                  if (controller.errorOnGroup.value) {
                    return Center(
                      child: GestureDetector(
                        onTap: () => controller.getGroupDetails(true),
                        child: RefreshProgressIndicator(
                          value: 1.0,
                        ),
                      ),
                    );
                  }

                  return RefreshIndicator(
                    child: ListView(
                      physics: AlwaysScrollableScrollPhysics(),
                      controller: controller.scrollController,
                      children: [
                        _buildImage(controller.groupDetails.value.group.imagePath),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 18, vertical: 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildDesc(),
                              _buildMembers(),
                              if (controller.groupDetails.value.group.isMember) _buildCreatePost(),
                            ],
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: controller.groupDetails.value.posts.length,
                          itemBuilder: (context, index) {
                            Widget card = TimelinePostCard(
                              post: controller.groupDetails.value.posts[index],
                              onCommentPressed: () => Get.toNamed(
                                CommentScreen.routeName(
                                    controller.groupDetails.value.posts[index].id),
                                arguments: {
                                  "updateCommentCount": controller.updateCommentCount,
                                  "timestamp": DateTime.now().toIso8601String(),
                                },
                              ),
                              onLikePressed: () => controller.likeComment(
                                controller.groupDetails.value.posts[index].id,
                                !controller.groupDetails.value.posts[index].liked,
                              ),
                              onLikesPressed: () => Get.toNamed(
                                LikesScreen.routeName(
                                    controller.groupDetails.value.posts[index].id),
                                arguments: {"timestamp": DateTime.now().toIso8601String()},
                              ),
                              onMorePressed: () => Get.dialog(
                                OptionsDialog(
                                  title: "Opções da Postagem",
                                  onReportPressed: controller.loggedUserId.value !=
                                          controller.groupDetails.value.posts[index].user.id
                                      ? () async {
                                          int reportedPostId =
                                              controller.groupDetails.value.posts[index].id;

                                          var reported = await Get.offNamed(
                                            ReportTimelinePostScreen.routeName,
                                            arguments: {
                                              "post": controller.groupDetails.value.posts[index]
                                            },
                                          );

                                          if (reported == true) {
                                            controller.groupDetails.value.posts.removeWhere(
                                              (post) => post.id == reportedPostId,
                                            );
                                          }
                                        }
                                      : null,
                                  onEditPressed: controller.loggedUserId.value ==
                                          controller.groupDetails.value.posts[index].user.id
                                      ? () async {
                                          await Get.offNamed(
                                            EditPostScreen.routeName(
                                                controller.groupDetails.value.posts[index].id),
                                          );
                                          controller.refreshPosts();
                                        }
                                      : null,
                                  onDeletePressed: controller.loggedUserId.value ==
                                          controller.groupDetails.value.posts[index].user.id
                                      ? () {
                                          Get.back();
                                          Get.dialog(
                                            ConfirmDialog(
                                              onConfirmPressed: () => controller.deletePost(
                                                controller.groupDetails.value.posts[index].id,
                                              ),
                                            ),
                                          );
                                        }
                                      : null,
                                ),
                              ),
                              onSendPressed: () {},
                              onUserPressed: () {
                                Get.toNamed(
                                  PetScreen.routeName(
                                      controller.groupDetails.value.posts[index].pet.id),
                                  arguments: {"timestamp": DateTime.now().toIso8601String()},
                                );
                              },
                            );

                            if (index == controller.groupDetails.value.posts.length - 1 &&
                                controller.hasMorePosts.value) {
                              return Column(
                                children: [
                                  card,
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 20),
                                    child: Obx(
                                      () => controller.errorOnMorePosts.value
                                          ? Center(
                                              child: GestureDetector(
                                                onTap: controller.getMorePosts,
                                                child: RefreshProgressIndicator(
                                                  value: 1.0,
                                                ),
                                              ),
                                            )
                                          : RefreshProgressIndicator(),
                                    ),
                                  ),
                                ],
                              );
                            }

                            return card;
                          },
                        ),
                      ],
                    ),
                    onRefresh: controller.refreshPosts,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildMoreOptions() {
    return Padding(
      padding: const EdgeInsets.only(right: 22.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Material(
            type: MaterialType.transparency,
            child: Ink(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: InkWell(
                borderRadius: BorderRadius.circular(1000.0),
                child: Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Icon(
                    Icons.more_vert,
                    size: 20.0,
                  ),
                ),
                onTap: () {
                  Get.dialog(
                    OptionsDialog(
                      title: "Opções do Grupo",
                      onEnterPressed: !controller.groupDetails.value.group.isMember
                          ? () => controller.addGroupMember()
                          : null,
                      onLeavePressed: controller.groupDetails.value.group.isMember
                          ? () => Get.dialog(
                                ConfirmDialog(
                                  onConfirmPressed: () => controller.leaveGroup(),
                                ),
                              )
                          : null,
                      onDeletePressed: (controller.groupDetails.value.group.isMember &&
                              controller.loggedMember.value.profile == 1)
                          ? () => Get.dialog(
                                ConfirmDialog(
                                  onConfirmPressed: () => controller.deleteGroup(),
                                ),
                              )
                          : null,
                      onEditPressed: (controller.groupDetails.value.group.isMember &&
                              controller.loggedMember.value.profile == 1)
                          ? () async {
                              Get.back();
                              final result = await Get.toNamed(
                                EditGroupScreen.id,
                                arguments: {"group": controller.groupDetails.value.group},
                              );

                              if (result == true) {
                                controller.getGroupDetails(false);
                              }
                            }
                          : null,
                      onSharePressed: () {
                        Clipboard.setData(
                          new ClipboardData(
                            text:
                                "https://pataki.com.br/#/group_screen/${controller.groupDetails.value.group.id}",
                          ),
                        );
                        Get.back();
                      },
                      shareType: "grupo",
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage(String imagePath) {
    return Container(
      width: Get.width,
      child: Image.network(
        imagePath ?? "",
        fit: BoxFit.fitWidth,
      ),
    );
  }

  Widget _buildDesc() {
    return Text(
      controller.groupDetails.value.group.desc ?? "",
      style: TextStyle(fontSize: 17, color: Color(0xFF66839F)),
    );
  }

  Widget _buildMembers() {
    return BuildCard(
      title: "Membros",
      titleHorizontalPadding: 0,
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          children: controller.groupDetails.value.members
              .map(
                (member) => Flexible(
                  child: FractionallySizedBox(
                    alignment: Alignment.center,
                    widthFactor: controller.groupDetails.value.members.length / 5,
                    child: Container(
                        padding: EdgeInsets.all(2),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(1000),
                          child: _buildImage(
                            member.user.imagePath,
                          ),
                        )),
                  ),
                ),
              )
              .toList(),
        ),
      ],
      onTap: () => Get.toNamed(
        MemberListScreen.routeName,
        arguments: {
          'groupId': controller.groupDetails.value.group.id,
          "timestamp": DateTime.now().toIso8601String(),
        },
      ),
    );
  }

  _buildCreatePost() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          width: 60,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(1000)),
            child: _buildImage(controller.loggedMember.value.user.imagePath),
          ),
        ),
        SizedBox(width: 16),
        Expanded(
          child: Material(
            type: MaterialType.transparency,
            child: Ink(
              decoration: BoxDecoration(
                border: Border.all(
                  color: PatakiColors.lightGray,
                  width: 2,
                ),
                borderRadius: BorderRadius.all(Radius.circular(1000)),
              ),
              child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(1000)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 18),
                  child: Text(
                    "Compartilhe algo",
                    style: TextStyle(
                      fontSize: 15,
                      color: PatakiColors.gray,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                onTap: () async {
                  final post = await Get.toNamed(
                    CreateGroupPostScreen.routeName,
                    arguments: {'groupId': controller.groupDetails.value.group.id},
                  );

                  if (post != null) {
                    controller.groupDetails.value.posts.insert(0, post);
                    controller.groupDetails.refresh();
                  }
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
