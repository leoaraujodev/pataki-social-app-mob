import 'package:get/get.dart';
import 'package:pataki/screens/group_list/group_list_controller.dart';

class GroupListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(GroupListController(), tag: Get.arguments["timestamp"]);
  }
}
