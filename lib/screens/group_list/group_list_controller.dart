import 'package:get/get.dart';
import 'package:pataki/core/endpoints/group_list_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/group_list_request.dart';
import 'package:pataki/core/models/group_list_response.dart';

class GroupListController extends GetxController {
  int userId = int.parse(Get.parameters["userId"]);
  int loggedUserId;
  final bool isFromMyProfile = Get.arguments["isFromMyProfile"];

  final RxList<Group> groupList = RxList([]);

  final groupListIsEmpty = true.obs;
  final edittingMode = false.obs;

  final loadingGroupList = false.obs;
  final errorOnGroupList = true.obs;

  @override
  void onInit() {
    getGroupList();
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getGroupList() async {
    errorOnGroupList.value = false;
    loadingGroupList.value = true;

    groupList.clear();

    try {
      loggedUserId = await GetUserIdInteractor().execute();

      GroupListResponse response = await GroupListEndpoint(GroupListRequest(userId: userId)).call();

      if (response.groups.isNotEmpty) {
        groupListIsEmpty.value = false;
        groupList.addAll(response.groups);
      }
    } catch (e) {
      errorOnGroupList.value = true;
      _showErrorSnackbar();
    }

    loadingGroupList.value = false;
  }

  @override
  void onClose() {
    super.onClose();
  }
}
