import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/create_group/create_group_screen.dart';
import 'package:pataki/screens/group/group_screen.dart';
import 'package:pataki/screens/group_list/group_list_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_group.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class GroupListScreen extends GetView<GroupListController> {
  static String routeName(userId) => "/group_list_screen/$userId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Grupos",
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 22.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(),
                if (controller.isFromMyProfile)
                  Material(
                    type: MaterialType.transparency,
                    child: Ink(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: PatakiColors.orange,
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(1000.0),
                        child: Padding(
                          padding: EdgeInsets.all(6.0),
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 20.0,
                          ),
                        ),
                        onTap: () async {
                          final refresh = await Get.toNamed(CreateGroupScreen.id);

                          if (refresh) {
                            controller.getGroupList();
                          }
                        },
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingGroupList.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnGroupList.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getGroupList,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                if (controller.groupListIsEmpty.value) {
                  return Center(
                    child: Text(
                      "Este usuário não participa de nenhum grupo",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey,
                      ),
                    ),
                  );
                }

                return ListView(
                  shrinkWrap: true,
                  children: [
                    BuildCard(
                      children: controller.groupList
                          .map(
                            (group) => BuildGroup(
                              group: group,
                              loggedUserId: controller.loggedUserId,
                              onPressed: () async {
                                await Get.toNamed(
                                  GroupScreen.routeName(group.id),
                                  arguments: {
                                    "timestamp": DateTime.now().toIso8601String(),
                                  },
                                );

                                controller.getGroupList();
                              },
                            ),
                          )
                          .toList(),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
