import 'package:get/get.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_binding.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/my_profile/my_profile_binding.dart';
import 'package:pataki/screens/notifications/notifications_binding.dart';
import 'package:pataki/screens/search/search_binding.dart';
import 'package:pataki/screens/timeline/timeline_binding.dart';
import 'package:pataki/screens/training/training_binding.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController());
    TimelineBinding().dependencies();
    SearchBinding().dependencies();
    TrainingBinding().dependencies();
    MyProfileBinding().dependencies();
    NotificationsBinding().dependencies();
    ChatRoomsBinding().dependencies();
  }
}
