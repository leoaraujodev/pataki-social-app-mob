import 'package:get/get.dart';
import 'package:pataki/core/endpoints/set_notify_token_endpoints.dart';
import 'package:pataki/core/endpoints/user_info_endpoint.dart';
import 'package:pataki/core/interactor/get_uri_interactor.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/interactor/set_uri_interactor.dart';
import 'package:pataki/core/models/set_notify_token_request.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/models/user_info_response.dart';
import 'package:pataki/core/services/notification_service.dart';

class HomeController extends GetxController {
  final currentIndex = 0.obs;
  RxBool doRefresh = false.obs;

  void changePage(int _index) => currentIndex.value = _index;

  @override
  Future<void> onInit() async {
    _setNotifyToken();
    super.onInit();
    redirectPageTo();
  }

  Future<void> _setNotifyToken() async {
    String lastRegisteredOneSignalUserId = await _getLastRegisteredOneSignalUserId();
    String currentOneSignalUserId = await Get.find<NotificationService>().getUserId();

    bool oneSignalUserIdHasChanged = lastRegisteredOneSignalUserId != currentOneSignalUserId;

    if (oneSignalUserIdHasChanged) {
      SetNotifyTokenEndpoint(SetNotifyTokenRequest(onesignalToken: currentOneSignalUserId)).call();
    }
  }

  Future<String> _getLastRegisteredOneSignalUserId() async {
    int userId = await GetUserIdInteractor().execute();
    UserInfoResponse response = await UserInfoEndpoint(UserInfoRequest(userId: userId)).call();

    return response.user.onesiginalToken;
  }

  redirectPageTo() async {
    final String link = await GetUriInteractor().execute();

    if (link == null || link.isEmpty) {
      return;
    }

    await SetUriInteractor().execute("");

    Get.toNamed(
      link,
      arguments: {"timestamp": DateTime.now().toIso8601String()},
    );
  }
}
