import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_controller.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/my_profile/my_profile_screen.dart';
import 'package:pataki/screens/notifications/notifications_screen.dart';
import 'package:pataki/screens/search/search_screen.dart';
import 'package:pataki/screens/timeline/timeline_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class HomeScreen extends GetView<HomeController> {
  static String id = "home_screen";
  final chatRoomsController = Get.find<ChatRoomsController>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        body: _pages[controller.currentIndex.value],
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: controller.currentIndex.value,
          type: BottomNavigationBarType.fixed,
          onTap: controller.changePage,
          selectedLabelStyle: TextStyle(height: 0, fontSize: 0),
          unselectedLabelStyle: TextStyle(height: 0, fontSize: 0),
          items: _items,
        ),
      ),
    );
  }

  List<Widget> get _pages => [
        TimelineScreen(),
        SearchScreen(),
        NotificationsScreen(),
        MyProfileScreen(),
      ];

  List<BottomNavigationBarItem> get _items => [
        _buildItem(
          iconPath: 'images/timeline_icon.svg',
          label: "News",
        ),
        _buildItem(
          iconPath: 'images/search_icon.svg',
          label: "Pesquisa",
        ),
        _buildItem(
          iconPath: 'images/notification_icon.svg',
          label: "Notificações",
          badge: chatRoomsController.hasNewMessages.value,
        ),
        _buildItem(
          iconPath: 'images/profile_icon.svg',
          label: "Perfil",
        ),
      ];

  _buildItem({String iconPath, String label, bool badge = false}) {
    return BottomNavigationBarItem(
      icon: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(vertical: 16),
        height: 48,
        width: 48,
        child: badge
            ? _buildBadge(
                child: SvgPicture.asset(iconPath),
                color: PatakiColors.orange,
              )
            : SvgPicture.asset(iconPath),
      ),
      label: label,
      activeIcon: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(vertical: 16),
        height: 48,
        width: 48,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: PatakiColors.orange,
        ),
        child: badge
            ? _buildBadge(
                child: SvgPicture.asset(iconPath, color: Colors.white),
                color: Colors.white,
              )
            : SvgPicture.asset(iconPath, color: Colors.white),
      ),
    );
  }

  _buildBadge({Widget child, Color color}) {
    return Stack(
      alignment: Alignment.topRight,
      children: [
        child,
        Container(
          height: 10,
          width: 10,
          margin: EdgeInsets.all(6),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
        ),
      ],
    );
  }
}
