import 'package:get/get.dart';
import 'package:pataki/screens/likes/likes_controller.dart';

class LikesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LikesController(), tag: Get.arguments["timestamp"]);
  }
}
