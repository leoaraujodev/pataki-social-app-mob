import 'package:get/get.dart';
import 'package:pataki/core/endpoints/single_timeline_endpoint.dart';
import 'package:pataki/core/models/single_timeline_request.dart';
import 'package:pataki/core/models/timeline_post.dart';

class LikesController extends GetxController {
  final post = Rx<TimelinePost>();

  final loadingPost = true.obs;
  final errorOnPost = false.obs;

  @override
  void onInit() {
    getPost();
    super.onInit();
  }

  Future<void> getPost() async {
    loadingPost.value = true;
    errorOnPost.value = false;

    try {
      post.value = await SingleTimelineEndpoint(
        SingleTimelineRequest(id: int.tryParse(Get.parameters["timelineId"])),
      ).call();
    } catch (e) {
      errorOnPost.value = true;
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    loadingPost.value = false;
  }
}
