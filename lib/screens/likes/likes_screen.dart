import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/screens/likes/likes_controller.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_tutor.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class LikesScreen extends GetView<LikesController> {
  static String routeName(timelineId) => "/likes_screen/$timelineId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Likes",
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (controller.loadingPost.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnPost.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getPost,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return ListView(
            shrinkWrap: true,
            children: [
              BuildCard(
                children: controller.post.value.likes
                    .map(
                      (like) => BuildTutor(
                        tutor: UserModel(imagePath: like.imagePath, name: like.name),
                        onPressed: () => Get.toNamed(
                          ProfileScreen.routeName(like.userId),
                          arguments: {"timestamp": DateTime.now().toIso8601String()},
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
          );
        },
      ),
    );
  }
}
