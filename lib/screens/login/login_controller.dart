import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/login_endpoint.dart';
import 'package:pataki/core/interactor/get_last_email_interactor.dart';
import 'package:pataki/core/interactor/set_token_interactor.dart';
import 'package:pataki/core/interactor/set_user_id_interactor.dart';
import 'package:pataki/core/models/login_request.dart';
import 'package:pataki/core/models/login_response.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/screens/home/home_screen.dart';
import 'package:pataki/screens/validators/email_validator.dart';

class LoginController extends GetxController with EmailValidator, Notifications {
  final formKey = GlobalKey<FormState>();

  final passwordFocusNode = FocusNode();
  final isPasswordValid = false.obs;
  final passwordAutovalidateMode = AutovalidateMode.disabled.obs;
  final passwordController = TextEditingController();
  final submitting = false.obs;

  @override
  onInit() async {
    emailController.text = await GetLastEmailInteractor().execute();

    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailValidator(emailController.text);
        emailOnChanged(emailController.text);
      }
    });
    passwordFocusNode.addListener(() {
      if (!passwordFocusNode.hasFocus) {
        passwordValidator(passwordController.text);
        passwordOnChanged(passwordController.text);
      }
    });
    super.onInit();
  }

  String passwordValidator(String value) {
    passwordAutovalidateMode.value = AutovalidateMode.always;
    if (value.isNotEmpty) {
      return null;
    }

    return 'A senha não pode estar em branco';
  }

  void passwordOnChanged(String value) {
    if (passwordAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = passwordValidator(value);
    if (error == null) {
      isPasswordValid.value = true;
    } else {
      isPasswordValid.value = false;
    }
  }

  Future<void> submit() async {
    emailValidator(emailController.text);
    passwordValidator(passwordController.text);

    passwordOnChanged(passwordController.text);
    emailOnChanged(emailController.text);

    if (formKey.currentState.validate()) {
      submitting.value = true;
      try {
        LoginResponse response = await LoginEndpoint(LoginRequest(
          username: emailController.text,
          password: passwordController.text,
        )).call();

        scheduleNotifications(response);

        await SetTokenInteractor().execute(response.token);
        await SetUserIdInteractor().execute(response.user.id);

        Get.offAllNamed(HomeScreen.id);
      } catch (e) {
        Get.snackbar("Erro", "Email e/ou senha incorretos");
      }
      submitting.value = false;
    }
  }

  Future<void> scheduleNotifications(LoginResponse response) async {
    if (response.petVaccines.isNotEmpty) {
      response.petVaccines.forEach((vaccine) async {
        await scheduleVaccineNotification(
          id: vaccine.id,
          petName: vaccine.pet.name,
          vaccineName: vaccine.name,
          notificationDate: vaccine.date,
        );
      });
    }
    if (response.petReminders.isNotEmpty) {
      response.petReminders.forEach((reminder) {
        scheduleReminderWeeklyOrDailyNotification(reminder: reminder, isPet: true);
      });
    }
    if (response.trickReminders.isNotEmpty) {
      response.trickReminders.forEach((reminder) {
        scheduleReminderWeeklyOrDailyNotification(reminder: reminder, isPet: false);
      });
    }
  }

  Future<void> scheduleReminderWeeklyOrDailyNotification({Reminder reminder, bool isPet}) async {
    final List<int> activedWeekdays = [];
    bool remindAllDays = true;

    reminder.onSun ? activedWeekdays.add(7) : remindAllDays = false;
    reminder.onMon ? activedWeekdays.add(1) : remindAllDays = false;
    reminder.onTue ? activedWeekdays.add(2) : remindAllDays = false;
    reminder.onWed ? activedWeekdays.add(3) : remindAllDays = false;
    reminder.onThu ? activedWeekdays.add(4) : remindAllDays = false;
    reminder.onFri ? activedWeekdays.add(5) : remindAllDays = false;
    reminder.onSat ? activedWeekdays.add(6) : remindAllDays = false;

    if (remindAllDays) {
      await scheduleReminderDailyNotification(
        id: reminder.id,
        title: isPet ? reminder.pet.name : "Hora do Treinamento",
        reminderName: reminder.name,
        time: reminder.time,
      );
    } else {
      activedWeekdays.forEach((int weekday) async {
        await scheduleReminderNotification(
          id: reminder.id,
          title: isPet ? reminder.pet.name : "Hora do Treinamento",
          reminderName: reminder.name,
          weekday: weekday,
          time: reminder.time,
        );
      });
    }
  }

  @override
  void onClose() {
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    super.onClose();
  }
}
