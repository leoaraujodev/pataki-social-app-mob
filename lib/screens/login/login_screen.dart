import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/interactor/set_last_email_interactor.dart';
import 'package:pataki/screens/forgot_pass/forgot_pass_screen.dart';
import 'package:pataki/screens/register/register_screen.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';
import 'login_controller.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/terms_bottom_navigation_bar.dart';

class LoginScreen extends GetView<LoginController> {
  LoginScreen({Key key}) : super(key: key);
  static String id = "login_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Bem-vindo!",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Form(
                      key: controller.formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          FormTextField(
                            focusNode: controller.emailFocusNode,
                            controller: controller.emailController,
                            checkMark: _buildCheckMark(
                              autovalidateMode: controller.emailAutovalidateMode.value,
                              isValid: controller.isEmailValid.value,
                            ),
                            hintText: "Email",
                            autovalidateMode: controller.emailAutovalidateMode.value,
                            validator: controller.emailValidator,
                            onFieldSubmitted: (value) {
                              controller.emailValidator(value);
                              controller.emailOnChanged(value);
                              FocusScope.of(context).requestFocus(controller.passwordFocusNode);
                            },
                            onChanged: (value) async {
                              controller.emailOnChanged(value);
                              await SetLastEmailInteractor().execute(value);
                            },
                            textInputAction: TextInputAction.next,
                          ),
                          FormTextField(
                            focusNode: controller.passwordFocusNode,
                            controller: controller.passwordController,
                            canObscure: true,
                            hintText: "Senha",
                            autovalidateMode: controller.passwordAutovalidateMode.value,
                            checkMark: _buildCheckMark(
                              autovalidateMode: controller.passwordAutovalidateMode.value,
                              isValid: controller.isPasswordValid.value,
                            ),
                            validator: controller.passwordValidator,
                            onFieldSubmitted: (value) {
                              controller.passwordValidator(value);
                              controller.passwordOnChanged(value);
                              FocusScope.of(context).unfocus();
                              controller.submit();
                            },
                            onChanged: controller.passwordOnChanged,
                            keyboardType: TextInputType.visiblePassword,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: LargeTextButton(
                              loading: controller.submitting.value,
                              onPressed: controller.submit,
                              text: 'ENTRAR',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: TextButton(
                        child: Text(
                          "Esqueci minha senha",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF66839F),
                          ),
                        ),
                        onPressed: () => Get.toNamed(ForgotPassScreen.id),
                      ),
                    ),
                    TextButton(
                      child: Text(
                        "CRIAR CONTA",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.orange,
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(RegisterScreen.id);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
          bottomNavigationBar: TermsBottomNavigationBar(),
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
