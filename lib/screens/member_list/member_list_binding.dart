import 'package:get/get.dart';
import 'package:pataki/screens/member_list/member_list_controller.dart';

class MemberListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MemberListController(), tag: Get.arguments["timestamp"]);
  }
}
