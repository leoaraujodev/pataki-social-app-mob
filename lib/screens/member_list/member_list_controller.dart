import 'package:get/get.dart';
import 'package:pataki/core/endpoints/group_details_endpoint.dart';
import 'package:pataki/core/endpoints/group_leave_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/group_details.dart';
import 'package:pataki/core/models/group_leave_request.dart';
import 'package:pataki/core/models/group_request.dart';
import 'package:pataki/core/models/member.dart';

class MemberListController extends GetxController {
  final int groupId = Get.arguments["groupId"];

  final RxList<Member> members = RxList<Member>();
  final Rx<GroupDetails> groupDetails = Rx<GroupDetails>();
  final Rx<Member> loggedMember = Rx<Member>();

  final loadingUserInfo = true.obs;
  final errorOnUserInfo = false.obs;

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getUserInfo();
    super.onInit();
  }

  Future<void> getUserInfo() async {
    loadingUserInfo.value = true;
    errorOnUserInfo.value = false;

    try {
      int loggedUserId = await GetUserIdInteractor().execute();

      groupDetails.value = await GroupDetailsEndpoint(GroupRequest(groupId: groupId)).call();

      members.clear();

      if (groupDetails.value.members != null) {
        members.addAll(groupDetails.value.members);
        if (members.any((member) => member.user.id == loggedUserId)) {
          loggedMember.value = members.firstWhere((member) => member.user.id == loggedUserId);
        }
      }
    } catch (e) {
      errorOnUserInfo.value = true;
      _showErrorSnackbar();
    }

    loadingUserInfo.value = false;
  }

  Future<void> removeFromGroup(int userId) async {
    try {
      final response = await GroupLeaveEndpoint(
        GroupLeaveRequest(
          groupId: groupDetails.value.group.id,
          userId: userId,
        ),
      ).call();

      getUserInfo();

      Get.back();

      if (response != null) {
        Get.snackbar("Pronto", response.message);
      } else {
        _showErrorSnackbar();
      }
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }
}
