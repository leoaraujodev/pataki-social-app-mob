import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/member_list/member_list_controller.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_tutor.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class MemberListScreen extends GetView<MemberListController> {
  static String routeName = "/member_list_screen";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Membros",
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingUserInfo.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnUserInfo.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getUserInfo,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                return ListView(
                  shrinkWrap: true,
                  children: [
                    BuildCard(
                      children: controller.members
                          .map(
                            (member) => Stack(
                              alignment: Alignment.center,
                              children: [
                                BuildTutor(
                                  tutor: member.user,
                                  onPressed: () => Get.toNamed(
                                    ProfileScreen.routeName(member.user.id),
                                    arguments: {"timestamp": DateTime.now().toIso8601String()},
                                  ),
                                ),
                                (controller.loggedMember.value != null &&
                                        controller.loggedMember.value.profile == 1)
                                    ? Positioned(
                                        right: 12,
                                        child: Row(
                                          children: [
                                            IconButton(
                                              icon: Icon(
                                                Icons.remove,
                                                color: Colors.red,
                                              ),
                                              onPressed: () {
                                                Get.dialog(
                                                  ConfirmDialog(
                                                      onConfirmPressed: () => controller
                                                          .removeFromGroup(member.user.id)),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
