import 'package:get/get.dart';
import 'package:pataki/screens/my_profile/my_profile_controller.dart';
import 'package:pataki/screens/my_profile/search_tag_controller.dart';

class MyProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MyProfileController());
    Get.put(SearchTagController());
  }
}
