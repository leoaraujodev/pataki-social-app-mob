import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/endpoints/logout_endpoint.dart';
import 'package:pataki/core/endpoints/user_info_endpoint.dart';
import 'package:pataki/core/endpoints/user_posts_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/interactor/set_token_interactor.dart';
import 'package:pataki/core/interactor/set_user_id_interactor.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/models/timeline_response.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/models/user_info_response.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/models/user_posts_request.dart';
import 'package:pataki/screens/edit_user/edit_user_screen.dart';
import 'package:pataki/screens/search/search_controller.dart';
import 'package:pataki/screens/timeline/timeline_controller.dart';
import 'package:pataki/screens/welcome_screen.dart';

class MyProfileController extends GetxController with Notifications {
  final Rx<UserModel> user = UserModel().obs;

  final scrollController = ScrollController();
  final posts = RxList<TimelinePost>([]);

  final hasMorePosts = false.obs;
  final errorOnMorePosts = false.obs;

  final loadingUserInfo = true.obs;
  final errorOnUserInfo = false.obs;

  int _lastId;
  bool _fetchingMutex = false;
  bool _likingMutex = false;

  final timelineController = Get.find<TimelineController>();
  final searchController = Get.find<SearchController>();

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getUserInfo();
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMorePosts.value && !errorOnMorePosts.value) getMorePosts();
      }
    });
    super.onInit();
  }

  Future<void> getUserInfo() async {
    loadingUserInfo.value = true;
    errorOnUserInfo.value = false;

    try {
      int userId = await GetUserIdInteractor().execute();

      UserInfoResponse response = await UserInfoEndpoint(UserInfoRequest(userId: userId)).call();

      user.value = response.user;

      user.refresh();

      final postsResponse = await _getPetPostsResponse();
      _setValues(postsResponse);
    } catch (e) {
      errorOnUserInfo.value = true;
      _showErrorSnackbar();
    }

    loadingUserInfo.value = false;
  }

  Future<void> updateUser(UserModel userToEdit) async {
    try {
      var newUser = await Get.toNamed(
        EditUserScreen.id,
        arguments: {"user": userToEdit},
      );

      if (newUser != null) {
        user.value = user.value.copyWith(
          name: newUser.name,
          email: newUser.email,
          imagePath: newUser.imagePath,
          desc: newUser.desc,
        );

        user.refresh();
      }
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousLastId = _lastId;

    try {
      _lastId = null;
      final response = await _getPetPostsResponse();
      posts.clear();
      _setValues(response);
    } catch (e) {
      _lastId = previousLastId;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMorePosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMorePosts.value = false;

    try {
      final response = await _getPetPostsResponse();
      _setValues(response);
    } catch (e) {
      errorOnMorePosts.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<TimelineResponse> _getPetPostsResponse() =>
      UserPostsEndpoint(UserPostsRequest(userId: user.value.id, lastId: _lastId)).call();

  void _setValues(TimelineResponse response) {
    posts.addAll(response.posts);

    if (response.posts.isNotEmpty) _lastId = response.posts.last.id;

    if (response.posts.length < 15) {
      hasMorePosts.value = false;
    } else {
      hasMorePosts.value = true;
    }
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: like,
        imagePath: posts[index].imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();
    } catch (e) {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: !like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: !like,
        imagePath: posts[index].imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      int index = posts.indexWhere((post) => post.id == id);

      posts.removeAt(index);
      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  void updateCommentCount(int timelineId, int newCount) {
    int index = posts.indexWhere((post) => post.id == timelineId);

    if (index == -1) return;

    posts[index] = posts[index].copyWith(
      imagePath: posts[index].imagePath,
      totComment: newCount,
    );
  }

  Future<void> logoutAction() async {
    try {
      await LogoutEndpoint(EmptyRequest()).call();

      await cancelAllNotifications();
      SetTokenInteractor().execute(null);
      SetUserIdInteractor().execute(null);
      Get.offAllNamed(WelcomeScreen.id);
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
