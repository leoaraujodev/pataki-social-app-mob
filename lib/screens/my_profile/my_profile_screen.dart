import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/about/about_screen.dart';
import 'package:pataki/screens/blocked_list/blocked_list_screen.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/edit_pass/edit_pass_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/group_list/group_list_screen.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/my_profile/search_tag_controller.dart';
import 'package:pataki/screens/my_profile/search_tag_dialog.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/pet_list/pet_list_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';
import 'my_profile_controller.dart';

double screenWidth;

class MyProfileScreen extends GetView<MyProfileController> {
  static String id = "/my_profile_screen";

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Obx(
      () {
        if (controller.loadingUserInfo.value) {
          return Center(child: RefreshProgressIndicator());
        }
        if (controller.errorOnUserInfo.value) {
          return Center(
            child: GestureDetector(
              onTap: controller.getUserInfo,
              child: RefreshProgressIndicator(
                value: 1.0,
              ),
            ),
          );
        }
        return Scaffold(
          backgroundColor: PatakiColors.lightGray,
          appBar: DefaultAppBar(
            title: "Perfil",
            iconOrange: true,
          ),
          endDrawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                UserAccountsDrawerHeader(
                  accountName: Text(
                    controller.user.value.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  accountEmail: Text(
                    controller.user.value.email,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.vpn_key_outlined),
                  title: Text("Alterar Senha"),
                  onTap: () => Get.toNamed(EditPassScreen.id),
                ),
                ListTile(
                  leading: Icon(Icons.block),
                  title: Text("Tutores Bloqueados"),
                  onTap: () => Get.toNamed(BlockedListScreen.routeName),
                ),
                ListTile(
                  leading: SvgPicture.asset(
                    'images/nfc_icon.svg',
                    color: Colors.black45,
                  ),
                  title: Text("Ler Tag"),
                  onTap: () {
                    Get.find<SearchTagController>().readTag();
                    Get.dialog(SearchTagDialog());
                  },
                ),
                ListTile(
                  leading: Icon(Icons.info_outline_rounded),
                  title: Text("Sobre"),
                  onTap: () => Get.toNamed(AboutScreen.routeName),
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("Logout"),
                  onTap: () => controller.logoutAction(),
                ),
              ],
            ),
          ),
          body: ListView(
            physics: AlwaysScrollableScrollPhysics(),
            controller: controller.scrollController,
            children: [
              _buildHeader(),
              SizedBox(height: 20),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: controller.posts.length,
                itemBuilder: (context, index) {
                  Widget card = TimelinePostCard(
                    post: controller.posts[index],
                    onCommentPressed: () => Get.toNamed(
                      CommentScreen.routeName(controller.posts[index].id),
                      arguments: {
                        "updateCommentCount": controller.updateCommentCount,
                        "timestamp": DateTime.now().toIso8601String(),
                      },
                    ),
                    onLikePressed: () => controller.likeComment(
                      controller.posts[index].id,
                      !controller.posts[index].liked,
                    ),
                    onLikesPressed: () => Get.toNamed(
                      LikesScreen.routeName(controller.posts[index].id),
                      arguments: {"timestamp": DateTime.now().toIso8601String()},
                    ),
                    onMorePressed: () => Get.dialog(
                      OptionsDialog(
                        title: "Opções da Postagem",
                        onEditPressed: () async {
                          await Get.offNamed(
                            EditPostScreen.routeName(controller.posts[index].id),
                          );
                        },
                        onDeletePressed: () {
                          Get.back();
                          Get.dialog(
                            ConfirmDialog(
                              onConfirmPressed: () => controller.deletePost(
                                controller.posts[index].id,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    onSendPressed: () {},
                    onUserPressed: () {
                      Get.toNamed(
                        PetScreen.routeName(controller.posts[index].pet.id),
                        arguments: {"timestamp": DateTime.now().toIso8601String()},
                      );
                    },
                  );

                  if (index == controller.posts.length - 1 && controller.hasMorePosts.value) {
                    return Column(
                      children: [
                        card,
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Obx(
                            () => controller.errorOnMorePosts.value
                                ? Center(
                                    child: GestureDetector(
                                      onTap: controller.getMorePosts,
                                      child: RefreshProgressIndicator(
                                        value: 1.0,
                                      ),
                                    ),
                                  )
                                : RefreshProgressIndicator(),
                          ),
                        ),
                      ],
                    );
                  }

                  return card;
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _buildHeader() {
    return Container(
      padding: screenWidth >= 375
          ? EdgeInsets.symmetric(horizontal: 24, vertical: 16)
          : EdgeInsets.symmetric(horizontal: 18, vertical: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          IntrinsicHeight(
            child: Row(
              children: [
                _buildUserPhoto(),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _buildProfileHeader(
                        userName: controller.user.value.name,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          _buildProfileSummary(
                            numericInfo: controller.user.value.pets != null
                                ? "${controller.user.value.pets.length}"
                                : "0",
                            typeInfo: "PETS",
                            onPressed: () => Get.toNamed(
                              PetListScreen.routeName(controller.user.value.id),
                              arguments: {
                                "timestamp": DateTime.now().toIso8601String(),
                              },
                            ),
                          ),
                          _buildProfileSummary(
                            numericInfo: "${controller.user.value.groups}",
                            typeInfo: "GRUPOS",
                            onPressed: () async {
                              await Get.toNamed(
                                GroupListScreen.routeName(controller.user.value.id),
                                arguments: {
                                  "timestamp": DateTime.now().toIso8601String(),
                                  "isFromMyProfile": true,
                                },
                              );
                              controller.getUserInfo();
                              controller.searchController
                                  .newSearch(controller.searchController.textController.text);
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              (controller.user.value.desc != null && controller.user.value.desc != "null")
                  ? "${controller.user.value.desc}"
                  : "",
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildUserPhoto() {
    return Padding(
      padding: EdgeInsets.only(right: 10),
      child: Container(
        width: screenWidth >= 375 ? 90 : 60,
        height: screenWidth >= 375 ? 90 : 60,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: NetworkImage(controller.user.value.imagePath),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  _buildProfileHeader({
    @required String userName,
  }) {
    if (userName != null && userName.length >= 17) {
      userName = userName.substring(0, 17) + "...";
    }

    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(top: 8),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$userName",
                  style: TextStyle(
                    fontSize: screenWidth >= 375 ? 20 : 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  color: PatakiColors.orange,
                  onPressed: () {
                    controller.updateUser(controller.user.value);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _buildProfileSummary({
    @required String numericInfo,
    @required String typeInfo,
    @required Function onPressed,
  }) {
    return TextButton(
      onPressed: onPressed,
      child: Column(
        children: [
          Text(
            numericInfo,
            style: TextStyle(
              color: Color(0xFF222222),
              fontSize: screenWidth >= 375 ? 22 : 18,
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            typeInfo,
            style: TextStyle(
              color: Color(0xFF222222),
              fontSize: screenWidth >= 375 ? 12 : 8,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
