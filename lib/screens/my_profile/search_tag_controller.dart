import 'package:get/get.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:pataki/core/endpoints/search_tag_endpoint.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/search_tag_request.dart';

class SearchTagController extends GetxController {
  RxBool isAvailable = true.obs;
  RxBool readingError = false.obs;
  RxString tagText = RxString();
  Rx<Pet> pet = Rx<Pet>();
  bool _readingMutex = false;
  RxBool searching = false.obs;
  RxBool searchingError = false.obs;

  readTag() async {
    if (_readingMutex) return;
    _readingMutex = true;

    isAvailable.value = true;
    readingError.value = false;
    tagText.value = null;
    pet.value = null;
    searchingError.value = false;

    try {
      bool isNDEFSupported = await NFC.isNDEFSupported;

      if (!isNDEFSupported) {
        isAvailable.value = false;
        _readingMutex = false;
        return;
      }

      NDEFMessage message = await NFC.readNDEF(once: true).first;
      String data = message.data;

      if (data != null) {
        tagText.value = data;
        searchTag();
      } else {
        throw "Tag data is null";
      }
    } catch (e) {
      readingError.value = true;
    }

    _readingMutex = false;
  }

  searchTag() async {
    if (searching.value) return;
    searching.value = true;

    searchingError.value = false;

    try {
      pet.value = await SearchTagEndpoint(SearchTagRequest(tag: tagText.value)).call();
    } catch (e) {
      searchingError.value = true;
    }

    searching.value = false;
  }
}
