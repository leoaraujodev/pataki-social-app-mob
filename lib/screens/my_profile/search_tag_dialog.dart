import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/my_profile/search_tag_controller.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';

class SearchTagDialog extends GetView<SearchTagController> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Obx(
        () => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 30),
            SvgPicture.asset(
              'images/nfc_icon.svg',
              height: 30,
              width: 30,
              color: PatakiColors.orange,
            ),
            SizedBox(height: 10),
            Text(
              "Ler Tag",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
            SizedBox(height: 30),
            Divider(height: 0),
            SizedBox(height: 30),
            controller.tagText.value != null ? _buildSearch() : _buildNfcIndicator(),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildCancelButton(),
                  if (controller.searchingError.value) _buildSearchRetryButton(),
                  if (controller.readingError.value) _buildReadRetryButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSearch() {
    return Column(
      children: [
        Container(
          height: 101,
          alignment: Alignment.center,
          child: controller.pet.value != null
              ? BuildPet(
                  pet: controller.pet.value,
                  onPressed: () => Get.offNamed(
                    PetScreen.routeName(controller.pet.value.id),
                    arguments: {"timestamp": DateTime.now().toIso8601String()},
                  ),
                )
              : Container(
                  width: 50,
                  height: 50,
                  child: controller.searching.value
                      ? CircularProgressIndicator()
                      : Icon(Icons.close, size: 50, color: PatakiColors.orange),
                ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            controller.pet.value != null
                ? "Pet identificado"
                : controller.searching.value
                    ? "Buscando tag:\n${controller.tagText.value}"
                    : controller.searchingError.value
                        ? "Ocorreu um erro de conexão ao buscar a tag"
                        : "Nenhum pet identificado com a tag:\n${controller.tagText.value}",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ],
    );
  }

  Widget _buildNfcIndicator() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(width: 50, height: 50),
            SvgPicture.asset('images/nfc_phone.svg'),
            Container(
              width: 50,
              height: 50,
              child: !controller.isAvailable.value
                  ? SizedBox()
                  : controller.readingError.value
                      ? Icon(Icons.close, size: 50, color: PatakiColors.orange)
                      : CircularProgressIndicator(),
            ),
          ],
        ),
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            !controller.isAvailable.value
                ? "Leitor NFC indisponível"
                : controller.readingError.value
                    ? "Não foi possível ler a Tag"
                    : "Aproxime o aparelho da Tag",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ],
    );
  }

  Widget _buildCancelButton() {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
      onPressed: () => Get.back(),
      child: Text("Cancelar"),
    );
  }

  Widget _buildReadRetryButton() {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
        onPressed: controller.readTag,
        child: Text("Tentar Novamente"),
      ),
    );
  }

  Widget _buildSearchRetryButton() {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
        onPressed: controller.searchTag,
        child: Text("Tentar Novamente"),
      ),
    );
  }
}
