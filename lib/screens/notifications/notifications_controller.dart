import 'package:get/get.dart';
import 'package:pataki/core/endpoints/get_user_notification_endpoint.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_user_notifications_response.dart';
import 'package:pataki/core/models/user_notification.dart';

class NotificationsController extends GetxController {
  RxList<UserNotification> notifications = RxList<UserNotification>([]);

  final loadingInitialNotifications = true.obs;
  final errorOnInitialNotifications = false.obs;

  bool _fetchingMutex = false;

  @override
  onInit() {
    getInitialNotifications();
    super.onInit();
  }

  Future<void> getInitialNotifications() async {
    loadingInitialNotifications.value = true;
    errorOnInitialNotifications.value = false;

    try {
      final response = await _getNotificationsResponse();
      notifications.assignAll(response.notifications);
    } catch (e) {
      errorOnInitialNotifications.value = true;
      _showErrorSnackbar();
    }

    loadingInitialNotifications.value = false;
  }

  Future<void> refreshNotifications() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    try {
      final response = await _getNotificationsResponse();
      notifications.assignAll(response.notifications);
    } catch (e) {
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<GetUserNotificationsResponse> _getNotificationsResponse() =>
      GetUserNotificationEndpoint(EmptyRequest()).call();
}
