import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/interactor/date_time_formatter.dart';
import 'package:pataki/core/models/user_notification.dart';
import 'package:pataki/core/services/notification_service.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_controller.dart';
import 'package:pataki/screens/chat_rooms/chat_rooms_screen.dart';
import 'package:pataki/screens/notifications/notifications_controller.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/screens/single_post/single_post_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class NotificationsScreen extends GetView<NotificationsController> {
  final notificationService = Get.find<NotificationService>();
  final chatRoomsController = Get.find<ChatRoomsController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Notificações",
        actions: [
          Obx(
            () => IconButton(
              icon: Stack(
                alignment: Alignment.topRight,
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    child: Icon(
                      Icons.chat_outlined,
                      color: Color(0xFF90AECC),
                    ),
                  ),
                  if (chatRoomsController.hasNewMessages.value)
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: PatakiColors.orange,
                      ),
                    ),
                ],
              ),
              padding: EdgeInsets.zero,
              onPressed: () async {
                notificationService.deactivateInFocusNotifications();
                await Get.toNamed(ChatRoomsScreen.routeName);
                notificationService.activateInFocusNotifications();
                chatRoomsController.markMessagesAsRead();
              },
            ),
          ),
        ],
      ),
      body: Obx(
        () {
          if (controller.loadingInitialNotifications.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnInitialNotifications.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getInitialNotifications,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return RefreshIndicator(
            onRefresh: controller.refreshNotifications,
            child: SafeArea(
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                itemCount: controller.notifications.length,
                itemBuilder: (context, index) {
                  return NotificationTile(
                    notification: controller.notifications[index],
                    onPressed: () => controller.notifications[index].action.contains("post")
                        ? Get.toNamed(
                            SinglePostScreen.routeName(controller.notifications[index].timeline.id),
                          )
                        : Get.toNamed(
                            ProfileScreen.routeName(controller.notifications[index].user.id),
                            arguments: {"timestamp": DateTime.now().toIso8601String()},
                          ),
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }
}

class NotificationTile extends StatelessWidget {
  final UserNotification notification;
  final void Function() onPressed;

  NotificationTile({
    @required this.notification,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
          padding: EdgeInsets.all(10),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    height: 34,
                    width: 34,
                    decoration: BoxDecoration(
                      color: PatakiColors.white,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage(notification.user.imagePath),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Flexible(
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                            text: notification.user.name,
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          TextSpan(text: " "),
                          TextSpan(text: notification.message)
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                DateTimeFormatter().execute(notification.createdAt),
                style: TextStyle(color: Color(0xFF90AECC)),
              ),
            ],
          )),
    );
  }
}
