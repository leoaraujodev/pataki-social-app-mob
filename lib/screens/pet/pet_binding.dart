import 'package:get/get.dart';
import 'package:pataki/screens/pet/register_tag_controller.dart';
import 'package:pataki/screens/pet/pet_controller.dart';

class PetBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PetController(), tag: Get.arguments["timestamp"]);
    Get.put(RegisterTagController());
  }
}
