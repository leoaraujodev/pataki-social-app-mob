import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/follow_pet_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/endpoints/pet_info_endpoint.dart';
import 'package:pataki/core/endpoints/pet_posts_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/follow_user_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_info_request.dart';
import 'package:pataki/core/models/pet_posts_request.dart';
import 'package:pataki/core/models/pet_posts_response.dart';
import 'package:pataki/core/models/timeline_post.dart';

class PetController extends GetxController {
  final loggedUserId = RxInt();
  int _petId = int.parse(Get.parameters["petId"]);

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final petSize = "".obs;
  final petAge = 0.obs;
  final followed = false.obs;
  final isPetOwner = true.obs;

  final pet = Pet().obs;

  final scrollController = ScrollController();
  final posts = RxList<TimelinePost>([]);

  final loadingPetInfo = true.obs;
  final errorOnPetInfo = false.obs;

  final hasMorePosts = false.obs;
  final errorOnMorePosts = false.obs;

  int _lastId;
  bool _fetchingMutex = false;
  bool _likingMutex = false;

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getPetInfo();
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMorePosts.value && !errorOnMorePosts.value) getMorePosts();
      }
    });
    super.onInit();
  }

  Future<void> getPetInfo() async {
    loggedUserId.value = await GetUserIdInteractor().execute();
    loadingPetInfo.value = true;
    errorOnPetInfo.value = false;

    try {
      pet.value = await PetInfoEndpoint(
        PetInfoRequest(petId: _petId),
      ).call();

      pet.refresh();

      followed.value = pet.value.followed;

      final response = await _getPetPostsResponse();
      _setValues(response);

      switch (pet.value.size) {
        case "S":
          petSize.value = "Pequeno";
          break;
        case "M":
          petSize.value = "Médio";
          break;
        case "L":
          petSize.value = "Grande";
          break;
      }

      petAge.value = (DateTime.now().difference(pet.value.birthDate).inDays.floor() / 365).floor();

      verifyIfIsPetOwner();
    } catch (e) {
      errorOnPetInfo.value = true;
      _showErrorSnackbar();
    }

    loadingPetInfo.value = false;
  }

  Future<void> verifyIfIsPetOwner() async {
    try {
      int _userLogged = await GetUserIdInteractor().execute();
      if (_userLogged != pet.value.tutor.id) {
        isPetOwner.value = false;
      }
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> followAction() async {
    try {
      await FollowPetEndpoint(FollowPetRequest(
        petId: _petId,
        follow: !followed.value,
      )).call();

      followed.value = !followed.value;
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousLastId = _lastId;

    try {
      _lastId = null;
      final response = await _getPetPostsResponse();
      posts.clear();
      _setValues(response);
    } catch (e) {
      _lastId = previousLastId;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMorePosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMorePosts.value = false;

    try {
      final response = await _getPetPostsResponse();
      _setValues(response);
    } catch (e) {
      errorOnMorePosts.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<PetPostsResponse> _getPetPostsResponse() =>
      PetPostsEndpoint(PetPostsRequest(petId: _petId, lastId: _lastId)).call();

  void _setValues(PetPostsResponse response) {
    posts.addAll(response.posts);

    if (response.posts.isNotEmpty) _lastId = response.posts.last.id;

    if (response.posts.length < 15) {
      hasMorePosts.value = false;
    } else {
      hasMorePosts.value = true;
    }
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: like,
        imagePath: posts[index].imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();
    } catch (e) {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: !like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: !like,
        imagePath: posts[index].imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      int index = posts.indexWhere((post) => post.id == id);

      posts.removeAt(index);
      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  void updateCommentCount(int timelineId, int newCount) {
    int index = posts.indexWhere((post) => post.id == timelineId);

    if (index == -1) return;

    posts[index] = posts[index].copyWith(
      imagePath: posts[index].imagePath,
      totComment: newCount,
    );
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
