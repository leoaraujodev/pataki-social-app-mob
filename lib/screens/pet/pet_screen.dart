import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/pet/register_tag_controller.dart';
import 'package:pataki/screens/pet/register_tag_dialog.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/screens/reminder/reminder_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/screens/vaccine/vaccine_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_button.dart';
import 'package:pataki/uikit/widgets/build_small_card.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';
import 'package:pataki/uikit/widgets/user_image.dart';
import 'pet_controller.dart';

double screenWidth;

class PetScreen extends GetView<PetController> {
  static String routeName(petId) => "/pet_screen/$petId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: controller.scaffoldKey,
      backgroundColor: PatakiColors.lightGray,
      appBar: DefaultAppBar(
        title: "Pet",
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert_rounded),
            onPressed: () => Get.dialog(
              OptionsDialog(
                title: "Opções do Pet",
                onSharePressed: () {
                  Clipboard.setData(
                    new ClipboardData(
                      text: "https://pataki.com.br/#/pet_screen/${controller.pet.value.id}",
                    ),
                  );
                  Get.back();
                },
                shareType: "pet",
              ),
            ),
          ),
        ],
      ),
      endDrawer: Obx(
        () => controller.isPetOwner.value ? _buildPetMenu(context: context) : null,
      ),
      body: Obx(
        () {
          if (controller.loadingPetInfo.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnPetInfo.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getPetInfo,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return RefreshIndicator(
            child: ListView(
              physics: AlwaysScrollableScrollPhysics(),
              controller: controller.scrollController,
              children: [
                _buildHeader(
                  context: context,
                  scaffoldCurrentState: controller.scaffoldKey.currentState,
                ),
                SizedBox(height: 20),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: controller.posts.length,
                  itemBuilder: (context, index) {
                    Widget card = TimelinePostCard(
                      post: controller.posts[index],
                      onCommentPressed: () => Get.toNamed(
                        CommentScreen.routeName(controller.posts[index].id),
                        arguments: {
                          "updateCommentCount": controller.updateCommentCount,
                          "timestamp": DateTime.now().toIso8601String(),
                        },
                      ),
                      onLikePressed: () => controller.likeComment(
                        controller.posts[index].id,
                        !controller.posts[index].liked,
                      ),
                      onLikesPressed: () => Get.toNamed(
                        LikesScreen.routeName(controller.posts[index].id),
                        arguments: {"timestamp": DateTime.now().toIso8601String()},
                      ),
                      onMorePressed: () => Get.dialog(
                        OptionsDialog(
                          title: "Opções da Postagem",
                          onReportPressed:
                              controller.loggedUserId.value != controller.posts[index].user.id
                                  ? () async {
                                      int reportedPostId = controller.posts[index].id;

                                      var reported = await Get.offNamed(
                                        ReportTimelinePostScreen.routeName,
                                        arguments: {"post": controller.posts[index]},
                                      );

                                      if (reported == true) {
                                        controller.posts.removeWhere(
                                          (post) => post.id == reportedPostId,
                                        );
                                      }
                                    }
                                  : null,
                          onEditPressed:
                              controller.loggedUserId.value == controller.posts[index].user.id
                                  ? () async {
                                      await Get.offNamed(
                                        EditPostScreen.routeName(controller.posts[index].id),
                                      );
                                    }
                                  : null,
                          onDeletePressed:
                              controller.loggedUserId.value == controller.posts[index].user.id
                                  ? () {
                                      Get.back();
                                      Get.dialog(
                                        ConfirmDialog(
                                          onConfirmPressed: () => controller.deletePost(
                                            controller.posts[index].id,
                                          ),
                                        ),
                                      );
                                    }
                                  : null,
                        ),
                      ),
                      onSendPressed: () {},
                      onUserPressed: () {
                        Get.toNamed(
                          PetScreen.routeName(controller.posts[index].pet.id),
                          arguments: {"timestamp": DateTime.now().toIso8601String()},
                        );
                      },
                    );

                    if (index == controller.posts.length - 1 && controller.hasMorePosts.value) {
                      return Column(
                        children: [
                          card,
                          Padding(
                            padding: EdgeInsets.only(bottom: 20),
                            child: Obx(
                              () => controller.errorOnMorePosts.value
                                  ? Center(
                                      child: GestureDetector(
                                        onTap: controller.getMorePosts,
                                        child: RefreshProgressIndicator(
                                          value: 1.0,
                                        ),
                                      ),
                                    )
                                  : RefreshProgressIndicator(),
                            ),
                          ),
                        ],
                      );
                    }

                    return card;
                  },
                ),
              ],
            ),
            onRefresh: controller.refreshPosts,
          );
        },
      ),
    );
  }

  _buildHeader({
    @required context,
    @required scaffoldCurrentState,
  }) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(24),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _buildTitle(
                  userName: "${controller.pet.value.name}",
                  context: context,
                  scaffoldCurrentState: scaffoldCurrentState),
              SizedBox(height: 20),
              _buildUserPhoto(imagePath: controller.pet.value.imagePath, dimensions: 130),
              SizedBox(height: 20),
              if (!controller.isPetOwner.value)
                BuildButton(
                  text: controller.followed.value ? "Seguindo" : "Seguir",
                  textColor: Colors.white,
                  backgroundColor:
                      controller.followed.value ? PatakiColors.blue : PatakiColors.orange,
                  onPressed: () => controller.followAction(),
                ),
              SizedBox(height: 20),
              Table(
                children: [
                  TableRow(
                    children: [
                      _buildProfileSummary(
                        numericInfo: controller.pet.value.breed.type.name,
                        typeInfo: "TIPO",
                      ),
                      _buildProfileSummary(
                        numericInfo: controller.pet.value.breed.name,
                        typeInfo: "RAÇA",
                      ),
                      _buildProfileSummary(
                        numericInfo: controller.petSize.value,
                        typeInfo: "PORTE",
                      ),
                    ],
                  ),
                  TableRow(children: [
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ]),
                  TableRow(
                    children: [
                      _buildProfileSummary(
                        numericInfo: "${controller.pet.value.totFollowers}",
                        typeInfo: "SEGUIDORES",
                      ),
                      if (controller.pet.value.weight != null)
                        _buildProfileSummary(
                          numericInfo: "${controller.pet.value.weight} kg",
                          typeInfo: "PESO",
                        ),
                      if (controller.pet.value.birthDate != null)
                        _buildProfileSummary(
                          numericInfo: controller.petAge.value != 1
                              ? "${controller.petAge.value} anos"
                              : "${controller.petAge.value} ano",
                          typeInfo: "IDADE",
                        ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 20),
              FlatButton(
                onPressed: () => Get.toNamed(
                  ProfileScreen.routeName(controller.pet.value.tutor.id),
                  arguments: {"timestamp": DateTime.now().toIso8601String()},
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Tutor",
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF90AECC),
                      ),
                    ),
                    SizedBox(width: 12),
                    _buildUserPhoto(
                      imagePath: controller.pet.value.tutor.imagePath,
                      dimensions: 40,
                    ),
                    SizedBox(width: 12),
                    Flexible(
                      child: Text(
                        controller.pet.value.tutor.name,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  _buildUserPhoto({@required String imagePath, @required double dimensions}) {
    return Container(
      width: dimensions,
      height: dimensions,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage(imagePath.toString()),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  _buildTitle({@required String userName, @required context, @required scaffoldCurrentState}) {
    return Container(
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Text(
            "$userName",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          if (controller.isPetOwner.value)
            Positioned(
              right: -10,
              child: IconButton(
                alignment: Alignment.centerRight,
                color: PatakiColors.orange,
                iconSize: 24,
                icon: Icon(Icons.menu),
                onPressed: () => scaffoldCurrentState.openEndDrawer(),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              ),
            )
        ],
      ),
    );
  }

  _buildProfileSummary({
    @required String numericInfo,
    @required String typeInfo,
  }) {
    if (numericInfo.length > 12) {
      numericInfo = numericInfo.substring(0, 12) + "...";
    }

    return Column(
      children: [
        Text(
          numericInfo,
          style: TextStyle(
            fontSize: screenWidth >= 375 ? 18 : 14,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          typeInfo,
          style: TextStyle(
            fontSize: screenWidth >= 375 ? 12 : 9,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }

  _buildPetMenu({@required context}) {
    return SizedBox(
      width: double.infinity,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              color: Color(0xFFFFF9F2),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(22.0, 44.0, 22.0, 26.0),
                child: Row(
                  children: [
                    Material(
                      type: MaterialType.transparency,
                      child: Ink(
                        decoration: BoxDecoration(
                          border: Border.all(color: PatakiColors.orange),
                          shape: BoxShape.circle,
                        ),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(1000.0),
                          child: Padding(
                            padding: EdgeInsets.all(15.0),
                            child: Icon(
                              Icons.arrow_back,
                              color: PatakiColors.orange,
                              size: 30.0,
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 22.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Detalhes',
                            style: TextStyle(
                              color: PatakiColors.orange,
                              fontSize: 24.0,
                            ),
                          ),
                          Row(
                            children: [
                              UserImage(imagePath: controller.pet.value.imagePath),
                              Text(
                                "${controller.pet.value.name}",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            BuildSmallCard(
              resizeToSmallScreen: screenWidth <= 375,
              isIcon: true,
              imagePath: "images/syringe_icon.svg",
              title: "Vacinas",
              onPressed: () => Get.toNamed(VaccineScreen.routeName(controller.pet.value.id)),
            ),
            BuildSmallCard(
              resizeToSmallScreen: screenWidth <= 375,
              isIcon: true,
              imagePath: 'images/reminder_icon.svg',
              title: "Rotinas",
              onPressed: () => Get.toNamed(ReminderScreen.routeName(controller.pet.value.id)),
            ),
            BuildSmallCard(
              resizeToSmallScreen: screenWidth <= 375,
              isIcon: true,
              imagePath: 'images/nfc_icon.svg',
              title: "Cadastrar Tag",
              onPressed: () {
                Get.find<RegisterTagController>().readTag();
                Get.dialog(RegisterTagDialog());
              },
            ),
          ],
        ),
      ),
    );
  }
}
