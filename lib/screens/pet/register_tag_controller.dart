import 'package:get/get.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:pataki/core/endpoints/add_tag_endpoint.dart';
import 'package:pataki/core/models/add_tag_request.dart';

class RegisterTagController extends GetxController {
  RxBool isAvailable = true.obs;
  RxBool error = false.obs;
  RxString tagText = RxString();
  bool _readingMutex = false;
  RxBool saving = false.obs;

  int _petId = int.parse(Get.parameters["petId"]);

  readTag() async {
    if (_readingMutex) return;
    _readingMutex = true;

    isAvailable.value = true;
    error.value = false;
    tagText.value = null;

    try {
      bool isNDEFSupported = await NFC.isNDEFSupported;

      if (!isNDEFSupported) {
        isAvailable.value = false;
        _readingMutex = false;
        return;
      }

      NDEFMessage message = await NFC.readNDEF(once: true).first;
      String data = message.data;

      if (data != null) {
        tagText.value = data;
      } else {
        throw "Tag data is null";
      }
    } catch (e) {
      error.value = true;
    }

    _readingMutex = false;
  }

  saveTag() async {
    if (saving.value) return;
    saving.value = true;

    try {
      await AddTagEndpoint(AddTagRequest(id: _petId, tag: tagText.value)).call();

      Get.back();
      Get.snackbar("Sucesso", "Tag salva com sucesso");
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }

    saving.value = false;
  }
}
