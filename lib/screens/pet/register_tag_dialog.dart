import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/pet/register_tag_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class RegisterTagDialog extends GetView<RegisterTagController> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Obx(
        () => Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 30),
            SvgPicture.asset(
              'images/nfc_icon.svg',
              height: 30,
              width: 30,
              color: PatakiColors.orange,
            ),
            SizedBox(height: 10),
            Text(
              "Cadastrar Tag",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
            SizedBox(height: 30),
            Divider(height: 0),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(width: 50, height: 50),
                SvgPicture.asset('images/nfc_phone.svg'),
                Container(
                  width: 50,
                  height: 50,
                  child: controller.tagText.value != null || !controller.isAvailable.value
                      ? SizedBox()
                      : controller.error.value
                          ? Icon(Icons.close, size: 50, color: PatakiColors.orange)
                          : CircularProgressIndicator(),
                ),
              ],
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Text(
                !controller.isAvailable.value
                    ? "Leitor NFC indisponível"
                    : controller.tagText.value != null
                        ? "Tag encontrada:\n${controller.tagText.value}"
                        : controller.error.value
                            ? "Não foi possível ler a Tag"
                            : "Aproxime o aparelho da Tag",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18),
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildCancelButton(),
                  if (controller.tagText.value != null) _buildSaveButton(),
                  if (controller.error.value) _buildRetryButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCancelButton() {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
      onPressed: () => Get.back(),
      child: Text("Cancelar"),
    );
  }

  Widget _buildSaveButton() {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
        onPressed: controller.saving.value ? () {} : controller.saveTag,
        child: controller.saving.value
            ? Container(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation(Colors.black),
                ),
              )
            : Text("Salvar"),
      ),
    );
  }

  Widget _buildRetryButton() {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
        onPressed: controller.readTag,
        child: Text("Tentar Novamente"),
      ),
    );
  }
}
