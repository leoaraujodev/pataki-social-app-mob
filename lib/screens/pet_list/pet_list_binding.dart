import 'package:get/get.dart';
import 'package:pataki/screens/pet_list/pet_list_controller.dart';

class PetListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PetListController(), tag: Get.arguments["timestamp"]);
  }
}
