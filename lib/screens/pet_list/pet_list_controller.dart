import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_pet_endpoint.dart';
import 'package:pataki/core/endpoints/get_user_pets_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/delete_pet_request.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/pet_list_response.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/screens/edit_pet/edit_pet_screen.dart';
import 'package:pataki/screens/register_pet/register_pet_screen.dart';
import 'package:pataki/screens/search/search_controller.dart';

class PetListController extends GetxController {
  final int _userId = int.tryParse(Get.parameters["userId"]);

  final RxList<Pet> pets = RxList([]);

  final scrollController = ScrollController();

  final editingMode = false.obs;
  final isTheLoggedUser = false.obs;

  final loadingPetsInfo = true.obs;
  final errorOnPetsInfo = false.obs;

  final searchController = Get.find<SearchController>();

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getPetsInfo();
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;
    });
    super.onInit();
  }

  Future<void> getPetsInfo() async {
    loadingPetsInfo.value = true;
    errorOnPetsInfo.value = false;

    try {
      int loggedUserId = await GetUserIdInteractor().execute();

      if (loggedUserId == _userId) {
        isTheLoggedUser.value = true;
      }

      PetListResponse response = await GetUserPetsEndpoint(UserInfoRequest(userId: _userId)).call();

      pets.clear();

      if (response.pets != null) {
        pets.addAll(response.pets);
      }
    } catch (e) {
      errorOnPetsInfo.value = true;
      _showErrorSnackbar();
    }

    loadingPetsInfo.value = false;
  }

  Future<void> createPet() async {
    try {
      var pet = await Get.toNamed(RegisterPetScreen.id);

      if (pet != null) {
        pets.add(pet);

        pets.refresh();
      }
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> updatePet(Pet pet) async {
    var refresh = await Get.toNamed(
      EditPetScreen.id,
      arguments: {"pet": pet},
    );

    if (refresh == true) {
      getPetsInfo();
    }
  }

  Future<void> deletePet(int petId) async {
    try {
      await DeletePetEndpoint(DeletePetRequest(petId: petId)).call();

      pets.removeWhere((pet) => pet.id == petId);

      var petOnSearched =
          searchController.pets.singleWhere((pet) => pet.id == petId, orElse: () => null);
      if (petOnSearched != null) {
        searchController.pets.removeWhere((pet) => pet.id == petId);
      }

      pets.refresh();
    } catch (e) {
      _showErrorSnackbar();
    }
    Get.back();
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
