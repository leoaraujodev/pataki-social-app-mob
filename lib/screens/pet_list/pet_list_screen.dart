import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/pet_list/pet_list_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class PetListScreen extends GetView<PetListController> {
  static String routeName(userId) => "/pet_list_screen/$userId";

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Pets",
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 22.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() {
                  if (controller.isTheLoggedUser.value)
                    return Material(
                      type: MaterialType.transparency,
                      child: Ink(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: PatakiColors.orange,
                        ),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(1000.0),
                          child: Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 20.0,
                            ),
                          ),
                          onTap: () {
                            controller.createPet();
                          },
                        ),
                      ),
                    );

                  return Container();
                }),
              ],
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingPetsInfo.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnPetsInfo.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getPetsInfo,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                if (controller.pets.isEmpty) {
                  return Center(
                    child: Text(
                      "Este tutor não possui pets",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey,
                      ),
                    ),
                  );
                }
                return ListView(
                  shrinkWrap: true,
                  children: [
                    BuildCard(
                      children: controller.pets
                          .map(
                            (pet) => Stack(
                              alignment: Alignment.center,
                              children: [
                                BuildPet(
                                  pet: pet,
                                  controller: controller,
                                  onLongPress: () {
                                    if (controller.isTheLoggedUser.value) {
                                      controller.editingMode.value = !controller.editingMode.value;
                                    }
                                  },
                                ),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
