import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pataki/uikit/widgets/build_paragraph.dart';
import 'package:pataki/uikit/widgets/build_section_title.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:url_launcher/url_launcher.dart';

class PolicyScreen extends StatelessWidget {
  static String routeName = "/policy_screen";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: "Política de Privacidade",
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: ListView(
          children: [
            BuildSectionTitle('1. Informações gerais'),
            BuildParagraph(
              'A presente Política de Privacidade contém informações a respeito do modo como tratamos, total ou parcialmente, de forma automatizada ou não, os dados pessoais dos usuários que acessam nosso aplicativo. Seu objetivo é esclarecer os interessados acerca dos tipos de dados que são coletados, dos motivos da coleta e da forma como o usuário poderá atualizar, gerenciar ou excluir estas informações.',
            ),
            BuildParagraph(
              'Esta Política de Privacidade foi elaborada em conformidade com a Lei Federal n. 12.965 de 23 de abril de 2014 (Marco Civil da Internet), com a Lei Federal n. 13.709, de 14 de agosto de 2018 (Lei de Proteção de Dados Pessoais) e com o Regulamento UE n. 2016/679 de 27 de abril de 2016 (Regulamento Geral Europeu de Proteção de Dados Pessoais - RGDP).',
            ),
            BuildParagraph(
              'Esta Política de Privacidade poderá ser atualizada em decorrência de eventual atualização normativa, razão pela qual se convida o usuário a consultar periodicamente esta seção.',
            ),
            BuildSectionTitle('2. Direitos do usuário'),
            BuildParagraph(
              'O aplicativo se compromete a cumprir as normas previstas no RGPD, em respeito aos seguintes princípios:',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão processados de forma lícita, leal e transparente (licitude, lealdade e transparência);',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão coletados apenas para finalidades determinadas, explícitas e legítimas, não podendo ser tratados posteriormente de uma forma incompatível com essas finalidades (limitação das finalidades);',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão coletados de forma adequada, pertinente e limitada às necessidades do objetivo para os quais eles são processados (minimização dos dados);',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão exatos e atualizados sempre que necessário, de maneira que os dados inexatos sejam apagados ou retificados quando possível (exatidão);',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão conservados de uma forma que permita a identificação dos titulares dos dados apenas durante o período necessário para as finalidades para as quais são tratados (limitação da conservação);',
            ),
            BuildParagraph(
              '- Os dados pessoais do usuário serão tratados de forma segura, protegidos do tratamento não autorizado ou ilícito e contra a sua perda, destruição ou danificação acidental, adotando as medidas técnicas ou organizativas adequadas (integridade e confidencialidade).',
            ),
            BuildParagraph(
              'O usuário do aplicativo possui os seguintes direitos, conferidos pela Lei de Proteção de Dados Pessoais e pelo RGPD:',
            ),
            BuildParagraph(
              '- Direito de confirmação e acesso: é o direito do usuário de obter do aplicativo a confirmação de que os dados pessoais que lhe digam respeito são ou não objeto de tratamento e, se for esse o caso, o direito de acessar os seus dados pessoais;',
            ),
            BuildParagraph(
              '- Direito de retificação: é o direito do usuário de obter do aplicativo, sem demora injustificada, a retificação dos dados pessoais inexatos que lhe digam respeito;',
            ),
            BuildParagraph(
              '- Direito à eliminação dos dados (direito ao esquecimento): é o direito do usuário de ter seus dados apagados do aplicativo;',
            ),
            BuildParagraph(
              '- Direito à limitação do tratamento dos dados: é o direito do usuário de limitar o tratamento de seus dados pessoais, podendo obtê-la quando contesta a exatidão dos dados, quando o tratamento for ilícito, quando o aplicativo não precisar mais dos dados para as finalidades propostas e quando tiver se oposto ao tratamento dos dados e em caso de tratamento de dados desnecessários;',
            ),
            BuildParagraph(
              '- Direito de oposição: é o direito do usuário de, a qualquer momento, se opor por motivos relacionados com a sua situação particular, ao tratamento dos dados pessoais que lhe digam respeito, podendo se opor ainda ao uso de seus dados pessoais para definição de perfil de marketing (profiling);',
            ),
            BuildParagraph(
              '- Direito de portabilidade dos dados: é o direito do usuário de receber os dados pessoais que lhe digam respeito e que tenha fornecido ao aplicativo, num formato estruturado, de uso corrente e de leitura automática, e o direito de transmitir esses dados a outro aplicativo;',
            ),
            BuildParagraph(
              '- Direito de não ser submetido a decisões automatizadas: é o direito do usuário de não ficar sujeito a nenhuma decisão tomada exclusivamente com base no tratamento automatizado, incluindo a definição de perfis (profiling), que produza efeitos na sua esfera jurídica ou que o afete significativamente de forma similar.',
            ),
            BuildParagraph(
              'O usuário poderá exercer os seus direitos por meio de comunicação escrita, especificando:',
            ),
            BuildParagraph(
              '- Nome completo ou razão social, número do CPF (Cadastro de Pessoas Físicas, da Receita Federal do Brasil) ou CNPJ (Cadastro Nacional de Pessoa Jurídica, da Receita Federal do Brasil) e endereço de e-mail do usuário e, se for o caso, do seu representante;',
            ),
            BuildParagraph(
              '- Direito que deseja exercer junto ao aplicativo;',
            ),
            BuildParagraph(
              '- Data do pedido e assinatura do usuário;',
            ),
            BuildParagraph(
              '- Todo documento que possa demonstrar ou justificar o exercício de seu direito.',
            ),
            BuildParagraph(
              'O pedido deverá ser enviado ao e-mail: contato@pataki.com.br, ou por correio, ao seguinte endereço:',
            ),
            BuildParagraph(
              'Rua Salvador Corrêa, 603 – Jardim Vergueiro – Sorocaba/SP, CEP: 18030-130.',
            ),
            BuildParagraph(
              'O usuário será informado em caso de retificação ou eliminação dos seus dados.',
            ),
            BuildSectionTitle('3. Dever de não fornecer dados de terceiros'),
            BuildParagraph(
              'Durante a utilização do aplicativo, a fim de resguardar e de proteger os direitos de terceiros, o usuário do aplicativo deverá fornecer somente seus dados pessoais, e não os de terceiros.',
            ),
            BuildSectionTitle('4. Informações coletadas'),
            BuildParagraph(
              'A coleta de dados dos usuários se dará em conformidade com o disposto nesta Política de Privacidade e dependerá do consentimento do usuário, sendo este dispensável somente nas hipóteses previstas no art. 11, inciso II, da Lei de Proteção de Dados Pessoais.',
            ),
            BuildSectionTitle('4.1. Tipos de dados coletados'),
            BuildSectionTitle('4.1.1. Dados informados no formulário de contato'),
            BuildParagraph(
              'Os dados eventualmente informados pelo usuário que utilizar o formulário de contato disponibilizado no aplicativo, incluindo o teor da mensagem enviada, serão coletados e armazenados.',
            ),
            BuildSectionTitle(
                '4.1.2. Dados relacionados à execução de contratos firmados com o usuário'),
            BuildParagraph(
              'Para a execução de contrato de compra e venda ou de prestação de serviços eventualmente firmado entre o aplicativo e o usuário, poderão ser coletados e armazenados outros dados relacionados ou necessários a sua execução, incluindo o teor de eventuais comunicações tidas com o usuário.',
            ),
            BuildSectionTitle('4.1.3. Dados sensíveis'),
            BuildParagraph(
              'Não serão coletados dados sensíveis dos usuários, assim entendidos aqueles definidos nos arts. 9º e 10 do RGPD e nos arts. 11 e seguintes da Lei de Proteção de Dados Pessoais. Assim, dentre outros, não haverá coleta dos seguintes dados:',
            ),
            BuildParagraph(
              '- dados que revelem a origem racial ou étnica, as opiniões políticas, as convicções religiosas ou filosóficas, ou a filiação sindical do usuário;',
            ),
            BuildParagraph(
              '- dados genéticos;',
            ),
            BuildParagraph(
              '- dados biométricos para identificar uma pessoa de forma inequívoca;',
            ),
            BuildParagraph(
              '- dados relativos à saúde do usuário;',
            ),
            BuildParagraph(
              '- dados relativos à vida sexual ou à orientação sexual do usuário;',
            ),
            BuildParagraph(
              '- dados relacionados a condenações penais ou a infrações ou com medidas de segurança conexas.',
            ),
            BuildSectionTitle('4.1.4. Coleta de dados não previstos expressamente'),
            BuildParagraph(
              'Eventualmente, outros tipos de dados não previstos expressamente nesta Política de Privacidade poderão ser coletados, desde que sejam fornecidos com o consentimento do usuário, ou, ainda, que a coleta seja permitida ou imposta por lei.',
            ),
            BuildSectionTitle('4.2. Fundamento jurídico para o tratamento dos dados pessoais'),
            BuildParagraph(
              'Ao utilizar os serviços do aplicativo, o usuário está consentindo com a presente Política de Privacidade.',
            ),
            BuildParagraph(
              'O usuário tem o direito de retirar seu consentimento a qualquer momento, não comprometendo a licitude do tratamento de seus dados pessoais antes da retirada. A retirada do consentimento poderá ser feita pelo e-mail: contato@pataki.com.br, ou por correio enviado ao seguinte endereço:',
            ),
            BuildParagraph(
              'Rua Salvador Corrêa, 603 – Jardim Vergueiro – Sorocaba/SP, CEP: 18030-130',
            ),
            BuildParagraph(
              'O consentimento dos relativamente ou absolutamente incapazes, especialmente de crianças menores de 16 (dezesseis) anos, apenas poderá ser feito, respectivamente, se devidamente assistidos ou representados.',
            ),
            BuildParagraph(
              'Poderão ainda ser coletados dados pessoais necessários para a execução e cumprimento dos serviços contratados pelo usuário no aplicativo.',
            ),
            BuildParagraph(
              'O tratamento de dados pessoais sem o consentimento do usuário apenas será realizado em razão de interesse legítimo ou para as hipóteses previstas em lei, ou seja, dentre outras, as seguintes:',
            ),
            BuildParagraph(
              '- para o cumprimento de obrigação legal ou regulatória pelo controlador;',
            ),
            BuildParagraph(
              '- para a realização de estudos por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;',
            ),
            BuildParagraph(
              '- quando necessário para a execução de contrato ou de procedimentos preliminares relacionados a contrato do qual seja parte o usuário, a pedido do titular dos dados;',
            ),
            BuildParagraph(
              '- para o exercício regular de direitos em processo judicial, administrativo ou arbitral, esse último nos termos da Lei nº 9.307, de 23 de setembro de 1996 (Lei de Arbitragem);',
            ),
            BuildParagraph(
              '- para a proteção da vida ou da incolumidade física do titular dos dados ou de terceiros;',
            ),
            BuildParagraph(
              '- para a tutela da saúde, em procedimento realizado por profissionais da área da saúde ou por entidades sanitárias;',
            ),
            BuildParagraph(
              '- quando necessário para atender aos interesses legítimos do controlador ou de terceiros, exceto no caso de prevalecerem direitos e liberdades fundamentais do titular dos dados que exijam a proteção dos dados pessoais;',
            ),
            BuildParagraph(
              '- para a proteção do crédito, inclusive quanto ao disposto na legislação pertinente.',
            ),
            BuildSectionTitle('4.3. Finalidades do tratamento dos dados pessoais'),
            BuildParagraph(
              'Os dados pessoais do usuário coletados pelo aplicativo têm por finalidade facilitar, agilizar e cumprir os compromissos estabelecidos com o usuário e a fazer cumprir as solicitações realizadas por meio do preenchimento de dados cadastrais.',
            ),
            BuildParagraph(
              'Os dados pessoais poderão ser utilizados também com uma finalidade comercial, para personalizar o conteúdo oferecido ao usuário, bem como para dar subsídio ao aplicativo para a melhora da qualidade e funcionamento de seus serviços.',
            ),
            BuildParagraph(
              'A coleta de dados relacionados ou necessários à execução de um contrato de compra e venda ou de prestação de serviços eventualmente firmado com o usuário terá a finalidade de conferir às partes segurança jurídica, além de facilitar e viabilizar a conclusão do negócio.',
            ),
            BuildParagraph(
              'O tratamento de dados pessoais para finalidades não previstas nesta Política de Privacidade somente ocorrerá mediante comunicação prévia ao usuário, sendo que, em qualquer caso, os direitos e obrigações aqui previstos permanecerão aplicáveis.',
            ),
            BuildSectionTitle('4.4. Prazo de conservação dos dados pessoais'),
            BuildParagraph(
              'Os dados pessoais do usuário serão conservados por um período não superior ao exigido para cumprir os objetivos em razão dos quais eles são processados.',
            ),
            BuildParagraph(
              'O período de conservação dos dados é definido de acordo com os seguintes critérios:',
            ),
            BuildParagraph(
              'Os dados serão armazenados pelo tempo necessário para a prestação dos serviços fornecidos pelo aplicativo.',
            ),
            BuildParagraph(
              'Os dados pessoais dos usuários apenas poderão ser conservados após o término de seu tratamento nas seguintes hipóteses:',
            ),
            BuildParagraph(
              '- para o cumprimento de obrigação legal ou regulatória pelo controlador;',
            ),
            BuildParagraph(
              '- para estudo por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;',
            ),
            BuildParagraph(
              '- para a transferência a terceiros, desde que respeitados os requisitos de tratamento de dados dispostos na legislação;',
            ),
            BuildParagraph(
              '- para uso exclusivo do controlador, vedado seu acesso por terceiros, e desde que anonimizados os dados.',
            ),
            BuildSectionTitle('4.5. Destinatários e transferência dos dados pessoais'),
            BuildParagraph(
              'Os dados pessoais do usuário poderão ser compartilhados com as seguintes pessoas ou empresas:',
            ),
            BuildParagraph(
              'Nome: ??',
            ),
            BuildParagraph(
              'Endereço: ',
            ),
            BuildParagraph(
              'A transferência apenas poderá ser feita para outro país caso o país ou território em questão ou a organização internacional em causa assegurem um nível de proteção adequado dos dados do usuário.',
            ),
            BuildParagraph(
              'Caso não haja nível de proteção adequado, o aplicativo se compromete a garantir a proteção dos seus dados de acordo com as regras mais rigorosas, por meio de cláusulas contratuais específicas para determinada transferência, cláusulas-padrão contratuais, normas corporativas globais ou selos, certificados e códigos de conduta regularmente emitidos.',
            ),
            BuildSectionTitle('5. Do tratamento dos dados pessoais'),
            BuildSectionTitle('5.1. Do responsável pelo tratamento dos dados (data controller)'),
            BuildParagraph(
              'O controlador, responsável pelo tratamento dos dados pessoais do usuário, é a pessoa física ou jurídica, a autoridade pública, a agência ou outro organismo que, individualmente ou em conjunto com outras, determina as finalidades e os meios de tratamento de dados pessoais.',
            ),
            BuildParagraph(
              'Neste aplicativo, o responsável pelo tratamento dos dados pessoais coletados é PATAKI CONEXAO PET LTDA, representada por Victor Guilherme Lopes dos Reis, que poderá ser contactado pelo e-mail: victor@pataki.com.br ou no endereço: Rua Salvador Corrêa, 603 – Jardim Vergueiro, Sorocaba/SP – CEP: 18030-130.',
            ),
            BuildSectionTitle('5.2. Do operador de dados subcontratado (data processor)'),
            BuildParagraph(
              'O operador de dados subcontratado é a pessoa física ou jurídica, a autoridade pública, a agência ou outro organismo que trata os dados pessoais sob a supervisão do responsável pelo tratamento dos dados do usuário.',
            ),
            BuildParagraph(
              'Os dados pessoais do usuário serão tratados pela pessoa jurídica: PATAKI CONEXÃO PET LTDA, devidamente registrada sob o CNPJ n. 39.364.280/0001-52, e-mail: victor@pataki.com.br, com sede em: Rua Salvador Corrêa, 603 – Jardim Vergueiro, Sorocaba/SP – CEP: 18030-130.',
            ),
            BuildSectionTitle('5.3. Do encarregado de proteção de dados (data protection officer)'),
            BuildParagraph(
              'O encarregado de proteção de dados (data protection officer) é o profissional encarregado de informar, aconselhar e controlar o responsável pelo tratamento dos dados e o processador de dados subcontratado, bem como os trabalhadores que tratem os dados, a respeito das obrigações do aplicativo nos termos do RGDP, da Lei de Proteção de Dados Pessoais e de outras disposições de proteção de dados presentes na legislação nacional e internacional, em cooperação com a autoridade de controle competente.',
            ),
            BuildParagraph(
              'Neste aplicativo o encarregado de proteção de dados (data protection officer) é Victor Guilherme Lopes dos Reis, que poderá ser contactado(a) pelo e-mail: victor@pataki.com.br.',
            ),
            BuildSectionTitle('6. Segurança no tratamento dos dados pessoais do usuário'),
            BuildParagraph(
              'O aplicativo se compromete a aplicar as medidas técnicas e organizativas aptas a proteger os dados pessoais de acessos não autorizados e de situações de destruição, perda, alteração, comunicação ou difusão de tais dados.',
            ),
            BuildParagraph(
              'Para a garantia da segurança, serão adotadas soluções que levem em consideração: as técnicas adequadas; os custos de aplicação; a natureza, o âmbito, o contexto e as finalidades do tratamento; e os riscos para os direitos e liberdades do usuário.',
            ),
            BuildParagraph(
              'O aplicativo utiliza certificado SSL (Secure Socket Layer) que garante que os dados pessoais se transmitam de forma segura e confidencial, de maneira que a transmissão dos dados entre o servidor e o usuário, e em retroalimentação, ocorra de maneira totalmente cifrada ou encriptada.',
            ),
            BuildParagraph(
              'No entanto, o aplicativo se exime de responsabilidade por culpa exclusiva de terceiros, como em caso de ataque de hackers ou crackers, ou culpa exclusiva do usuário, como no caso em que ele mesmo transfere seus dados a terceiros. O aplicativo se compromete, ainda, a comunicar o usuário em prazo adequado caso ocorra algum tipo de violação da segurança de seus dados pessoais que possa lhe causar um alto risco para seus direitos e liberdades pessoais.',
            ),
            BuildParagraph(
              'A violação de dados pessoais é uma violação de segurança que provoque, de modo acidental ou ilícito, a destruição, a perda, a alteração, a divulgação ou o acesso não autorizado a dados pessoais transmitidos, conservados ou sujeitos a qualquer outro tipo de tratamento.',
            ),
            BuildParagraph(
              'Por fim, o aplicativo se compromete a tratar os dados pessoais do usuário com confidencialidade, dentro dos limites legais.',
            ),
            BuildSectionTitle('7. Dados de navegação (cookies)'),
            BuildParagraph(
              'Cookies são pequenos arquivos de texto enviados pelo aplicativo ao computador do usuário e que nele ficam armazenados, com informações relacionadas à navegação do aplicativo.',
            ),
            BuildParagraph(
              'Por meio dos cookies, pequenas quantidades de informação são armazenadas pelo navegador do usuário para que nosso servidor possa lê-las posteriormente. Podem ser armazenados, por exemplo, dados sobre o dispositivo utilizado pelo usuário, bem como seu local e horário de acesso ao aplicativo.',
            ),
            BuildParagraph(
              'Os cookies não permitem que qualquer arquivo ou informação sejam extraídos do disco rígido do usuário, não sendo possível, ainda, que, por meio deles, se tenha acesso a informações pessoais que não tenham partido do usuário ou da forma como utiliza os recursos do aplicativo.',
              sizedBox: true,
            ),
            BuildParagraph(
              'É importante ressaltar que nem todo cookie contém informações que permitem a identificação do usuário, sendo que determinados tipos de cookies podem ser empregados simplesmente para que o aplicativo seja carregado corretamente ou para que suas funcionalidades funcionem do modo esperado.',
            ),
            BuildParagraph(
              'As informações eventualmente armazenadas em cookies que permitam identificar um usuário são consideradas dados pessoais. Dessa forma, todas as regras previstas nesta Política de Privacidade também lhes são aplicáveis.',
            ),
            BuildSectionTitle('7.1. Cookies do aplicativoc'),
            BuildParagraph(
              'Os cookies do aplicativo são aqueles enviados ao computador ou dispositivo do usuário e administrador exclusivamente pelo aplicativo.',
            ),
            BuildParagraph(
              'As informações coletadas por meio destes cookies são utilizadas para melhorar e personalizar a experiência do usuário, sendo que alguns cookies podem, por exemplo, ser utilizados para lembrar as preferências e escolhas do usuário, bem como para o oferecimento de conteúdo personalizado.',
            ),
            BuildSectionTitle('7.2. Cookies de redes sociais'),
            BuildParagraph(
              'O aplicativo utiliza plugins de redes sociais, que permitem acessá-las a partir do aplicativo. Assim, ao fazê-lo, os cookies utilizados por elas poderão ser armazenados no navegador do usuário.',
            ),
            BuildParagraph(
              'Cada rede social possui sua própria política de privacidade e de proteção de dados pessoais, sendo as pessoas físicas ou jurídicas que as mantêm responsáveis pelos dados coletados e pelas práticas de privacidade adotadas.',
            ),
            BuildParagraph(
              'O usuário pode pesquisar, junto às redes sociais, informações sobre como seus dados pessoais são tratados. A título informativo, disponibilizamos os seguintes links, a partir dos quais poderão ser consultadas as políticas de privacidade e de cookies adotadas por algumas das principais redes sociais:',
            ),
            _buildLink(
              text: 'Facebook: ',
              link: 'https://www.facebook.com/policies/cookies/',
            ),
            _buildLink(
              text: 'Twitter: ',
              link: 'https://twitter.com/pt/privacy',
            ),
            _buildLink(
              text: 'Instagram: ',
              link: 'https://help.instagram.com/1896641480634370?ref=ig',
            ),
            _buildLink(
              text: 'Youtube: ',
              link: 'https://policies.google.com/privacy?hl=pt-BR&gl=pt',
            ),
            _buildLink(
              text: 'Google+: ',
              link: 'https://policies.google.com/technologies/cookies?hl=pt',
            ),
            _buildLink(
              text: 'Pinterest: ',
              link: 'https://policy.pinterest.com/pt-br/privacy-policy',
            ),
            _buildLink(
              text: 'LinkedIn: ',
              link: 'https://www.linkedin.com/legal/cookie-policy?trk=hp-cookies',
            ),
            BuildSectionTitle('8. Reclamação a uma autoridade de controle'),
            BuildParagraph(
              'Sem prejuízo de qualquer outra via de recurso administrativo ou judicial, todos os titulares de dados têm direito a apresentar reclamação a uma autoridade de controle. A reclamação poderá ser feita à autoridade da sede do aplicativo, do país de residência habitual do usuário, do seu local de trabalho ou do local onde foi alegadamente praticada a infração.',
            ),
            BuildSectionTitle('9. Das alterações'),
            BuildParagraph(
              'A presente versão desta Política de Privacidade foi atualizada pela última vez em: 15/12/2020.',
            ),
            BuildParagraph(
              'O editor se reserva o direito de modificar, a qualquer momento e sem qualquer aviso prévio, o aplicativo, as presentes normas, especialmente para adaptá-las às evoluções do aplicativo Pataki, seja pela disponibilização de novas funcionalidades, seja pela supressão ou modificação daquelas já existentes.',
            ),
            BuildParagraph(
              'Dessa forma, convida-se o usuário a consultar periodicamente esta página para verificar as atualizações.',
            ),
            BuildParagraph(
              'Ao utilizar o serviço após eventuais modificações, o usuário demonstra sua concordância com as novas normas. Caso discorde de alguma das modificações, deverá interromper, imediatamente, o acesso ao aplicativo e apresentar a sua ressalva ao serviço de atendimento, se assim o desejar.',
            ),
            BuildSectionTitle('10. Do Direito aplicável e do foro'),
            BuildParagraph(
              'Para a solução das controvérsias decorrentes do presente instrumento, será aplicado integralmente o Direito brasileiro.',
            ),
            BuildParagraph(
              'Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede do editor do aplicativo.',
            ),
          ],
        ),
      ),
    );
  }

  _buildLink({@required String text, @required String link}) {
    return RichText(
      text: TextSpan(
        text: text,
        style: TextStyle(
          color: Colors.black,
          fontSize: 14,
        ),
        children: [
          TextSpan(
            text: link,
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                _launchURL(link);
              },
          )
        ],
      ),
    );
  }

  _launchURL(String link) async {
    if (await canLaunch(link)) {
      await launch(link);
    } else {
      throw 'Could not launch $link';
    }
  }
}
