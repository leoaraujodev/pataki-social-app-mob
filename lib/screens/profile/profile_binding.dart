import 'package:get/get.dart';
import 'package:pataki/screens/profile/profile_controller.dart';

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ProfileController(), tag: Get.arguments["timestamp"]);
  }
}
