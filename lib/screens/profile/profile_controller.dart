import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/block_user_endpoint.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/endpoints/user_info_endpoint.dart';
import 'package:pataki/core/endpoints/user_posts_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/block_user_request.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/models/timeline_response.dart';
import 'package:pataki/core/models/user_info_request.dart';
import 'package:pataki/core/models/user_info_response.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/models/user_posts_request.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/home/home_screen.dart';

class ProfileController extends GetxController {
  final homeController = Get.find<HomeController>();

  int userId = int.parse(Get.parameters["userId"]);
  final Rx<UserModel> user = UserModel().obs;
  final loggedUserId = RxInt();

  final scrollController = ScrollController();
  final posts = RxList<TimelinePost>([]);

  final hasMorePosts = false.obs;
  final errorOnMorePosts = false.obs;

  final loadingUserInfo = true.obs;
  final errorOnUserInfo = false.obs;

  int _lastId;
  bool _fetchingMutex = false;
  bool _likingMutex = false;

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onInit() {
    getUserInfo().then((value) {
      user.refresh();
    });
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMorePosts.value && !errorOnMorePosts.value) getMorePosts();
      }
    });
    super.onInit();
  }

  Future<void> getUserInfo() async {
    loggedUserId.value = await GetUserIdInteractor().execute();
    loadingUserInfo.value = true;
    errorOnUserInfo.value = false;

    try {
      UserInfoResponse userResponse =
          await UserInfoEndpoint(UserInfoRequest(userId: userId)).call();

      user.value = userResponse.user;

      user.refresh();

      final postsResponse = await _getPetPostsResponse();
      _setValues(postsResponse);
    } catch (e) {
      errorOnUserInfo.value = true;
      _showErrorSnackbar();
    }

    loadingUserInfo.value = false;
  }

  Future<void> blockUser(int id) async {
    try {
      SuccessMessageResponse response = await BlockUserEndpoint(BlockUserRequest(
        id: id,
        block: true,
      )).call();

      homeController.doRefresh.value = true;

      Get.until((route) => Get.currentRoute == HomeScreen.id);
      Get.snackbar(response.message, "O tutor não poderá mais ver seu usuário, pets e postagens.");
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousLastId = _lastId;

    try {
      _lastId = null;
      final response = await _getPetPostsResponse();
      posts.clear();
      _setValues(response);
    } catch (e) {
      _lastId = previousLastId;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMorePosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMorePosts.value = false;

    try {
      final response = await _getPetPostsResponse();
      _setValues(response);
    } catch (e) {
      errorOnMorePosts.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<TimelineResponse> _getPetPostsResponse() =>
      UserPostsEndpoint(UserPostsRequest(userId: userId, lastId: _lastId)).call();

  void _setValues(TimelineResponse response) {
    posts.addAll(response.posts);

    if (response.posts.isNotEmpty) _lastId = response.posts.last.id;

    if (response.posts.length < 15) {
      hasMorePosts.value = false;
    } else {
      hasMorePosts.value = true;
    }
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: like,
        imagePath: posts[index].imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();
    } catch (e) {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: !like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: !like,
        imagePath: posts[index].imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      int index = posts.indexWhere((post) => post.id == id);

      posts.removeAt(index);
      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  void updateCommentCount(int timelineId, int newCount) {
    int index = posts.indexWhere((post) => post.id == timelineId);

    if (index == -1) return;

    posts[index] = posts[index].copyWith(
      imagePath: posts[index].imagePath,
      totComment: newCount,
    );
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
