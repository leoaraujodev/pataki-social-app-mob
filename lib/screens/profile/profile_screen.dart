import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/core/services/notification_service.dart';
import 'package:pataki/screens/chat_room_details/chat_room_details_screen.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/following/following_screen.dart';
import 'package:pataki/screens/group_list/group_list_screen.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/pet_list/pet_list_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/screens/report_user/report_user_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_button.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';
import 'profile_controller.dart';

double screenWidth;

class ProfileScreen extends GetView<ProfileController> {
  static String routeName(userId) => "/profile_screen/$userId";

  final notificationService = Get.find<NotificationService>();

  @override
  final String tag = Get.arguments["timestamp"];

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: PatakiColors.lightGray,
      appBar: DefaultAppBar(
        title: "Tutor",
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (controller.loadingUserInfo.value) {
            return Center(child: RefreshProgressIndicator());
          }

          return ListView(
            physics: AlwaysScrollableScrollPhysics(),
            controller: controller.scrollController,
            children: [
              _buildHeader(),
              SizedBox(height: 20),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: controller.posts.length,
                itemBuilder: (context, index) {
                  Widget card = TimelinePostCard(
                    post: controller.posts[index],
                    onCommentPressed: () => Get.toNamed(
                      CommentScreen.routeName(controller.posts[index].id),
                      arguments: {
                        "updateCommentCount": controller.updateCommentCount,
                        "timestamp": DateTime.now().toIso8601String(),
                      },
                    ),
                    onLikePressed: () => controller.likeComment(
                      controller.posts[index].id,
                      !controller.posts[index].liked,
                    ),
                    onLikesPressed: () => Get.toNamed(
                      LikesScreen.routeName(controller.posts[index].id),
                      arguments: {"timestamp": DateTime.now().toIso8601String()},
                    ),
                    onMorePressed: () => Get.dialog(
                      OptionsDialog(
                        title: "Opções da Postagem",
                        onReportPressed:
                            controller.loggedUserId.value != controller.posts[index].user.id
                                ? () async {
                                    int reportedPostId = controller.posts[index].id;

                                    var reported = await Get.offNamed(
                                      ReportTimelinePostScreen.routeName,
                                      arguments: {"post": controller.posts[index]},
                                    );

                                    if (reported == true) {
                                      controller.posts.removeWhere(
                                        (post) => post.id == reportedPostId,
                                      );
                                    }
                                  }
                                : null,
                        onEditPressed:
                            controller.loggedUserId.value == controller.posts[index].user.id
                                ? () async {
                                    await Get.offNamed(
                                      EditPostScreen.routeName(controller.posts[index].id),
                                    );
                                  }
                                : null,
                        onDeletePressed:
                            controller.loggedUserId.value == controller.posts[index].user.id
                                ? () {
                                    Get.back();
                                    Get.dialog(
                                      ConfirmDialog(
                                        onConfirmPressed: () => controller.deletePost(
                                          controller.posts[index].id,
                                        ),
                                      ),
                                    );
                                  }
                                : null,
                      ),
                    ),
                    onSendPressed: () {},
                    onUserPressed: () {
                      Get.toNamed(
                        PetScreen.routeName(controller.posts[index].pet.id),
                        arguments: {"timestamp": DateTime.now().toIso8601String()},
                      );
                    },
                  );

                  if (index == controller.posts.length - 1 && controller.hasMorePosts.value) {
                    return Column(
                      children: [
                        card,
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Obx(
                            () => controller.errorOnMorePosts.value
                                ? Center(
                                    child: GestureDetector(
                                      onTap: controller.getMorePosts,
                                      child: RefreshProgressIndicator(
                                        value: 1.0,
                                      ),
                                    ),
                                  )
                                : RefreshProgressIndicator(),
                          ),
                        ),
                      ],
                    );
                  }

                  return card;
                },
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      padding: screenWidth >= 375
          ? EdgeInsets.symmetric(horizontal: 24, vertical: 16)
          : EdgeInsets.symmetric(horizontal: 18, vertical: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          IntrinsicHeight(
            child: Row(
              children: [
                _buildUserPhoto(),
                Expanded(
                  child:
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _buildProfileHeader(
                        user: controller.user.value,
                      ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            _buildButtonProfileSummary(
                              numericInfo: "${controller.user.value.following}",
                              typeInfo: "SEGUINDO",
                              onPressed: () {
                                Get.toNamed(
                                  FollowingScreen.routeName(controller.user.value.id),
                                );
                              },
                            ),
                            _buildButtonProfileSummary(
                              numericInfo: controller.user.value.pets != null
                                  ? "${controller.user.value.pets.length}"
                                  : "0",
                              typeInfo: "PETS",
                              onPressed: () {
                                Get.toNamed(
                                  PetListScreen.routeName(controller.user.value.id),
                                  arguments: {
                                    "timestamp": DateTime.now().toIso8601String(),
                                  },
                                );
                              },
                            ),
                            _buildButtonProfileSummary(
                              numericInfo: "${controller.user.value.groups}",
                              typeInfo: "GRUPOS",
                              onPressed: () {
                                Get.toNamed(
                                  GroupListScreen.routeName(controller.user.value.id),
                                  arguments: {
                                    "timestamp": DateTime.now().toIso8601String(),
                                    "isFromMyProfile": false,
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            width: double.infinity,
            child: Text(
              (controller.user.value.desc != null && controller.user.value.desc != "null")
                  ? "${controller.user.value.desc}"
                  : "",
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //TODO
          // Container(
          //   height: 0, // REMOVE THIS LINE TO SHOW BADGE "ADVANCED"
          //   width: 0, // REMOVE THIS LINE TO SHOW BADGE "ADVANCED"
          //   padding: EdgeInsets.symmetric(vertical: 6, horizontal: 14),
          //   decoration: BoxDecoration(
          //     color: Color(0xFFFBBC05),
          //     borderRadius: BorderRadius.circular(30),
          //   ),
          //   child: Text(
          //     "AVANÇADO",
          //     style: TextStyle(
          //       fontSize: 12,
          //       color: Colors.white,
          //     ),
          //   ),
          // ),
          if (controller.userId != controller.loggedUserId.value)
            BuildButton(
              text: "Enviar mensagem",
              textColor: Colors.black,
              borderColor: PatakiColors.lightGray,
              onPressed: () async {
                notificationService.deactivateInFocusNotifications();
                await Get.toNamed(
                  ChatRoomDetailsScreen.routeName(controller.user.value.id),
                  arguments: {"timestamp": DateTime.now().toIso8601String()},
                );
                notificationService.activateInFocusNotifications();
              },
            ),
          //   ],
          // ),
        ],
      ),
    );
  }

  _buildUserPhoto() {
    return Padding(
      padding: EdgeInsets.only(right: 10),
      child: Container(
        width: screenWidth >= 375 ? 90 : 60,
        height: screenWidth >= 375 ? 90 : 60,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: NetworkImage(controller.user.value.imagePath),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  _buildProfileHeader({
    @required UserModel user,
    String userName,
  }) {
    if (user.name != null && user.name.length >= 17) {
      userName = user.name.substring(0, 17) + "...";
    } else {
      userName = user.name;
    }

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8),
          child: Text(
            "$userName",
            style: TextStyle(
              fontSize: screenWidth >= 375 ? 20 : 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 36,
          width: 36,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            padding: EdgeInsets.zero,
            child: Icon(
              Icons.more_vert,
              color: Color(0xFF90AECC),
            ),
            onPressed: () => Get.dialog(
              OptionsDialog(
                title: "Opções do Usuário",
                onReportPressed: controller.loggedUserId.value != controller.userId
                    ? () async {
                        await Get.offNamed(
                          ReportUserScreen.routeName,
                          arguments: {"user": user},
                        );
                      }
                    : null,
                onBlockPressed: controller.loggedUserId.value != controller.userId
                    ? () {
                        Get.back();
                        Get.dialog(
                          ConfirmDialog(
                            onConfirmPressed: () => controller.blockUser(
                              controller.user.value.id,
                            ),
                            action: "bloquear",
                          ),
                        );
                      }
                    : null,
                onSharePressed: () {
                  Clipboard.setData(
                    new ClipboardData(
                      text: "https://pataki.com.br/#/profile_screen/${controller.user.value.id}",
                    ),
                  );
                  Get.back();
                },
                shareType: "perfil",
              ),
            ),
          ),
        ),
      ],
    );
  }

  _buildButtonProfileSummary({
    @required String numericInfo,
    @required String typeInfo,
    Function onPressed,
  }) {
    return onPressed == null
        ? _buildProfileSummary(numericInfo: numericInfo, typeInfo: typeInfo)
        : TextButton(
            onPressed: onPressed,
            child: _buildProfileSummary(numericInfo: numericInfo, typeInfo: typeInfo),
          );
  }

  _buildProfileSummary({
    @required String numericInfo,
    @required String typeInfo,
  }) {
    return Column(
      children: [
        Text(
          numericInfo,
          style: TextStyle(
            color: Color(0xFF222222),
            fontSize: screenWidth >= 375 ? 22 : 18,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          typeInfo,
          style: TextStyle(
            color: Color(0xFF222222),
            fontSize: screenWidth >= 375 ? 12 : 8,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
