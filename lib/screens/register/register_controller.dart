import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/create_user_endpoint.dart';
import 'package:pataki/core/interactor/set_token_interactor.dart';
import 'package:pataki/core/interactor/set_user_id_interactor.dart';
import 'package:pataki/core/models/create_user_request.dart';
import 'package:pataki/screens/register_pet_option_screen/register_pet_option_screen.dart';
import 'package:pataki/screens/validators/email_validator.dart';
import 'package:pataki/screens/validators/name_validator.dart';

class RegisterController extends GetxController with EmailValidator, NameValidator {
  final formKey = GlobalKey<FormState>();

  final passwordFocusNode = FocusNode();
  final isPasswordValid = false.obs;
  final passwordAutovalidateMode = AutovalidateMode.disabled.obs;
  final passwordController = TextEditingController();

  final newsletter = false.obs;
  final terms = false.obs;
  final policy = false.obs;
  final submitting = false.obs;

  @override
  onInit() {
    nameFocusNode.addListener(() {
      if (!nameFocusNode.hasFocus) {
        nameValidator(nameController.text);
        nameOnChanged(nameController.text);
      }
    });
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailValidator(emailController.text);
        emailOnChanged(emailController.text);
      }
    });
    passwordFocusNode.addListener(() {
      if (!passwordFocusNode.hasFocus) {
        passwordValidator(passwordController.text);
        passwordOnChanged(passwordController.text);
      }
    });
    super.onInit();
  }

  String passwordValidator(String value) {
    passwordAutovalidateMode.value = AutovalidateMode.always;
    if (value.length >= 8) {
      return null;
    }

    return 'Senha precisa ter no mínimo 8 dígitos';
  }

  void passwordOnChanged(String value) {
    if (passwordAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = passwordValidator(value);
    if (error == null) {
      isPasswordValid.value = true;
    } else {
      isPasswordValid.value = false;
    }
  }

  Future<void> submit() async {
    if (!terms.value) {
      Get.snackbar(
          "Atenção", "Você deve concordar com nossos termos de uso para concluir o cadastro");
      return;
    }
    if (!policy.value) {
      Get.snackbar("Atenção",
          "Você deve concordar com nossa política de privacidade para concluir o cadastro");
      return;
    }

    nameValidator(nameController.text);
    emailValidator(emailController.text);
    passwordValidator(passwordController.text);

    nameOnChanged(nameController.text);
    emailOnChanged(emailController.text);
    passwordOnChanged(passwordController.text);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seus dados, um ou mais campos apresentam erros");
      return;
    }

    if (submitting.value == true) return;
    submitting.value = true;

    var request = CreateUserRequest(
        name: nameController.text,
        email: emailController.text,
        password: passwordController.value.text);

    try {
      var userResponse = await CreateUserEndpoint(request).call();
      await SetTokenInteractor().execute(userResponse.token);
      await SetUserIdInteractor().execute(userResponse.user.id);
      Get.offAllNamed(RegisterPetOptionScreen.id);
    } catch (exception) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
    submitting.value = false;
  }

  @override
  void onClose() {
    nameFocusNode.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    super.onClose();
  }
}
