import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/register/register_controller.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';
import 'package:pataki/uikit/widgets/terms_bottom_navigation_bar.dart';

class RegisterScreen extends GetView<RegisterController> {
  RegisterScreen({Key key}) : super(key: key);
  static String id = "register_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Criar Conta",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FormTextField(
                        focusNode: controller.nameFocusNode,
                        controller: controller.nameController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.nameAutovalidateMode.value,
                          isValid: controller.isNameValid.value,
                        ),
                        hintText: "Nome",
                        autovalidateMode: controller.nameAutovalidateMode.value,
                        validator: controller.nameValidator,
                        onFieldSubmitted: (value) {
                          controller.nameValidator(value);
                          controller.nameOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.emailFocusNode);
                        },
                        onChanged: controller.nameOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.emailFocusNode,
                        controller: controller.emailController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.emailAutovalidateMode.value,
                          isValid: controller.isEmailValid.value,
                        ),
                        hintText: "Email",
                        autovalidateMode: controller.emailAutovalidateMode.value,
                        validator: controller.emailValidator,
                        onFieldSubmitted: (value) {
                          controller.emailValidator(value);
                          controller.emailOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.passwordFocusNode);
                        },
                        onChanged: controller.emailOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      FormTextField(
                        focusNode: controller.passwordFocusNode,
                        controller: controller.passwordController,
                        canObscure: true,
                        hintText: "Senha",
                        autovalidateMode: controller.passwordAutovalidateMode.value,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.passwordAutovalidateMode.value,
                          isValid: controller.isPasswordValid.value,
                        ),
                        validator: controller.passwordValidator,
                        onFieldSubmitted: (value) {
                          controller.passwordValidator(value);
                          controller.passwordOnChanged(value);
                          FocusScope.of(context).unfocus();
                        },
                        onChanged: controller.passwordOnChanged,
                        keyboardType: TextInputType.visiblePassword,
                      ),
                      //TODO
                      // Padding(
                      //   padding: EdgeInsets.only(left: 20),
                      //   child: _buildCheckboxRow(
                      //     text: Text(
                      //       "Desejo receber ofertas por email",
                      //       style: TextStyle(
                      //         fontWeight: FontWeight.w500,
                      //         color: Color(0xFF66839F),
                      //       ),
                      //     ),
                      //     value: controller.newsletter.value,
                      //     onChanged: (value) => controller.newsletter.value = value,
                      //   ),
                      // ),
                      Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: _buildCheckboxRow(
                          text: Text(
                            "Li e aceito os Termos de Uso",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF66839F),
                            ),
                          ),
                          value: controller.terms.value,
                          onChanged: (value) => controller.terms.value = value,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, bottom: 20),
                        child: _buildCheckboxRow(
                          text: Text(
                            "Li e aceito a Política de Privacidade",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF66839F),
                            ),
                          ),
                          value: controller.policy.value,
                          onChanged: (value) => controller.policy.value = value,
                        ),
                      ),
                      LargeTextButton(
                        loading: controller.submitting.value,
                        onPressed: controller.submit,
                        text: 'CRIAR CONTA',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
          bottomNavigationBar: TermsBottomNavigationBar(),
        ),
      ),
    );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }

  Widget _buildCheckboxRow({
    @required Widget text,
    @required bool value,
    @required void Function(bool) onChanged,
  }) {
    return Row(
      children: [
        Expanded(
          child: text,
        ),
        Theme(
          data: ThemeData(unselectedWidgetColor: Color(0xFF90AECC)),
          child: Checkbox(
            visualDensity: VisualDensity.compact,
            value: value,
            onChanged: onChanged,
          ),
        ),
      ],
    );
  }
}
