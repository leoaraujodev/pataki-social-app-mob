import 'package:get/get.dart';
import 'package:pataki/screens/register_pet/register_pet_controller.dart';

class RegisterPetBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(RegisterPetController());
  }
}
