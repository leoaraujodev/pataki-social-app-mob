import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:pataki/core/endpoints/create_pet_endpoint.dart';
import 'package:pataki/core/endpoints/get_breeds_endpoint.dart';
import 'package:pataki/core/endpoints/get_pet_types_endpoint.dart';
import 'package:pataki/core/models/breeds_list_response.dart';
import 'package:pataki/core/models/create_pet_request.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/get_pet_types_response.dart';
import 'package:pataki/core/models/pet_type_info_request.dart';
import 'package:pataki/screens/validators/name_validator.dart';

class RegisterPetController extends GetxController with NameValidator {
  final formKey = GlobalKey<FormState>();
  final image = Rx<File>();
  final picker = ImagePicker();
  final birthFocusNode = FocusNode();
  final isBirthValid = true.obs;
  final birthAutovalidateMode = AutovalidateMode.disabled.obs;
  final birthController = TextEditingController();
  DateTime birthDateSelected = DateTime.now();
  final formatter = DateFormat('dd/MM/yyyy').format;
  DateTime validBirthDate;

  final weightFocusNode = FocusNode();
  final isWeightValid = true.obs;
  final weightAutovalidateMode = AutovalidateMode.disabled.obs;
  final weightController = TextEditingController();

  final petTypeFocusNode = FocusNode();
  final isPetTypeValid = false.obs;
  final petTypeAutovalidateMode = AutovalidateMode.disabled.obs;
  final petTypeSelected = 0.obs;
  final Rx<dynamic> petTypeValue = Rx<dynamic>();
  RxList<DropdownMenuItem<String>> petTypesItems = RxList([]);

  final petSizeFocusNode = FocusNode();
  final isPetSizeValid = false.obs;
  final petSizeAutovalidateMode = AutovalidateMode.disabled.obs;
  final petSizeSelected = "".obs;
  final Rx<dynamic> petSizeValue = Rx<dynamic>();

  final petBreedFocusNode = FocusNode();
  final isPetBreedValid = false.obs;
  final petBreedAutovalidateMode = AutovalidateMode.disabled.obs;
  final petBreedIsEnabled = false.obs;
  final petBreedSelected = 0.obs;
  final Rx<dynamic> petBreedValue = Rx<dynamic>();
  RxList<DropdownMenuItem<String>> petBreedsById = RxList([]);

  final creating = false.obs;

  @override
  void onInit() {
    getPetTypes();
    nameFocusNode.addListener(() {
      if (!nameFocusNode.hasFocus) {
        nameValidator(nameController.text);
        nameOnChanged(nameController.text);
      }
    });
    birthFocusNode.addListener(() {
      if (!birthFocusNode.hasFocus) {
        birthValidator(birthController.text);
        birthOnChanged(birthController.text);
      }
    });
    weightFocusNode.addListener(() {
      if (!weightFocusNode.hasFocus) {
        weightValidator(weightController.text);
        weightOnChanged(weightController.text);
      }
    });
    super.onInit();
  }

  @override
  void onClose() {
    nameFocusNode.dispose();
    birthFocusNode.dispose();
    weightFocusNode.dispose();
    super.onClose();
  }

  String birthValidator(String value) {
    birthAutovalidateMode.value = AutovalidateMode.always;

    if (value.length == 10) {
      var d = value.substring(0, 2);
      var m = value.substring(3, 5);
      var y = value.substring(6, 10);
      var validDateFormat = y + m + d;

      if (formatter(DateTime.parse(validDateFormat)) == value) {
        validBirthDate = DateTime.parse(validDateFormat);
        return null;
      }
    }

    return 'Preencha com uma data válida';
  }

  void birthOnChanged(String value) {
    if (birthAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = birthValidator(value);
    if (error == null) {
      isBirthValid.value = true;
    } else {
      isBirthValid.value = false;
    }
  }

  Future<Null> selectDate() async {
    birthAutovalidateMode.value = AutovalidateMode.always;

    final DateTime picker = await showDatePicker(
      context: Get.context,
      initialDate: birthDateSelected,
      firstDate: DateTime(1850),
      lastDate: DateTime.now(),
      locale: Locale('pt'),
    );

    if (picker != null && picker != birthDateSelected) {
      birthDateSelected = picker;
      birthController.text = formatter(birthDateSelected);

      birthOnChanged(birthController.text);
    }
    FocusScope.of(Get.context).requestFocus(weightFocusNode);
  }

  String weightValidator(String value) {
    weightAutovalidateMode.value = AutovalidateMode.always;

    if (double.tryParse(value) != null) {
      return null;
    }

    return 'Este campo só aceita números';
  }

  void weightOnChanged(String value) {
    if (weightAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = weightValidator(value);
    if (error == null) {
      isWeightValid.value = true;
    } else {
      isWeightValid.value = false;
    }
  }

  String petTypeValidator(dynamic value) {
    petTypeAutovalidateMode.value = AutovalidateMode.always;

    if (value != null) {
      return null;
    }
    return 'Selecione uma opção';
  }

  void petTypeOnChanged(String value) {
    if (petTypeAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = petTypeValidator(value);
    if (error == null) {
      petTypeSelected.value = int.parse(value);

      if (petTypeValue.value != value) {
        getBreedsById(int.tryParse(value));
        petTypeValue.value = value;
        petBreedValue.value = null;
      }

      isPetTypeValid.value = true;
    } else {
      isPetTypeValid.value = false;
    }
  }

  String petSizeValidator(dynamic value) {
    petSizeAutovalidateMode.value = AutovalidateMode.always;

    if (value != null) {
      return null;
    }

    return 'Selecione uma opção';
  }

  void petSizeOnChanged(String value) {
    if (petSizeAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = petSizeValidator(value);
    if (error == null) {
      petSizeSelected.value = value;
      isPetSizeValid.value = true;
    } else {
      isPetSizeValid.value = false;
    }
  }

  String petBreedValidator(dynamic value) {
    petBreedAutovalidateMode.value = AutovalidateMode.always;

    if (value != null) {
      return null;
    }

    return 'Selecione uma opção';
  }

  void petBreedOnChanged(String value) {
    if (petBreedAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = petBreedValidator(value);
    if (error == null) {
      petBreedSelected.value = int.parse(value);

      petBreedValue.value = value;

      isPetBreedValid.value = true;
    } else {
      isPetBreedValid.value = false;
    }
  }

  Future<void> getPetTypes() async {
    try {
      GetPetTypeResponse response = await GetPetTypes(EmptyRequest()).call();

      response.petTypes.forEach((petType) {
        petTypesItems.add(
          DropdownMenuItem(
            value: petType.id.toString(),
            child: Text(petType.name),
          ),
        );
      });
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
  }

  Future<void> getBreedsById(petTypeId) async {
    try {
      BreedsListResponse response =
          await GetBreedsEndpoint(PetTypeInfoRequest(petTypeId: petTypeId)).call();

      petBreedsById.clear();

      response.pets.forEach((petBreed) {
        petBreedsById.add(
          DropdownMenuItem(
            value: petBreed.id.toString(),
            child: Text(petBreed.name),
          ),
        );
      });
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
  }

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      image.value = File(pickedFile.path);
    }
  }

  Future<void> submit() async {
    nameValidator(nameController.text);
    nameOnChanged(nameController.text);
    birthValidator(birthController.text);
    birthOnChanged(birthController.text);
    weightValidator(weightController.text);
    weightOnChanged(weightController.text);

    if (!formKey.currentState.validate()) {
      Get.snackbar("Atenção", "Verifique seus dados, um ou mais campos apresentam erros");
      return;
    }

    createPet();
  }

  Future<void> createPet() async {
    if (creating.value == true) return;
    creating.value = true;

    try {
      final pet = await CreatePetEndpoint(
        CreatePetRequest(
          name: nameController.text,
          size: petSizeSelected.value,
          weight: double.tryParse(weightController.text),
          breedId: petBreedSelected.value,
          birthDate: validBirthDate,
          image: image.value,
        ),
      ).call();
      Get.back(result: pet);
      Get.snackbar("Sucesso", "Pet criado com sucesso");
    } catch (e) {
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
    creating.value = false;
  }
}
