import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pataki/screens/register_pet/register_pet_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_dropdown_field.dart';
import 'package:pataki/uikit/widgets/form_field_check_mark.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';
import 'package:pataki/uikit/widgets/form_text_field.dart';
import 'package:pataki/uikit/widgets/image_picker_dialog.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';

class RegisterPetScreen extends GetView<RegisterPetController> {
  RegisterPetScreen({
    Key key,
  }) : super(key: key);
  static String id = "register_pet_screen";

  final Map<String, String> petSize = {
    'P': 'S',
    'M': 'M',
    'G': 'L',
  };

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        controller.formKey.currentState.validate();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Cadastrar Pet",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Obx(
                () => Form(
                  key: controller.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildImagePicker(),
                      FormTextField(
                        focusNode: controller.nameFocusNode,
                        controller: controller.nameController,
                        checkMark: _buildCheckMark(
                          autovalidateMode: controller.nameAutovalidateMode.value,
                          isValid: controller.isNameValid.value,
                        ),
                        hintText: "Nome",
                        autovalidateMode: controller.nameAutovalidateMode.value,
                        validator: controller.nameValidator,
                        onFieldSubmitted: (value) {
                          controller.nameValidator(value);
                          controller.nameOnChanged(value);
                          FocusScope.of(context).requestFocus(controller.birthFocusNode);
                        },
                        onChanged: controller.nameOnChanged,
                        textInputAction: TextInputAction.next,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: FormTextField(
                              focusNode: controller.birthFocusNode,
                              controller: controller.birthController,
                              checkMark: _buildCheckMark(
                                autovalidateMode: controller.birthAutovalidateMode.value,
                                isValid: controller.isBirthValid.value,
                              ),
                              hintText: "Nascimento",
                              autovalidateMode: controller.birthAutovalidateMode.value,
                              validator: controller.birthValidator,
                              onFieldSubmitted: (value) {
                                controller.birthValidator(value);
                                controller.birthOnChanged(value);
                              },
                              onTap: () => controller.selectDate(),
                              showCursor: true,
                              readOnly: true,
                              textInputAction: TextInputAction.none,
                              onChanged: controller.birthOnChanged,
                            ),
                          ),
                          Flexible(
                            child: FormTextField(
                              focusNode: controller.weightFocusNode,
                              controller: controller.weightController,
                              checkMark: _buildCheckMark(
                                autovalidateMode: controller.weightAutovalidateMode.value,
                                isValid: controller.isWeightValid.value,
                              ),
                              hintText: "Peso (kg)",
                              autovalidateMode: controller.weightAutovalidateMode.value,
                              validator: controller.weightValidator,
                              onFieldSubmitted: (value) {
                                controller.weightValidator(value);
                                controller.weightOnChanged(value);
                                FocusScope.of(context).requestFocus(controller.petTypeFocusNode);
                              },
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              onChanged: controller.weightOnChanged,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: FormDropdownField(
                              value: controller.petTypeValue.value,
                              items: controller.petTypesItems,
                              hintText: "Tipo",
                              autovalidateMode: controller.petTypeAutovalidateMode.value,
                              focusNode: controller.petTypeFocusNode,
                              icon: _buildDropdownCheckMark(
                                autovalidateMode: controller.petTypeAutovalidateMode.value,
                                isValid: controller.isPetTypeValid.value,
                              ),
                              validator: controller.petTypeValidator,
                              onChanged: (value) {
                                controller.petTypeValidator(value);
                                controller.petTypeOnChanged(value);
                                FocusScope.of(context).requestFocus(controller.petSizeFocusNode);
                              },
                            ),
                          ),
                          Flexible(
                            child: FormDropdownField(
                              value: controller.petSizeValue.value,
                              items: petSize
                                  .map((value, id) {
                                    return MapEntry(
                                        value,
                                        DropdownMenuItem(
                                          value: id.toString(),
                                          child: Text(value),
                                        ));
                                  })
                                  .values
                                  .toList(),
                              hintText: "Porte",
                              autovalidateMode: controller.petSizeAutovalidateMode.value,
                              focusNode: controller.petSizeFocusNode,
                              icon: _buildDropdownCheckMark(
                                autovalidateMode: controller.petSizeAutovalidateMode.value,
                                isValid: controller.isPetSizeValid.value,
                              ),
                              validator: controller.petSizeValidator,
                              onChanged: (value) {
                                controller.petSizeValidator(value);
                                controller.petSizeOnChanged(value);
                                FocusScope.of(context).requestFocus(controller.petBreedFocusNode);
                              },
                            ),
                          ),
                        ],
                      ),
                      FormDropdownField(
                        value: controller.petBreedValue.value,
                        items: controller.petBreedsById,
                        hintText: "Raça",
                        disabledHint: "Raça",
                        autovalidateMode: controller.petBreedAutovalidateMode.value,
                        focusNode: controller.petBreedFocusNode,
                        icon: _buildDropdownCheckMark(
                          autovalidateMode: controller.petBreedAutovalidateMode.value,
                          isValid: controller.isPetBreedValid.value,
                        ),
                        validator: controller.petBreedValidator,
                        onChanged: (value) {
                          controller.petBreedValidator(value);
                          controller.petBreedOnChanged(value);
                          FocusScope.of(context).unfocus();
                        },
                      ),
                      LargeTextButton(
                        loading: controller.creating.value,
                        onPressed: controller.submit,
                        text: 'SALVAR',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildImagePicker() {
    return controller.image.value == null
        ? GestureDetector(
            onTap: () => Get.dialog(
              ImagePickerDialog(
                onCameraPressed: () => controller.getImage(ImageSource.camera),
                onGalleryPressed: () => controller.getImage(ImageSource.gallery),
              ),
            ),
            child: Container(
              height: 150,
              width: 150,
              margin: EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFC7D9EB)),
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: Icon(
                Icons.add_a_photo,
                size: 50,
                color: PatakiColors.gray,
              ),
            ),
          )
        : Container(
            height: 150,
            width: 150,
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xFFC7D9EB)),
              borderRadius: BorderRadius.all(Radius.circular(6)),
              image: DecorationImage(
                image: FileImage(controller.image.value),
                fit: BoxFit.cover,
              ),
            ),
            alignment: Alignment.bottomCenter,
            child: FlatButton(
              shape: StadiumBorder(),
              color: Colors.white.withOpacity(0.75),
              onPressed: () => Get.dialog(
                ImagePickerDialog(
                  onCameraPressed: () => controller.getImage(ImageSource.camera),
                  onGalleryPressed: () => controller.getImage(ImageSource.gallery),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.cached),
                  SizedBox(width: 10),
                  Text("Alterar"),
                ],
              ),
            ),
          );
  }

  _buildCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return null;

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }

  _buildDropdownCheckMark({
    @required AutovalidateMode autovalidateMode,
    @required bool isValid,
  }) {
    if (autovalidateMode == AutovalidateMode.disabled) return Icon(Icons.keyboard_arrow_down);

    return FormFieldCheckMark(
      isValid: isValid,
    );
  }
}
