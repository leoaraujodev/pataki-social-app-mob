import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/home/home_screen.dart';
import 'package:pataki/screens/register_pet/register_pet_screen.dart';
//import 'package:pataki/screens/register_pet/register_pet_screen.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';
import 'package:pataki/uikit/widgets/form_layout.dart';

class RegisterPetOptionScreen extends StatelessWidget {
  RegisterPetOptionScreen({Key key}) : super(key: key);
  static String id = "register_pet_option_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: LayoutBuilder(
        builder: (context, constraints) => FormLayout(
          title: "Bem-vindo!",
          children: [
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight - FormAppBar.height,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: LargeTextButton(
                      onPressed: () => Get.toNamed(RegisterPetScreen.id),
                      text: 'CADASTRAR PET',
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30, bottom: 150),
                    child: TextButton(
                        child: Text(
                          "PULAR",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF66839F),
                          ),
                        ),
                        onPressed: () {
                          Get.offAllNamed(HomeScreen.id);
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
