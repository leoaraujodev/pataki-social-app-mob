import 'package:get/get.dart';
import 'package:pataki/screens/reminder/reminder_controller.dart';

class ReminderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ReminderController());
  }
}
