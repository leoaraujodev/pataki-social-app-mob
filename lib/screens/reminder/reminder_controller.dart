import 'package:get/get.dart';
import 'package:pataki/core/endpoints/reminder_pet_delete_endpoint.dart';
import 'package:pataki/core/endpoints/reminders_endpoint.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/reminder_delete_request.dart';
import 'package:pataki/core/models/reminders_request.dart';
import 'package:pataki/core/models/reminders_response.dart';
import 'package:pataki/core/models/success_message_response.dart';

class ReminderController extends GetxController with Notifications {
  final loggedUserId = RxInt();
  int petId = int.parse(Get.parameters["petId"]);

  final RxList<Reminder> reminders = RxList([]);

  final reminderIsEmpty = true.obs;
  final edittingMode = false.obs;

  final loadingReminders = false.obs;
  final errorOnReminders = true.obs;

  @override
  void onInit() {
    getPetReminders();
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getPetReminders() async {
    errorOnReminders.value = false;
    loadingReminders.value = true;

    reminders.clear();

    try {
      RemindersResponse response = await RemindersEndpoint(RemindersRequest(petId: petId)).call();

      if (response.reminders.isNotEmpty) {
        reminderIsEmpty.value = false;
        reminders.addAll(response.reminders);
      }
    } catch (e) {
      errorOnReminders.value = true;
      _showErrorSnackbar();
    }

    loadingReminders.value = false;
  }

  Future<void> deleteNotification(reminderId) async {
    final Reminder reminder = reminders.firstWhere((reminder) => reminder.id == reminderId);
    bool allDaysTrue = true;

    try {
      SuccessMessageResponse response = await ReminderDeleteEndpoint(ReminderDeleteRequest(
        petId: petId,
        reminderId: reminder.id,
      )).call();

      cancelDailyOrWeeklyNotifications(reminder: reminder, allDaysTrue: allDaysTrue);

      getPetReminders();

      Get.back();
      Get.snackbar("Pronto", response.message);
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
