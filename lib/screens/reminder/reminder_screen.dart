import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/create_reminder/pet/create_pet_reminder_screen.dart';
import 'package:pataki/screens/reminder/reminder_controller.dart';
import 'package:pataki/uikit/widgets/build_notification.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/rounded_button.dart';

double screenWidth;

class ReminderScreen extends GetView<ReminderController> {
  static String routeName(petId) => "/reminder_screen/$petId";

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: "Rotinas",
        centerTitle: true,
        actions: [
          Obx(
            () {
              if (controller.edittingMode.value)
                return RoundedButton(
                  icon: Icon(
                    Icons.edit_off,
                    color: Colors.red,
                    size: 30.0,
                  ),
                  backgroundColor: null,
                  padding: 12,
                  innerPadding: 2,
                  onTap: () => controller.edittingMode.value = false,
                );

              return Container();
            },
          ),
          RoundedButton(
            icon: Icon(
              Icons.add,
              color: Colors.white,
              size: 20.0,
            ),
            onTap: () async {
              final response =
                  await Get.toNamed(CreatePetReminderScreen.routeName(controller.petId));

              if (response != null) {
                controller.getPetReminders();
                Get.snackbar("Pronto", "Rotina cadastrada com sucesso");
              }
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Obx(
            () {
              if (controller.loadingReminders.value) {
                return Center(child: RefreshProgressIndicator());
              }
              if (controller.errorOnReminders.value) {
                return Center(
                  child: GestureDetector(
                    onTap: controller.getPetReminders,
                    child: RefreshProgressIndicator(
                      value: 1.0,
                    ),
                  ),
                );
              }
              if (controller.reminderIsEmpty.value) {
                return Center(
                  child: Text(
                    "Sem rotinas cadastradas",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey,
                    ),
                  ),
                );
              }

              return ListView(
                shrinkWrap: true,
                children: controller.reminders
                    .map((reminder) => BuildNotification(
                          screenWidth: screenWidth,
                          name: reminder.name,
                          time: reminder.time + " • " + controller.notificationDays(reminder),
                          controller: controller,
                          notificationId: reminder.id,
                        ))
                    .toList(),
              );
            },
          )
        ],
      ),
    );
  }
}
