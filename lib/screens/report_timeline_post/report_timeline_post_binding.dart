import 'package:get/get.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_controller.dart';

class ReportTimelinePostBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ReportTimelinePostController());
  }
}
