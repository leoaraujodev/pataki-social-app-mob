import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/report_timeline_post_endpoint.dart';
import 'package:pataki/core/models/report_reasons.dart';
import 'package:pataki/core/models/report_timeline_post_request.dart';
import 'package:pataki/core/models/timeline_post.dart';

class ReportTimelinePostController extends GetxController {
  final textController = TextEditingController();
  final selectedReason = Rx<ReportReasons>();
  final reporting = false.obs;
  final TimelinePost reportingTimelinePost = Get.arguments["post"];

  report() async {
    if (reporting.value) return;
    reporting.value = true;
    try {
      final response = await ReportTimelinePostEndpoint(ReportTimelinePostRequest(
        id: reportingTimelinePost.id,
        comment: textController.text,
        reason: selectedReason.value,
      )).call();
      reporting.value = false;
      Get.back(result: true);
      Get.snackbar("Denúncia realizada", response.message);
    } catch (e) {
      reporting.value = false;
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
