import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_controller.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/report.dart';

class ReportTimelinePostScreen extends GetView<ReportTimelinePostController> {
  static String routeName = "/report_timeline_post_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(title: "Denunciar"),
        body: SafeArea(
          child: Report(
            controller: controller,
            typeOfReport: "post",
            user: controller.reportingTimelinePost.user,
          ),
        ),
      ),
    );
  }
}
