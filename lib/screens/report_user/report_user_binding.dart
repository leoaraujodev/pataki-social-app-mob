import 'package:get/get.dart';
import 'package:pataki/screens/report_user/report_user_controller.dart';

class ReportUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ReportUserController());
  }
}
