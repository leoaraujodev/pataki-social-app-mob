import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/report_user_endpoint.dart';
import 'package:pataki/core/models/report_reasons.dart';
import 'package:pataki/core/models/report_user_request.dart';
import 'package:pataki/core/models/user_model.dart';

class ReportUserController extends GetxController {
  final textController = TextEditingController();
  final selectedReason = Rx<ReportReasons>();
  final reporting = false.obs;
  final UserModel reportingUser = Get.arguments["user"];

  @override
  void onInit() {
    super.onInit();
  }

  report() async {
    if (reporting.value) return;
    reporting.value = true;
    try {
      final response = await ReportUserEndpoint(ReportUserRequest(
        id: reportingUser.id,
        comment: textController.text,
        reason: selectedReason.value,
      )).call();
      reporting.value = false;
      Get.back();
      Get.snackbar("Denúncia realizada", response.message);
    } catch (e) {
      reporting.value = false;
      Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
    }
  }

  @override
  void onClose() {
    textController.dispose();
    super.onClose();
  }
}
