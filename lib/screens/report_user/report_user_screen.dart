import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/report_user/report_user_controller.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/report.dart';

class ReportUserScreen extends GetView<ReportUserController> {
  static String routeName = "/report_user_screen";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DefaultAppBar(title: "Denunciar"),
        // resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Report(
            controller: controller,
            typeOfReport: "user",
            user: controller.reportingUser,
          ),
        ),
      ),
    );
  }
}
