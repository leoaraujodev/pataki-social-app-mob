import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/search_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/core/models/search_request.dart';
import 'package:pataki/core/models/user_model.dart';

class SearchController extends GetxController {
  BuildContext screenContext;

  final scrollController = ScrollController();
  final textController = TextEditingController();
  final RxList<UserModel> tutors = RxList([]);
  final RxList<Pet> pets = RxList([]);
  final RxList<Group> groups = RxList([]);
  final selectedFilter = SearchFilters.all.obs;
  final searching = false.obs;

  final RxInt loggedUserId = 0.obs;

  int page = 1;
  final hasNextPage = false.obs;
  final currentSearchTerm = RxString();

  final resultIsEmpty = false.obs;

  final hasErrorOnInitialPage = false.obs;
  final hasErrorOnNextPage = false.obs;

  @override
  void onInit() async {
    loggedUserId.value = await GetUserIdInteractor().execute();

    debounce(currentSearchTerm, newSearch);
    newSearch(textController.text);

    scrollController.addListener(() {
      FocusScope.of(screenContext).unfocus();

      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasNextPage.value && !hasErrorOnNextPage.value) search(currentSearchTerm.value);
      }
    });
    super.onInit();
  }

  Future<void> newSearch(String term) async {
    if (searching.value) return;
    page = 1;

    tutors.clear();
    pets.clear();
    groups.clear();

    await search(term);
  }

  Future<void> search(String term) async {
    if (searching.value) return;
    searching.value = true;
    hasErrorOnInitialPage.value = false;
    hasErrorOnNextPage.value = false;
    resultIsEmpty.value = true;

    try {
      final response = await SearchEndpoint(
        SearchRequest(
          page: page,
          filter: selectedFilter.value,
          term: currentSearchTerm.value ?? "",
        ),
      ).call();

      if (response.tutors != null && response.tutors.isNotEmpty) {
        tutors.addAll(response.tutors);
        resultIsEmpty.value = false;
      }
      if (response.pets != null && response.pets.isNotEmpty) {
        pets.addAll(response.pets);
        resultIsEmpty.value = false;
      }
      if (response.groups != null && response.groups.isNotEmpty) {
        groups.addAll(response.groups);
        resultIsEmpty.value = false;
      }

      page++;
      hasNextPage.value = response.hasNextPage;
    } catch (e) {
      page == 1 ? hasErrorOnInitialPage.value = true : hasErrorOnNextPage.value = true;
      _showErrorSnackbar();
    }
    searching.value = false;
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onClose() {
    scrollController.dispose();
    textController.dispose();
    super.onClose();
  }
}
