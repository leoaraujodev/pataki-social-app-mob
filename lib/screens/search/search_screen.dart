import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/search_request.dart';
import 'package:pataki/screens/group/group_screen.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/profile/profile_screen.dart';
import 'package:pataki/screens/search/search_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_group.dart';
import 'package:pataki/uikit/widgets/build_pet.dart';
import 'package:pataki/uikit/widgets/build_tutor.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class SearchScreen extends GetView<SearchController> {
  static String id = "search_screen";

  final homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    controller.screenContext = context;

    return Scaffold(
      appBar: DefaultAppBar(title: "Pesquisa"),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: CommentTextField(
                controller: controller.textController,
                maxLines: 1,
                textInputAction: TextInputAction.search,
                hintText: "Buscar",
                onChanged: (String value) => controller.currentSearchTerm.value = value,
              ),
            ),
            Obx(
              () => Container(
                color: Colors.white,
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(10),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      _buildSearchOption(
                        title: "Tudo",
                        onPressed: () {
                          FocusScope.of(context).unfocus();

                          controller.selectedFilter.value = SearchFilters.all;
                          controller.newSearch(controller.currentSearchTerm.value);
                        },
                        isSelected: controller.selectedFilter.value == SearchFilters.all,
                      ),
                      _buildSearchOption(
                        title: "Pessoas",
                        onPressed: () {
                          FocusScope.of(context).unfocus();

                          controller.selectedFilter.value = SearchFilters.tutors;
                          controller.newSearch(controller.currentSearchTerm.value);
                        },
                        isSelected: controller.selectedFilter.value == SearchFilters.tutors,
                      ),
                      _buildSearchOption(
                        title: "Pets",
                        onPressed: () {
                          FocusScope.of(context).unfocus();

                          controller.selectedFilter.value = SearchFilters.pets;
                          controller.newSearch(controller.currentSearchTerm.value);
                        },
                        isSelected: controller.selectedFilter.value == SearchFilters.pets,
                      ),
                      _buildSearchOption(
                        title: "Grupos",
                        onPressed: () {
                          FocusScope.of(context).unfocus();

                          controller.selectedFilter.value = SearchFilters.groups;
                          controller.newSearch(controller.currentSearchTerm.value);
                        },
                        isSelected: controller.selectedFilter.value == SearchFilters.groups,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Obx(
                () {
                  if (controller.searching.value) {
                    return Center(child: RefreshProgressIndicator());
                  }

                  if (controller.hasErrorOnInitialPage.value) {
                    return Center(
                      child: GestureDetector(
                        onTap: () => controller.newSearch(controller.currentSearchTerm.value),
                        child: RefreshProgressIndicator(
                          value: 1.0,
                        ),
                      ),
                    );
                  }
                  if (controller.resultIsEmpty.value) {
                    return Center(
                      child: Text(
                        "Nenhum resultado encontrado",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey,
                        ),
                      ),
                    );
                  }
                  return ListView(
                    controller: controller.scrollController,
                    shrinkWrap: true,
                    children: [
                      if ((controller.selectedFilter.value == SearchFilters.tutors ||
                              controller.selectedFilter.value == SearchFilters.all) &&
                          controller.tutors.isNotEmpty)
                        BuildCard(
                          title: "Pessoas",
                          children: controller.tutors
                              .map((tutor) => BuildTutor(
                                    tutor: tutor,
                                    onPressed: () async {
                                      await Get.toNamed(
                                        ProfileScreen.routeName(tutor.id),
                                        arguments: {"timestamp": DateTime.now().toIso8601String()},
                                      );

                                      if (homeController.doRefresh.value) {
                                        controller.newSearch(controller.textController.text);
                                        homeController.doRefresh.value = false;
                                      }
                                    },
                                  ))
                              .toList(),
                        ),
                      if ((controller.selectedFilter.value == SearchFilters.pets ||
                              controller.selectedFilter.value == SearchFilters.all) &&
                          controller.pets.isNotEmpty)
                        BuildCard(
                          title: "Pets",
                          children: controller.pets.map((pet) => BuildPet(pet: pet)).toList(),
                        ),
                      if ((controller.selectedFilter.value == SearchFilters.groups ||
                              controller.selectedFilter.value == SearchFilters.all) &&
                          controller.groups.isNotEmpty)
                        BuildCard(
                          title: "Grupos",
                          children: controller.groups
                              .map(
                                (group) => BuildGroup(
                                  group: group,
                                  onPressed: () async {
                                    await Get.toNamed(
                                      GroupScreen.routeName(group.id),
                                      arguments: {
                                        "timestamp": DateTime.now().toIso8601String(),
                                      },
                                    );
                                    controller.newSearch(controller.textController.text);
                                  },
                                  loggedUserId: controller.loggedUserId.value,
                                ),
                              )
                              .toList(),
                        ),
                      if (controller.hasNextPage.value)
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Obx(
                            () => controller.hasErrorOnNextPage.value
                                ? Center(
                                    child: GestureDetector(
                                      onTap: () =>
                                          controller.search(controller.currentSearchTerm.value),
                                      child: RefreshProgressIndicator(
                                        value: 1.0,
                                      ),
                                    ),
                                  )
                                : RefreshProgressIndicator(),
                          ),
                        ),
                    ],
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildSearchOption({String title, Function onPressed, bool isSelected = false}) {
    return FlatButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
      onPressed: onPressed,
      child: Text(
        title,
        style: TextStyle(
          color: PatakiColors.orange,
          fontSize: 18,
          fontWeight: isSelected ? FontWeight.w600 : FontWeight.w400,
        ),
      ),
    );
  }
}
