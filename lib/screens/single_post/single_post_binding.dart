import 'package:get/get.dart';
import 'package:pataki/screens/single_post/single_post_controller.dart';

class SinglePostBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SinglePostController());
  }
}
