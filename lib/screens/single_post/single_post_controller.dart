import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/endpoints/single_timeline_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/single_timeline_request.dart';
import 'package:pataki/core/models/timeline_post.dart';

class SinglePostController extends GetxController {
  final loggedUserId = RxInt();
  final post = Rx<TimelinePost>();

  final loadingInitialPosts = true.obs;
  final errorOnInitialPosts = false.obs;

  bool _fetchingMutex = false;
  bool _likingMutex = false;

  @override
  void onInit() {
    getInitialPosts();

    super.onInit();
  }

  Future<void> getInitialPosts() async {
    loggedUserId.value = await GetUserIdInteractor().execute();
    loadingInitialPosts.value = true;
    errorOnInitialPosts.value = false;

    try {
      post.value = await _getTimelineResponse();
    } catch (e) {
      errorOnInitialPosts.value = true;
      _showErrorSnackbar();
    }

    loadingInitialPosts.value = false;
  }

  void updateCommentCount(int timelineId, int newCount) {
    post.value = post.value.copyWith(
      imagePath: post.value.imagePath,
      totComment: newCount,
    );
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    try {
      post.value = await _getTimelineResponse();
    } catch (e) {
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<TimelinePost> _getTimelineResponse() =>
      SingleTimelineEndpoint(SingleTimelineRequest(id: int.tryParse(Get.parameters["timelineId"])))
          .call();

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      post.value = post.value.copyWith(
        totLikes: like ? post.value.totLikes + 1 : post.value.totLikes - 1,
        liked: like,
        imagePath: post.value.imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();
    } catch (e) {
      post.value = post.value.copyWith(
        totLikes: !like ? post.value.totLikes + 1 : post.value.totLikes - 1,
        liked: !like,
        imagePath: post.value.imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      Get.until((route) => route.isFirst);
    } catch (e) {
      _showErrorSnackbar();
    }
  }
}
