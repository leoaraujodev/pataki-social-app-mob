import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/screens/single_post/single_post_controller.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';

class SinglePostScreen extends GetView<SinglePostController> {
  static String routeName(id) => "/single_post_screen/$id";

  final homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Post",
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (controller.loadingInitialPosts.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnInitialPosts.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getInitialPosts,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return RefreshIndicator(
            child: SafeArea(
              child: ListView(
                physics: AlwaysScrollableScrollPhysics(),
                children: [
                  TimelinePostCard(
                    post: controller.post.value,
                    onCommentPressed: () async {
                      await Get.toNamed(
                        CommentScreen.routeName(controller.post.value.id),
                        arguments: {
                          "updateCommentCount": controller.updateCommentCount,
                          "timestamp": DateTime.now().toIso8601String(),
                        },
                      );

                      if (homeController.doRefresh.value) {
                        controller.getInitialPosts();
                        homeController.doRefresh.value = false;
                      }
                    },
                    onLikePressed: () => controller.likeComment(
                      controller.post.value.id,
                      !controller.post.value.liked,
                    ),
                    onLikesPressed: () => Get.toNamed(
                      LikesScreen.routeName(controller.post.value.id),
                      arguments: {"timestamp": DateTime.now().toIso8601String()},
                    ),
                    onMorePressed: () => Get.dialog(
                      OptionsDialog(
                        title: "Opções da Postagem",
                        onReportPressed:
                            controller.loggedUserId.value != controller.post.value.user.id
                                ? () async {
                                    var reported = await Get.offNamed(
                                      ReportTimelinePostScreen.routeName,
                                      arguments: {"post": controller.post.value},
                                    );

                                    if (reported == true) {
                                      Get.back();
                                    }
                                  }
                                : null,
                        onEditPressed:
                            controller.loggedUserId.value == controller.post.value.user.id
                                ? () async {
                                    await Get.offNamed(
                                      EditPostScreen.routeName(controller.post.value.id),
                                    );
                                  }
                                : null,
                        onSharePressed: () {
                          Clipboard.setData(
                            new ClipboardData(
                              text:
                                  "https://pataki.com.br/#/single_post_screen/${controller.post.value.id}",
                            ),
                          );
                          Get.back();
                        },
                        shareType: "post",
                        onDeletePressed:
                            controller.loggedUserId.value == controller.post.value.user.id
                                ? () {
                                    Get.back();
                                    Get.dialog(
                                      ConfirmDialog(
                                        onConfirmPressed: () => controller.deletePost(
                                          controller.post.value.id,
                                        ),
                                      ),
                                    );
                                  }
                                : null,
                      ),
                    ),
                    onSendPressed: () {},
                    onUserPressed: controller.post.value.user.id != 10
                        ? () async {
                            await Get.toNamed(
                              PetScreen.routeName(controller.post.value.pet.id),
                              arguments: {"timestamp": DateTime.now().toIso8601String()},
                            );

                            if (homeController.doRefresh.value) {
                              controller.getInitialPosts();
                              homeController.doRefresh.value = false;
                            }
                          }
                        : null,
                  ),
                ],
              ),
            ),
            onRefresh: controller.refreshPosts,
          );
        },
      ),
    );
  }
}
