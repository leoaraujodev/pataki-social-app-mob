import 'package:get/get.dart';
import 'package:pataki/core/endpoints/refresh_endpoint.dart';
import 'package:pataki/core/interactor/get_token_interactor.dart';
import 'package:pataki/core/interactor/set_token_interactor.dart';
import 'package:pataki/core/models/empty_request.dart';
import 'package:pataki/core/models/refresh_response.dart';
import 'package:pataki/screens/home/home_screen.dart';
import 'package:pataki/screens/welcome_screen.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    validateToken();
    super.onInit();
  }

  Future<void> validateToken() async {
    final token = await GetTokenInteractor().execute();

    if (token == null) Get.offNamed(WelcomeScreen.id);

    try {
      RefreshResponse response = await RefreshEndpoint(EmptyRequest()).call();
      await SetTokenInteractor().execute(response.token);
      Get.offNamed(HomeScreen.id);
    } catch (e) {
      await SetTokenInteractor().execute(null);
      Get.offNamed(WelcomeScreen.id);
    }
  }
}
