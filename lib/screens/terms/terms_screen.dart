import 'package:flutter/material.dart';
import 'package:pataki/uikit/widgets/build_paragraph.dart';
import 'package:pataki/uikit/widgets/build_section_title.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

class TermsScreen extends StatelessWidget {
  static String routeName = "/terms_screen";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: "Termos e Condições de Uso",
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: ListView(
          children: [
            BuildParagraph(
              'De acordo com esses Termos e Condições de Uso, o usuário não poderá utilizar nosso serviço caso seja considerado indivíduo incapaz na forma do Código Civil Brasileiro (Lei n.º 10.406, de 10 de Janeiro de 2002), por não ter capacidade legal de firmar Termos e condições de usos com efeitos jurídicos. A aceitação dos termos e condições de uso deverá ser precedida de orientação e representação, incluindo as disposições previstas pelo Estatuto da Criança e do Adolescente (Lei n.º 8.069, de 13 de Julho de 1990), no caso de menores de idade. O uso do serviço por indivíduos incapazes sem a legal orientação e representação não acarreta responsabilidades adicionais à PATAKI em caso de danos experimentados pelo incapaz ou por terceiros, aplicando-se a excludente de responsabilidade prevista pelo Art. 14 §3°. da Lei 8.070, de 11 de Setembro de 1990, além da disposição prevista pelo Art. 932 incisos I e II do Código Civil.',
              sizedBox: true,
            ),
            BuildParagraph(
              'Nossa intenção é de não introduzir com frequência alterações aos nossos termos e condições, mas nos reservamos o direito de fazer sempre que necessário. Contudo, é sempre bom confirmar, quando visitar o nosso site, a fim de se assegurar que leu e concordou com os nossos termos e condições em vigor. Quaisquer alterações aos nossos termos e condições serão de aplicação imediata. A continuada utilização deste site após tais mudanças constituirá na sua aceitação integral.',
            ),
            BuildSectionTitle('1. Criação de Conta'),
            BuildParagraph(
              'Para se inscrever como Membro da PATAKI, rede social de pets, é necessário criar uma conta procedendo ao registo dela. Nessa seção, será solicitada a escolha de um nome de usuário e uma senha para o perfil de Membro. O usuário é inteiramente responsável pela manutenção da confidencialidade da sua senha. O usuário concorda em notificar imediatamente à PATAKI caso suspeite de qualquer utilização não autorizada por parte de terceiro do seu perfil de Membro ou de acesso à sua senha. O usuário é o único responsável por toda e qualquer utilização do perfil de Membro.',
            ),
            BuildSectionTitle('2. Duração dos Termos e Condições de Uso'),
            BuildParagraph(
              'O presente Termo deverá manter-se em vigor e com efeito na totalidade enquanto o Membro da PATAKI fizer parte da Comunidade e utilizar os Serviços. Qualquer membro que deseje finalizar a sua participação, poderá encerrar sua conta de forma gratuita e sem aviso prévio.',
            ),
            BuildParagraph(
              'O conteúdo enviado ao serviço pelo usuário poderá continuar hospedado, ser visualizado e utilizado mesmo após o encerramento de sua conta. O encerramento da conta não acarreta a exclusão de direitos, obrigações e responsabilidades do usuário e da PATAKI, que continuarão em vigor indefinidamente.',
            ),
            BuildParagraph(
              'De igual modo, poderemos encerrar a filiação do usuário, por qualquer motivo, em qualquer altura, sem aviso, incluindo, mas não se limitando a casos de violação do presente termo por parte do usuário, exigência legal ou judicial e término do fornecimento do serviço.',
            ),
            BuildParagraph(
              'Mesmo após a cessação do presente “Termos e condições de Uso”, as seguintes cláusulas 3, 4, 5, 8 e 9 permanecerão em vigor.',
            ),
            BuildSectionTitle(
              '3. Utilização dos Conteúdos',
            ),
            BuildParagraph(
              'Todo o conteúdo enviado ao serviço é de responsabilidade exclusiva do usuário que o enviou. O usuário concorda que a PATAKI possa pré-selecionar, rever, marcar, filtrar, modificar, recusar ou remover qualquer conteúdo presente no serviço, sem haver qualquer obrigação para que assim o faça.',
            ),
            BuildParagraph(
              'O usuário assume que ao autorizar a utilização pela PATAKI de todos os conteúdos inseridos no site renunciará definitivamente a qualquer interpelação para pagamento de quantias a título de utilização dos referidos conteúdos.',
            ),
            BuildParagraph(
              'O usuário concorda que ao utilizar o serviço poderá estar exposto a conteúdo que possa considerar ofensivo, estando a PATAKI excluída de qualquer responsabilidade, seja por conta da excludente de responsabilidade prevista pelo Art. 14 §3°. da Lei 8.070, de 11 de Setembro de 1990, seja por ser o usuário sabedor que utiliza o serviço por sua conta e risco e que existem softwares específicos para limitar o acesso a material que possa considerar ofensivo.',
            ),
            BuildSectionTitle(
              '4. Direito de Propriedade Intelectual',
            ),
            BuildParagraph(
              'Ao publicar Conteúdos em qualquer área dos Serviços, o usuário concede automaticamente, à PATAKI, uma licença internacional irrevogável, vitalícia, não exclusiva, sem direito a pagamento a título de direitos de autor, direito à imagem, direito ao nome ou qualquer outro direito de personalidade, autorizando expressamente a PATAKI a utilizar, reproduzir, distribuir, apresentar publicamente, executar e utilizar os Conteúdos de outras formas que achar conveniente.',
            ),
            BuildParagraph(
              'O usuário concorda em não utilizar qualquer dos itens dispostos e apresentados no serviço, incluindo, mas não se restringindo a marcas, logotipos, nomes de domínios e nomes empresariais, incluindo-se nesta vedação a utilização de marcas, logotipos, nomes de domínios ou nomes empresariais semelhantes, que possam vir a causar confusão na utilização de diferentes serviços.',
            ),
            BuildSectionTitle(
              '5. Direitos de Autor',
            ),
            BuildParagraph(
              'A PATAKI reconhece e aceita que não detém qualquer direito sobre qualquer conteúdo enviado, publicado ou transmitido pelo usuário e colocados à disposição de demais usuários em seções públicas do serviço ou através de comunicação privada com outro usuário do serviço.',
            ),
            BuildParagraph(
              'O usuário concorda que somente enviará, publicará ou exibirá conteúdo que retenha direitos autorais em obediência ao ordenamento jurídico vigente no Brasil e/ou no país onde o conteúdo foi criado, produzido ou modificado.',
            ),
            BuildParagraph(
              'Ainda que seja possível copiar parte deste web site para o seu próprio computador, para uso pessoal, o usuário não poderá copiar ou incorporar qualquer conteúdo disponível no website para qualquer outro espaço de trabalho, incluindo o seu próprio website, nem utilizar o conteúdo de qualquer outra forma pública ou comercial. Isto significa que não poderá disponibilizar a terceiros nem distribuir qualquer parte do nosso website a menos que para tal possua licença por nós concedida. De igual modo, significa ainda que a PATAKI detém os direitos totais, completos e vitalícios de todo esse conteúdo, incluindo qualquer software ou código que possa ser importado, quaisquer imagens incorporadas ou geradas pelo software, bem como quaisquer dados que as acompanhem. Pelo exposto, não é permitido aos usuários copiar, modificar, reproduzir, importar, transmitir, distribuir, realizar atos de engenharia inversa, desmontar ou de qualquer modo converter para qualquer outro formato.',
            ),
            BuildSectionTitle(
              '6. Materiais Enviados',
            ),
            BuildParagraph(
              'Teremos todo o gosto em receber as suas histórias, comentários, sugestões, ideias, gráficos, imagens ou outro material, mas deverá sempre ter presente que se exportar ou enviar tais materiais através deste site, isso deve ser feito para fins lícitos e admitidos no regramento jurídico brasileiro.',
            ),
            BuildParagraph(
              'Os materiais enviados poderão ter seu espaço de armazenamento limitado sem aviso prévio.',
            ),
            BuildParagraph(
              'A PATAKI obriga-se a respeitar os seus direitos relativos à proteção dos dados pessoais.',
            ),
            BuildSectionTitle(
              '7. Restrições de Utilização',
            ),
            BuildParagraph(
              'O usuário não poderá empreender quaisquer atividades através do website que sejam ilegais, ofensivas, abusivas, que violem os direitos de terceiros, ou discriminação de qualquer tipo que impeça os outros usuários de utilizar e apreciar este site.',
            ),
            BuildParagraph(
              'Os conteúdos utilizados e as fotografias publicadas no perfil de Membro não poderão conter nudez, violência, conteúdos sexualmente explícitos ou ofensivos da moral e bons costumes.',
            ),
            BuildParagraph(
              'O usuário concorda em indenizar, defender e isentar de responsabilidades a PATAKI e as suas filiais, associadas, funcionários e empregados relativamente a quaisquer danos, responsabilidades e despesas (incluindo honorários razoáveis de advogados) decorrentes de reclamações de terceiros como consequência ou resultado da sua utilização do website.',
            ),
            BuildParagraph(
              'O usuário não usará esta rede de maneira que gere vírus de computador ou qualquer outro código, arquivos ou programas de computador desenvolvidos para interromper, inutilizar ou limitar a funcionalidade de qualquer software ou hardware de computador ou equipamento de telecomunicações. Nem tampouco se utilizar de forma caluniosa, difamatória, ameaçadora, abusiva, violenta, assediadora, mal-intencionada ou invasiva da privacidade alheia.',
            ),
            BuildParagraph(
              'Além disso, é expressamente proibido a qualquer usuário:',
            ),
            BuildParagraph(
              '- representar qualquer outra pessoa, declarar falsamente ou fingir de qualquer outra forma sua relação com qualquer pessoa ou entidade, ou obter acesso a esta Rede sem autorização;',
            ),
            BuildParagraph(
              '- incluir informações pessoais ou de identificação sobre outra pessoa, sem o consentimento explícito desta;',
            ),
            BuildParagraph(
              '- empregar e-mail ou endereços de IP enganosos, cabeçalhos falsos ou identificadores manipulados de qualquer outra forma para disfarçar a origem do conteúdo transmitido através desta rede ou aos usuários;',
            ),
            BuildParagraph(
              '- "Perseguir" ou assediar alguém de qualquer forma;',
            ),
            BuildParagraph(
              '- Coletar, usar ou revelar dados, inclusive informações pessoais, sobre outros usuários sem seu consentimento ou para fins ilícitos ou em violação das leis ou regulamentações aplicáveis;',
            ),
            BuildParagraph(
              '- Solicitar, prospectar ou obter de qualquer outra forma acesso a nomes de usuário, senhas ou outras credenciais de autenticação de qualquer membro desta Rede ou usar credenciais de autenticação em nome de qualquer membro desta Rede com o propósito de automatizar logins nesta rede;',
            ),
            BuildParagraph(
              '- Postar qualquer conteúdo que contenha pornografia infantil nesta rede;',
            ),
            BuildParagraph(
              '- Postar qualquer conteúdo que represente ou contenha situações de estupro, violência extrema, assassinato, bestialidade, incesto ou outro conteúdo similar;',
            ),
            BuildParagraph(
              '- Postar qualquer conteúdo que constitua pornografia, contenha nudez ou seja adulto por natureza.',
            ),
            BuildSectionTitle(
              '8. Produtos ou Serviços de Terceiros',
            ),
            BuildParagraph(
              'Embora este website possa mencionar produtos ou serviços de terceiros ou conter links para sites ou informações de terceiros, não damos o nosso aval, não oferecemos quaisquer garantias nem fazemos representações deles.',
            ),
            BuildParagraph(
              'Todos os links para outros sites são fornecidos apenas para sua comodidade. A decisão sobre estes produtos, serviços e websites de terceiros é da inteira e exclusiva responsabilidade dos usuários.',
            ),
            BuildSectionTitle(
              '9. Política de Privacidade',
            ),
            BuildParagraph(
              'A Política de Privacidade da PATAKI destina-se a informar o usuário das políticas e procedimentos no que diz respeito ao recolhimento, utilização e divulgação de quaisquer Dados Pessoais enviados através da nossa Página da Internet.',
            ),
            BuildParagraph(
              'Assim sendo, não deixe de ler os documentos sobre Política de Privacidade a fim de ficar sabendo que tipo de informações pessoais recolhemos neste site, como elas são tratadas e a que fins se destinam, bem como os consentimentos e autorizações para utilização dos conteúdos.',
            ),
            BuildSectionTitle(
              '10. Declaração de Isenção e Limitação de Responsabilidades',
            ),
            BuildParagraph(
              'Nem a PATAKI e nem ninguém que tenha ajudado a desenvolver, criar, produzir ou fornecer o conteúdo deste site é responsável por quaisquer danos decorrentes da sua utilização ou incapacidade para utilizá-lo, mesmo se tiver sido avisado da possibilidade de tais danos, a menos que essas pessoas tenham agido intencionalmente de forma dolosa ou demonstrado negligência grosseira ou no caso de a responsabilidade não poder ser de outra forma excluída legalmente. Isto inclui, mas não se limita a:',
            ),
            BuildParagraph(
              '- Compensações por quaisquer perdas ou prejuízos;',
            ),
            BuildParagraph(
              '- Danos que não decorrem diretamente de uma ação, mas apenas de algumas das consequências ou resultados dessa ação (danos indiretos);',
            ),
            BuildParagraph(
              '- Outros danos e despesas diversos. (danos acidentais).',
              sizedBox: true,
            ),
            BuildParagraph(
              'A PATAKI empreenderá os esforços razoáveis para assegurar que a informação fornecida neste site seja exata e precisa, ainda que não possa garantir tal exatidão, e não faça qualquer representação referente à utilização ou resultados de qualquer conteúdo deste site em termos da sua exatidão, fiabilidade ou qualquer outra.',
            ),
            BuildParagraph(
              'Sem limitação para o que foi dito, e na medida do legalmente permitido, as funções contidas neste site, as operações deste site e todo o conteúdo deste site são fornecidos "como estão", sem garantia de qualquer espécie, expressa ou implícita, incluindo, mas não limitado, a quaisquer garantias implícitas de comerciabilidade, adequação a um determinado fim, exatidão ou não infração de direitos (mas excluindo qualquer garantia implícita ou condição que não possa ser legalmente excluída). Não há qualquer garantia relativamente à satisfação que o usuário pretende obter das informações contidas neste website. Não há qualquer garantia que este website, a informação nele contida ou os nossos esforços satisfarão qualquer um dos seus objetivos ou necessidades dos seus usuários.',
            ),
            BuildParagraph(
              'Concorda que a PATAKI em todo o mundo não é responsável, nem lhe pode ser imputada qualquer conduta difamatória, ofensiva, de infração ou ilegal, de qualquer visitante deste ou de terceiros.',
            ),
            BuildSectionTitle(
              '11. Litígios entre Membros',
            ),
            BuildParagraph(
              'A PATAKI não se responsabiliza pelas interações entre os membros ou entre os Membros e Agentes de Publicidade, os quais são da exclusiva responsabilidade deles.',
            ),
            BuildParagraph(
              'O usuário concorda que a PATAKI não será responsável se ocorrer qualquer disputa entre si e terceiros (incluindo, sem limitação, a qualquer membro), não tendo qualquer obrigação de estar envolvido, não tendo qualquer obrigação de monitorar disputas entre si e outros membros.',
            ),
            BuildSectionTitle(
              '12. Lei Vigente',
            ),
            BuildParagraph(
              'Este “Termos e Condições de Uso” será regido pelas leis brasileiras, não obstante a possibilidade de aplicação da lei de outra jurisdição em caso de ocorrência de conflitos de leis.',
            ),
            BuildSectionTitle(
              '13. Outros',
            ),
            BuildParagraph(
              'No caso de se descobrir que qualquer disposição deste “Termos e Condições de Uso” é nula, ilegal ou não possível de se fazer cumprir por qualquer motivo, tal disposição será considerada independente destes termos e não afetará a validade e o cumprimento das restantes disposições.',
            ),
          ],
        ),
      ),
    );
  }
}
