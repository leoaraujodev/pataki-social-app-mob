import 'package:get/get.dart';
import 'package:pataki/screens/timeline/timeline_controller.dart';

class TimelineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TimelineController());
  }
}
