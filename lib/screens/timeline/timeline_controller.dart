import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/endpoints/delete_post_endpoint.dart';
import 'package:pataki/core/endpoints/like_timeline_endpoint.dart';
import 'package:pataki/core/endpoints/timeline_endpoint.dart';
import 'package:pataki/core/interactor/get_user_id_interactor.dart';
import 'package:pataki/core/models/delete_post_request.dart';
import 'package:pataki/core/models/like_timeline_request.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/models/timeline_request.dart';
import 'package:pataki/core/models/timeline_response.dart';

class TimelineController extends GetxController {
  final loggedUserId = RxInt();
  final scrollController = ScrollController();
  final posts = RxList<TimelinePost>([]);

  final loadingInitialPosts = true.obs;
  final errorOnInitialPosts = false.obs;

  final hasMorePosts = false.obs;
  final errorOnMorePosts = false.obs;

  int _lastId;
  bool _fetchingMutex = false;
  bool _likingMutex = false;

  @override
  void onInit() {
    getInitialPosts();
    scrollController.addListener(() {
      if (!scrollController.hasClients) return;

      if (scrollController.offset >= scrollController.position.maxScrollExtent) {
        if (hasMorePosts.value && !errorOnMorePosts.value) getMorePosts();
      }
    });
    super.onInit();
  }

  Future<void> getInitialPosts() async {
    loggedUserId.value = await GetUserIdInteractor().execute();
    loadingInitialPosts.value = true;
    errorOnInitialPosts.value = false;

    try {
      final response = await _getTimelineResponse();
      _setValues(response);
    } catch (e) {
      errorOnInitialPosts.value = true;
      _showErrorSnackbar();
    }

    loadingInitialPosts.value = false;
  }

  void updateCommentCount(int timelineId, int newCount) {
    int index = posts.indexWhere((post) => post.id == timelineId);

    if (index == -1) return;

    posts[index] = posts[index].copyWith(
      imagePath: posts[index].imagePath,
      totComment: newCount,
    );
  }

  Future<void> refreshPosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    int previousLastId = _lastId;

    try {
      _lastId = null;
      final response = await _getTimelineResponse();
      posts.clear();
      _setValues(response);
    } catch (e) {
      _lastId = previousLastId;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<void> getMorePosts() async {
    if (_fetchingMutex) return;
    _fetchingMutex = true;

    errorOnMorePosts.value = false;

    try {
      final response = await _getTimelineResponse();
      _setValues(response);
    } catch (e) {
      errorOnMorePosts.value = true;
      _showErrorSnackbar();
    }

    _fetchingMutex = false;
  }

  Future<TimelineResponse> _getTimelineResponse() =>
      TimelineEndpoint(TimelineRequest(lastId: _lastId)).call();

  void _setValues(TimelineResponse response) {
    posts.addAll(response.posts);

    if (response.posts.isNotEmpty) _lastId = response.posts.last.id;

    if (response.posts.length < 15) {
      hasMorePosts.value = false;
    } else {
      hasMorePosts.value = true;
    }
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> likeComment(int timelineId, bool like) async {
    if (_likingMutex) return;
    _likingMutex = true;

    try {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: like,
        imagePath: posts[index].imagePath,
      );

      await LikeTimelineEndpoint(LikeTimelineRequest(timelineId: timelineId, like: like)).call();
    } catch (e) {
      int index = posts.indexWhere((timeline) => timeline.id == timelineId);

      posts[index] = posts[index].copyWith(
        totLikes: !like ? posts[index].totLikes + 1 : posts[index].totLikes - 1,
        liked: !like,
        imagePath: posts[index].imagePath,
      );
      _showErrorSnackbar();
    }

    _likingMutex = false;
  }

  Future<void> deletePost(int id) async {
    try {
      await DeletePostEndpoint(DeletePostRequest(id: id)).call();

      int index = posts.indexWhere((post) => post.id == id);

      posts.removeAt(index);
      Get.back();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }
}
