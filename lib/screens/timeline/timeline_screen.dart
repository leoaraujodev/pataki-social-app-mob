import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/comment/comment_screen.dart';
import 'package:pataki/screens/create_post/create_post_screen.dart';
import 'package:pataki/screens/edit_post/edit_post_screen.dart';
import 'package:pataki/screens/home/home_controller.dart';
import 'package:pataki/screens/likes/likes_screen.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/screens/report_timeline_post/report_timeline_post_screen.dart';
import 'package:pataki/screens/timeline/timeline_controller.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/options_dialog.dart';
import 'package:pataki/uikit/widgets/timeline_post_card.dart';

class TimelineScreen extends GetView<TimelineController> {
  static String id = "timeline_screen";

  final homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "News",
        actions: [
          IconButton(
            icon: Container(
              alignment: Alignment.center,
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                color: PatakiColors.orange,
              ),
              child: Icon(
                Icons.add_box_outlined,
                color: Colors.white,
              ),
            ),
            onPressed: () => Get.toNamed(CreatePostScreen.routeName),
            padding: EdgeInsets.zero,
          )
        ],
      ),
      body: Obx(
        () {
          if (controller.loadingInitialPosts.value) {
            return Center(child: RefreshProgressIndicator());
          }

          if (controller.errorOnInitialPosts.value) {
            return Center(
              child: GestureDetector(
                onTap: controller.getInitialPosts,
                child: RefreshProgressIndicator(
                  value: 1.0,
                ),
              ),
            );
          }

          return RefreshIndicator(
            child: SafeArea(
              child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                controller: controller.scrollController,
                itemCount: controller.posts.length,
                itemBuilder: (context, index) {
                  Widget card = TimelinePostCard(
                    post: controller.posts[index],
                    onCommentPressed: () async {
                      await Get.toNamed(
                        CommentScreen.routeName(controller.posts[index].id),
                        arguments: {
                          "updateCommentCount": controller.updateCommentCount,
                          "timestamp": DateTime.now().toIso8601String(),
                        },
                      );

                      if (homeController.doRefresh.value) {
                        controller.getInitialPosts();
                        homeController.doRefresh.value = false;
                      }
                    },
                    onLikePressed: () => controller.likeComment(
                      controller.posts[index].id,
                      !controller.posts[index].liked,
                    ),
                    onLikesPressed: () => Get.toNamed(
                      LikesScreen.routeName(controller.posts[index].id),
                      arguments: {"timestamp": DateTime.now().toIso8601String()},
                    ),
                    onMorePressed: () => Get.dialog(
                      OptionsDialog(
                        title: "Opções da Postagem",
                        onReportPressed:
                            controller.loggedUserId.value != controller.posts[index].user.id
                                ? () async {
                                    int reportedPostId = controller.posts[index].id;

                                    var reported = await Get.offNamed(
                                      ReportTimelinePostScreen.routeName,
                                      arguments: {"post": controller.posts[index]},
                                    );

                                    if (reported == true) {
                                      controller.posts.removeWhere(
                                        (post) => post.id == reportedPostId,
                                      );
                                    }
                                  }
                                : null,
                        onEditPressed:
                            controller.loggedUserId.value == controller.posts[index].user.id
                                ? () async {
                                    await Get.offNamed(
                                      EditPostScreen.routeName(controller.posts[index].id),
                                    );
                                    controller.refreshPosts();
                                  }
                                : null,
                        onDeletePressed:
                            controller.loggedUserId.value == controller.posts[index].user.id
                                ? () {
                                    Get.back();
                                    Get.dialog(
                                      ConfirmDialog(
                                        onConfirmPressed: () => controller.deletePost(
                                          controller.posts[index].id,
                                        ),
                                      ),
                                    );
                                  }
                                : null,
                      ),
                    ),
                    onSendPressed: () {},
                    onUserPressed: controller.posts[index].user.id != 10
                        ? () async {
                            await Get.toNamed(
                              PetScreen.routeName(controller.posts[index].pet.id),
                              arguments: {"timestamp": DateTime.now().toIso8601String()},
                            );

                            if (homeController.doRefresh.value) {
                              controller.getInitialPosts();
                              homeController.doRefresh.value = false;
                            }
                          }
                        : null,
                  );

                  if (index == controller.posts.length - 1 && controller.hasMorePosts.value) {
                    return Column(
                      children: [
                        card,
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Obx(
                            () => controller.errorOnMorePosts.value
                                ? Center(
                                    child: GestureDetector(
                                      onTap: controller.getMorePosts,
                                      child: RefreshProgressIndicator(
                                        value: 1.0,
                                      ),
                                    ),
                                  )
                                : RefreshProgressIndicator(),
                          ),
                        ),
                      ],
                    );
                  }

                  return card;
                },
              ),
            ),
            onRefresh: controller.refreshPosts,
          );
        },
      ),
    );
  }
}
