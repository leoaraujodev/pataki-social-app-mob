import 'package:get/get.dart';
import 'package:pataki/screens/training/training_controller.dart';

class TrainingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TrainingController());
  }
}
