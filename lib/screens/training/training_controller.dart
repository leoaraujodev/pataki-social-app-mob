import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:pataki/core/endpoints/trick_groups_endpoint.dart';
import 'package:pataki/core/models/empty_request.dart';

class TrainingController extends GetxController {
  final scrollController = ScrollController();
  final textController = TextEditingController();
  final isRequesting = false.obs;

  final player = AudioPlayer();

  final trickGroups = List();
  final searchTrickGroups = RxList();

  final currentSearchingTerm = RxString();

  final resultIsEmpty = false.obs;

  final hasErrorOnRequest = true.obs;

  @override
  void onInit() {
    setAudioFilePath();
    getGroups();

    debounce(currentSearchingTerm, newSearch);
    super.onInit();
  }

  setAudioFilePath() async {
    try {
      await player.setAsset('assets/clicker.mp3');
      player.load();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  playAudioFile() {
    try {
      player.play();

      setAudioFilePath();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  String noAccentsLowerCase(String term) {
    term = term.toLowerCase();
    term = term.replaceAll(RegExp(r'[áàãâä]'), 'a');
    term = term.replaceAll(RegExp(r'[éèêë]'), 'e');
    term = term.replaceAll(RegExp(r'[íìîï]'), 'i');
    term = term.replaceAll(RegExp(r'[óòõôö]'), 'o');
    term = term.replaceAll(RegExp(r'[úùûü]'), 'u');
    term = term.replaceAll('ç', 'c');
    term = term.replaceAll('ñ', 'n');

    return term;
  }

  newSearch(String term) {
    if (isRequesting.value) {
      return;
    }
    searchTrickGroups.clear();

    if (term == "") {
      searchTrickGroups.addAll(trickGroups);
    } else {
      searchTrickGroups.addAll(
        trickGroups.where(
          (group) => noAccentsLowerCase(group.name).contains(noAccentsLowerCase(term)),
        ),
      );
    }
  }

  Future<void> getGroups() async {
    if (isRequesting.value) {
      return;
    }
    isRequesting.value = true;
    resultIsEmpty.value = true;
    hasErrorOnRequest.value = false;

    try {
      final response = await TrickGroupsEndpoint(EmptyRequest()).call();

      if (response.trickGroups.isEmpty) {
        return;
      }
      resultIsEmpty.value = false;

      trickGroups.clear();
      trickGroups.addAll(response.trickGroups);

      searchTrickGroups.clear();
      searchTrickGroups.addAll(response.trickGroups);
    } catch (e) {
      hasErrorOnRequest.value = true;
      _showErrorSnackbar();
    }
    isRequesting.value = false;
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  @override
  void onClose() {
    textController.dispose();
    player.stop();
    player.dispose();
    super.onClose();
  }
}
