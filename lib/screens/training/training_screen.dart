import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/training/training_controller.dart';
import 'package:pataki/screens/training_category/training_category_screen.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/clicker.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

double screenWidth;

class TrainingScreen extends GetView<TrainingController> {
  static String id = "training_screen";

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: DefaultAppBar(title: "Treinamento"),
      body: Column(
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: CommentTextField(
              controller: controller.textController,
              maxLines: 1,
              textInputAction: TextInputAction.search,
              hintText: "Buscar",
              onChanged: (String value) => controller.currentSearchingTerm.value = value,
            ),
          ),
          Expanded(
            child: Obx(
              () {
                if (controller.isRequesting.value) {
                  return Center(child: RefreshProgressIndicator());
                }

                if (controller.hasErrorOnRequest.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: () => controller.newSearch(controller.currentSearchingTerm.value),
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                if (controller.resultIsEmpty.value) {
                  return Center(
                    child: Text(
                      "Nenhum resultado encontrado",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey,
                      ),
                    ),
                  );
                }
                return ListView(
                  controller: controller.scrollController,
                  shrinkWrap: true,
                  children: [
                    ...controller.searchTrickGroups
                        .map(
                          (group) => BuildCard(
                            title: group.name,
                            titleSize: screenWidth >= 375 ? 24 : 19,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
                                child: Text(
                                  group.desc,
                                  style: TextStyle(fontSize: screenWidth >= 375 ? 16 : 13),
                                ),
                              ),
                            ],
                            onTap: () => Get.toNamed(
                              TrainingCategoryScreen.routeName,
                              arguments: {"trickGroup": group},
                            ),
                          ),
                        )
                        .toList(),
                    Clicker(controller),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
