import 'package:get/get.dart';
import 'package:pataki/screens/training_category/training_category_controller.dart';

class TrainingCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(TrainingCategoryController());
  }
}
