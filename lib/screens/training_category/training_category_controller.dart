import 'package:get/get.dart';
import 'package:pataki/core/endpoints/get_tricks_by_group_endpoint.dart';
import 'package:pataki/core/models/trick.dart';
import 'package:pataki/core/models/trick_group.dart';
import 'package:pataki/core/models/tricks_by_group_request.dart';
import 'package:pataki/core/models/tricks_by_group_response.dart';

class TrainingCategoryController extends GetxController {
  TrickGroup trickGroup = Get.arguments["trickGroup"];

  final tricks = RxList<Trick>([]);

  final tricksIsEmpty = true.obs;
  final edittingMode = false.obs;

  final loadingTricks = false.obs;
  final errorOnTricks = true.obs;

  @override
  void onInit() {
    getTricksByGroup();

    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getTricksByGroup() async {
    errorOnTricks.value = false;
    loadingTricks.value = true;

    tricks.clear();

    try {
      TricksByGroupResponse response =
          await GetTricksByGroupEndpoint(TricksByGroupRequest(groupId: trickGroup.id)).call();

      if (response.tricks.isNotEmpty) {
        tricksIsEmpty.value = false;
        tricks.addAll(response.tricks);
      }
    } catch (e) {
      errorOnTricks.value = true;
      _showErrorSnackbar();
    }

    loadingTricks.value = false;
  }

  @override
  void onClose() {
    super.onClose();
  }
}
