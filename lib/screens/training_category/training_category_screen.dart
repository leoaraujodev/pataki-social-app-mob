import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/training_category/training_category_controller.dart';
import 'package:pataki/screens/training_detail/training_detail_screen.dart';
import 'package:pataki/uikit/widgets/build_small_card.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';

double screenWidth;

class TrainingCategoryScreen extends GetView<TrainingCategoryController> {
  static String routeName = "/training_category_screen";

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: controller.trickGroup.name,
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingTricks.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnTricks.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getTricksByGroup,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }
                if (controller.tricksIsEmpty.value) {
                  return Center(
                    child: Text(
                      "Nenhum treinamento disponível",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey,
                      ),
                    ),
                  );
                }

                return ListView(
                  shrinkWrap: true,
                  children: controller.tricks
                      .map((trick) => BuildSmallCard(
                            title: trick.title,
                            resizeToSmallScreen: screenWidth <= 375,
                            onPressed: () => Get.toNamed(
                              TrainingDetailScreen.routeName,
                              arguments: {"trick": trick},
                            ),
                            media: trick.media,
                          ))
                      .toList(),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
