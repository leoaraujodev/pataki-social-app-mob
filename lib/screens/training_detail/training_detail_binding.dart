import 'package:get/get.dart';
import 'package:pataki/screens/training_detail/training_detail_controller.dart';

class TrainingDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(TrainingDetailController());
  }
}
