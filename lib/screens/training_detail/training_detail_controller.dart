import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:pataki/core/endpoints/reminder_trick_delete_endpoint.dart';
import 'package:pataki/core/endpoints/trick_details_endpoint.dart';
import 'package:pataki/core/endpoints/trick_step_state_endpoint.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/reminder.dart';
import 'package:pataki/core/models/reminder_delete_request.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/models/trick.dart';
import 'package:pataki/core/models/trick_details_request.dart';
import 'package:pataki/core/models/trick_step_state_request.dart';

class TrainingDetailController extends GetxController with Notifications {
  Rx<Trick> trick = Rx(Get.arguments["trick"]);

  final RxDouble progress = 0.0.obs;
  final RxString progressStatus = "".obs;

  final RxBool showPlayerIconButton = true.obs;
  final RxBool hasChangedPlayerStatus = false.obs;

  final player = AudioPlayer();

  final RxBool sendedRequest = false.obs;
  final RxBool failedOnRequest = false.obs;
  final RxBool finishedRequest = false.obs;

  final tricksIsEmpty = true.obs;
  final edittingMode = false.obs;

  final loadingTrick = false.obs;
  final errorOnTrick = true.obs;

  @override
  void onInit() {
    getTrickDetails();
    setAudioFilePath();
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  setAudioFilePath() async {
    try {
      await player.setAsset('assets/clicker.mp3');
      player.load();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  playAudioFile() {
    try {
      player.play();

      setAudioFilePath();
    } catch (e) {
      _showErrorSnackbar();
    }
  }

  Future<void> getTrickDetails() async {
    errorOnTrick.value = false;
    loadingTrick.value = true;

    try {
      trick.value = await TrickDetailsEndpoint(TrickDetailsRequest(trickId: trick.value.id)).call();

      _setProgressValues(trick.value.state);

      trick.refresh();
    } catch (e) {
      errorOnTrick.value = true;
      _showErrorSnackbar();
    }

    loadingTrick.value = false;
  }

  _setProgressValues(String state) {
    switch (state) {
      case "no_progress":
        progress.value = 0;
        progressStatus.value = "no_progress";
        break;

      case "in_progress":
        progress.value = 0.5;
        progressStatus.value = "in_progress";
        break;

      case "completed":
        progress.value = 1;
        progressStatus.value = "completed";
        break;
    }
  }

  Future<void> trickStepSetState(String state) async {
    finishedRequest.value = false;

    try {
      final response = await TrickStepStateEndpoint(
        TrickStepStateRequest(
          trickId: trick.value.id,
          state: state,
        ),
      ).call();

      trick.value.state = response.state;

      trick.refresh();

      _setProgressValues(trick.value.state);

      failedOnRequest.value = false;
    } catch (e) {
      failedOnRequest.value = true;
      _showErrorSnackbar();
    }

    finishedRequest.value = true;
  }

  Future<void> deleteNotification(reminderId) async {
    final Reminder reminder =
        trick.value.notifications.firstWhere((reminder) => reminder.id == reminderId);
    bool allDaysTrue = true;

    try {
      SuccessMessageResponse response = await ReminderTrickDeleteEndpoint(ReminderDeleteRequest(
        trickId: trick.value.id,
        reminderId: reminder.id,
      )).call();

      cancelDailyOrWeeklyNotifications(reminder: reminder, allDaysTrue: allDaysTrue);

      getTrickDetails();

      Get.back();
      Get.snackbar("Pronto", response.message);
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    player.stop();
    player.dispose();

    super.onClose();
  }
}
