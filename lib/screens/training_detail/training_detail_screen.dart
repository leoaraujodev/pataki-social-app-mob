import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/media.dart';
import 'package:pataki/screens/create_reminder/user/create_user_reminder_screen.dart';
import 'package:pataki/screens/training_detail/training_detail_controller.dart';
import 'package:pataki/uikit/widgets/build_card.dart';
import 'package:pataki/uikit/widgets/build_notification.dart';
import 'package:pataki/uikit/widgets/clicker.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/form_dropdown_field.dart';
import 'package:pataki/uikit/widgets/media_player.dart';
import 'package:pataki/uikit/widgets/rounded_text_button.dart';

double screenWidth;

class TrainingDetailScreen extends GetView<TrainingDetailController> {
  static String routeName = "/training_detail_screen";

  final textGreyStyle = TextStyle(fontSize: 17, color: Color(0xFF66839F));
  final horizontalPadding = EdgeInsets.symmetric(horizontal: 18);

  final Map<String, String> progressStatus = {
    'Sem progresso': 'no_progress',
    'Em andamento': 'in_progress',
    'Concluído': 'completed',
  };

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: controller.trick.value.title,
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
            child: Obx(
              () {
                if (controller.loadingTrick.value) {
                  return Center(child: RefreshProgressIndicator());
                }
                if (controller.errorOnTrick.value) {
                  return Center(
                    child: GestureDetector(
                      onTap: controller.getTrickDetails,
                      child: RefreshProgressIndicator(
                        value: 1.0,
                      ),
                    ),
                  );
                }

                return ListView(
                  children: [
                    _buildMedia(mediaObject: controller.trick.value.media),
                    _buildDifficultyAndProgress(),
                    _buildProgressDropdown(),
                    _buildTrainingNotifications(),
                    _buildSteps(),
                    _buildTips(),
                    Clicker(controller),
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMedia({Media mediaObject}) {
    if (mediaObject.imagePath == null) {
      return Container();
    }

    if (mediaObject.fileType == FileType.image) {
      return Container(
        width: Get.width,
        child: Image.network(
          mediaObject.imagePath,
          fit: BoxFit.fitWidth,
        ),
      );
    } else if (mediaObject.fileType == FileType.video) {
      return MediaPlayer(videoPath: mediaObject.imagePath);
    }

    return Container();
  }

  Widget _buildDifficultyAndProgress() {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: horizontalPadding,
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            children: [
              TableRow(
                children: [
                  Container(
                    alignment: AlignmentDirectional.centerStart,
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      "DIFICULDADE",
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1.9,
                        color: Color(0xFF66839F),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      for (var i = 1; i <= 4; i++)
                        Container(
                          alignment: AlignmentDirectional.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          height: 48,
                          width: 24,
                          child: SvgPicture.asset(
                            "images/training_bone.svg",
                            color:
                                controller.trick.value.difficulty >= i ? null : Color(0xFFDEE7F0),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
              TableRow(
                children: [
                  Container(
                    alignment: AlignmentDirectional.centerStart,
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      "PROGRESSO",
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1.9,
                        color: Color(0xFF66839F),
                      ),
                    ),
                  ),
                  Container(
                    alignment: AlignmentDirectional.center,
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(1000)),
                      child: LinearProgressIndicator(
                        value: controller.progress.value,
                        minHeight: 10,
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFBBC05)),
                        backgroundColor: Color(0xFFDEE7F0),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildProgressDropdown() {
    return Container(
      alignment: AlignmentDirectional.center,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
      child: FormDropdownField(
        value: controller.progressStatus.value,
        items: progressStatus
            .map((value, id) {
              return MapEntry(
                  value,
                  DropdownMenuItem(
                    value: id.toString(),
                    child: Text(value),
                  ));
            })
            .values
            .toList(),
        icon: _buildDropdownCheckMark(sendedRequest: controller.sendedRequest.value),
        onChanged: (value) {
          if (controller.progressStatus.value != value) {
            controller.sendedRequest.value = true;
            controller.trickStepSetState(value);
          }
        },
      ),
    );
  }

  _buildDropdownCheckMark({@required bool sendedRequest}) {
    return Row(
      children: [
        if (sendedRequest) _buildModificationsIcon(),
        Icon(Icons.keyboard_arrow_down),
      ],
    );
  }

  _buildModificationsIcon() {
    if (!controller.finishedRequest.value) {
      return Container(
        margin: EdgeInsets.only(right: 8),
        width: 18,
        height: 18,
        child: new CircularProgressIndicator(),
      );
    }

    return Container(
      margin: EdgeInsets.only(right: 8),
      width: 18,
      height: 18,
      child: controller.failedOnRequest.value
          ? Icon(
              Icons.close,
              color: Colors.red,
            )
          : Icon(
              Icons.check,
              color: Colors.green,
            ),
    );
  }

  Widget _buildTrainingNotifications() {
    return Padding(
      padding: horizontalPadding,
      child: BuildCard(
        title: "Treinamentos",
        titleHorizontalPadding: 0,
        controller: controller,
        hasButtons: true,
        addButtonOnTap: () async {
          var refresh =
              await Get.toNamed(CreateUserReminderScreen.routeName(controller.trick.value.id));

          if (refresh != null) {
            controller.getTrickDetails();
            Get.snackbar("Pronto", "Rotina cadastrada com sucesso");
          }
        },
        children: [
          ...controller.trick.value.notifications
              .map((notification) => Padding(
                    padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
                    child: BuildNotification(
                      screenWidth: screenWidth,
                      name: notification.name,
                      time: notification.time + " • " + controller.notificationDays(notification),
                      controller: controller,
                      notificationId: notification.id,
                      hasNoPadding: true,
                    ),
                  ))
              .toList(),
          SizedBox(height: 12),
          Text(
            controller.trick.value.desc,
            style: textGreyStyle,
          ),
        ],
      ),
    );
  }

  Widget _buildSteps() {
    if (controller.trick.value.steps.isEmpty) return Container();

    return BuildCard(
      title: "Passos",
      titleHorizontalPadding: horizontalPadding.horizontal / 2,
      children: [
        ...controller.trick.value.steps
            .asMap()
            .entries
            .map((step) => Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: horizontalPadding.horizontal / 2,
                        vertical: 12,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RoundedTextButton(
                            text: (step.key + 1).toString(),
                            actived: false,
                            onPressed: null,
                            screenWidth: screenWidth,
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Text(
                              step.value.desc,
                              softWrap: true,
                              style: textGreyStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    _buildMedia(mediaObject: step.value.media),
                    if (step.key + 1 != controller.trick.value.steps.length) SizedBox(height: 40),
                  ],
                ))
            .toList(),
      ],
    );
  }

  Widget _buildTips() {
    if (controller.trick.value.tips.isEmpty) return Container();

    return Padding(
      padding: horizontalPadding,
      child: BuildCard(
        title: "Dicas",
        titleHorizontalPadding: 0,
        marginBottom: 0,
        children: [
          ...controller.trick.value.tips
              .map(
                (tip) => Column(
                  children: [
                    Text(
                      tip.tip,
                      softWrap: true,
                      style: textGreyStyle,
                    ),
                    SizedBox(height: 14),
                  ],
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
