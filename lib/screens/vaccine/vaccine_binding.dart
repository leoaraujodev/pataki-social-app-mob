import 'package:get/get.dart';
import 'package:pataki/screens/vaccine/vaccine_controller.dart';

class VaccineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => VaccineController());
  }
}
