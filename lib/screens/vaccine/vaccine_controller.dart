import 'package:get/get.dart';
import 'package:pataki/core/endpoints/vaccines_endpoint.dart';
import 'package:pataki/core/endpoints/vaccine_delete_endpoint.dart';
import 'package:pataki/core/models/notifications.dart';
import 'package:pataki/core/models/success_message_response.dart';
import 'package:pataki/core/models/vaccine_delete_request.dart';
import 'package:pataki/core/models/vaccines_request.dart';
import 'package:pataki/core/models/vaccines_response.dart';
import 'package:pataki/core/models/vaccine.dart';

class VaccineController extends GetxController with Notifications {
  final loggedUserId = RxInt();
  int petId = int.parse(Get.parameters["petId"]);

  final RxList<Vaccine> vaccines = RxList([]);

  final vaccinesIsEmpty = true.obs;
  final edittingMode = false.obs;

  final loadingVaccines = false.obs;
  final errorOnVaccines = true.obs;

  @override
  void onInit() {
    getPetVaccines();
    super.onInit();
  }

  void _showErrorSnackbar() {
    Get.snackbar("Erro de conexão", "Verifique sua conexão com a Internet e tente novamente");
  }

  Future<void> getPetVaccines() async {
    errorOnVaccines.value = false;
    loadingVaccines.value = true;

    vaccines.clear();

    try {
      VaccinesResponse response = await VaccinesEndpoint(VaccinesRequest(petId: petId)).call();

      if (response.vaccines.isNotEmpty) {
        vaccinesIsEmpty.value = false;
        vaccines.addAll(response.vaccines);
      }
    } catch (e) {
      errorOnVaccines.value = true;
      _showErrorSnackbar();
    }

    loadingVaccines.value = false;
  }

  Future<void> deleteNotification(vaccineId) async {
    try {
      SuccessMessageResponse response = await VaccineDeleteEndpoint(VaccineDeleteRequest(
        petId: petId,
        vaccineId: vaccineId,
      )).call();

      // The 1 before the $id identifies that this is a vaccine notification
      await cancelNotification(int.tryParse("1$vaccineId"));

      getPetVaccines();

      Get.back();
      Get.snackbar("Pronto", response.message);
    } catch (e) {
      Get.back();
      _showErrorSnackbar();
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
