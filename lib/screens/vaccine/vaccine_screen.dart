import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pataki/screens/create_vaccine/create_vaccine_screen.dart';
import 'package:pataki/screens/vaccine/vaccine_controller.dart';
import 'package:pataki/uikit/widgets/build_notification.dart';
import 'package:pataki/uikit/widgets/default_app_bar.dart';
import 'package:pataki/uikit/widgets/rounded_button.dart';

double screenWidth;

class VaccineScreen extends GetView<VaccineController> {
  static String routeName(petId) => "/vaccine_screen/$petId";
  final dateFormat = DateFormat("dd/MM/yyyy");

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar(
        title: "Vacinas",
        centerTitle: true,
        actions: [
          Obx(
            () {
              if (controller.edittingMode.value)
                return RoundedButton(
                  icon: Icon(
                    Icons.edit_off,
                    color: Colors.red,
                    size: 30.0,
                  ),
                  backgroundColor: null,
                  padding: 12,
                  innerPadding: 2,
                  onTap: () => controller.edittingMode.value = false,
                );

              return Container();
            },
          ),
          RoundedButton(
            icon: Icon(
              Icons.add,
              color: Colors.white,
              size: 20.0,
            ),
            onTap: () async {
              var response = await Get.toNamed(CreateVaccineScreen.routeName(controller.petId));

              if (response != null) {
                controller.getPetVaccines();
                Get.snackbar("Pronto", "Vacina cadastrada com sucesso");
              }
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Obx(
            () {
              if (controller.loadingVaccines.value) {
                return Center(child: RefreshProgressIndicator());
              }
              if (controller.errorOnVaccines.value) {
                return Center(
                  child: GestureDetector(
                    onTap: controller.getPetVaccines,
                    child: RefreshProgressIndicator(
                      value: 1.0,
                    ),
                  ),
                );
              }
              if (controller.vaccinesIsEmpty.value) {
                return Center(
                  child: Text(
                    "Sem vacinas cadastradas",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey,
                    ),
                  ),
                );
              }

              return ListView(
                shrinkWrap: true,
                children: controller.vaccines
                    .map((vaccine) => BuildNotification(
                          screenWidth: screenWidth,
                          name: vaccine.name,
                          time: dateFormat.format(vaccine.date),
                          controller: controller,
                          notificationId: vaccine.id,
                        ))
                    .toList(),
              );
            },
          )
        ],
      ),
    );
  }
}
