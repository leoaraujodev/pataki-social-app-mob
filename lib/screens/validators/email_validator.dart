import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin EmailValidator {
  final emailFocusNode = FocusNode();
  final isEmailValid = false.obs;
  final emailAutovalidateMode = AutovalidateMode.disabled.obs;
  final emailController = TextEditingController();

  String emailValidator(String value) {
    emailAutovalidateMode.value = AutovalidateMode.always;

    if (GetUtils.isEmail(value)) {
      return null;
    }

    return 'Este não é um e-mail válido';
  }

  void emailOnChanged(String value) {
    if (emailAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = emailValidator(value);
    if (error == null) {
      isEmailValid.value = true;
    } else {
      isEmailValid.value = false;
    }
  }
}
