import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin NameValidator {
  final nameFocusNode = FocusNode();
  final isNameValid = false.obs;
  final nameAutovalidateMode = AutovalidateMode.disabled.obs;
  final nameController = TextEditingController();

  String nameValidator(String value) {
    nameAutovalidateMode.value = AutovalidateMode.always;

    if (value.isNotEmpty) {
      return null;
    }

    return 'Este não é um nome válido';
  }

  void nameOnChanged(String value) {
    if (nameAutovalidateMode.value == AutovalidateMode.disabled) return;

    String error = nameValidator(value);
    if (error == null) {
      isNameValid.value = true;
    } else {
      isNameValid.value = false;
    }
  }
}
