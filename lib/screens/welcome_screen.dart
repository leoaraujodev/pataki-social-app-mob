import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/login/login_screen.dart';
import 'package:pataki/screens/register/register_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key, this.title}) : super(key: key);
  static String id = "welcome_screen";
  final String title;

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'images/splash.png',
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.topCenter,
          ),
          SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: SizedBox(),
                  flex: 2,
                ),
                Text(
                  "Bem-vindo ao Pataki!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30.0,
                    letterSpacing: -1,
                    color: PatakiColors.orange,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Expanded(
                  child: SizedBox(),
                  flex: 3,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: LargeTextButton(
                    backgroundColor: PatakiColors.blue,
                    onPressed: () => Get.toNamed(LoginScreen.id),
                    text: "COMEÇAR",
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: LargeTextButton(
                    onPressed: () => {Navigator.pushNamed(context, RegisterScreen.id)},
                    text: "CRIAR CONTA",
                    foregroundColor: PatakiColors.orange,
                    backgroundColor: Colors.transparent,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
