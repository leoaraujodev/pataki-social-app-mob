import 'dart:ui';

class PatakiColors {
  static const orange = Color(0xFFFF9000);
  static const yellowDark = Color(0xFFF2B43B);
  static const yellow = Color(0xFFFFDDB1);
  static const yellowLight = Color(0xFFF9F0E3);

  static const gray = Color(0xFFA1A4B2);
  static const lightGray = Color(0xFFE9EFF5);
  static const white = Color(0xFFF8F9FB);
  static const blue = Color(0xFF003087);
}
