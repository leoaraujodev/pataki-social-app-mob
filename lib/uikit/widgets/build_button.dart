import 'package:flutter/material.dart';

class BuildButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color borderColor;
  final Color backgroundColor;
  final double verticalPadding;
  final double horizontalPadding;
  final Function onPressed;

  BuildButton({
    @required this.text,
    @required this.textColor,
    this.borderColor = Colors.transparent,
    this.backgroundColor,
    this.verticalPadding = 0,
    this.horizontalPadding = 8,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontalPadding,
          vertical: verticalPadding,
        ),
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: textColor,
          ),
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(40.0),
        side: BorderSide(
          color: borderColor,
          width: 2,
        ),
      ),
      color: backgroundColor,
      onPressed: onPressed,
    );
  }
}
