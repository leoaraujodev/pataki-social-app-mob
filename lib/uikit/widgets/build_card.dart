import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:pataki/uikit/widgets/rounded_button.dart';

class BuildCard extends StatelessWidget {
  final String title;
  final List<Widget> children;
  final controller;
  final Function onTap;
  final double titleSize;
  final double titleHorizontalPadding;
  final double marginBottom;
  final bool hasButtons;
  final Function addButtonOnTap;

  BuildCard({
    this.title,
    @required this.children,
    this.controller,
    this.onTap,
    this.titleSize = 24,
    this.titleHorizontalPadding = 20,
    this.marginBottom = 20,
    this.hasButtons = false,
    this.addButtonOnTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      margin: EdgeInsets.only(bottom: marginBottom),
      width: Get.width,
      color: Colors.white,
      child: Material(
        color: Colors.transparent,
        child: Ink(
          child: InkWell(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (this.title != null)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: titleHorizontalPadding, vertical: 10),
                        child: Text(
                          title,
                          style: TextStyle(fontSize: titleSize),
                        ),
                      ),
                      if (hasButtons)
                        Row(
                          children: [
                            if (controller != null)
                              Obx(
                                () {
                                  if (controller.edittingMode.value)
                                    return RoundedButton(
                                      icon: Icon(
                                        Icons.edit_off,
                                        color: Colors.red,
                                        size: 30.0,
                                      ),
                                      backgroundColor: null,
                                      padding: 12,
                                      innerPadding: 2,
                                      onTap: () => controller.edittingMode.value = false,
                                    );

                                  return Container();
                                },
                              ),
                            RoundedButton(
                              padding: 0,
                              icon: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 20.0,
                              ),
                              onTap: addButtonOnTap,
                            ),
                          ],
                        ),
                    ],
                  ),
                ...children,
              ],
            ),
            onTap: onTap,
          ),
        ),
      ),
    );
  }
}
