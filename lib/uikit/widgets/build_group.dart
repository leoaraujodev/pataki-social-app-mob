import 'package:flutter/material.dart';
import 'package:pataki/core/models/group.dart';
import 'package:pataki/core/models/member.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class BuildGroup extends StatelessWidget {
  final Group group;
  final Function onLongPress;
  final Function onPressed;
  final int loggedUserId;

  BuildGroup({
    @required this.group,
    this.onLongPress,
    @required this.onPressed,
    @required this.loggedUserId,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildImage(size: 60, imagePath: group.imagePath.toString()),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    group.name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                  group.desc == null
                      ? Container()
                      : Text(
                          group.desc,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF90AECC),
                          ),
                        ),
                  SizedBox(height: 2),
                  if (group.totalMembers != 0)
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildLastUsersPictures(),
                        _buildLastUsersNames(),
                      ],
                    ),
                ],
              ),
            ),
          ],
        ),
        onPressed: onPressed,
        onLongPress: onLongPress,
      ),
    );
  }

  _buildImage({
    @required double size,
    double margin = 10,
    @required String imagePath,
  }) {
    if (imagePath == "null") {
      return Container();
    }

    return Container(
      margin: EdgeInsets.all(margin),
      height: size,
      width: size,
      decoration: BoxDecoration(
        color: PatakiColors.white,
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage(imagePath),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  _buildLastUsersPictures() {
    final double usersToShow = group.lastMembers.length.toDouble();

    return Container(
      width: (usersToShow) * 15 + 15,
      margin: EdgeInsets.only(right: 5),
      child: Stack(
        children: [
          if (group.lastMembers.length > 0)
            _buildImage(
              size: 30,
              margin: 0,
              imagePath: group.lastMembers[0].user.imagePath.toString(),
            ),
          if (group.lastMembers.length > 1)
            Positioned(
              left: 15,
              child: _buildImage(
                size: 30,
                margin: 0,
                imagePath: group.lastMembers[1].user.imagePath.toString(),
              ),
            ),
          if (group.lastMembers.length > 2)
            Positioned(
              left: 15,
              child: _buildImage(
                size: 30,
                margin: 0,
                imagePath: group.lastMembers[2].user.imagePath.toString(),
              ),
            ),
        ],
      ),
    );
  }

  _buildLastUsersNames() {
    final List<String> namesToShow = [];
    String textToShow;

    List<Member> membersToShow =
        group.lastMembers.length >= 3 ? group.lastMembers.take(3).toList() : group.lastMembers;

    Member firstUser = membersToShow.firstWhere(
      (member) => member.user.id == loggedUserId,
      orElse: () => null,
    );

    if (firstUser != null) {
      namesToShow.add("Você");
    }

    namesToShow.addAll(
      membersToShow
          .where(
        (member) => member.user.id != loggedUserId,
      )
          .map(
        (member) {
          return member.user.name.split(" ")[0];
        },
      ),
    );

    switch (group.totalMembers) {
      case 1:
        textToShow = namesToShow[0];
        break;
      case 2:
        textToShow = namesToShow.join(" e ");
        break;
      case 3:
        textToShow = "${namesToShow[0]}, ${namesToShow[1]} e ${namesToShow[2]}";
        break;
      case 4:
        textToShow = "${namesToShow[0]}, ${namesToShow[1]}, ${namesToShow[2]} e 1 membro";
        break;

      default:
        textToShow =
            "${namesToShow[0]}, ${namesToShow[1]} e ${namesToShow[2]} e ${group.totalMembers - 3} membros";
        break;
    }

    return Flexible(
      child: Text(
        textToShow,
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
      ),
    );
  }
}
