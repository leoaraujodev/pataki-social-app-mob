import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';

class BuildNotification extends StatelessWidget {
  final double screenWidth;
  final String name;
  final String time;
  final controller;
  final int notificationId;
  final bool hasNoPadding;

  BuildNotification({
    @required this.screenWidth,
    @required this.name,
    @required this.time,
    @required this.controller,
    @required this.notificationId,
    this.hasNoPadding = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: _notificationPadding(),
      child: Stack(
        alignment: Alignment.centerRight,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xFFFFEBD0),
            ),
            child: Material(
              color: Colors.transparent,
              child: Ink(
                child: InkWell(
                  onLongPress: () => controller.edittingMode.value = !controller.edittingMode.value,
                  child: Padding(
                    padding: screenWidth >= 375
                        ? EdgeInsets.symmetric(vertical: 16, horizontal: 24)
                        : EdgeInsets.symmetric(vertical: 16, horizontal: 18),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              name,
                              style: TextStyle(
                                fontSize: screenWidth >= 375 ? 18 : 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              time,
                              style: TextStyle(
                                color: PatakiColors.orange,
                                fontSize: screenWidth >= 375 ? 14 : 11,
                              ),
                            ),
                            Obx(() {
                              if (controller.edittingMode.value) return SizedBox(width: 20);
                              return Container();
                            }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Obx(
            () {
              if (controller.edittingMode.value)
                return Positioned(
                  right: 0,
                  child: IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      Get.dialog(
                        ConfirmDialog(
                            onConfirmPressed: () => controller.deleteNotification(notificationId)),
                      );
                    },
                  ),
                );

              return Container();
            },
          ),
        ],
      ),
    );
  }

  _notificationPadding() {
    if (hasNoPadding) {
      return EdgeInsets.all(0);
    }

    return (screenWidth >= 375
        ? EdgeInsets.fromLTRB(22, 20, 22, 0)
        : EdgeInsets.fromLTRB(16, 20, 16, 0));
  }
}
