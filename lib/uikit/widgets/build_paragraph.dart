import 'package:flutter/material.dart';

class BuildParagraph extends StatelessWidget {
  final String text;
  final bool sizedBox;

  BuildParagraph(this.text, {this.sizedBox = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          text,
          textAlign: TextAlign.justify,
          style: TextStyle(fontSize: 15),
        ),
        if (sizedBox) SizedBox(height: 14),
      ],
    );
  }
}
