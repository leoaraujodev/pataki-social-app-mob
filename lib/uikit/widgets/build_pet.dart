import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/pet.dart';
import 'package:pataki/screens/pet/pet_screen.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/confirm_dialog.dart';

class BuildPet extends StatelessWidget {
  final Pet pet;
  final Function onLongPress;
  final Function onPressed;
  final controller;

  BuildPet({
    @required this.pet,
    this.onLongPress,
    this.onPressed,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: 70,
          child: FlatButton(
            padding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(width: 10),
                Container(
                  margin: EdgeInsets.all(10),
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    color: PatakiColors.white,
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(pet.imagePath.toString()),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        pet.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        ),
                      ),
                      if (pet.tutor == null && pet.birthDate != null)
                        Text(
                          _calculateAge(pet: pet),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF90AECC),
                          ),
                        ),
                      if (pet.tutor != null)
                        Text(
                          'Tutor',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF90AECC),
                          ),
                        ),
                      if (pet.tutor != null)
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5, top: 2),
                              height: 18,
                              width: 18,
                              decoration: BoxDecoration(
                                color: PatakiColors.white,
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: NetworkImage(pet.tutor.imagePath.toString()),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Text(
                              pet.tutor.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                    ],
                  ),
                ),
              ],
            ),
            onPressed: onPressed ??
                () {
                  Get.toNamed(
                    PetScreen.routeName(pet.id),
                    arguments: {"timestamp": DateTime.now().toIso8601String()},
                  );
                },
            onLongPress: onLongPress,
          ),
        ),
        if (controller != null)
          Obx(
            () {
              if (controller.editingMode?.value ?? false)
                return Positioned(
                  right: 12,
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          Get.dialog(
                            ConfirmDialog(onConfirmPressed: () => controller.deletePet(pet.id)),
                          );
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Colors.blue,
                        ),
                        onPressed: () {
                          controller.updatePet(pet);
                        },
                      ),
                    ],
                  ),
                );

              return Container();
            },
          ),
      ],
    );
  }

  _calculateAge({Pet pet}) {
    String ageString = "";

    DateTime currentDate = DateTime.now();
    int age = currentDate.year - pet.birthDate.year;
    int month1 = currentDate.month;
    int month2 = pet.birthDate.month;
    int day1 = currentDate.day;
    int day2 = pet.birthDate.day;

    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      if (day2 > day1) {
        age--;
      }
    }

    ageString = age.toString() + (age != 1 ? " anos" : " ano");

    return ageString;
  }
}
