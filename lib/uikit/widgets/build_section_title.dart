import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class BuildSectionTitle extends StatelessWidget {
  final String text;

  BuildSectionTitle(this.text);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 22),
        Text(
          text,
          style: TextStyle(
            fontSize: 19,
            color: PatakiColors.orange,
          ),
        ),
        SizedBox(height: 14),
      ],
    );
  }
}
