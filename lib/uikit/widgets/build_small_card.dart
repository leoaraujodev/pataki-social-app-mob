import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/media.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/static_media_player.dart';

class BuildSmallCard extends StatelessWidget {
  final String imagePath;
  final String title;
  final Function onPressed;
  final bool isIcon;
  final Media media;
  final bool resizeToSmallScreen;

  BuildSmallCard({
    this.imagePath,
    @required this.title,
    @required this.onPressed,
    this.isIcon = false,
    this.media,
    this.resizeToSmallScreen = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(22.0, 26.0, 22.0, 0.0),
      child: OutlineButton(
        child: Row(
          children: [
            if (isIcon) _buildIcon(),
            if (media != null && media.fileType == FileType.image) _buildImage(),
            if (media != null && media.fileType == FileType.video) _buildVideo(),
            Text(
              title,
              style: TextStyle(fontSize: resizeToSmallScreen ? 18.0 : 22.0),
            ),
          ],
        ),
        onPressed: onPressed,
      ),
    );
  }

  _buildIcon() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 18.0),
      child: Container(
        height: 32,
        width: 32,
        child: SvgPicture.asset(
          imagePath,
          fit: BoxFit.scaleDown,
          color: PatakiColors.orange,
        ),
      ),
    );
  }

  _buildImage() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10.0, 10.0, 10.0),
      height: resizeToSmallScreen ? 63 : 70,
      width: resizeToSmallScreen ? 63 : 70,
      decoration: BoxDecoration(
        color: PatakiColors.white,
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage(media.imagePath.toString()),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  _buildVideo() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
      height: resizeToSmallScreen ? 63 : 70,
      width: resizeToSmallScreen ? 63 : 70,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(1000),
        child: StaticMediaPlayer(videoPath: media.imagePath),
      ),
    );
  }
}
