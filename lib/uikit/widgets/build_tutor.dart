import 'package:flutter/material.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class BuildTutor extends StatelessWidget {
  final UserModel tutor;
  final int maxCharacters;
  final Function onPressed;

  BuildTutor({
    @required this.tutor,
    this.maxCharacters = 255,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: FlatButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(width: 10),
            Container(
              margin: EdgeInsets.all(10),
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: PatakiColors.white,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(tutor.imagePath.toString()),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _reducedToMaxCharacters(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    'Tutor',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF90AECC),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        onPressed: onPressed,
      ),
    );
  }

  _reducedToMaxCharacters() {
    if (tutor.name != null && tutor.name.length >= maxCharacters) {
      return tutor.name.substring(0, maxCharacters) + "...";
    } else {
      return tutor.name;
    }
  }
}
