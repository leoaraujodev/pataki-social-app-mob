import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:link_text/link_text.dart';
import 'package:pataki/core/interactor/message_date_time_formatter.dart';
import 'package:pataki/core/models/chat_message.dart';
import 'package:pataki/uikit/widgets/zoomable_image.dart';

class ChatMessageBubble extends StatelessWidget {
  final ChatMessage message;
  final bool isContactMessage;
  final void Function(ChatMessage) retryToPost;

  ChatMessageBubble({@required this.message, this.isContactMessage, this.retryToPost});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: message.status == ChatMessageStatus.error ? () => retryToPost(message) : null,
      child: Row(
        mainAxisAlignment: isContactMessage ? MainAxisAlignment.start : MainAxisAlignment.end,
        children: [
          Container(
            constraints: BoxConstraints(minWidth: 0, maxWidth: Get.width - 80),
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: isContactMessage
                  ? Colors.white
                  : message.status == ChatMessageStatus.error
                      ? Colors.red[50]
                      : Colors.orange[50],
              boxShadow: [BoxShadow(blurRadius: 1, color: Colors.grey[300])],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (message.image != null)
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.file(message.image),
                        message.status == ChatMessageStatus.error
                            ? RefreshProgressIndicator(value: 1.0)
                            : RefreshProgressIndicator(),
                      ],
                    ),
                  ),
                if (message.imagePath != null)
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: ZoomableImage(imagePath: message.imagePath),
                  ),
                if (message.sticker != null)
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.network(
                      message.sticker.path,
                      width: 72,
                      height: 72,
                    ),
                  ),
                if (message.message != null) Flexible(child: LinkText(text: message.message)),
                SizedBox(height: 5),
                RichText(
                  text: TextSpan(
                    children: [
                      if (!isContactMessage) TextSpan(text: "${message.status.toText()} "),
                      TextSpan(text: MessageDateTimeFormatter().execute(message.createdAt))
                    ],
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: isContactMessage
                          ? Colors.grey[350]
                          : message.status == ChatMessageStatus.error
                              ? Colors.deepOrange
                              : Colors.orange[300],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
