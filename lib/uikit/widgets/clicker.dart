import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';

class Clicker extends StatelessWidget {
  final controller;

  Clicker(this.controller);

  @override
  Widget build(Object context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
      child: Material(
        color: Colors.transparent,
        child: Ink(
          child: InkWell(
            enableFeedback: false,
            onTap: controller.playAudioFile,
            child: LargeTextButton(
              text: "CLICKER",
              backgroundColor: PatakiColors.orange,
              forceBackgroundColor: true,
              onPressed: null,
            ),
          ),
        ),
      ),
    );
  }
}
