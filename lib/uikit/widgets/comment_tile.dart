import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pataki/core/interactor/time_difference_formatter.dart';
import 'package:pataki/core/models/comment_model.dart';
import 'package:pataki/core/models/mention.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class CommentTile extends StatelessWidget {
  final CommentModel comment;
  final void Function() onLikePressed;
  final void Function() onCommentPressed;
  final void Function() onCommentLongPressed;

  CommentTile({
    @required this.comment,
    @required this.onLikePressed,
    this.onCommentPressed,
    this.onCommentLongPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onCommentPressed,
      onLongPress: onCommentLongPressed,
      child: Container(
        padding: EdgeInsets.only(
          left: 20,
          right: 10,
          bottom: 10,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 15, right: 10),
              height: 34,
              width: 34,
              decoration: BoxDecoration(
                color: PatakiColors.white,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(comment.user.imagePath),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                              text: comment.user.name,
                              style: TextStyle(fontWeight: FontWeight.w700),
                            ),
                            TextSpan(text: " "),
                            ...Mention.listTextSpanWithRoutes(comment.comment),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          TimeDifferenceFormatter().execute(comment.createdAt),
                          style: TextStyle(color: Color(0xFF90AECC)),
                        ),
                        Text(
                          comment.totLikes.toString() +
                              (comment.totLikes == 1 ? " Curtida" : " Curtidas"),
                          style: TextStyle(fontWeight: FontWeight.w700, color: Color(0xFF90AECC)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              height: 34,
              width: 34,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(17),
                ),
                padding: EdgeInsets.zero,
                child: SvgPicture.asset(
                  'images/like_icon.svg',
                  width: 14,
                  color: comment.liked ? Colors.red : null,
                ),
                onPressed: onLikePressed,
              ),
            )
          ],
        ),
      ),
    );
  }
}
