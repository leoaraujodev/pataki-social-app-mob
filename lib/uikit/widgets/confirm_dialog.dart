import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmDialog extends StatefulWidget {
  final Future<void> Function() onConfirmPressed;
  final String action;

  ConfirmDialog({
    @required this.onConfirmPressed,
    this.action = "remover",
  });

  @override
  _ConfirmDialogState createState() => _ConfirmDialogState();
}

class _ConfirmDialogState extends State<ConfirmDialog> {
  bool confirming = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Confirmação",
                style: TextStyle(fontSize: 18),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Deseja realmente ${widget.action}?",
              ),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: () => Get.back(),
                  child: Text("Não"),
                ),
                SizedBox(width: 10),
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  onPressed: confirming
                      ? () {}
                      : () async {
                          setState(() => confirming = true);
                          await widget.onConfirmPressed();
                          setState(() => confirming = false);
                        },
                  child: confirming
                      ? Container(
                          height: 20,
                          width: 20,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                            valueColor: AlwaysStoppedAnimation(Colors.black),
                          ),
                        )
                      : Text("Sim"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
