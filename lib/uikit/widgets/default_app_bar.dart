import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class DefaultAppBar extends AppBar {
  DefaultAppBar({
    @required String title,
    bool centerTitle = false,
    bool iconOrange = false,
    List<Widget> actions,
  }) : super(
          title: Text(
            title,
            style: TextStyle(
              color: Color(0xFF3F414E),
              fontWeight: FontWeight.normal,
              fontSize: 18,
            ),
          ),
          centerTitle: centerTitle,
          brightness: Brightness.light,
          iconTheme: iconOrange
              ? IconThemeData(color: PatakiColors.orange)
              : IconThemeData(color: Color(0xFF3F414E)),
          backgroundColor: Colors.white,
          elevation: 0.0,
          actions: actions,
        );
}
