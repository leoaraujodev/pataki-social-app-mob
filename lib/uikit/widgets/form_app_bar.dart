import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FormAppBar extends StatelessWidget {
  static const double height = 134;

  final String title;

  FormAppBar({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 0,
            top: 0,
            child: _buildBackButton(context),
          ),
          Positioned(
            bottom: 10,
            child: _buildTitle(),
          ),
          Positioned(
            right: 0,
            top: 0,
            child: _buildAppBarImage(),
          ),
        ],
      ),
    );
  }

  Widget _buildBackButton(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(top: 20, left: 20),
        height: 55,
        width: 55,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Color(0xFFC7D9EB)),
        ),
        child: IconButton(
          splashRadius: 27.5,
          icon: Icon(
            Icons.arrow_back,
            color: Color(0xFF023188),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Text(
      title,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 28,
        color: Color(0xFF023188),
      ),
    );
  }

  Widget _buildAppBarImage() {
    return SvgPicture.asset(
      'images/login_appbar.svg',
      height: 134,
      fit: BoxFit.cover,
    );
  }
}
