import 'package:flutter/material.dart';

class FormDropdownField extends StatefulWidget {
  final UniqueKey key;
  final items;
  final String hintText;
  final FormFieldValidator validator;
  final Widget checkMark;
  final AutovalidateMode autovalidateMode;
  final FocusNode focusNode;
  final void Function(dynamic) onChanged;
  final Widget icon;
  final String disabledHint;
  final dynamic value;

  FormDropdownField({
    this.key,
    @required this.items,
    this.hintText = "",
    this.validator,
    this.checkMark,
    this.autovalidateMode,
    this.focusNode,
    this.onChanged,
    this.icon,
    this.disabledHint = "",
    this.value,
  });

  @override
  _FormDropdownFieldState createState() => _FormDropdownFieldState();
}

class _FormDropdownFieldState extends State<FormDropdownField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, top: 0),
      child: Stack(
        children: [
          Container(height: 75),
          DropdownButtonFormField(
            key: widget.key,
            items: widget.items,
            value: widget.value,
            isExpanded: true,
            disabledHint: Text(
              widget.disabledHint,
              style: TextStyle(
                color: Color(0xFFD3DCE6),
                fontWeight: FontWeight.w300,
                fontSize: 16,
              ),
            ),
            autovalidateMode: widget.autovalidateMode,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(12, 18, 12, 17),
              filled: true,
              fillColor: Colors.white,
              hintText: (widget.hintText),
              hintStyle: TextStyle(
                color: Color(0xFF66839F),
                fontWeight: FontWeight.w300,
                fontSize: 16,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFFC7D9EB)),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              errorStyle: TextStyle(height: 0.5),
            ),
            focusNode: widget.focusNode,
            icon: widget.icon,
            validator: widget.validator,
            onChanged: widget.onChanged,
          ),
        ],
      ),
    );
  }
}
