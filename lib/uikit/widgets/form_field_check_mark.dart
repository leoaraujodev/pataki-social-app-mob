import 'package:flutter/material.dart';

class FormFieldCheckMark extends StatelessWidget {
  final bool isValid;

  FormFieldCheckMark({@required this.isValid});

  @override
  Widget build(BuildContext context) {
    return isValid
        ? Icon(
            Icons.check,
            color: Colors.green,
          )
        : Icon(
            Icons.close,
            color: Colors.red,
          );
  }
}
