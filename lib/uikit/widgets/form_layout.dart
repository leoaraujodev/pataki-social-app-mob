import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pataki/uikit/widgets/form_app_bar.dart';

class FormLayout extends StatelessWidget {
  final String title;
  final List<Widget> children;
  final bottomNavigationBar;

  FormLayout({
    @required this.title,
    @required this.children,
    this.bottomNavigationBar,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LayoutBuilder(
        builder: (context, constraints) => Stack(
          alignment: Alignment.topRight,
          children: [
            SvgPicture.asset(
              'images/login_background.svg',
              width: constraints.maxWidth,
            ),
            Column(
              children: [
                FormAppBar(title: title),
                Container(
                  height: constraints.maxHeight - FormAppBar.height,
                  child: ListView(
                    primary: false,
                    shrinkWrap: true,
                    children: children,
                    padding: EdgeInsets.zero,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: bottomNavigationBar,
    );
  }
}
