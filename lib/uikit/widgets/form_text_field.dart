import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class FormTextField extends StatefulWidget {
  final TextEditingController controller;
  final Widget checkMark;
  final String hintText;
  final bool canObscure;
  final AutovalidateMode autovalidateMode;
  final FocusNode focusNode;
  final String Function(String) validator;
  final void Function(String) onFieldSubmitted;
  final void Function() onTap;
  final TextInputType keyboardType;
  final bool showCursor;
  final bool readOnly;
  final TextInputAction textInputAction;
  final void Function(String) onChanged;
  final double innerTextSize;
  final FontWeight innerTextWeight;
  final bool noEnabledBorder;
  final bool noBorder;
  final bool noInnerPadding;
  final bool noOutsidePadding;

  FormTextField({
    @required this.controller,
    this.hintText = "",
    this.checkMark,
    this.canObscure = false,
    this.validator,
    this.autovalidateMode,
    this.onFieldSubmitted,
    this.onTap,
    this.focusNode,
    this.keyboardType,
    this.showCursor,
    this.readOnly = false,
    this.textInputAction,
    this.onChanged,
    this.innerTextSize,
    this.innerTextWeight = FontWeight.w300,
    this.noEnabledBorder = false,
    this.noBorder = false,
    this.noInnerPadding = false,
    this.noOutsidePadding = false,
  });

  @override
  _FormTextFieldState createState() => _FormTextFieldState();
}

class _FormTextFieldState extends State<FormTextField> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.noOutsidePadding
          ? EdgeInsets.only(left: 0, right: 0, top: 0)
          : EdgeInsets.only(left: 10, right: 10, top: 0),
      child: Stack(
        children: [
          Container(height: 75),
          TextFormField(
            autovalidateMode: widget.autovalidateMode,
            controller: widget.controller,
            obscureText: widget.canObscure && obscureText,
            style: TextStyle(
              fontSize: widget.innerTextSize,
            ),
            decoration: InputDecoration(
              suffixIcon: _buildSuffixIcon(),
              filled: true,
              fillColor: Colors.white,
              hintText: (widget.hintText),
              hintStyle: TextStyle(
                color: Color(0xFF66839F),
                fontWeight: widget.innerTextWeight,
                fontSize: widget.innerTextSize,
              ),
              enabledBorder: widget.noEnabledBorder
                  ? UnderlineInputBorder(
                      borderSide: BorderSide(color: PatakiColors.orange),
                    )
                  : OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFC7D9EB)),
                    ),
              border: widget.noBorder
                  ? UnderlineInputBorder(
                      borderSide: BorderSide(color: PatakiColors.orange),
                    )
                  : OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
              errorStyle: TextStyle(height: 0.5),
              contentPadding:
                  widget.noInnerPadding ? EdgeInsets.symmetric(vertical: 14, horizontal: 0) : null,
            ),
            onFieldSubmitted: widget.onFieldSubmitted,
            onTap: widget.onTap,
            focusNode: widget.focusNode,
            validator: widget.validator,
            keyboardType: widget.keyboardType,
            showCursor: widget.showCursor,
            readOnly: widget.readOnly,
            textInputAction: widget.textInputAction,
            onChanged: widget.onChanged,
          ),
        ],
      ),
    );
  }

  Widget _buildSuffixIcon() {
    if (!widget.canObscure) return widget.checkMark;

    Widget visibilityButton = GestureDetector(
      child: Icon(
        obscureText ? Icons.visibility : Icons.visibility_off,
      ),
      onTap: () => setState(() => obscureText = !obscureText),
    );

    if (widget.checkMark == null) {
      return visibilityButton;
    } else {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          visibilityButton,
          Padding(
            padding: EdgeInsets.only(left: 8, right: 12),
            child: widget.checkMark,
          ),
        ],
      );
    }
  }
}
