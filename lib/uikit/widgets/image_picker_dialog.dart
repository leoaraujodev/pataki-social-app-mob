import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ImagePickerDialog extends StatelessWidget {
  final void Function() onCameraPressed;
  final void Function() onGalleryPressed;

  ImagePickerDialog({
    @required this.onCameraPressed,
    @required this.onGalleryPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Seleção da Imagem",
                style: TextStyle(fontSize: 18),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
              onPressed: () {
                onCameraPressed();
                Get.back();
              },
              child: Text("Câmera"),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
              onPressed: () {
                onGalleryPressed();
                Get.back();
              },
              child: Text("Galeria"),
            ),
          ],
        ),
      ),
    );
  }
}
