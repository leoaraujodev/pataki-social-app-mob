import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class LargeTextButton extends StatelessWidget {
  final String text;
  final void Function() onPressed;
  final bool loading;
  final double progress;
  final Color backgroundColor;
  final Color foregroundColor;
  final bool forceBackgroundColor;

  LargeTextButton({
    @required this.text,
    @required this.onPressed,
    this.loading = false,
    this.progress,
    this.backgroundColor = PatakiColors.orange,
    this.forceBackgroundColor = false,
    this.foregroundColor = const Color(0xFFF6F1FB),
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(StadiumBorder()),
        minimumSize: MaterialStateProperty.all(Size.fromHeight(63)),
        backgroundColor: MaterialStateProperty.resolveWith(_getButtonColor),
        textStyle: MaterialStateProperty.all(
          TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
        foregroundColor: MaterialStateProperty.all(foregroundColor),
      ),
      onPressed: loading ? () {} : onPressed,
      child: loading
          ? CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(foregroundColor),
              value: progress,
            )
          : Text(text),
    );
  }

  Color _getButtonColor(Set<MaterialState> states) {
    if (forceBackgroundColor) {
      return backgroundColor;
    }
    return states.contains(MaterialState.disabled) ? Colors.grey[400] : backgroundColor;
  }
}
