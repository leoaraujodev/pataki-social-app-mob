import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class MediaPlayer extends StatefulWidget {
  final String videoPath;
  MediaPlayer({@required this.videoPath});

  @override
  _MediaPlayerState createState() => _MediaPlayerState();
}

class _MediaPlayerState extends State<MediaPlayer> {
  bool showPlayerIconButton = true;
  bool hasChangedPlayerStatus = false;
  VideoPlayerController controller;
  bool isReady = false;

  @override
  initState() {
    controller = VideoPlayerController.network(widget.videoPath);
    controller.setLooping(true);
    controller.initialize().then((_) => setState(() => isReady = true));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isReady
        ? AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: Stack(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      hasChangedPlayerStatus = false;
                      showPlayerIconButton = true;
                    });
                    _hideButtonAfterPeriod();
                  },
                  child: VideoPlayer(controller),
                ),
                if (showPlayerIconButton)
                  Container(
                    color: Colors.black.withOpacity(0.4),
                    child: Center(
                      child: _playerIconButton(),
                    ),
                  )
              ],
            ),
          )
        : Container(
            height: 100,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }

  _hideButtonAfterPeriod() => Future.delayed(Duration(seconds: 2), () {
        if (!hasChangedPlayerStatus) {
          setState(() => showPlayerIconButton = false);
        }
      });

  Widget _playerIconButton() {
    return InkWell(
      borderRadius: BorderRadius.circular(1000.0),
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.transparent,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
              blurRadius: 7,
              spreadRadius: 4,
            ),
          ],
          border: Border.all(
            color: Colors.white,
            width: 3,
          ),
        ),
        child: Icon(
          controller.value.isPlaying ? Icons.pause : Icons.play_arrow_rounded,
          size: 50,
          color: Colors.white,
        ),
      ),
      onTap: () {
        setState(() => hasChangedPlayerStatus = true);
        controller.value.isPlaying ? controller.pause() : _play();
      },
    );
  }

  _play() {
    controller.play();
    setState(() => showPlayerIconButton = false);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
