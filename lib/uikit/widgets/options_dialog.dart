import 'package:flutter/material.dart';

class OptionsDialog extends StatelessWidget {
  final String title;
  final void Function() onEditPressed;
  final void Function() onDeletePressed;
  final void Function() onReportPressed;
  final void Function() onBlockPressed;
  final void Function() onEnterPressed;
  final void Function() onLeavePressed;
  final void Function() onSharePressed;
  final String shareType;

  OptionsDialog({
    @required this.title,
    this.onEditPressed,
    this.onDeletePressed,
    this.onReportPressed,
    this.onBlockPressed,
    this.onEnterPressed,
    this.onLeavePressed,
    this.onSharePressed,
    this.shareType,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                title,
                style: TextStyle(fontSize: 18),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            if (onEditPressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onEditPressed,
                child: Text("Editar"),
              ),
            if (onSharePressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onSharePressed,
                child: Text("Copiar link para $shareType"),
              ),
            if (onDeletePressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onDeletePressed,
                child: Text("Deletar"),
              ),
            if (onReportPressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onReportPressed,
                child: Text("Denunciar"),
              ),
            if (onBlockPressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onBlockPressed,
                child: Text("Bloquear"),
              ),
            if (onEnterPressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onEnterPressed,
                child: Text("Entrar"),
              ),
            if (onLeavePressed != null)
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                onPressed: onLeavePressed,
                child: Text("Sair"),
              ),
          ],
        ),
      ),
    );
  }
}
