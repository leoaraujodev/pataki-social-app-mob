import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/report_reasons.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/comment_text_field.dart';
import 'package:pataki/uikit/widgets/large_text_button.dart';
import 'package:pataki/uikit/widgets/timeline_report_post_card.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class Report extends StatelessWidget {
  final controller;
  final String typeOfReport;
  final UserModel user;

  Report({
    @required this.controller,
    @required this.typeOfReport,
    @required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => CustomScrollView(
        slivers: [
          SliverPadding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Text("Denunciar $typeOfReport:"),
                  ),
                  if (typeOfReport == "post")
                    TimelineReportPostCard(
                      post: controller.reportingTimelinePost,
                      user: user,
                    ),
                  if (typeOfReport == "user")
                    TimelineReportPostCard(
                      user: user,
                    ),
                  Divider(),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text("Selecione um motivo:"),
                  ),
                  Wrap(
                    children: ReportReasons.values
                        .map(
                          (e) => ChoiceChip(
                            label: Text(e.toDisplayString()),
                            selected: controller.selectedReason.value == e,
                            onSelected: (_) {
                              controller.selectedReason.value = e;
                            },
                            backgroundColor: Colors.grey[400],
                            selectedColor: PatakiColors.orange,
                            labelStyle: TextStyle(color: Colors.white),
                          ),
                        )
                        .toList(),
                    spacing: 15,
                  ),
                  if (controller.selectedReason.value != null) ...[
                    Divider(),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: CommentTextField(
                        controller: controller.textController,
                        hintText: "Descreva o motivo (Opcional)",
                        minLines: 4,
                      ),
                    ),
                  ],
                ],
              ),
            ),
          ),
          SliverFillRemainingBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
              alignment: Alignment.bottomCenter,
              child: LargeTextButton(
                loading: controller.reporting.value,
                text: "DENUNCIAR",
                onPressed: controller.selectedReason.value != null ? controller.report : null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
