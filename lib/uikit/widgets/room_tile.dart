import 'package:flutter/material.dart';
import 'package:pataki/core/interactor/date_time_formatter.dart';
import 'package:pataki/core/models/chat_room.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class RoomTile extends StatelessWidget {
  final ChatRoom room;
  final void Function() onPressed;

  RoomTile({
    @required this.room,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  height: 34,
                  width: 34,
                  decoration: BoxDecoration(
                    color: PatakiColors.white,
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(room.contact.imagePath),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          room.contact.name,
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                      ),
                      if (room.lastMessage != null)
                        Flexible(
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(color: Colors.black),
                              children: [
                                if (room.contact.id != room.lastMessage.senderId)
                                  TextSpan(text: "Você: "),
                                TextSpan(
                                  text: room.lastMessage.message != null
                                      ? room.lastMessage.message.replaceAll('\n', ' ')
                                      : room.lastMessage.imagePath != null
                                          ? "Imagem"
                                          : "Figurinha",
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
            if (room.lastMessage != null)
              Text(
                DateTimeFormatter().execute(room.lastMessage.createdAt),
                style: TextStyle(color: Color(0xFF90AECC)),
              ),
          ],
        ),
      ),
    );
  }
}
