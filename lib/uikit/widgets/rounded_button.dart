import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class RoundedButton extends StatelessWidget {
  final Icon icon;
  final Function onTap;
  final Color backgroundColor;
  final double padding;
  final double innerPadding;

  RoundedButton({
    @required this.icon,
    @required this.onTap,
    this.backgroundColor = PatakiColors.orange,
    this.padding = 22,
    this.innerPadding = 6,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: padding),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Material(
            type: MaterialType.transparency,
            child: Ink(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: backgroundColor,
              ),
              child: InkWell(
                borderRadius: BorderRadius.circular(1000.0),
                child: Padding(
                  padding: EdgeInsets.all(innerPadding),
                  child: icon,
                ),
                onTap: onTap,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
