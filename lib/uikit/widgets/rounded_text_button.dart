import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class RoundedTextButton extends StatelessWidget {
  final String text;
  final bool actived;
  final Function onPressed;
  final double screenWidth;

  RoundedTextButton({
    @required this.text,
    @required this.actived,
    @required this.onPressed,
    @required this.screenWidth,
  });

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
        width: screenWidth >= 375 ? 40 : 30,
        height: screenWidth >= 375 ? 40 : 30,
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              text,
              style: TextStyle(
                fontSize: screenWidth >= 375 ? 20 : 16,
                color: actived ? PatakiColors.orange : Colors.black,
              ),
            ),
          ),
          ElevatedButton(
            child: Text(
              "",
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.transparent,
              shadowColor: Colors.transparent,
              shape: CircleBorder(),
              side: actived
                  ? BorderSide(
                      color: PatakiColors.orange,
                      width: 1,
                    )
                  : null,
            ),
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }
}
