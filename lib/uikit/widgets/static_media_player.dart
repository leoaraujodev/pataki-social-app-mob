import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class StaticMediaPlayer extends StatefulWidget {
  final String videoPath;
  StaticMediaPlayer({@required this.videoPath});

  @override
  _StaticMediaPlayerState createState() => _StaticMediaPlayerState();
}

class _StaticMediaPlayerState extends State<StaticMediaPlayer> {
  VideoPlayerController controller;
  bool isReady = false;

  @override
  initState() {
    controller = VideoPlayerController.network(widget.videoPath);
    controller.initialize().then((_) => setState(() => isReady = true));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isReady ? VideoPlayer(controller) : CircularProgressIndicator();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
