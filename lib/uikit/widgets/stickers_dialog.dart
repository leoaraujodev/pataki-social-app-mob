import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/models/sticker.dart';

class StickersDialog extends StatelessWidget {
  final List<Sticker> stickers;
  final void Function(Sticker) onStickerPressed;

  StickersDialog({
    @required this.stickers,
    @required this.onStickerPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Selecione o emoji",
                style: TextStyle(fontSize: 18),
              ),
            ),
            SizedBox(
              height: 230,
              child: ListView(
                shrinkWrap: true,
                children: [
                  Wrap(
                    spacing: 20,
                    runSpacing: 20,
                    alignment: WrapAlignment.spaceEvenly,
                    children: stickers
                        .map(
                          (model) => InkWell(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            child: Image.network(
                              model.path,
                              width: 72,
                              height: 72,
                            ),
                            onTap: () {
                              onStickerPressed(model);
                              Get.back();
                            },
                          ),
                        )
                        .toList(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
