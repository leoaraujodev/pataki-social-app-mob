import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/screens/policy/policy_screen.dart';
import 'package:pataki/screens/terms/terms_screen.dart';

class TermsBottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            child: Text(
              "Termos de uso",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Color(0xFF66839F),
              ),
            ),
            onPressed: () => Get.toNamed(TermsScreen.routeName),
          ),
          TextButton(
            child: Text(
              "Política de Privacidade",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Color(0xFF66839F),
              ),
            ),
            onPressed: () => Get.toNamed(PolicyScreen.routeName),
          ),
        ],
      ),
    );
  }
}
