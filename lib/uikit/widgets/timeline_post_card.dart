import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pataki/core/interactor/date_time_formatter.dart';
import 'package:pataki/core/models/file_type.dart';
import 'package:pataki/core/models/mention.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/uikit/pataki_colors.dart';
import 'package:pataki/uikit/widgets/media_player.dart';
import 'package:pataki/uikit/widgets/zoomable_image.dart';

class TimelinePostCard extends StatelessWidget {
  final TimelinePost post;
  final void Function() onLikePressed;
  final void Function() onLikesPressed;
  final void Function() onCommentPressed;
  final void Function() onSendPressed;
  final void Function() onMorePressed;
  final void Function() onUserPressed;

  TimelinePostCard({
    @required this.post,
    @required this.onLikePressed,
    @required this.onLikesPressed,
    @required this.onCommentPressed,
    @required this.onSendPressed,
    @required this.onMorePressed,
    @required this.onUserPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      width: Get.width,
      color: Colors.white,
      child: Column(
        children: [
          _buildHeader(),
          _buildMedia(context),
          _buildCounts(),
          if (post.desc != null && post.desc != "") _buildDescription(),
          Divider(color: Color(0xFF90AECC), height: 0, thickness: 0.5),
          _buildButtons(),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      height: 56,
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 60,
              child: FlatButton(
                padding: EdgeInsets.zero,
                shape: RoundedRectangleBorder(),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 10, top: 11, bottom: 10),
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        color: PatakiColors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: NetworkImage(post.pet.imagePath.toString()),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            post.pet.name,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF2B2B2B),
                            ),
                          ),
                          Text(
                            DateTimeFormatter().execute(post.createdAt),
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 11,
                              color: Color(0xFF66839F),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                onPressed: onUserPressed,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(4),
            height: 48,
            width: 48,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
              padding: EdgeInsets.zero,
              child: Icon(
                Icons.more_vert,
                color: Color(0xFF90AECC),
              ),
              onPressed: onMorePressed,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMedia(context) {
    if (post.imagePath == null) return Container();

    return ConstrainedBox(
      constraints: new BoxConstraints(
        maxHeight: 0.65 * MediaQuery.of(context).size.height,
      ),
      child: _media(),
    );
  }

  Widget _media() {
    FileType fileType = getFileType(post.imagePath);

    if (fileType == FileType.image) {
      return ZoomableImage(imagePath: post.imagePath);
    } else if (fileType == FileType.video) {
      return ConstrainedBox(
        constraints: new BoxConstraints.expand(),
        child: MediaPlayer(videoPath: post.imagePath),
      );
    }

    return Container();
  }

  Widget _buildCounts() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: onLikesPressed,
            child: RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xFF3F414E),
                ),
                children: [
                  TextSpan(
                    text: "Curtido por ",
                    style: TextStyle(fontWeight: FontWeight.w700),
                  ),
                  TextSpan(text: post.totLikes.toString()),
                  post.totLikes == 1 ? TextSpan(text: " tutor") : TextSpan(text: " tutores")
                ],
              ),
            ),
          ),
          TextButton(
            onPressed: onCommentPressed,
            child: Text(
              post.totComment.toString() + (post.totComment == 1 ? " Comentário" : " Comentários"),
              style: TextStyle(
                fontSize: 12,
                color: PatakiColors.orange,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDescription() {
    return Padding(
      padding: EdgeInsets.only(bottom: 15, left: 20, right: 20),
      child: Row(
        children: [
          Expanded(
            child: RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xFF3F414E),
                ),
                children: Mention.listTextSpanWithRoutes(post.desc),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildButtons() {
    return Container(
      height: 60,
      child: Row(
        children: [
          _TimelineCardBottomButton(
            imagePath: 'images/like_icon.svg',
            label: post.liked ? "Descurtir" : "Curtir",
            color: post.liked ? Colors.red : null,
            onPressed: onLikePressed,
          ),
          _TimelineCardBottomButton(
            imagePath: 'images/chat_icon.svg',
            label: "Comentar",
            onPressed: onCommentPressed,
          ),
          // _TimelineCardBottomButton(
          //   imagePath: 'images/send_icon.svg',
          //   label: "Enviar",
          //   onPressed: onSendPressed,
          // ),
        ],
      ),
    );
  }
}

class _TimelineCardBottomButton extends StatelessWidget {
  final String imagePath;
  final String label;
  final Color color;
  final void Function() onPressed;

  _TimelineCardBottomButton({
    @required this.imagePath,
    @required this.label,
    this.color,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 60,
        child: FlatButton(
          shape: RoundedRectangleBorder(),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 24,
                width: 24,
                child: SvgPicture.asset(
                  imagePath,
                  fit: BoxFit.scaleDown,
                  color: color,
                ),
              ),
              SizedBox(width: 5),
              Text(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: Color(0xFF90AECC),
                ),
              ),
            ],
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
