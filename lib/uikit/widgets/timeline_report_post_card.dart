import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pataki/core/interactor/date_time_formatter.dart';
import 'package:pataki/core/models/timeline_post.dart';
import 'package:pataki/core/models/user_model.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class TimelineReportPostCard extends StatelessWidget {
  final TimelinePost post;
  final UserModel user;

  TimelineReportPostCard({
    this.post,
    @required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildHeader(),
          if (!(post == null))
            Row(
              children: [
                _buildImage(),
                if (post.desc != null && post.desc != "") _buildDescription(),
              ],
            )
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      height: 60,
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.only(left: 20, right: 10, top: 11, bottom: 10),
            height: 35,
            width: 35,
            decoration: BoxDecoration(
              color: PatakiColors.white,
              shape: BoxShape.circle,
              image: DecorationImage(
                image: NetworkImage(user.imagePath.toString()),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  user.name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF2B2B2B),
                  ),
                ),
                if (!(post == null))
                  Text(
                    DateTimeFormatter().execute(post.createdAt),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 11,
                      color: Color(0xFF66839F),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    if (post.imagePath == null) return Container();

    return Container(
      constraints: BoxConstraints.loose(Size(100, 100)),
      margin: EdgeInsets.only(left: 20),
      child: Image.network(
        post.imagePath,
        fit: BoxFit.contain,
      ),
    );
  }

  Widget _buildDescription() {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Text(
          post.desc,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xFF3F414E),
          ),
        ),
      ),
    );
  }
}
