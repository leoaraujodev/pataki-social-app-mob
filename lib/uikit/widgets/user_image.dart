import 'package:flutter/material.dart';
import 'package:pataki/uikit/pataki_colors.dart';

class UserImage extends StatelessWidget {
  final String imagePath;

  UserImage({@required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return imagePath != null
        ? Container(
            margin: EdgeInsets.only(right: 10),
            height: 34,
            width: 34,
            decoration: BoxDecoration(
              color: PatakiColors.white,
              shape: BoxShape.circle,
              image: DecorationImage(
                image: NetworkImage(imagePath),
                fit: BoxFit.cover,
              ),
            ),
          )
        : Container(
            color: PatakiColors.white,
            margin: EdgeInsets.only(right: 10),
            height: 34,
            width: 34,
          );
  }
}
