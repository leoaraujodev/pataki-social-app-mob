import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ZoomableImage extends StatelessWidget {
  final String imagePath;

  ZoomableImage({@required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(
        Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            alignment: Alignment.topRight,
            children: [
              InteractiveViewer(
                child: Container(
                  height: Get.height,
                  width: Get.width,
                  alignment: Alignment.center,
                  child: Hero(
                    tag: imagePath.hashCode,
                    child: Image.network(
                      imagePath,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              SafeArea(
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                  onPressed: () => Get.back(),
                ),
              ),
            ],
          ),
        ),
      ),
      child: Container(
        width: Get.width,
        child: Hero(
          tag: imagePath.hashCode,
          child: Image.network(
            imagePath,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }
}
